package com.lakshya.lakshya.error;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class CustomErrorResponse {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp;
    private int statuscode;
    private String error;
    private Boolean status=false;

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
    
    public int getStatusCode() {
        return statuscode;
    }
    
    public Boolean getStatus() {
        return status;
    }
    
    public void setStatusCode(int statuscode) {
        this.statuscode = statuscode;
    }

    public String getError() {
    	return error;
    }
    
    public void setError(String error) {
        this.error = error;
    }
}