package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExamAssignStudent;

public interface OfflineExamAssignStudentRepository extends CrudRepository<OfflineExamAssignStudent, Long>{

}
