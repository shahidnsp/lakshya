package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExam;

public interface OfflineExamRepository extends CrudRepository<OfflineExam, Long>{

}
