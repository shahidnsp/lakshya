package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExamGrade;

public interface OfflineExamGradeRepository extends CrudRepository<OfflineExamGrade, Long> {

}
