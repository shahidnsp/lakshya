package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.ExamGroup;

public interface ExamGroupRepository extends CrudRepository<ExamGroup, Long>{

}
