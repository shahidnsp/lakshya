package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExamMainGroup;

public interface OfflineExamMainGroupRepository extends CrudRepository<OfflineExamMainGroup, Long>{

}
