package com.lakshya.lakshya.offlinerepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExamAssignBatch;

public interface OfflineExamAssignBatchRepository extends CrudRepository< OfflineExamAssignBatch, Long> {
 
}
