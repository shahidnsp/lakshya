package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineAnswerSheetOMR;

public interface OfflineAnswerSheetOMRRepository extends CrudRepository<OfflineAnswerSheetOMR, Long> {

}
