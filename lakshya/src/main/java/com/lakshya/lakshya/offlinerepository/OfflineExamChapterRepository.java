package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExamChapter;

public interface OfflineExamChapterRepository extends CrudRepository<OfflineExamChapter, Long>{ 

}
