package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExamMark;

public interface OfflineExamMarkRepository extends CrudRepository<OfflineExamMark, Long>{

}
