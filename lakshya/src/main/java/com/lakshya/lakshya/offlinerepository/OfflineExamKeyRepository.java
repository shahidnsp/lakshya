package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExamKey;

public interface OfflineExamKeyRepository extends CrudRepository<OfflineExamKey, Long> {

}
