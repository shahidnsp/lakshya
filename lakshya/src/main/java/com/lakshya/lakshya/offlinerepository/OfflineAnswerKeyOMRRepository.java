package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineAnswerKeyOMR;

public interface OfflineAnswerKeyOMRRepository extends CrudRepository<OfflineAnswerKeyOMR, Long> {

}
