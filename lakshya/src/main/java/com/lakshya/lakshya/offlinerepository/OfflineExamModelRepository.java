package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExamModel;

public interface OfflineExamModelRepository extends CrudRepository<OfflineExamModel, Long> {

}
