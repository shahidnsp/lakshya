package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineAnswerSheetOMRList;

public interface OfflineAnswerSheetOMRListRepository extends CrudRepository< OfflineAnswerSheetOMRList, Long>{

}
