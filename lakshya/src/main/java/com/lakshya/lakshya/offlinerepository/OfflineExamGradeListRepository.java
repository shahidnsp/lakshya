package com.lakshya.lakshya.offlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.offlineexam.OfflineExamGradeList;

public interface OfflineExamGradeListRepository extends CrudRepository<OfflineExamGradeList, Long> {

}
