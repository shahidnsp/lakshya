package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.StudentGuardian;
import com.lakshya.lakshya.repository.StudentGuardianRepository;
import com.lakshya.lakshya.repository.StudentRepository;

@RestController
@RequestMapping("/api/studentguardian")
public class StudentGuardianController {
	@Autowired
	StudentRepository studentRepository;

	@Autowired
	StudentGuardianRepository studentGuardianRepository;
	
	/*
	 * GET Method for listing Enquiry Task
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllGuardianByStudentId(@RequestParam Long studentId) {
		List<StudentGuardian> studentGuardian = (List<StudentGuardian>) studentGuardianRepository.findAllByStudentId(studentId);
		return ResponseFormat.createFormat(studentGuardian, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Enquiry Task Records By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getGuardianById(@PathVariable(value = "id") Long studentGuardianId) {
		StudentGuardian studentGuardian = studentGuardianRepository.findById(studentGuardianId)
				.orElseThrow(() -> new ItemNotFoundException("StudentGuardian", "id", studentGuardianId));
		return ResponseFormat.createFormat(studentGuardian, "Listed Successfully");
	}
	
	/*
	 * Method for Creating Student Guardian
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) StudentGuardian studentGuardian) {
		
		Student newStudent = studentRepository.findById(studentGuardian.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", studentGuardian.getStudentId()));
		studentGuardian.setStudent(newStudent);
		

		StudentGuardian newStudentGuardian = (StudentGuardian) studentGuardianRepository.save(studentGuardian);
		return ResponseFormat.createFormat(newStudentGuardian, "Created Succefully.");
	}
	
	/*
	 * Method for Updating Student Guardian
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long studentGuardianId,
			@Valid @RequestBody(required = false) StudentGuardian studentGuardian) {
		
		StudentGuardian newStudentGuardian = studentGuardianRepository.findById(studentGuardianId)
				.orElseThrow(() -> new ItemNotFoundException("StudentGuardian", "id", studentGuardianId));
		
		Student newStudent = studentRepository.findById(studentGuardian.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", studentGuardian.getStudentId()));
		studentGuardian.setStudent(newStudent);
		
		studentGuardian.setId(studentGuardianId);
		studentGuardian.setCreatedAt(newStudentGuardian.getCreatedAt());

		StudentGuardian updatedStudentGuardian = studentGuardianRepository.save(studentGuardian);
		return ResponseFormat.createFormat(updatedStudentGuardian, "Updated Succefully");
	}
	
	/* DELETE Method for Delete Student Guardian */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long studentGuardianId) {
		StudentGuardian studentGuardian = studentGuardianRepository.findById(studentGuardianId)
				.orElseThrow(() -> new ItemNotFoundException("StudentGuardian", "id", studentGuardianId));

		studentGuardianRepository.delete(studentGuardian);
		return ResponseFormat.createFormat(studentGuardian, "Deleted Successfully");
	}

}
