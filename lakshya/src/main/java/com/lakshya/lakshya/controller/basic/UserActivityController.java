package com.lakshya.lakshya.controller.basic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.EnquiryStage;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.StudentDocument;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.UserActivity;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.EnquiryStageRepository;
import com.lakshya.lakshya.repository.StudentDocumentRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserActivityRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/useractivity")
public class UserActivityController {
	@Autowired
	UserActivityRepository userActivityRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	EnquiryStageRepository enquiryStageRepository;
	
	@Autowired
	CourseRepository courseRepository;
	/*
	 * GET Method for listing User Activity
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllStudentDocuments() {
		List<UserActivity> studentDocument = (List<UserActivity>) userActivityRepository.findAll();
		return ResponseFormat.createFormat(studentDocument, "Listed Successfully");
	}

	/*
	 * GET Method for listing User Activity Records By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getStudentDocumentById(@PathVariable(value = "id") Long userActivityId) {
		UserActivity userActivity = userActivityRepository.findById(userActivityId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Note", "id", userActivityId));
		return ResponseFormat.createFormat(userActivity, "Listed Successfully");
	}

	/*
	 * Method for Creating User Activity
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) UserActivity userActivity) {

		Student newStudent = studentRepository.findById(userActivity.getStudent_Id())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", userActivity.getStudent_Id()));
		userActivity.setStudent(newStudent);

		User newUser = userRepository.findById(userActivity.getUser_Id())
				.orElseThrow(() -> new ItemNotFoundException("User with Id", "id", userActivity.getUser_Id()));
		userActivity.setUser(newUser);
		
		EnquiryStage newEnquiryStage = enquiryStageRepository.findById(userActivity.getEnquiry_Stage_Id())
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Stage with Id", "id", userActivity.getUser_Id()));
		userActivity.setEnquiryStage(newEnquiryStage);

		Course newCourse = courseRepository.findById(userActivity.getCourse_Id())
				.orElseThrow(() -> new ItemNotFoundException("Course with Id", "id", userActivity.getUser_Id()));
		userActivity.setCourse(newCourse);
		
		UserActivity newUserActivity = (UserActivity) userActivityRepository.save(userActivity);
		return ResponseFormat.createFormat(newUserActivity, "Created Successfully.");
	}

	/*
	 * Method for Updating User Activity
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long userActivityId,
			@Valid @RequestBody(required = false) UserActivity userActivity) {
		UserActivity newUserActivity = userActivityRepository.findById(userActivityId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", userActivityId));

		Student newStudent = studentRepository.findById(userActivity.getStudent_Id())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", userActivity.getStudent_Id()));
		userActivity.setStudent(newStudent);

		User newUser = userRepository.findById(userActivity.getUser_Id())
				.orElseThrow(() -> new ItemNotFoundException("User with Id", "id", userActivity.getUser_Id()));
		userActivity.setUser(newUser);
		
		EnquiryStage newEnquiryStage = enquiryStageRepository.findById(userActivity.getEnquiry_Stage_Id())
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Stage with Id", "id", userActivity.getUser_Id()));
		userActivity.setEnquiryStage(newEnquiryStage);

		Course newCourse = courseRepository.findById(userActivity.getCourse_Id())
				.orElseThrow(() -> new ItemNotFoundException("Course with Id", "id", userActivity.getUser_Id()));
		userActivity.setCourse(newCourse);

		userActivity.setId(userActivityId);
		userActivity.setCreatedAt(newUserActivity.getCreatedAt());

		UserActivity updatedUserActivity = userActivityRepository.save(userActivity);
		return ResponseFormat.createFormat(updatedUserActivity, "Updated Successfully");
	}

	/* DELETE Method for Delete User Activity */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long userActivityId) {
		UserActivity userActivity = userActivityRepository.findById(userActivityId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", userActivityId));

		userActivityRepository.delete(userActivity);
		return ResponseFormat.createFormat(userActivity, "Deleted Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}

}
