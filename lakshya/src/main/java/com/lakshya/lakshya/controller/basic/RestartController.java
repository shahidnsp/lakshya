package com.lakshya.lakshya.controller.basic;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.LakshyaApplication;

@RestController
public class RestartController {
	
	@PostMapping("/restart")
    public void restart() {
        LakshyaApplication.restart();
    } 
}
