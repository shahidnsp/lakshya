package com.lakshya.lakshya.controller.basic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.LossReason;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.LossReasonRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;
import com.lakshya.lakshya.service.impl.ComboInterface;

/*
 * Controller For Enquiry Source 
 * 
 * Created By Shahid Neermunda
 * Date: 24/09/2019
 */

@RestController
@RequestMapping("/api/lossreason")
public class LossReasonController {

	@Autowired
	LossReasonRepository lossReasonRepository;

	@Autowired
	private UserRepository userRepository;

	/*
	 * GET Method for listing Loss Reason
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAllLossReason(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<LossReason> lossReasons = lossReasonRepository.findAll(paging);

		return ResponseFormat.createFormat(lossReasons, "Listed Successfully.");

	}
	
	/*
	 * GET Method for listing Chapter  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBranchById(@PathVariable(value = "id") Long lossReasonId) {
		LossReason newLossReason = lossReasonRepository.findById(lossReasonId)
				.orElseThrow(() -> new ItemNotFoundException("Loss Reason", "id", lossReasonId));
		return ResponseFormat.createFormat(newLossReason, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Loss Reason for Combobox
	 *
	 */
	@GetMapping("/getLossReasonForCombo")
	public HashMap<String, Object> getLossReasonForCombo() {
		List<ComboInterface> newLossReason = lossReasonRepository.getLossReasonForCombo();
		return ResponseFormat.createFormat(newLossReason, "Listed Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}

	/*
	 * POST Method for Creating Loss Reason
	 * 
	 */
	@PostMapping
	public Object createLossReason(@Valid @RequestBody(required = false) LossReason lossReason) {

		
		lossReason.setUser(this.getUser()); 

		LossReason newLossReason = (LossReason) lossReasonRepository.save(lossReason);
		return ResponseFormat.createFormat(newLossReason, "Created Successfully.");
	}

	/*
	 * PUT Method for Updating Loss Reason
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateLossReason(@PathVariable(value = "id") Long lossReasonId,
			@Valid @RequestBody(required = false) LossReason lossReason) {

		LossReason newLossReason = lossReasonRepository.findById(lossReasonId)
				.orElseThrow(() -> new ItemNotFoundException("Loss Reason", "id", lossReasonId));

		lossReason.setUser(this.getUser());
		
		lossReason.setId(lossReasonId);
		lossReason.setCreatedAt(newLossReason.getCreatedAt());
		
//		newLossReason.setName(lossReason.getName());
//		newLossReason.setColor(lossReason.getColor());
//		newLossReason.setRemark(lossReason.getRemark());

		LossReason updateLossReason = lossReasonRepository.save(lossReason);

		return ResponseFormat.createFormat(updateLossReason, "Updated Successfully.");
	}

	/*
	 * DELETE Method for Delete Loss Reason
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteLossReason(@PathVariable(value = "id") Long lossReasonId) {
		LossReason lossReason = lossReasonRepository.findById(lossReasonId)
				.orElseThrow(() -> new ItemNotFoundException("Loss Reason", "id", lossReasonId));

		lossReasonRepository.delete(lossReason);
		return ResponseFormat.createFormat(lossReason, "Deleted Successfully.");

	}

	/*
	 * PUT Method for Updating Loss Reason Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeLossReasonStatus(@PathVariable(value = "id") Long enquirySourceId) {

		LossReason lossReason = lossReasonRepository.findById(enquirySourceId)
				.orElseThrow(() -> new ItemNotFoundException("Loss Reason", "id", enquirySourceId));

		if (lossReason.getActive() == true)
			lossReason.setActive(false);
		else
			lossReason.setActive(true);

		LossReason updateLossReason = lossReasonRepository.save(lossReason);

		return ResponseFormat.createFormat(updateLossReason, "Status Updated Successfully.");
	}
	
	/*
	 * GET Method for Search in Loss reason table
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends LossReason> resultList = new ArrayList<LossReason>();
		try {
			//Class type (required) for query execution
			Class<?> classType = LossReason.class;
			//Fields need to be searched
			String[] fields = {"name","remark"};
			
			resultList = (List<? extends LossReason>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Loss Reasons Successfully. using " + text);
	}

}