package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.offlineexam.ExamGroup;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.model.offlineexam.OfflineExamMainGroup;
import com.lakshya.lakshya.offlinerepository.ExamGroupRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamMainGroupRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.repository.SubjectRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@AnalyzerDef(name = "offlinelowercase",
			tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
			filters = {
					@TokenFilterDef(factory = LowerCaseFilterFactory.class),
					@TokenFilterDef(factory = SnowballPorterFilterFactory.class),
					@TokenFilterDef(factory = NGramFilterFactory.class
							,params = { @Parameter(name = "minGramSize", value = "2"),@Parameter(name = "maxGramSize", value = "5")})
			}
		)
@RestController
@RequestMapping("/api/examgroup")
public class ExamGroupController {
	@Autowired
	ExamGroupRepository examGroupRepository; 

	@Autowired
	OfflineExamRepository offlineExamRepository; 
	
	@Autowired
	SubjectRepository subjectRepository;
	
	@Autowired
	OfflineExamMainGroupRepository offlineExamMainGroupRepository;
	/*
	 * GET Method for listing ExamGroups
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getExamGroups() {
		List<ExamGroup> examGroupList = (List<ExamGroup>) examGroupRepository.findAll();
		return ResponseFormat.createFormat(examGroupList, "Listed Successfully");
	}

	/*
	 * Method for Creating ExamGroups
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) ExamGroup examGroup) {
		
		OfflineExamMainGroup offlineExamMainGroup = offlineExamMainGroupRepository.findById(examGroup.getOfflineExamsMainGroupId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id",examGroup.getOfflineExamsMainGroupId()));
		examGroup.setOfflineExamMainGroup(offlineExamMainGroup );  
		
		Subject subject = subjectRepository.findById(examGroup.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id",examGroup.getSubjectId()));
		examGroup.setSubject(subject);
		
		OfflineExam offlineExam =  offlineExamRepository.findById(examGroup.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",examGroup.getOfflineExamId()));
		examGroup.setOfflineExam(offlineExam);		
		
		ExamGroup newExamGroup = (ExamGroup) examGroupRepository.save(examGroup);
		return ResponseFormat.createFormat(newExamGroup,"Created Successfully.");
		
//		return ResponseFormat.createFormat(examGroup,"Created Successfully.");
	}

	/*
	 * Method for Updating ExamGroups
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long examGroupId,
			@Valid @RequestBody(required = false) ExamGroup examGroup) {
		ExamGroup newExamGroup = examGroupRepository.findById(examGroupId)
				.orElseThrow(() -> new ItemNotFoundException("Exam Group", "id", examGroupId));
		
		OfflineExamMainGroup offlineExamMainGroup = offlineExamMainGroupRepository.findById(examGroup.getOfflineExamsMainGroupId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id",examGroup.getOfflineExamsMainGroupId()));
		examGroup.setOfflineExamMainGroup(offlineExamMainGroup );  
		
		Subject subject = subjectRepository.findById(examGroup.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id",examGroup.getSubjectId()));
		examGroup.setSubject(subject);
		
		OfflineExam offlineExam =  offlineExamRepository.findById(examGroup.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",examGroup.getOfflineExamId()));
		examGroup.setOfflineExam(offlineExam);		
		
		examGroup.setId(examGroupId);
		examGroup.setCreatedAt(newExamGroup.getCreatedAt());				
		
		ExamGroup updatedExamGroup = examGroupRepository.save(examGroup);
		return ResponseFormat.createFormat(updatedExamGroup, "Updated Successfully");		
	}

	/* DELETE Method for Delete ExamGroups */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long examGroupId) {
		ExamGroup newExamGroup = examGroupRepository.findById(examGroupId)
				.orElseThrow(() -> new ItemNotFoundException("ExamGroup", "id", examGroupId));

		examGroupRepository.delete(newExamGroup);
		return ResponseFormat.createFormat(newExamGroup, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in ExamGroups
	 *
	 */
//	@Autowired
//	private HibernateSearchService searchservice;
//
//	@SuppressWarnings("unchecked")
//	@GetMapping("/search/{text}")
//	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
//		List<? extends ExamGroup> resultList = new ArrayList<ExamGroup>();
//		try {
//			//Class type (required) for query execution
//			Class<?> classType = ExamGroup.class;
//			//Fields need to be searched
////			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
//			String[] fields = {"no_of_questions","score"};
//
//			resultList = (List<? extends ExamGroup>) searchservice.search(text,classType,resultList,fields);			
//		} catch (Exception ex) {
//			System.out.println("Error " + ex);
//		}
//		return ResponseFormat.createFormat(resultList, "Listed Exam Groups Successfully. using " + text);
//	}
}
