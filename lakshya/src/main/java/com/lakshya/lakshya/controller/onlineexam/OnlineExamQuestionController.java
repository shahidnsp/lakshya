package com.lakshya.lakshya.controller.onlineexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sound.midi.Sequence;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//import com.fasterxml.jackson.databind.ObjectMapper;
import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.onlineexam.Faculty;
import com.lakshya.lakshya.model.onlineexam.OnlineExam;
import com.lakshya.lakshya.model.onlineexam.OnlineExamQuestion;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestion;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionOption;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionPart;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionSection;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionSectionItem;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionTopic;
import com.lakshya.lakshya.model.onlineexam.QuestionYear;
import com.lakshya.lakshya.onlinerepository.OnlineExamQuestionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineExamRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionOptionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionPartRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionSectionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionTopicRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.FacultiesRepository;
import com.lakshya.lakshya.repository.QuestionYearRepository;
import com.lakshya.lakshya.repository.SubjectRepository;

@RestController
@RequestMapping("/api/onlineexamquestion")
public class OnlineExamQuestionController {

	@Autowired
	OnlineExamQuestionRepository onlineExamQuestionRepository;

	@Autowired
	OnlineExamRepository onlineExamRepository;
	
	@Autowired
	OnlineQuestionSectionRepository onlineQuestionSectionRepository;

	@Autowired
	OnlineQuestionRepository onlineQuestionRepository;

	@Autowired
	OnlineQuestionPartRepository onlineQuestionPartRepository;

	@Autowired
	OnlineQuestionOptionRepository onlineQuestionOptionRepository;
	
	@Autowired
	OnlineQuestionTopicRepository onlineQuestionTopicRepository;
	
	@Autowired
	SubjectRepository subjectRepository;

	@Autowired
	CourseRepository courseRepository;
	

	@Autowired
	FacultiesRepository facultiesRepository;

	@Autowired
	QuestionYearRepository questionYearRepository;

	/*
	 * GET Method for getting Online Exam by Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getOnlineExamQuestionById(@PathVariable(value = "id") Long onlineExamId) {

//		OnlineExamQuestion newOnlineExamQuestion = onlineExamQuestionRepository.findById(onlineExamId)
//				.orElseThrow(() -> new ItemNotFoundException("OnlineExamQuestion", "id", onlineExamId));
		
		/*
		 * List<OnlineExamQuestion> newOnlineExamQuestion = null; try {
		 * newOnlineExamQuestion = (List<OnlineExamQuestion>)
		 * onlineExamQuestionRepository
		 * .findOnlineExamQuestionByOnlineExamId(onlineExamId); } catch (Exception e) {
		 * new ItemNotFoundException("OnlineExamQuestion", "id", onlineExamId); } return
		 * ResponseFormat.createFormat(newOnlineExamQuestion, "Fetched Successfully");
		 */

		
		OnlineExam onlineExam = onlineExamRepository.findById(onlineExamId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineExam", "id", onlineExamId));
		
		
		if(onlineExam.getExamType().equals("NEET")) {
			List<OnlineExamQuestion> onlineExamQuestions = null;
			onlineExamQuestions = (List<OnlineExamQuestion>) onlineExamQuestionRepository
					.findOnlineExamQuestionByOnlineExamId(onlineExamId);
			
			HashMap<String, Object> myQuestions = new HashMap<String, Object>();
			List<HashMap<String, Object>> sectionLists = new ArrayList<HashMap<String, Object>>();
			HashMap<String, Object> sectionList = new HashMap<String, Object>();
			int sectionQno=1;
			sectionList.put("sectionId", 1);
			sectionList.put("section","");
			sectionList.put("sectionText", "");
			
			
			myQuestions.put("onlineExamId", onlineExamId);
			
			List<HashMap<String, Object>> questionLists = new ArrayList<HashMap<String, Object>>();
			for(OnlineExamQuestion onlineExamQuestion:onlineExamQuestions) {
				
				HashMap<String, Object> onlineQuestionList = new HashMap<String, Object>();
				OnlineQuestion onlineQuestion=onlineExamQuestion.getOnlineQuestion();
				onlineQuestionList.put("id", onlineQuestion.getId());
				onlineQuestionList.put("format", onlineQuestion.getFormat());
				onlineQuestionList.put("sectionId", 1);
				onlineQuestionList.put("section", "");
				onlineQuestionList.put("sectionText", "");
				
				HashMap<String, Object> onlineQuestionPartList = new HashMap<String, Object>();
				Set<OnlineQuestionPart> onlineQuestionParts=onlineQuestion.getOnlineQuestionPart();
				
				onlineQuestionList.put("onlineQuestionParts", onlineQuestionParts);
				
				OnlineQuestionTopic onlineQuestionTopic=onlineQuestion.getOnlineQuestionTopic();
				
				HashMap<String, Object> questionTopic=new HashMap<String, Object>();
				if(onlineQuestionTopic==null) {
					questionTopic.put("id", null);
					questionTopic.put("content", null);
					
					onlineQuestionList.put("onlineQuestionTopic",questionTopic );
					System.out.println("SSSSSSSSSSSSSSSSSHHHHHHHH");
				}
				else {
					questionTopic.put("id", onlineQuestionTopic.getId());
					questionTopic.put("content", onlineQuestionTopic.getContent());
					
					onlineQuestionList.put("onlineQuestionTopic",questionTopic);
				}
					
				
				/*for(OnlineQuestion onlineQuestion:onlineQuestions) {
					
				}*/
				questionLists.add(onlineQuestionList);
				
				sectionQno++;
				
			}
			
			sectionList.put("sectionQno", sectionQno);
			sectionLists.add(sectionList);
			
			myQuestions.put("questions", questionLists);
			return ResponseFormat.createFormat(myQuestions, "Fetched Successfully");
		}else {
			
			HashMap<String, Object> myQuestions = new HashMap<String, Object>();
			myQuestions.put("onlineExamId", onlineExamId);
			
			List<HashMap<String, Object>> questionLists = new ArrayList<HashMap<String, Object>>();
			
			List<HashMap<String, Object>> sectionLists = new ArrayList<HashMap<String, Object>>();
			
			List<OnlineQuestionSection> onlineQuestionSections=onlineQuestionSectionRepository.findAllByOnlineExamIdOrderByOrderBy(onlineExamId);
			
			int qno=1;
			
			for(OnlineQuestionSection onlineQuestionSection:onlineQuestionSections) {
				
				HashMap<String, Object> sectionList = new HashMap<String, Object>();
				int sectionQno=1;
				sectionList.put("sectionId", onlineQuestionSection.getId());
				sectionList.put("section", onlineQuestionSection.getName());
				sectionList.put("sectionText", onlineQuestionSection.getSectionText());
				
				for(OnlineQuestionSectionItem getOnlineQuestionSectionItem:onlineQuestionSection.getOnlineQuestionSectionItems()) {
					OnlineQuestion onlineQuestion=getOnlineQuestionSectionItem.getOnlineQuestion();
					HashMap<String, Object> onlineQuestionList = new HashMap<String, Object>();
					
					onlineQuestionList.put("id", onlineQuestion.getId());
					onlineQuestionList.put("qno", qno);
					onlineQuestionList.put("format", onlineQuestion.getFormat());
					onlineQuestionList.put("sectionId", onlineQuestionSection.getId());
					onlineQuestionList.put("section", onlineQuestionSection.getName());
					onlineQuestionList.put("sectionText", onlineQuestionSection.getSectionText());
					
					HashMap<String, Object> onlineQuestionPartList = new HashMap<String, Object>();
					Set<OnlineQuestionPart> onlineQuestionParts=onlineQuestion.getOnlineQuestionPart();
					
					onlineQuestionList.put("onlineQuestionParts", onlineQuestionParts);
					
					OnlineQuestionTopic onlineQuestionTopic=onlineQuestion.getOnlineQuestionTopic();
					
					HashMap<String, Object> questionTopic=new HashMap<String, Object>();
					if(onlineQuestionTopic==null) {
						questionTopic.put("id", null);
						questionTopic.put("content", null);
						
						onlineQuestionList.put("onlineQuestionTopic",questionTopic );
						System.out.println("SSSSSSSSSSSSSSSSSHHHHHHHH");
					}
					else {
						questionTopic.put("id", onlineQuestionTopic.getId());
						questionTopic.put("content", onlineQuestionTopic.getContent());
						
						onlineQuestionList.put("onlineQuestionTopic",questionTopic);
					}
						
					
					/*for(OnlineQuestion onlineQuestion:onlineQuestions) {
						
					}*/
					questionLists.add(onlineQuestionList);
					qno++;
					sectionQno++;

				}
				
				sectionList.put("sectionQno", sectionQno);
				sectionLists.add(sectionList);
				
			}
			
			myQuestions.put("questions", questionLists);
			myQuestions.put("sections", sectionLists);
			return ResponseFormat.createFormat(myQuestions, "Fetched Successfully");
		}
	}

	/*
	 * POST Method for Creating OnlineExam
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(
			@Valid @RequestBody(required = false) OnlineExamQuestion[] onlineExamQuestions) {

		List<OnlineExamQuestion> listOnlineExamQuestion = new ArrayList<OnlineExamQuestion>();

		for (OnlineExamQuestion onlineExamQuestion : onlineExamQuestions) {

			OnlineQuestion onlineQuestion = onlineQuestionRepository.findById(onlineExamQuestion.getOnlineQuestionId())
					.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id",
							onlineExamQuestion.getOnlineQuestionId()));
			onlineExamQuestion.setOnlineQuestion(onlineQuestion);

			OnlineExam onlineExam = onlineExamRepository.findById(onlineExamQuestion.getOnlineExamId()).orElseThrow(
					() -> new ItemNotFoundException("OnlineExam", "id", onlineExamQuestion.getOnlineExamId()));
			onlineExamQuestion.setOnlineExam(onlineExam);

			OnlineExamQuestion newOnlineExamQuestion = (OnlineExamQuestion) onlineExamQuestionRepository
					.save(onlineExamQuestion);
			listOnlineExamQuestion.add(newOnlineExamQuestion);

		}
		return ResponseFormat.createFormat(listOnlineExamQuestion,
				listOnlineExamQuestion.size() + " Added to Online Exam");

	}
	

	/*
	 * Method for Creating OnlineExam formatted response -->
	 * /api/onlineexamquestion/format/5
	 * 
	 */
	@GetMapping("/format/{id}")
	public HashMap<String, Object> getFormattedOnlineExamQuestionById(@PathVariable(value = "id") Long onlineExamId) {

//		OnlineExamQuestion newOnlineExamQuestion = onlineExamQuestionRepository.findById(onlineExamId)
//				.orElseThrow(() -> new ItemNotFoundException("OnlineExamQuestion", "id", onlineExamId));

		HashMap<String, Object> newResult = new HashMap<String, Object>();
		List<OnlineExamQuestion> newOnlineExamQuestion = null;
		List<OnlineQuestion> newOnlineQuestion = new ArrayList<OnlineQuestion>();

		try {

			newOnlineExamQuestion = (List<OnlineExamQuestion>) onlineExamQuestionRepository
					.findOnlineExamQuestionByOnlineExamId(onlineExamId);
			System.out.println("Chcking");
			newResult.put("onlineExamId", onlineExamId);

			for (OnlineExamQuestion onlineExamQuestion : newOnlineExamQuestion) {
//				Set<OnlineQuestionPart> answersList = onlineExamQuestion.getOnlineQuestion().getOnlineQuestionPart();
//				for (OnlineQuestionPart onlineQuestionPart : answersList) {
//					System.out.println("------> OnlineQuestionOptions : "+onlineQuestionPart.getOnlineQuestionOptions().size());
//				}
//				System.out.println("Set size : "+answersList.size());
				newOnlineQuestion.add(onlineExamQuestion.getOnlineQuestion());
			}
			newResult.put("onlineQuestion", newOnlineQuestion);

		} catch (Exception e) {
			new ItemNotFoundException("OnlineExamQuestion", "id", onlineExamId);
		}
		return ResponseFormat.createFormat(newResult, "Fetched Successfully");
//		return ResponseFormat.createFormat(newOnlineExamQuestion, "Fetched Successfully");
	}

//	public void dontWriteSomtimes(OnlineExamQuestion so) {
//	    ObjectMapper mapper = new ObjectMapper();
//	    SimpleFilterProvider filters = new SimpleFilterProvider()  
//	      .addFilter("filter properties by name", SimpleBeanPropertyFilter.serializeAllExcept("somtimeIgnore"));  
//	    com.fasterxml.jackson.databind.ObjectWriter writer = mapper.writer(filters);  
//	    try {
//	        System.out.println(writer.writeValueAsString(so));
//	    } catch (Exception e) {
//	        e.printStackTrace();
//	    }
//	}
}
