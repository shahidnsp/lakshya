package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.model.LeadStatus;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.LeadStatusRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/leadsstatus")
public class LeadStatusController {

	@Autowired
	LeadStatusRepository leadStatusRepository;

	@Autowired
	UserRepository userRepository;

	/*
	 * GET Method for listing Lead Status
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAllLeadStatus(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<LeadStatus> leadStatus = leadStatusRepository.findAll(paging);
		return ResponseFormat.createFormat(leadStatus, "Listed Successfully.");

	}

	/*
	 * GET Method for listing LeadStatus By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getLeadStatusById(@PathVariable(value = "id") Long leadStatusId) {
		LeadStatus newLeadStatus = leadStatusRepository.findById(leadStatusId)
				.orElseThrow(() -> new ItemNotFoundException("LeadStatus", "id", leadStatusId));
		return ResponseFormat.createFormat(newLeadStatus, "Listed Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}

	/*
	 * POST Method for Creating LeadStatus
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> createBranch(@Valid @RequestBody(required = false) LeadStatus leadStatus) {

		leadStatus.setUser(this.getUser());

		LeadStatus newLeadStatus = leadStatusRepository.save(leadStatus);
		return ResponseFormat.createFormat(newLeadStatus, "Created Successfully.");

	}

	/*
	 * PUT Method for Updating LeadStatus
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateBranch(@PathVariable(value = "id") Long leadStatusId,
			@Valid @RequestBody(required = false) LeadStatus leadStatus) {

		LeadStatus newLeadStatus = leadStatusRepository.findById(leadStatusId)
				.orElseThrow(() -> new ItemNotFoundException("LeadStatus", "id", leadStatusId));
		
		leadStatus.setUser(this.getUser());

		leadStatus.setId(leadStatusId);
		newLeadStatus.setCreatedAt(newLeadStatus.getCreatedAt());
		
		LeadStatus updatedLeadStatus = leadStatusRepository.save(leadStatus);
		return ResponseFormat.createFormat(updatedLeadStatus, "Updated Successfully.");

	}

	/*
	 * DELETE Method for Delete LeadStatus
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteBranch(@PathVariable(value = "id") Long leadStatusId) {
		LeadStatus leadStatus = leadStatusRepository.findById(leadStatusId)
				.orElseThrow(() -> new ItemNotFoundException("LeadStatus", "id", leadStatusId));

		leadStatusRepository.delete(leadStatus);
		return ResponseFormat.createFormat(leadStatus, "Deleted Successfully.");
	}
}
