package com.lakshya.lakshya.controller.basic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.EnquiryNote;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.EnquiryNoteRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/enquirynote")
public class EnquiryNoteController {
	@Autowired
	EnquiryNoteRepository enquiryNoteRepository;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	StudentRepository studentRepository;

	
	/*
	 * GET Method for listing Student Academic Records
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllEnquiryNote(@RequestParam Long studentId) {
		List<EnquiryNote> enquiryNotes = (List<EnquiryNote>) enquiryNoteRepository.findAllByStudentId(studentId);
		
		return ResponseFormat.createFormat(enquiryNotes, "Listed Successfully");
	}

	/*
	 * GET Method for listing Student Academic Record By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getEnquiryNoteById(@PathVariable(value = "id") Long enquiryNoteId) {
		EnquiryNote enquiryNote = enquiryNoteRepository.findById(enquiryNoteId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Note", "id", enquiryNoteId));
		return ResponseFormat.createFormat(enquiryNote, "Listed Successfully");
	}

	/*
	 * Method for Creating Student Academic Record
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@RequestBody(required = false) EnquiryNote enquiryNote) {
		
		Student newStudent = studentRepository.findById(enquiryNote.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", enquiryNote.getStudentId()));
		enquiryNote.setStudent(newStudent);
	
		enquiryNote.setUser(getUser());
		
		EnquiryNote newEnquiryNote = (EnquiryNote) enquiryNoteRepository.save(enquiryNote);
		return ResponseFormat.createFormat(newEnquiryNote, "Created Successfully.");
	}

	/*
	 * Method for Updating Student Academic Record
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long enquiryNoteId,
			@Valid @RequestBody(required = false) EnquiryNote enquiryNote) {
		EnquiryNote newEnquiryNote = enquiryNoteRepository.findById(enquiryNoteId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Note", "id", enquiryNoteId));
		
		Student newStudent = studentRepository.findById(enquiryNote.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", enquiryNote.getStudentId()));
		enquiryNote.setStudent(newStudent);
		
		enquiryNote.setUser(getUser());
		
		enquiryNote.setId(enquiryNoteId);
		enquiryNote.setCreatedAt(newEnquiryNote.getCreatedAt());

		EnquiryNote updatedEnquiryNote = enquiryNoteRepository.save(enquiryNote);
		return ResponseFormat.createFormat(updatedEnquiryNote, "Updated Successfully");
	}

	/* DELETE Method for Delete Enquiry Collaborator */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long enquiryNoteId) {
		EnquiryNote newEnquiryNote = enquiryNoteRepository.findById(enquiryNoteId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", enquiryNoteId));

		enquiryNoteRepository.delete(newEnquiryNote);
		return ResponseFormat.createFormat(newEnquiryNote, "Deleted Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}

}
