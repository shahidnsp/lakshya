package com.lakshya.lakshya.controller.basic;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;
import com.lakshya.lakshya.service.impl.ComboInterface;

@RestController
@RequestMapping("/api/course")
public class CourseController {
	@Autowired
	CourseRepository courseRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	UserRepository userRepository;
	
	/*
	 * GET Method for listing all courses Paginated
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAll(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<Course> courses = courseRepository.findAll(paging);
		return ResponseFormat.createFormat(courses, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing all courses
	 * 
	 */
	@GetMapping("/all")
	public HashMap<String, Object> getAll() {
		List<Course> courses = (List<Course>) courseRepository.findAll();
		return ResponseFormat.createFormat(courses, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing course  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBranchById(@PathVariable(value = "id") Long courseId) {
		Course newCourse = courseRepository.findById(courseId)
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseId));
		return ResponseFormat.createFormat(newCourse, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing course  By Id
	 *
	 */
	@GetMapping("/courseforselectbox")
	public HashMap<String, Object> getCourseForCombo() {
		List<ComboInterface> newCourse = courseRepository.getCourseForCombo();
		return ResponseFormat.createFormat(newCourse, "Listed Successfully");
	}

	/*
	 * POST Method for Create course
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) Course course) {

//		if (course.getBranchId() == null) {
//			course.setSubjectFrom("Main");
//		} else {
//			Branch branch = branchRepository.findById(course.getBranchId())
//					.orElseThrow(() -> new ItemNotFoundException("Branch", "id", course.getBranchId()));
//			course.setBranch(branch);
//		}
		
//		Branch  branch = branchRepository.findById(course.getBranchId())
//				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", course.getBranchId()));
//		course.setBranch(branch);
		
		//User user = userRepository.findById(course.getUserId()).orElseThrow(() -> new ItemNotFoundException("User", "id", course.getUserId()));
		course.setUser(this.getUser());
		
		Course newCourse = (Course) courseRepository.save(course);
		return ResponseFormat.createFormat(newCourse, "Created successfully");
	
	}
	
	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}

	/*
	 * PUT Method for Update given course
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long courseId,
			@Valid @RequestBody(required = false) Course course) {
		
		Course newCourse = courseRepository.findById(courseId)
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseId));
		
		//User user = userRepository.findById(course.getUserId()).orElseThrow(() -> new ItemNotFoundException("User", "id", course.getUserId()));
		course.setUser(this.getUser());

		
//		if (course.getBranchId() == null) {
//			course.setSubjectFrom("Main");
//		} else {
//			Branch branch = branchRepository.findById(course.getBranchId())
//					.orElseThrow(() -> new ItemNotFoundException("Branch", "id", course.getBranchId()));
//			newCourse.setBranch(branch);		
//		}

		course.setId(courseId);
		course.setCreatedAt(newCourse.getCreatedAt());
		
		Course updateCourse = courseRepository.save(course);
		return ResponseFormat.createFormat(updateCourse, "Updated Succesfully");
	}

	/*
	 * DELETE Method for Update given course
	 * 
	 */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteCourse(@PathVariable(value = "id") Long courseId) {
		Course course = courseRepository.findById(courseId)
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseId));
	
		courseRepository.delete(course);
		return ResponseFormat.createFormat(course, "Deleted Successfully");
	}

	/*
	 * PUT Method for Updating Course Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeBranchCategoryStatus(@PathVariable(value = "id") Long courseId) {

		Course course = courseRepository.findById(courseId)
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseId));

		if (course.getActive() == true)
			course.setActive(false);
		else
			course.setActive(true);

		Course updatedCourse = courseRepository.save(course);
		return ResponseFormat.createFormat(updatedCourse, "Status Updated Successfully.");
	}

	
	/*
	 * GET Method for Search in Course table
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends Course> resultList = new ArrayList<Course>();
		try {
			//Class type (required) for query execution
			Class<?> classType = Course.class;
			//Fields need to be searched
			String[] fields = {"name","remark"};
			
			resultList = (List<? extends Course>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Courses Successfully. using " + text);
	}
}
