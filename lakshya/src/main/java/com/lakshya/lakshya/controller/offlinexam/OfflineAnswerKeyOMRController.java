package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineAnswerKeyOMR;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.model.offlineexam.OfflineExamKey;
import com.lakshya.lakshya.model.offlineexam.OfflineExamMainGroup;
import com.lakshya.lakshya.model.offlineexam.OfflineExamMark;
import com.lakshya.lakshya.offlinerepository.OfflineAnswerKeyOMRRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamKeyRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamMainGroupRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineanswerkeyomr")
public class OfflineAnswerKeyOMRController {
	
	@Autowired
	OfflineAnswerKeyOMRRepository offlineAnswerKeyOMRRepository;
	
	@Autowired
	OfflineExamRepository offlineExamRepository;
	
	@Autowired
	OfflineExamKeyRepository offlineExamKeyRepository;
	
	@Autowired
	OfflineExamMainGroupRepository offlineExamMainGroupRepository;
	
	@Autowired
	UserRepository userRepository;
	
	/*
	 * GET Method for listing OfflineExamMark
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineAnswerKeyOMR> offlineAnswerKeyOMR = (List<OfflineAnswerKeyOMR>)offlineAnswerKeyOMRRepository.findAll();
		return ResponseFormat.createFormat(offlineAnswerKeyOMR, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamMark
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineAnswerKeyOMR offlineAnswerKeyOMR) {
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineAnswerKeyOMR.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineAnswerKeyOMR.getOfflineExamId()));
		offlineAnswerKeyOMR.setOfflineExam(offlineExam );  
		
		OfflineExamKey offlineExamKey = offlineExamKeyRepository.findById(offlineAnswerKeyOMR.getOfflineExamKeyId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamKeyId", "id",offlineAnswerKeyOMR.getOfflineExamKeyId()));
		offlineAnswerKeyOMR.setOfflineExamKey(offlineExamKey);  
		
		OfflineExamMainGroup offlineExamMainGroup= offlineExamMainGroupRepository.findById(offlineAnswerKeyOMR.getOfflineExamMainGroupId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id",offlineAnswerKeyOMR.getOfflineExamMainGroupId()));
		offlineAnswerKeyOMR.setOfflineExamMainGroup(offlineExamMainGroup); 
		
		User user= userRepository.findById(offlineAnswerKeyOMR.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineAnswerKeyOMR.getUserId()));
		offlineAnswerKeyOMR.setUser(user); 		

		OfflineAnswerKeyOMR newOfflineAnswerKeyOMR = (OfflineAnswerKeyOMR) offlineAnswerKeyOMRRepository.save(offlineAnswerKeyOMR);
		return ResponseFormat.createFormat(newOfflineAnswerKeyOMR,"Created Successfully.");				
	}

	/*
	 * Method for Updating OfflineExamMark
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineAnswerKeyOMRId,
			@Valid @RequestBody(required = false) OfflineAnswerKeyOMR offlineAnswerKeyOMR) {
		
		OfflineAnswerKeyOMR newOfflineAnswerKeyOMR= offlineAnswerKeyOMRRepository.findById(offlineAnswerKeyOMRId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineAnswerKeyOMR", "id",offlineAnswerKeyOMRId));
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineAnswerKeyOMR.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineAnswerKeyOMR.getOfflineExamId()));
		offlineAnswerKeyOMR.setOfflineExam(offlineExam );  
		
		OfflineExamKey offlineExamKey = offlineExamKeyRepository.findById(offlineAnswerKeyOMR.getOfflineExamKeyId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamKeyId", "id",offlineAnswerKeyOMR.getOfflineExamKeyId()));
		offlineAnswerKeyOMR.setOfflineExamKey(offlineExamKey);  
		
		OfflineExamMainGroup offlineExamMainGroup= offlineExamMainGroupRepository.findById(offlineAnswerKeyOMR.getOfflineExamMainGroupId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id",offlineAnswerKeyOMR.getOfflineExamMainGroupId()));
		offlineAnswerKeyOMR.setOfflineExamMainGroup(offlineExamMainGroup); 
		
		User user= userRepository.findById(offlineAnswerKeyOMR.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineAnswerKeyOMR.getUserId()));
		offlineAnswerKeyOMR.setUser(user); 		
		
		offlineAnswerKeyOMR.setId(offlineAnswerKeyOMRId);
		offlineAnswerKeyOMR.setCreatedAt(newOfflineAnswerKeyOMR.getCreatedAt());			
		
		OfflineAnswerKeyOMR updatedOfflineAnswerKeyOMR = (OfflineAnswerKeyOMR) offlineAnswerKeyOMRRepository.save(offlineAnswerKeyOMR);
		return ResponseFormat.createFormat(updatedOfflineAnswerKeyOMR, "Updated Successfully");					
	}

	/* DELETE Method for Delete OfflineExamMark */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long OfflineAnswerKeyOMRId) {
		OfflineAnswerKeyOMR newOfflineAnswerKeyOMR = offlineAnswerKeyOMRRepository.findById(OfflineAnswerKeyOMRId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineAnswerKeyOMR", "id", OfflineAnswerKeyOMRId));
		offlineAnswerKeyOMRRepository.delete(newOfflineAnswerKeyOMR);
		return ResponseFormat.createFormat(newOfflineAnswerKeyOMR, "Deleted Successfully");
		
	}
	
	/*
	 * GET Method for Search in OfflineAnswerKeyOMR
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineAnswerKeyOMR> resultList = new ArrayList<OfflineAnswerKeyOMR>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineAnswerKeyOMR.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"offline_answer_key_omrscol","course_code","quesion_no","answer"};

			resultList = (List<? extends OfflineAnswerKeyOMR>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineAnswerKeyOMR Successfully. using " + text);
	}
}
