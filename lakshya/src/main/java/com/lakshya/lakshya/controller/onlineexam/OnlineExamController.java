package com.lakshya.lakshya.controller.onlineexam;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.onlineexam.OnlineExam;
import com.lakshya.lakshya.model.onlineexam.OnlineExamBatch;
import com.lakshya.lakshya.model.onlineexam.OnlineExamResult;
import com.lakshya.lakshya.model.onlineexam.OnlineExamResultList;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestion;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionOption;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionPart;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionSection;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionSectionItem;
import com.lakshya.lakshya.onlinerepository.OnlineExamBatchRepository;
import com.lakshya.lakshya.onlinerepository.OnlineExamRepository;
import com.lakshya.lakshya.onlinerepository.OnlineExamResultListRepository;
import com.lakshya.lakshya.onlinerepository.OnlineExamResultRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionOptionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionPartRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionSectionItemRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionSectionRepository;
import com.lakshya.lakshya.repository.BatchRepository;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/onlineexam")
public class OnlineExamController {
	@Autowired
	OnlineExamRepository onlineExamRepository;
	
	@Autowired
	OnlineExamBatchRepository onlineExamBatchRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	BatchRepository batchRepository;
	
	@Autowired
	OnlineQuestionPartRepository onlineQuestionPartRepository;

	@Autowired
	OnlineQuestionOptionRepository onlineQuestionOptionRepository;

	@Autowired
	OnlineQuestionRepository onlineQuestionRepository;
	
	@Autowired
	OnlineQuestionSectionRepository  onlineQuestionSectionRepository;
	
	@Autowired
	OnlineQuestionSectionItemRepository onlineQuestionSectionItemRepository;

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	OnlineExamResultRepository onlineExamResultRepository;

	@Autowired
	OnlineExamResultListRepository onlineExamResultListRepository;

	/*
	 * GET Method for listing OnlineExam
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getQuestionYears() {
		List<OnlineExam> OnlineExam = (List<OnlineExam>) onlineExamRepository.findAll();
		return ResponseFormat.createFormat(OnlineExam, "Listed Successfully");
	}

	/*
	 * GET Method for getting Online Exam by Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getOnlineExamById(@PathVariable(value = "id") Long onlineExamId) {
		OnlineExam newOnlineExam = onlineExamRepository.findById(onlineExamId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineExamId));
		return ResponseFormat.createFormat(newOnlineExam, "Fetched Successfully");
	}

	/*
	 * Method for Creating OnlineExam
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) OnlineExam onlineExam, Map<String, Object> responseBody) {

		Branch branch = branchRepository.findById(onlineExam.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", onlineExam.getBranchId()));
		onlineExam.setBranch(branch);

		
		
		Set<OnlineExamBatch> onlineExamBatches=onlineExam.getOnlineExamBatchs();
		for(OnlineExamBatch onlineExamBatch:onlineExamBatches) {
			Batch batch = batchRepository.findById(onlineExamBatch.getBatchId())
					.orElseThrow(() -> new ItemNotFoundException("Batch", "id", onlineExamBatch.getBatchId()));
			
			onlineExamBatch.setBatch(batch);
			onlineExamBatch.setOnlineExam(onlineExam);
		}
		
		Set<OnlineQuestionSection> onlineQuestionSections=onlineExam.getOnlineQuestionSections();
		for(OnlineQuestionSection onlineQuestionSection:onlineQuestionSections) {
			onlineQuestionSection.setOnlineExam(onlineExam);
			Set<OnlineQuestionSectionItem> onlineQuestionSectionItems=onlineQuestionSection.getOnlineQuestionSectionItems();
			for(OnlineQuestionSectionItem onlineQuestionSectionItem:onlineQuestionSectionItems) {
				//onlineQuestionSectionItem.setOnlineQuestionSection(onlineQuestionSection);
				OnlineQuestion onlineQuestion=onlineQuestionRepository.findById(onlineQuestionSectionItem.getOnlineQuestionId())
						.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id",onlineQuestionSectionItem.getOnlineQuestionId()));
				onlineQuestionSectionItem.setOnlineQuestion(onlineQuestion);
			}
		}
		
		OnlineExam newOnlineExam = (OnlineExam) onlineExamRepository.save(onlineExam);
		
		return ResponseFormat.createFormat(newOnlineExam, "Created Successfully.");
		
	}

	/*
	 * Method for Updating OnlineExam
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long onlineExamId,
			@Valid @RequestBody(required = false) OnlineExam onlineExam) {
		OnlineExam newOnlineExam = onlineExamRepository.findById(onlineExamId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineExam", "id", onlineExamId));

		Branch branch = branchRepository.findById(onlineExam.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", onlineExam.getBranchId()));
		onlineExam.setBranch(branch);

		onlineExam.setId(onlineExamId);
		onlineExam.setCreatedAt(newOnlineExam.getCreatedAt());
		
		
		//onlineExamBatchRepository.deleteByOnlineExamId(onlineExamId);
		Set<OnlineExamBatch> onlineExamBatches=onlineExam.getOnlineExamBatchs();
		for(OnlineExamBatch onlineExamBatch:onlineExamBatches) {
			Batch batch = batchRepository.findById(onlineExamBatch.getBatchId())
					.orElseThrow(() -> new ItemNotFoundException("Batch", "id", onlineExamBatch.getBatchId()));
			
			onlineExamBatch.setBatch(batch);
			onlineExamBatch.setOnlineExam(onlineExam);
		}
		
		
		//onlineQuestionSectionRepository.deleteByOnlineExamId(onlineExamId);
		Set<OnlineQuestionSection> onlineQuestionSections=onlineExam.getOnlineQuestionSections();
		for(OnlineQuestionSection onlineQuestionSection:onlineQuestionSections) {
			onlineQuestionSection.setOnlineExam(onlineExam);
			Set<OnlineQuestionSectionItem> onlineQuestionSectionItems=onlineQuestionSection.getOnlineQuestionSectionItems();
			for(OnlineQuestionSectionItem onlineQuestionSectionItem:onlineQuestionSectionItems) {
				//onlineQuestionSectionItem.setOnlineQuestionSection(onlineQuestionSection);
				OnlineQuestion onlineQuestion=onlineQuestionRepository.findById(onlineQuestionSectionItem.getOnlineQuestionId())
						.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id",onlineQuestionSectionItem.getOnlineQuestionId()));
				onlineQuestionSectionItem.setOnlineQuestion(onlineQuestion);
			}
		}

		OnlineExam updatedOnlineExam = onlineExamRepository.save(onlineExam);
		return ResponseFormat.createFormat(updatedOnlineExam, "Updated Successfully");
	}

	/* DELETE Method for Delete OnlineExam */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long onlineExamId) {
		OnlineExam newOnlineExam = onlineExamRepository.findById(onlineExamId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineExam", "id", onlineExamId));

		onlineExamRepository.delete(newOnlineExam);
		return ResponseFormat.createFormat(newOnlineExam, "Deleted Successfully");
	}

	/*
	 * GET Method for Search in OnlineExam
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OnlineExam> resultList = new ArrayList<OnlineExam>();
		try {
			// Class type (required) for query execution
			Class<?> classType = OnlineExam.class;
			// Fields need to be searched
			// String[] fields =
			// {"name","address","branchPhones.phone","branchLandLines.phone",
			// "branchCategory.name"};
			String[] fields = { "name", "examType", "examModel", "branch.name" };
			resultList = (List<? extends OnlineExam>) searchservice.search(text, classType, resultList, fields);
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OnlineExams Successfully. using " + text);
	}

	/*
	 * GET Method for _____________
	 *
	 */
	@GetMapping("/test")
	public HashMap<String, Object> test(@RequestParam List<String> values) {
		System.out.println(values);
		return ResponseFormat.createFormat(values, "Listed OnlineExams Successfully. using ");
	}

	/*
	 * GET Method for getting Exam Result
	 *
	 */
	@GetMapping("/examresult/{id}")
	public HashMap<String, Object> getExamResult(@PathVariable(value = "id") Long onlineExamId) {

//		Student student = studentRepository.findById(studentId)
//				.orElseThrow(() -> new ItemNotFoundException("Student", "id", studentId));

		OnlineExam onlineExam = onlineExamRepository.findById(onlineExamId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineExam", "id", onlineExamId));

		OnlineExamResult onlineExamResult = onlineExamResultRepository.findOnlineExamResultByOnlineExamId(onlineExamId);
//				.orElseThrow(() -> new ItemNotFoundException("OnlineExamResult", "id", onlineExamId));

		return ResponseFormat.createFormat(onlineExamResult, "Listed OnlineExams Successfully");
	}

	/*
	 * POST Method for Submit Exam Exam
	 *
	 */
	@PostMapping("submitExam/{id}")
	public HashMap<String, Object> submitExam(@PathVariable(value = "id") Long onlineExamId,
			@RequestBody(required = false) Map<String, Object> object) {

		OnlineExam onlineExam = onlineExamRepository.findById(onlineExamId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineExam", "id", onlineExamId));

		float correctMark=(float) 0;
		float negativeMark=(float) 0;
		
		if(onlineExam.getExamType().equals("NEET")) {
			correctMark = onlineExam.getCorrectMark();
			negativeMark = onlineExam.getNegativeMark();
		}

		Float totalCorrectMark = (float) 0;
		Float totalNegativeMark = (float) 0;
		int noofAnswered = 0;
		int noofUnAnswered = 0;
		int noofCorrect = 0;
		int noofInCorrect = 0;

		Long studentId = Long.parseLong(object.get("studentId").toString());
		Student student = studentRepository.findById(studentId)
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", studentId));

		OnlineExamResult onlineExamResult = new OnlineExamResult();
		onlineExamResult.setOnlineExam(onlineExam);
		onlineExamResult.setStudent(student);
		onlineExamResult.setNoOfAnswered(noofAnswered);
		onlineExamResult.setNoOfUnAnswered(noofUnAnswered);
		onlineExamResult.setNoOfCorrect(noofCorrect);
		onlineExamResult.setNoOfInCorrect(noofInCorrect);
		onlineExamResult.setTotalCorrectMark(totalCorrectMark);
		onlineExamResult.setTotalNegativeMark(totalNegativeMark);
		onlineExamResult.setTotalMark(totalCorrectMark);
		
		
		OnlineExamResult newOnlineExamResult = (OnlineExamResult) onlineExamResultRepository.save(onlineExamResult);
		Long curOnlineResultId = newOnlineExamResult.getId();

		ArrayList<String> answers = (ArrayList<String>) object.get("answers");

		Map<String, Object>[] answerLists = answers.toArray(new HashMap[answers.size()]);
		for (Map<String, Object> answerList : answerLists) {
			noofAnswered++;
			ArrayList<String> givenAnswers = (ArrayList<String>) answerList.get("answer");
			Map<String, Object>[] givenAnswerLists = givenAnswers.toArray(new HashMap[givenAnswers.size()]);
			Long curQuestionId = Long.parseLong(answerList.get("questionId").toString());
			Long curPartId = Long.parseLong(answerList.get("partId").toString());

			OnlineQuestion onlineQuestion = onlineQuestionRepository.findById(curQuestionId)
					.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", curQuestionId));

			OnlineQuestionPart onlineQuestionPart = onlineQuestionPartRepository.findById(curPartId)
					.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionPart", "id", curPartId));

			String CorrectAnswer = "";
			String GivenAnswer = "";

			Boolean isCorrectAnswer = false;
			for (Map<String, Object> givenAnswerList : givenAnswerLists) {
				String curAnswer = givenAnswerList.get("answer").toString();
				GivenAnswer = GivenAnswer + curAnswer + ", ";
				List<OnlineQuestionOption> onlineQuestionOptions = onlineQuestionOptionRepository
						.findAllByonlineQuestionPartIdAndIsCorrectAndAnswerValue(curPartId, true, curAnswer);
				for (OnlineQuestionOption onlineQuestionOption : onlineQuestionOptions) {
					CorrectAnswer = CorrectAnswer + onlineQuestionOption.getAnswerValue() + ", ";
					if (onlineQuestionOption.getAnswerValue().equalsIgnoreCase(curAnswer)) {
						isCorrectAnswer = true;
					}
				}

				List<OnlineQuestionOption> onlineQuestionCorrectOptions = onlineQuestionOptionRepository
						.findAllByonlineQuestionPartIdAndIsCorrect(curPartId, true);
				for (OnlineQuestionOption onlineQuestionCorrectOption : onlineQuestionCorrectOptions) {
					CorrectAnswer = CorrectAnswer + onlineQuestionCorrectOption.getAnswerValue() + ", ";
				}
			}

			if (isCorrectAnswer) {
				if(!onlineExam.getExamType().equals("NEET")) {
					correctMark=onlineQuestionPart.getMark();
				}
				
				totalCorrectMark += correctMark;
				noofCorrect++;
				System.err.println(totalCorrectMark);
			} else {
				totalNegativeMark += negativeMark;
				noofInCorrect++;
				System.out.println(totalNegativeMark);
			}

			// Save Each Marked Answers......................
			OnlineExamResultList examResultList = new OnlineExamResultList();
			examResultList.setOnlineExamResult(newOnlineExamResult);
			examResultList.setOnlineQuestion(onlineQuestion);
			examResultList.setOnlineQuestionPart(onlineQuestionPart);
			examResultList.setMarkedAnswer(GivenAnswer);
			examResultList.setCorrectAnswer(CorrectAnswer);
			if (isCorrectAnswer)
				examResultList.setMark(correctMark);
			else
				examResultList.setMark(negativeMark);
			examResultList.setIsCorrect(isCorrectAnswer);

			onlineExamResultListRepository.save(examResultList);
		}

		// Update Result with Marks Details
		OnlineExamResult myOnlineExamResult = onlineExamResultRepository.findById(curOnlineResultId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineExamResult", "id", curOnlineResultId));
		myOnlineExamResult.setOnlineExam(onlineExam);
		myOnlineExamResult.setStudent(student);
		myOnlineExamResult.setNoOfAnswered(noofAnswered);
		myOnlineExamResult.setNoOfUnAnswered(noofUnAnswered);
		myOnlineExamResult.setNoOfCorrect(noofCorrect);
		myOnlineExamResult.setNoOfInCorrect(noofInCorrect);
		myOnlineExamResult.setTotalCorrectMark(totalCorrectMark);
		myOnlineExamResult.setTotalNegativeMark(totalNegativeMark);
		Float grantTotal = totalCorrectMark - totalNegativeMark;
		myOnlineExamResult.setTotalMark(grantTotal);

		OnlineExamResult UpdatedOnlineExamResult = (OnlineExamResult) onlineExamResultRepository.save(myOnlineExamResult);

		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("Attented", noofAnswered);
		result.put("skipped", noofUnAnswered);
		result.put("correctAnswer", noofCorrect);
		result.put("incorrectAnswer", noofInCorrect);
		result.put("mark", grantTotal);

		int onofquestion = onlineExam.getNoOfQuestions();

		result.put("outOf", onofquestion * correctMark);

		return ResponseFormat.createFormat(result, "Submit Successfully.");
	}

}
