package com.lakshya.lakshya.controller.basic;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.AdditionalFee;
import com.lakshya.lakshya.model.CourseDetail;
import com.lakshya.lakshya.model.EnquiryList;
import com.lakshya.lakshya.model.EnquirySource;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.StudentAdmission;
import com.lakshya.lakshya.model.StudentAdmissionItem;
import com.lakshya.lakshya.repository.AcademicYearRepository;
import com.lakshya.lakshya.repository.AdditionalFeeRepository;
import com.lakshya.lakshya.repository.CourseDetailRepository;
import com.lakshya.lakshya.repository.EnquiryListRepository;
import com.lakshya.lakshya.repository.EnquirySourceRepository;
import com.lakshya.lakshya.repository.StudentAdmissionItemRepository;
import com.lakshya.lakshya.repository.StudentAdmissionRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/studentadmission")
public class StudentAdmissionController {
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	StudentAdmissionRepository studentAdmissionRepository;
	
	@Autowired
	StudentAdmissionItemRepository studentAdmissionItemRepository;
	
	@Autowired
	AdditionalFeeRepository additionalFeeRepository;
	
	@Autowired
	CourseDetailRepository courseDetailRepository;
	
	/*
	 * GET Method for listing StudentAdmission  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getStudentAdmissionById(@PathVariable(value = "id") Long studentAdmissionId) {
		StudentAdmission studentAdmission = studentAdmissionRepository.findById(studentAdmissionId)
				.orElseThrow(() -> new ItemNotFoundException("StudentAdmission", "id", studentAdmissionId));
		return ResponseFormat.createFormat(studentAdmission, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing StudentAdmission  By Student Id
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getStudentAdmissionByStudentId(@RequestParam Long studentId) {
		List<StudentAdmission> studentAdmission = studentAdmissionRepository.findAllByStudentId(studentId);
		return ResponseFormat.createFormat(studentAdmission, "Listed Successfully");
	}
	
	/*
	 * POST Method for Creating StudentAdmission
	 * 
	 */
	@PostMapping
	public Object create(@Valid @RequestBody(required = false) StudentAdmission studentAdmission) {

		Student student = studentRepository.findById(studentAdmission.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", studentAdmission.getStudentId()));
		studentAdmission.setStudent(student);
		
		BigDecimal zeroValue = new BigDecimal(0);
		studentAdmission.setPaid(zeroValue);
		studentAdmission.setUncleared(zeroValue);
		studentAdmission.setRefund(zeroValue);
		studentAdmission.setBalance(studentAdmission.getTotalFee());
		
		StudentAdmission newStudentAdmission = (StudentAdmission) studentAdmissionRepository.save(studentAdmission);
		
		Set<StudentAdmissionItem> admissionItems= newStudentAdmission.getStudentAdmissionItem();
		
	    for (StudentAdmissionItem admissionItem : admissionItems) {
	    	studentAdmissionItemRepository.delete(admissionItem);
	    }
	    
	    Set<StudentAdmissionItem> newadmissionItems= studentAdmission.getStudentAdmissionItem();
	    
	    StudentAdmission newStudentAdmission1 = studentAdmissionRepository.findById(newStudentAdmission.getId())
				.orElseThrow(() -> new ItemNotFoundException("StudentAdmission", "id", newStudentAdmission.getId()));
		
	    for (StudentAdmissionItem newadmissionItem : newadmissionItems) {
	    	StudentAdmissionItem admissionItem=new StudentAdmissionItem();
	    	admissionItem.setStudentAdmission(newStudentAdmission1);
	    	admissionItem.setFeetype(newadmissionItem.getFeetype());
	    	admissionItem.setAmount(newadmissionItem.getAmount());
	    	if(newadmissionItem.getAdditionalFeeId()!=null) {
	    		AdditionalFee additionalFee = additionalFeeRepository.findById(newadmissionItem.getAdditionalFeeId())
	    				.orElseThrow(() -> new ItemNotFoundException("AdditionalFee", "id", newadmissionItem.getAdditionalFeeId()));
	    		newadmissionItem.setAdditionalFee(additionalFee);
	    	}
	    	
	    	if(newadmissionItem.getCourseDetailId()!=null) {
	    		CourseDetail courseDetail = courseDetailRepository.findById(newadmissionItem.getCourseDetailId())
	    				.orElseThrow(() -> new ItemNotFoundException("AdditionalFee", "id", newadmissionItem.getCourseDetailId()));
	    		newadmissionItem.setCourseDetail(courseDetail);
	    	}
	    	studentAdmissionItemRepository.save(newadmissionItem);
	    	
	    }
		
	    StudentAdmission updatedStudentAdmission = studentAdmissionRepository.findById(newStudentAdmission.getId())
				.orElseThrow(() -> new ItemNotFoundException("StudentAdmission", "id", newStudentAdmission.getId()));
		
		return ResponseFormat.createFormat(updatedStudentAdmission, "Created Successfully.");	
	}
	
	/*
	 * PUT Method for Updating StudentAdmission
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long studentAdmissionId,
			@Valid @RequestBody(required = false) StudentAdmission studentAdmission) {

		StudentAdmission newStudentAdmission = studentAdmissionRepository.findById(studentAdmissionId)
				.orElseThrow(() -> new ItemNotFoundException("StudentAdmission", "id", studentAdmissionId));

		Student student = studentRepository.findById(studentAdmission.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", studentAdmission.getStudentId()));
		studentAdmission.setStudent(student);
		
		studentAdmission.setId(studentAdmissionId);
		studentAdmission.setCreatedAt(newStudentAdmission.getCreatedAt());
		
		Set<StudentAdmissionItem> admissionItems= newStudentAdmission.getStudentAdmissionItem();
		
	    for (StudentAdmissionItem admissionItem : admissionItems) {
	    	studentAdmissionItemRepository.delete(admissionItem);
	    }
				
		StudentAdmission updatedStudentAdmission = studentAdmissionRepository.save(studentAdmission);
		
		
	    Set<StudentAdmissionItem> newadmissionItems= studentAdmission.getStudentAdmissionItem();
	    
	    StudentAdmission newStudentAdmission1 = studentAdmissionRepository.findById(newStudentAdmission.getId())
				.orElseThrow(() -> new ItemNotFoundException("StudentAdmission", "id", newStudentAdmission.getId()));
		
	    for (StudentAdmissionItem newadmissionItem : newadmissionItems) {
	    	StudentAdmissionItem admissionItem=new StudentAdmissionItem();
	    	admissionItem.setStudentAdmission(newStudentAdmission1);
	    	admissionItem.setFeetype(newadmissionItem.getFeetype());
	    	admissionItem.setAmount(newadmissionItem.getAmount());
	    	if(newadmissionItem.getAdditionalFeeId()!=null) {
	    		AdditionalFee additionalFee = additionalFeeRepository.findById(newadmissionItem.getAdditionalFeeId())
	    				.orElseThrow(() -> new ItemNotFoundException("AdditionalFee", "id", newadmissionItem.getAdditionalFeeId()));
	    		newadmissionItem.setAdditionalFee(additionalFee);
	    	}
	    	
	    	if(newadmissionItem.getCourseDetailId()!=null) {
	    		CourseDetail courseDetail = courseDetailRepository.findById(newadmissionItem.getCourseDetailId())
	    				.orElseThrow(() -> new ItemNotFoundException("AdditionalFee", "id", newadmissionItem.getCourseDetailId()));
	    		newadmissionItem.setCourseDetail(courseDetail);
	    	}
	    	studentAdmissionItemRepository.save(newadmissionItem);
	    	
	    }
		
	    StudentAdmission updatedStudentAdmission1 = studentAdmissionRepository.findById(newStudentAdmission.getId())
				.orElseThrow(() -> new ItemNotFoundException("StudentAdmission", "id", newStudentAdmission.getId()));
		
		return ResponseFormat.createFormat(updatedStudentAdmission1, "Created Successfully.");
			
	}
	
	/*
	 * DELETE Method for Delete StudentAdmission
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long enquiryListId) {
		StudentAdmission studentAdmission = studentAdmissionRepository.findById(enquiryListId)
				.orElseThrow(() -> new ItemNotFoundException("EnquiryList", "id", enquiryListId));

		studentAdmissionRepository.delete(studentAdmission);
		return ResponseFormat.createFormat(studentAdmission, "Deleted Successfully.");
	}
}
