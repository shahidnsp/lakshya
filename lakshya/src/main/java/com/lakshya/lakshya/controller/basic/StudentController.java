package com.lakshya.lakshya.controller.basic;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
//import java.net.http.HttpHeaders;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.internal.ContentType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.EnquiryList;
import com.lakshya.lakshya.model.EnquiryStage;
import com.lakshya.lakshya.model.LossReason;
import com.lakshya.lakshya.model.School;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.StudentGuardian;
import com.lakshya.lakshya.model.StudentGuardianPhone;
import com.lakshya.lakshya.model.StudentPhone;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.UserAssignedStudent;
import com.lakshya.lakshya.payload.UploadFileResponse;
import com.lakshya.lakshya.property.FileStorageProperties;
import com.lakshya.lakshya.repository.BatchRepository;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.EnquiryStageRepository;
import com.lakshya.lakshya.repository.EnquiryTaskRepository;
import com.lakshya.lakshya.repository.LossReasonRepository;
import com.lakshya.lakshya.repository.SchoolRepository;
import com.lakshya.lakshya.repository.StudentGuardianPhoneRepository;
import com.lakshya.lakshya.repository.StudentGuardianRepository;
import com.lakshya.lakshya.repository.StudentPhoneRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserAssignedStudentRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.FileStorageService;
import com.lakshya.lakshya.service.impl.UserInterface;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

//import net.lingala.zip4j.ZipFile;
//import net.lingala.zip4j.exception.ZipException;

//import org.apache.poi.ss.usermodel.Workbook;

@RestController
@RequestMapping("/api/student")
public class StudentController {

	static String uploadPath = "/home/cloudbery-01/projects/lakshya/lakshya/uploads";

	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserAssignedStudentRepository userAssignedStudentRepository;

	@Autowired
	EnquiryStageRepository enquiryStageRepository;

	@Autowired
	LossReasonRepository lossReasonRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	BatchRepository batchRepository;

	@Autowired
	SchoolRepository schoolRepository;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	StudentPhoneRepository studentPhoneRepository;

	@Autowired
	StudentGuardianRepository studentGuardianRepository;

	@Autowired
	StudentGuardianPhoneRepository studentGuardianPhoneRepository;

	@Autowired
	FileStorageProperties fileStorageProperties;

	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired(required = false)
    private JavaMailSender javaMailSender;
	
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

	/*
	 * GET Method for listing All Students ----> /api/student/all
	 * 
	 */
	@GetMapping("/all")
	public HashMap<String, Object> getAllStudents() {
//		Pageable paging = (Pageable) PageRequest.of(0, 3);
//		Page<Student> students = studentRepository.findAll(paging);
		List<Student> students = (List<Student>) studentRepository.findAll();
		return ResponseFormat.createFormat(students, "Listed Successfully.");
	}

	/*
	 * GET Method for listing Students By branchId ----> /api/student?branchId=1
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAllStudentsList(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy,
			@RequestParam(defaultValue = "0") Long branchId) {
		System.out.println(branchId);
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		//Pageable paging = (Pageable) PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
//		Page<Student> students = studentRepository.findAll(paging);
		Page<Student> students = studentRepository.findAllByBranchId(branchId, paging);

//		Page<Student> students=studentRepository.findAllByBranchIdAndStatus(branchId,"Student",paging);

		return ResponseFormat.createFormat(students, "Listed Successfully.");
	}

	/*
	 * GET Method for listing Students By Id ---->/api/student/5
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getStudentById(@PathVariable(value = "id") Long id) {
		Student newStudent = studentRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", id));
		return ResponseFormat.createFormat(newStudent, "Listed Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}

	/*
	 * POST Method for Creating Students ---->/api/student
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> createStudent(@Valid @RequestBody(required = false) Student student) {

		student.setUser(this.getUser());

		// For Enquiry Status "Joined"
		EnquiryStage enquiryStage = enquiryStageRepository.findById(2L)
				.orElseThrow(() -> new ItemNotFoundException("EnquiryStage ", "id", 2L));
		student.setEnquiryStage(enquiryStage);

		
		 Batch batch = batchRepository.findById(student.getBatchId()) .orElseThrow(()
		  -> new ItemNotFoundException("Batch ", "id", student.getBatchId()));
		 student.setBatch(batch);
		 

		Branch branch = branchRepository.findById(student.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch ", "id", student.getBranchId()));
		student.setBranch(branch);

		Course course = courseRepository.findById(student.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", student.getCourseId()));
		student.setCourse(course);

		student.setStatus("Student");

		Student newStudent = (Student) studentRepository.save(student);
		
		//Add Student User..............
		String username="";
		String password="";
		bCryptPasswordEncoder = new BCryptPasswordEncoder();
		
		if(newStudent.getEmail().equals(""))
			username=this.getAlphaNumericString(6);
		else
			username=newStudent.getEmail();
		
		if(newStudent.getDateofbirth()==null)
			password=this.getAlphaNumericString(6);
		else
			password=this.getDateOfBirthString(newStudent.getDateofbirth());
		
		User user = new User(newStudent.getName(), newStudent.getEmail(),username , bCryptPasswordEncoder.encode(password), false,"", "", "", 1L, "profile.png", "Student");
		user.setPass(password);
		user.setStudent(newStudent);
		User newUser=userRepository.save(user);
		
		UserAssignedStudent userAssignedStudent=new UserAssignedStudent();
		userAssignedStudent.setStudent(newStudent);
		userAssignedStudent.setUser(newUser);
		userAssignedStudentRepository.save(userAssignedStudent);
		
		
		
		return ResponseFormat.createFormat(newStudent, "Created Successfully.");
//		return ResponseFormat.createFormat(student, "Created Successfully.");
	}
	
	/*
	 * POST Method for Creating Students ---->/api/student
	 * 
	 */
	@GetMapping("/test")
	public HashMap<String, Object> createStudentTest() {

		Student newStudent = studentRepository.findById(1L)
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", 1L));
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newStudent.getDateofbirth());
		
		SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo("shahidnsp4u@gmail.com");

        msg.setSubject("Testing from Spring Boot");
        msg.setText("Hello World \n Spring Boot Email");

        javaMailSender.send(msg);

		
		return ResponseFormat.createFormat(calendar.get(Calendar.MONTH)+1, "Created Successfully.");
	}
	
	
	// function to generate a date of birth as string in ddmmyyyy format
    static String getDateOfBirthString(Date dateInString) 
    { 
    	Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateInString);
    	
    	int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        
        
        
        String Day="";
        String Month="";
        String Year=Integer.toString(year);
        month=month+1;
        
        if(day<10)
        	Day="0"+Integer.toString(day);
        else
        	Day=Integer.toString(day);
        
        if(month<10)
        	Month="0"+Integer.toString(month);
        else
        	Month=Integer.toString(month);
        
        
    	return Day+Month+Year;
    }
	
	// function to generate a random string of length n 
    static String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    } 

	/*
	 * PUT Method for Updating Student ---->/api/student/5
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateStudent(@PathVariable(value = "id") Long studentId,
			@Valid @RequestBody(required = false) Student student) {

		Student newStudent = studentRepository.findById(studentId)
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", studentId));

		Branch branch = branchRepository.findById(student.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch ", "id", student.getBranchId()));
		student.setBranch(branch);

		Course course = courseRepository.findById(student.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", student.getCourseId()));
		student.setCourse(course);
		
		Batch batch = batchRepository.findById(student.getBatchId()) .orElseThrow(()
				  -> new ItemNotFoundException("Batch ", "id", student.getBatchId()));
				 student.setBatch(batch);

		// student.setUser(this.getUser());

		student.setId(studentId);
		student.setCreatedAt(newStudent.getCreatedAt());

		Student updatedStudent = studentRepository.save(student);
		return ResponseFormat.createFormat(updatedStudent, "Updated Successfully");
	}

	/*
	 * DELETE Method for Delete Student ---->/api/student/5
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteStudent(@PathVariable(value = "id") Long studentId) {
		Student student = studentRepository.findById(studentId)
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", studentId));

		studentRepository.delete(student);
		return ResponseFormat.createFormat(student, "Deleted Successfully.");

	}

	/*
	 * POST Method for Creating Enquiry ---->/api/student/createenquiry
	 * 
	 */
//	@PostMapping("/createenquiry")
//	public HashMap<String, Object> createEnquiry(@Valid @RequestBody(required = false) Student student) {
//
//		student.setUser(this.getUser());
//
//		// For Enquiry Status "In Process"
//		EnquiryStage enquiryStage = enquiryStageRepository.findById(1L)
//				.orElseThrow(() -> new ItemNotFoundException("EnquiryStage ", "id", 1L));
//		student.setEnquiryStage(enquiryStage);
//
//		student.setStatus("Enquiry");
//
//		Branch branch = branchRepository.findById(student.getBranchId())
//				.orElseThrow(() -> new ItemNotFoundException("Branch ", "id", student.getBranchId()));
//		student.setBranch(branch);
//
//		Course course = courseRepository.findById(student.getCourseId())
//				.orElseThrow(() -> new ItemNotFoundException("Course", "id", student.getCourseId()));
//		student.setCourse(course);
//
//		if (student.getSchoolId() != null) {
//			School school = schoolRepository.getOne(student.getSchoolId());
//			student.setSchool(school);
//		}
//
//		Student newStudent = (Student) studentRepository.save(student);
//		return ResponseFormat.createFormat(newStudent, "Created Successfully.");
//	}

	/*
	 * GET Method for listing Enquiry List ---->/api/student/enquirylist
	 * 
	 */
	@GetMapping("/enquirylist")
	public HashMap<String, Object> getAllEnquiry(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy,
			@RequestParam(defaultValue = "0") Long branchId) {

		Pageable paging = (Pageable) PageRequest.of(0, 3, Sort.by(sortBy));

		Page<Student> students = studentRepository.findAllByBranchIdAndStatus(branchId, "Enquiry", paging);

		return ResponseFormat.createFormat(students, "Listed Successfully.");
	}

	/*
	 * GET Method for listing Enquiry By Id ---->/api/student/enquiry/5
	 *
	 */
	@GetMapping("/enquiry/{id}")
	public HashMap<String, Object> getEnquiryById(@PathVariable(value = "id") Long id) {
		Student newStudent = studentRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", id));
		return ResponseFormat.createFormat(newStudent, "Listed Successfully");
	}

	/*
	 * POST Method for Change Enquiry Status ----> /api/student/changeenquirystatus
	 * 
	 */
	@PostMapping("/changeenquirystatus")
	public HashMap<String, Object> changeEnquiryStatus(
			@RequestParam(defaultValue = "0", name = "studentId") Long studentId,
			@RequestParam(defaultValue = "0", name = "enquiryStageId") Long enquiryStageId,
			@RequestParam(defaultValue = "0", name = "closedon") Date closedon,
			@RequestParam(defaultValue = "0", name = "loseReasonId") Long loseReasonId) {

		if (studentId == 0) {
			return ResponseFormat.createFormat(null, "studentId required...");
		} else {

			Student newStudent = studentRepository.findById(studentId)
					.orElseThrow(() -> new ItemNotFoundException("Student", "id", studentId));

			if (enquiryStageId == 0) {
				return ResponseFormat.createFormat(null, "enquiryStageId required...");
			} else if (enquiryStageId == 2) {
				EnquiryStage enquiryStage = enquiryStageRepository.findById(enquiryStageId)
						.orElseThrow(() -> new ItemNotFoundException("EnquiryStage ", "id", enquiryStageId));
				newStudent.setEnquiryStage(enquiryStage);
				newStudent.setClosedOn(closedon);
				newStudent.setStatus("Student");
			} else if (enquiryStageId == 3) {
				if (loseReasonId == 0) {
					return ResponseFormat.createFormat(null, "loseReasonId required...");
				}
				EnquiryStage enquiryStage = enquiryStageRepository.findById(enquiryStageId)
						.orElseThrow(() -> new ItemNotFoundException("EnquiryStage ", "id", enquiryStageId));
				newStudent.setEnquiryStage(enquiryStage);
				newStudent.setClosedOn(closedon);

				LossReason lossReason = lossReasonRepository.findById(loseReasonId)
						.orElseThrow(() -> new ItemNotFoundException("LossReason ", "id", loseReasonId));
				newStudent.setLossReason(lossReason);

			} else {
				EnquiryStage enquiryStage = enquiryStageRepository.findById(enquiryStageId)
						.orElseThrow(() -> new ItemNotFoundException("EnquiryStage ", "id", enquiryStageId));
				newStudent.setEnquiryStage(enquiryStage);
			}
			Student student = (Student) studentRepository.save(newStudent);

			return ResponseFormat.createFormat(student, "Created Successfully.");
		}
	}

	/*
	 * POST Method for Change Transfer branch ----> /api/student/transferbranch
	 * 
	 */
	@PostMapping("/transferbranch")
	public HashMap<String, Object> transferBranch(@RequestBody Map<String, Object> object) {

		String branchIdString = object.get("branchId").toString();
		Long branchId = Long.parseLong(branchIdString);

		Branch branch = branchRepository.findById(branchId)
				.orElseThrow(() -> new ItemNotFoundException("Branch ", "id", branchId));

		ArrayList<String> students = (ArrayList<String>) object.get("students");

		Map<String, Object>[] studentlists = students.toArray(new HashMap[students.size()]);
		for (Map<String, Object> studentlist : studentlists) {
			Long studentId = Long.parseLong(studentlist.get("studentId").toString());

			Student newStudent = studentRepository.findById(studentId)
					.orElseThrow(() -> new ItemNotFoundException("Student", "id", studentId));
			newStudent.setBranch(branch);
			studentRepository.save(newStudent);
		}

		return ResponseFormat.createFormat(studentlists, "Changed Successfully.");
	}

	/*
	 * POST method for uploading single file to store into database table ---->
	 * /api/student/import
	 *
	 */
	@PostMapping("/import")
	public HashMap<String, Object> importStudents(@RequestParam("file") MultipartFile file) throws IOException {

		Student student = null;
		Sheet worksheet = null;
		Workbook workbook = null;
		String name, rollNo, photo, gender, religion, caste, adharno, dateofbirth, address, landmark, city, pincode,
				email;

		String fileExtensionName = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			workbook = new XSSFWorkbook(file.getInputStream());
			worksheet = (XSSFSheet) workbook.getSheetAt(0);
		} else if (fileExtensionName.equals(".xls")) {
			workbook = new HSSFWorkbook(file.getInputStream());
			worksheet = (HSSFSheet) workbook.getSheetAt(0);
		}

		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
			Row row = worksheet.getRow(i);
			student = new Student();

			// Here AdharNo & Pincode are Numeric fields

			try {
				rollNo = row.getCell(0).getStringCellValue();
				student.setRollno(rollNo);
			} catch (IllegalStateException ex) {
				try {
					rollNo = String.valueOf((long) row.getCell(0).getNumericCellValue());
					student.setRollno(rollNo);
				} catch (NullPointerException e) {
					student.setRollno(null);
				}
			} catch (NullPointerException e) {
				student.setRollno(null);
			}

			try {
				name = row.getCell(1).getStringCellValue();
				student.setName(name);
			} catch (NullPointerException e) {
				student.setName(null);
			}

			try {
				gender = row.getCell(2).getStringCellValue();
				student.setGender(gender);
			} catch (NullPointerException e) {
				student.setGender(null);
			}

			try {
				religion = row.getCell(3).getStringCellValue();
				student.setReligion(religion);
			} catch (NullPointerException e) {
				student.setReligion(null);
			}

			try {
				caste = row.getCell(4).getStringCellValue();
				student.setCaste(caste);
			} catch (NullPointerException e) {
				student.setCaste(null);
			}

			try {
				adharno = String.valueOf(row.getCell(5).getStringCellValue());
				student.setAdharno(adharno);
			} catch (IllegalStateException e) {
				try {
					adharno = String.valueOf((long) row.getCell(5).getNumericCellValue());
					student.setAdharno(adharno);
				} catch (NullPointerException exception) {
					student.setAdharno(null);
				}
			} catch (NullPointerException exception) {
				student.setAdharno(null);
			}

			try {
				dateofbirth = row.getCell(6).getStringCellValue();
				
				//student.setDateofbirth(Date.parse(dateofbirth));
				student.setDateofbirth(null);
			} catch (NullPointerException e) {
				student.setDateofbirth(null);
			}

			try {
				address = row.getCell(7).getStringCellValue();
				student.setAddress(address);
			} catch (NullPointerException e) {
				student.setAddress(null);
			}

			try {
				pincode = String.valueOf(row.getCell(8).getStringCellValue());
				student.setPincode(pincode);
			} catch (IllegalStateException e) {
				try {
					pincode = String.valueOf((long) row.getCell(8).getNumericCellValue());
					student.setPincode(pincode);
				} catch (NullPointerException exception) {
					student.setPincode(null);
				}
			} catch (NullPointerException exception) {
				student.setPincode(null);
			}

			try {
				email = row.getCell(9).getStringCellValue();
				student.setEmail(email);
			} catch (NullPointerException e) {
				student.setEmail(null);
			}
			student.setStatus("Student");
			studentRepository.save(student);
		}

		System.out.println("File Imported");
		Map<String, Object> output = new HashMap<>();
		output.put("filename ", file.getOriginalFilename());
		return ResponseFormat.createFormat(output, "Imported Successfully.");
	}

	/*
	 * POST method for uploading single file to store into database table
	 */
	@PostMapping("/importenquiry")
	public HashMap<String, Object> importEnquiry(@RequestParam("file") MultipartFile file,
			@RequestParam("branchId") Long branchId) throws IOException {

		Branch branch = branchRepository.findById(branchId)
				.orElseThrow(() -> new ItemNotFoundException("Branch ", "id", branchId));

		Student student = null;
		Sheet worksheet = null;
		Workbook workbook = null;
		String name, gender, studentphone, parentname, parentphone, address, landmark, pincode, dateofbirth;

		String fileExtensionName = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			workbook = new XSSFWorkbook(file.getInputStream());
			worksheet = (XSSFSheet) workbook.getSheetAt(0);
		} else if (fileExtensionName.equals(".xls")) {
			workbook = new HSSFWorkbook(file.getInputStream());
			worksheet = (HSSFSheet) workbook.getSheetAt(0);
		}

		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
			Row row = worksheet.getRow(i);
			student = new Student();

			// HerePincode are Numeric fields

			try {
				name = row.getCell(0).getStringCellValue();
				student.setName(name);
			} catch (NullPointerException e) {
				student.setName(null);
			}

			try {
				gender = row.getCell(1).getStringCellValue();
				student.setGender(gender);
			} catch (NullPointerException e) {
				student.setGender(null);
			}

			try {
				studentphone = row.getCell(2).getStringCellValue();
			} catch (IllegalStateException e) {
				try {
					studentphone = String.valueOf((long) row.getCell(2).getNumericCellValue());
				} catch (NullPointerException exception) {
					studentphone = "";
				}
			} catch (NullPointerException e) {
				studentphone = "";
			}

			try {
				parentname = row.getCell(3).getStringCellValue();
			} catch (NullPointerException e) {
				parentname = "";
			}

			try {
				parentphone = String.valueOf(row.getCell(4).getStringCellValue());
			} catch (IllegalStateException e) {
				try {
					parentphone = String.valueOf((long) row.getCell(4).getNumericCellValue());
				} catch (NullPointerException exception) {
					parentphone = "";
				}
			} catch (NullPointerException exception) {
				parentphone = "";
			}

			try {
				address = row.getCell(5).getStringCellValue();
				student.setAddress(address);
			} catch (NullPointerException e) {
				student.setAddress(null);
			}

			try {
				landmark = row.getCell(6).getStringCellValue();
				student.setLandmark(landmark);
			} catch (NullPointerException e) {
				student.setLandmark(null);
			}

			try {
				pincode = String.valueOf(row.getCell(7).getStringCellValue());
				student.setPincode(pincode);
			} catch (IllegalStateException e) {
				try {
					pincode = String.valueOf((long) row.getCell(7).getNumericCellValue());
					student.setPincode(pincode);
				} catch (NullPointerException exception) {
					student.setPincode(null);
				}
			} catch (NullPointerException exception) {
				student.setPincode(null);
			}

			try {
				dateofbirth = row.getCell(8).getStringCellValue();
				//student.setDateofbirth(dateofbirth);
				student.setDateofbirth(null);
			} catch (NullPointerException e) {
				student.setDateofbirth(null);
			}

			// For Enquiry Status "In Process"
			EnquiryStage enquiryStage = enquiryStageRepository.findById(1L)
					.orElseThrow(() -> new ItemNotFoundException("EnquiryStage ", "id", 1L));
			student.setEnquiryStage(enquiryStage);

			student.setStatus("Enquiry");
			student.setBranch(branch);
			student.setUser(getUser());
			Student createdStudent = studentRepository.save(student);

			// Create Student Phone List
			if (!studentphone.equals("")) {
				StudentPhone studentPhone = new StudentPhone();
				studentPhone.setStudent(createdStudent);
				studentPhone.setPhone(studentphone);
				studentPhoneRepository.save(studentPhone);
			}

			// Create Guardian List
			if (!parentphone.equals("")) {
				if (!parentname.equals("")) {
					StudentGuardian studentGuardian = new StudentGuardian();
					studentGuardian.setName(parentname);
					studentGuardian.setStudent(createdStudent);
					StudentGuardian createdStudentGuardian = studentGuardianRepository.save(studentGuardian);

					StudentGuardianPhone guardianPhone = new StudentGuardianPhone();
					guardianPhone.setPhone(parentphone);
					guardianPhone.setStudentGuardian(createdStudentGuardian);
					studentGuardianPhoneRepository.save(guardianPhone);
				}

			}
			
			//Add Student User..............
			bCryptPasswordEncoder = new BCryptPasswordEncoder();
			String username=this.getAlphaNumericString(6);
			String password=this.getAlphaNumericString(6);
			User user = new User(createdStudent.getName(), createdStudent.getEmail(),username , bCryptPasswordEncoder.encode(password), false,"", "", "", 1L, "profile.png", "Student");
			user.setPass(password);
			user.setStudent(createdStudent);
			userRepository.save(user);
		}

		System.out.println("File Imported");
		Map<String, Object> output = new HashMap<>();
		output.put("filename ", file.getOriginalFilename());
		return ResponseFormat.createFormat(output, "Imported Successfully.");
	}

	// method for uploading single file to store into database table
	@PostMapping("/importstudent")
	public HashMap<String, Object> importStudent(@RequestParam("file") MultipartFile file,
			@RequestParam("branchId") Long branchId) throws IOException, ParseException {

		Branch branch = branchRepository.findById(branchId)
				.orElseThrow(() -> new ItemNotFoundException("Branch ", "id", branchId));

		Student student = null;
		Sheet worksheet = null;
		Workbook workbook = null;
		String rollno, name, gender, religion, caste, category, adharno, dateofbirth, address, area, landmark, city,
				state, pincode, email, studentphone, parentname, parentphone;

		String fileExtensionName = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			workbook = new XSSFWorkbook(file.getInputStream());
			worksheet = (XSSFSheet) workbook.getSheetAt(0);
		} else if (fileExtensionName.equals(".xls")) {
			workbook = new HSSFWorkbook(file.getInputStream());
			worksheet = (HSSFSheet) workbook.getSheetAt(0);
		}

		// Error List
		Map<String, Object> errorList = new HashMap<>();

		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
			Row row = worksheet.getRow(i);
			student = new Student();

			// HerePincode are Numeric fields
			// Roll No
			try {
				rollno = row.getCell(0).getStringCellValue();
			} catch (IllegalStateException ex) {
				try {
					rollno = String.valueOf((long) row.getCell(0).getNumericCellValue());
				} catch (NullPointerException e) {
					rollno = null;
				}
			} catch (NullPointerException e) {
				rollno = null;
			}

			if (rollno != null) {
				if (!checkRollNoExist(rollno)) {
					// Name
					try {
						name = row.getCell(1).getStringCellValue();
					} catch (NullPointerException e) {
						name = null;
					}

					// Gender
					try {
						gender = row.getCell(2).getStringCellValue();
					} catch (NullPointerException e) {
						gender = null;
					}

					// Religion
					try {
						religion = row.getCell(3).getStringCellValue();
					} catch (NullPointerException e) {
						religion = null;
					}

					// Caste
					try {
						caste = row.getCell(4).getStringCellValue();
					} catch (NullPointerException e) {
						caste = null;
					}

					// Category
					try {
						category = row.getCell(5).getStringCellValue();
					} catch (NullPointerException e) {
						category = null;
					}

					// Adharno
					try {
						adharno = row.getCell(6).getStringCellValue();
					} catch (IllegalStateException ex) {
						try {
							adharno = String.valueOf((long) row.getCell(6).getNumericCellValue());
						} catch (NullPointerException e) {
							adharno = null;
						}
					} catch (NullPointerException e) {
						adharno = null;
					}

					// DOB
					try {
						dateofbirth = row.getCell(7).getStringCellValue();
					} catch (NullPointerException e) {
						dateofbirth = null;
					}

					// Address
					try {
						address = row.getCell(8).getStringCellValue();
					} catch (NullPointerException e) {
						address = null;
					}

					// Area
					try {
						area = row.getCell(9).getStringCellValue();
					} catch (NullPointerException e) {
						area = null;
					}

					// Landmark
					try {
						landmark = row.getCell(10).getStringCellValue();
					} catch (NullPointerException e) {
						landmark = null;
					}

					// City
					try {
						city = row.getCell(11).getStringCellValue();
					} catch (NullPointerException e) {
						city = null;
					}

					// State
					try {
						state = row.getCell(12).getStringCellValue();
					} catch (NullPointerException e) {
						state = null;
					}

					// Pincode
					try {
						pincode = String.valueOf(row.getCell(13).getStringCellValue());
					} catch (IllegalStateException e) {
						try {
							pincode = String.valueOf((long) row.getCell(13).getNumericCellValue());
						} catch (NullPointerException exception) {
							pincode = null;
						}
					} catch (NullPointerException exception) {
						pincode = null;
					}

					// Email
					try {
						email = row.getCell(14).getStringCellValue();
					} catch (NullPointerException e) {
						email = null;
					}

					try {
						studentphone = row.getCell(15).getStringCellValue();
					} catch (IllegalStateException e) {
						try {
							studentphone = String.valueOf((long) row.getCell(15).getNumericCellValue());
						} catch (NullPointerException exception) {
							studentphone = "";
						}
					} catch (NullPointerException e) {
						studentphone = "";
					}

					try {
						parentname = row.getCell(16).getStringCellValue();
					} catch (NullPointerException e) {
						parentname = "";
					}

					try {
						parentphone = String.valueOf(row.getCell(17).getStringCellValue());
					} catch (IllegalStateException e) {
						try {
							parentphone = String.valueOf((long) row.getCell(17).getNumericCellValue());
						} catch (NullPointerException exception) {
							parentphone = "";
						}
					} catch (NullPointerException exception) {
						parentphone = "";
					}

					student.setRollno(rollno);
					student.setName(name);
					student.setGender(gender);
					student.setReligion(religion);
					student.setCaste(caste);
					student.setCategory(category);
					student.setAdharno(adharno);
					
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

					Date date = formatter.parse(dateofbirth);
					
					student.setDateofbirth(date);
					student.setAddress(address);
					student.setArea(area);
					student.setLandmark(landmark);
					student.setCity(city);
					student.setState(state);
					student.setPincode(pincode);
					student.setEmail(email);

					// For Enquiry Status "Joined"
					EnquiryStage enquiryStage = enquiryStageRepository.findById(2L)
							.orElseThrow(() -> new ItemNotFoundException("EnquiryStage ", "id", 1L));
					student.setEnquiryStage(enquiryStage);

					student.setStatus("Student");
					student.setBranch(branch);
					student.setUser(getUser());
					Student createdStudent = studentRepository.save(student);

					// Create Student Phone List
					if (!studentphone.equals("")) {
						StudentPhone studentPhone = new StudentPhone();
						studentPhone.setStudent(createdStudent);
						studentPhone.setPhone(studentphone);
						studentPhoneRepository.save(studentPhone);
					}

					// Create Guardian List
					if (!parentphone.equals("")) {
						if (!parentname.equals("")) {
							StudentGuardian studentGuardian = new StudentGuardian();
							studentGuardian.setName(parentname);
							studentGuardian.setStudent(createdStudent);
							StudentGuardian createdStudentGuardian = studentGuardianRepository.save(studentGuardian);

							StudentGuardianPhone guardianPhone = new StudentGuardianPhone();
							guardianPhone.setPhone(parentphone);
							guardianPhone.setStudentGuardian(createdStudentGuardian);
							studentGuardianPhoneRepository.save(guardianPhone);
						}

					}
				} else {
					errorList.put("error", "Rollno Already Exist in ROW#" + (i + 1));
				}
			} else {
				errorList.put("error", "Rollno empty in ROW#" + (i + 1));
			}

		}

		System.out.println("File Imported");
		Map<String, Object> output = new HashMap<>();
		output.put("filename ", file.getOriginalFilename());
		output.put("errors", errorList);
		return ResponseFormat.createFormat(output, "Imported Successfully.");
	}

	// Check Roll Number exist in database..........
	public Boolean checkRollNoExist(String rollno) {

		Long count = studentRepository.countByRollno(rollno);
		if (count > 0)
			return true;
		else
			return false;
	}

	/*
	 * POST method for uploading ZIPped images file to store into database table
	 * ----> /api/student/uploadBulkImage
	 *
	 */
	@PostMapping("/uploadBulkImage")
	public HashMap<String, Object> add(@RequestParam("file") MultipartFile file) throws IOException {
		List<FileHeader> fileHeaderList = null;
		/**
		 * save file to temp
		 */
		File zip = File.createTempFile(UUID.randomUUID().toString(), "temp");
		FileOutputStream o = new FileOutputStream(zip);
		IOUtils.copy(file.getInputStream(), o);
		o.close();

		/**
		 * unizp file from temp by zip4j
		 */

		Path destination = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
		try {
			ZipFile zipFile = new ZipFile(zip);
			zipFile.extractAll(destination.toString() + "/students");
			fileHeaderList = zipFile.getFileHeaders();

			for (FileHeader fileHeader : fileHeaderList) {

				String fileName = fileHeader.getFileName();
				// Create thumb image
				String thumbDest = destination.toString() + "/students/thumb/";
				BufferedImage img = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
				img.createGraphics().drawImage(ImageIO.read(new File(destination.toString() + "/students/" + fileName))
						.getScaledInstance(200, 200, Image.SCALE_SMOOTH), 0, 0, null);
				ImageIO.write(img, "jpg", new File(thumbDest + fileName));

				String rollNo = fileName.substring(0, fileName.toString().lastIndexOf("."));
				try {

					Student student = studentRepository.getStudentByRollno(rollNo);

					String fileurl = ServletUriComponentsBuilder.fromCurrentContextPath().path("api/student/img/")
							.path(fileName).toUriString();

//					Delete current file
					String curPhoto = student.getPhoto();
					FileUtils.forceDelete(FileUtils.getFile(destination.toString() + "/students/" + curPhoto));
					FileUtils.forceDelete(FileUtils.getFile(thumbDest + curPhoto));

					student.setPhoto(fileurl);
					studentRepository.save(student);
				} catch (Exception e) {
					System.out.println("No Student with id : " + rollNo);
				}
			}

		} catch (ZipException e) {
			e.printStackTrace();
		} finally {
			/**
			 * delete temp file
			 */
			zip.delete();
		}
//	    Map<String, Object> output = new HashMap<>();
//		output.put("success", 10);
//		output.put("failed", 10);

		return ResponseFormat.createFormat(fileHeaderList.size() + " Files has been uploaded", "Upload Success.");

	}

	/*
	 * GET Method for listing Student with necessary details---->
	 * /api/student/details/5
	 * 
	 */
	@SuppressWarnings("unchecked")
	@GetMapping("/details/{id}")
	public HashMap<String, Object> getStudentDetails(@PathVariable(value = "id") Long studentId) {

		try {
			Student student = studentRepository.getOne(studentId);

			HashMap<String, Object> studentObject = new HashMap<String, Object>();
			studentObject.put("id", String.valueOf(student.getId()));
			studentObject.put("name", student.getName());
			studentObject.put("rollno", student.getRollno());
			studentObject.put("gender", student.getGender());
			studentObject.put("religion", student.getReligion());
			studentObject.put("caste", student.getCaste());
			studentObject.put("category", student.getCategory());
			studentObject.put("dateofbirth", student.getDateofbirth());
			studentObject.put("address", student.getAddress());
			studentObject.put("pincode", student.getPincode());
			studentObject.put("email", student.getEmail());

			Branch branch = student.getBranch();
			// Setting Branch details
			HashMap<String, Object> branchObject = new HashMap<String, Object>();
			branchObject.put("name", branch.getName());
			branchObject.put("code", branch.getCode());
			branchObject.put("address", branch.getAddress());
			branchObject.put("branchPhones", branch.getBranchPhones());

			Course course = student.getCourse();
			HashMap<String, Object> courseObject = new HashMap<String, Object>();
			courseObject.put("name", course.getName());
			courseObject.put("code", course.getCode());

			HashMap<String, Object> studentObjects = new HashMap<String, Object>();
			studentObjects.put("Student", studentObject);
			studentObjects.put("Branch", branchObject);
			studentObjects.put("Course", courseObject);

			Set<StudentGuardian> studentGuardianSet = student.getStudentGuardians();

//			HashMap<String, Object> studentGuardianMainList = new HashMap<String, Object>();
			Set<StudentGuardian> newGuardianObjectList = new HashSet<StudentGuardian>();

			for (StudentGuardian studentGuardian : studentGuardianSet) {
				newGuardianObjectList.add(studentGuardian);
//				studentGuardianMainList.put("guardian", newGuardianObjectList);
			}

			studentObjects.put("Guardian", newGuardianObjectList);
			return (ResponseFormat.createFormat(studentObjects, "Fetched Successfully."));
		} catch (Exception e) {
			throw new ItemNotFoundException("Student", "id", studentId);			
		}
	}

	/**
	 * Get student Full Image
	 * 
	 * @param fileName
	 * @param request
	 * @return
	 */
	@GetMapping("/img/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
		// Load file as Resource
		Resource resource = fileStorageService.loadFileAsResource("students/" + fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	/**
	 * Get Student thumb image
	 * 
	 * @param fileName
	 * @param request
	 * @return
	 */

	@GetMapping("/img/thumb/{fileName:.+}")
	public ResponseEntity<Resource> getThumb(@PathVariable String fileName, HttpServletRequest request) {
		// Load file as Resource
		Resource resource = fileStorageService.loadFileAsResource("students/thumb/" + fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	@PostMapping("/updateProfile/{id}")
	public HashMap<String, Object> uploadFile(@RequestParam("file") MultipartFile file,
			@PathVariable(value = "id") Long id) {

		String path = fileStorageProperties.getUploadDir() + "/students";

		String fileName = fileStorageService.store(file, path);

		String fileurl = ServletUriComponentsBuilder.fromCurrentContextPath().path("api/student/img/thumb/")
				.path(fileName).toUriString();

		Student student = studentRepository.findById(id)
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", id));

		String curPhoto = student.getPhoto();

		student.setPhoto(fileName);
		studentRepository.save(student);

		try {
			FileUtils.forceDelete(FileUtils.getFile(path + "/" + curPhoto));
			FileUtils.forceDelete(FileUtils.getFile(path + "/thumb/" + curPhoto));
		} catch (Exception e) {
			// TODO: handle exception
		}

		return ResponseFormat.createFormat(student, "profile image updated");

	}
	
	/*
	 * GET Method for listing Student Username & Password
	 * 
	 */
	@GetMapping("/userlists")
	public HashMap<String, Object> getStudentUserDetails(@RequestParam Long studentId) {

		List<UserInterface> users = userRepository.getStudentUserList(studentId);
		

		return ResponseFormat.createFormat(users, "Listed Successfully.");
	}
	
	

}
