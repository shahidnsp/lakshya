package com.lakshya.lakshya.controller.basic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;

import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.AcademicYearRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;
import com.lakshya.lakshya.service.impl.ComboInterface;

@RestController
@RequestMapping("/api/academicyear")
public class AcademicYearController {

	@Autowired
	AcademicYearRepository academicYearRepository;

	@Autowired
	private UserRepository userRepository;

	/*
	 * GET Method for listing Academic Year
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllAcademicYear() {
		List<AcademicYear> academicYears = (List<AcademicYear>) academicYearRepository.findAll();
		return ResponseFormat.createFormat(academicYears, "Listed Successfully");
	}

	/*
	 * GET Method for listing Academic Year By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getAcademicYearById(@PathVariable(value = "id") Long AcademicYearId) {
		AcademicYear newAcademicYear = academicYearRepository.findById(AcademicYearId)
				.orElseThrow(() -> new ItemNotFoundException("Academic Year", "id", AcademicYearId));

		return ResponseFormat.createFormat(newAcademicYear, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Academic Year By Id
	 *
	 */
	@GetMapping("/academicyearforselectbox")
	public HashMap<String, Object> getAcademicYearForCombo() {
		List<ComboInterface> newAcademicYear = academicYearRepository.getAcademicYearForCombo();

		return ResponseFormat.createFormat(newAcademicYear, "Listed Successfully");
	}

	/*
	 * Method for Creating academic year
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) AcademicYear academicYear) {

		academicYear.setUser(getUser());

		AcademicYear newAcademicYear = (AcademicYear) academicYearRepository.save(academicYear);
		return ResponseFormat.createFormat(newAcademicYear, "Created Successfully.");

	}

	/*
	 * Method for Updating ACademic Year
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long AcademicYearId,
			@Valid @RequestBody(required = false) AcademicYear academicYear) {
		AcademicYear newAcademicYear = academicYearRepository.findById(AcademicYearId)
				.orElseThrow(() -> new ItemNotFoundException("Academic Year", "id", AcademicYearId));

		academicYear.setUser(this.getUser());

		academicYear.setId(AcademicYearId);
		academicYear.setCreatedAt(newAcademicYear.getCreatedAt());

//		newAcademicYear.setName(academicYear.getName());
//		newAcademicYear.setRemark(academicYear.getRemark());

		AcademicYear updatedAcademicYear = academicYearRepository.save(academicYear);
		return ResponseFormat.createFormat(updatedAcademicYear, "Updated Successfully");
	}

	/* DELETE Method for Delete Academic Year */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long academicYearId) {
		AcademicYear academicYear = academicYearRepository.findById(academicYearId)
				.orElseThrow(() -> new ItemNotFoundException("AcademicYear", "id", academicYearId));

		academicYearRepository.delete(academicYear);
		return ResponseFormat.createFormat(academicYear, "Deleted Successfully");
	}

	/*
	 * PUT Method for Updating Academic Year Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeSubjectStatus(@PathVariable(value = "id") Long academicYearId) {

		AcademicYear academicYear = academicYearRepository.findById(academicYearId)
				.orElseThrow(() -> new ItemNotFoundException("AcademicYear", "id", academicYearId));

		if (academicYear.getActive() == true)
			academicYear.setActive(false);
		else
			academicYear.setActive(true);

		AcademicYear updatedacademicYear = academicYearRepository.save(academicYear);

		return ResponseFormat.createFormat(updatedacademicYear, "Status Updated Successfully.");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}

	/*
	 * GET Method for Search in Academic year table
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends AcademicYear> resultList = new ArrayList<AcademicYear>();
		try {

			// Class type (required) for query execution
			Class<?> classType = AcademicYear.class;
			// Fields need to be searched
			String[] fields = { "name", "remark" };

			resultList = (List<? extends AcademicYear>) searchservice.search(text, classType, resultList, fields);
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Academic Year Successfully. using " + text);
	}

}
