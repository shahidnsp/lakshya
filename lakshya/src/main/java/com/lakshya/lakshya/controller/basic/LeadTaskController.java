package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.EnquiryTask;
import com.lakshya.lakshya.model.Lead;
import com.lakshya.lakshya.model.LeadTask;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.EnquiryTaskRepository;
import com.lakshya.lakshya.repository.LeadRepository;
import com.lakshya.lakshya.repository.LeadTaskRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/leadtask")
public class LeadTaskController {
	@Autowired
	LeadTaskRepository leadTaskRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	LeadRepository leadRepository;
	
	/*
	 * GET Method for listing Lead Task 
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllTask() {
		List<LeadTask> leadTask = (List<LeadTask>) leadTaskRepository.findAll();
		return ResponseFormat.createFormat(leadTask, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Lead Task
	 *
	 */
	@GetMapping("/duetasklist")
	public HashMap<String, Object> getAllDueTaskByUser(@RequestParam Long leadId) {
		List<LeadTask> leadTask = (List<LeadTask>) leadTaskRepository.findAllByLeadIdAndIsnoted(leadId,false);
		return ResponseFormat.createFormat(leadTask, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Enquiry Task
	 *
	 */
	@GetMapping("/followuplists")
	public HashMap<String, Object> getAllFollowUpByUser() {
		User user=this.getUser();
		List<LeadTask> leadTask = (List<LeadTask>) leadTaskRepository.findAllByUserIdAndIsnoted(user.getId(), false);
		return ResponseFormat.createFormat(leadTask, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Enquiry Task
	 *
	 */
	@PutMapping("changeleadtaskstatus/{id}")
	public HashMap<String, Object> changeEnquryTaskStatus(@PathVariable(value = "id") Long leadTaskId) {
		LeadTask newLeadTask = leadTaskRepository.findById(leadTaskId)
				.orElseThrow(() -> new ItemNotFoundException("Lead Task", "id", leadTaskId));
		
		if (newLeadTask.getIsnoted() == true)
			newLeadTask.setIsnoted(false);
		else
			newLeadTask.setIsnoted(true);

		LeadTask updatedLeadTask = leadTaskRepository.save(newLeadTask);
		
		return ResponseFormat.createFormat(updatedLeadTask, "Listed Successfully");
	}

	/*
	 * GET Method for listing Enquiry Task Records By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getStudentDocumentById(@PathVariable(value = "id") Long leadTaskId) {
		LeadTask leadTask = leadTaskRepository.findById(leadTaskId)
				.orElseThrow(() -> new ItemNotFoundException("Lead Task", "id", leadTaskId));
		return ResponseFormat.createFormat(leadTask, "Listed Successfully");
	}

	/*
	 * Method for Creating Enquiry Task
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) LeadTask leadTask) {
		
		Lead newLead = leadRepository.findById(leadTask.getLeadId())
				.orElseThrow(() -> new ItemNotFoundException("Lead with Id", "id", leadTask.getLeadId()));
		leadTask.setLead(newLead);
		
		User user = userRepository.findById(leadTask.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id", leadTask.getUserId()));
		leadTask.setUser(user);
		
		LeadTask newLeadTask = (LeadTask) leadTaskRepository.save(leadTask);
		return ResponseFormat.createFormat(newLeadTask, "Created Succefully.");
	}

	/*
	 * Method for Updating Enquiry Task
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long leadTaskId,
			@Valid @RequestBody(required = false) LeadTask leadTask) {
		
		LeadTask newLeadTask = leadTaskRepository.findById(leadTaskId)
				.orElseThrow(() -> new ItemNotFoundException("Lead Task", "id", leadTaskId));
		
		Lead newLead = leadRepository.findById(leadTask.getLeadId())
				.orElseThrow(() -> new ItemNotFoundException("Lead with Id", "id", leadTask.getLeadId()));
		leadTask.setLead(newLead);
		
		User user = userRepository.findById(leadTask.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id", leadTask.getUserId()));
		leadTask.setUser(user);
		
		leadTask.setId(leadTaskId);
		leadTask.setCreatedAt(newLeadTask.getCreatedAt());

		LeadTask updatedLeadTask = leadTaskRepository.save(leadTask);
		return ResponseFormat.createFormat(updatedLeadTask, "Updated Succefully");
	}

	/* DELETE Method for Delete Enquiry Task */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long leadTaskId) {
		LeadTask leadTask = leadTaskRepository.findById(leadTaskId)
				.orElseThrow(() -> new ItemNotFoundException("Lead Task", "id", leadTaskId));

		leadTaskRepository.delete(leadTask);
		return ResponseFormat.createFormat(leadTask, "Deleted Successfully");
	}
	
	

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}
}
