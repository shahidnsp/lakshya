package com.lakshya.lakshya.controller.onlineexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.onlineexam.OnlineExam;
import com.lakshya.lakshya.model.onlineexam.OnlineExamBatch;
import com.lakshya.lakshya.onlinerepository.OnlineExamBatchRepository ;
import com.lakshya.lakshya.onlinerepository.OnlineExamRepository;
import com.lakshya.lakshya.repository.BatchRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/onlineexambatch")
public class OnlineExamBatchController {
	@Autowired
	OnlineExamBatchRepository onlineExamBatchRepository;

	@Autowired
	BatchRepository batchRepository;

	@Autowired
	OnlineExamRepository onlineExamRepository;
	/*
	 * GET Method for listing OnlineExamBatch
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OnlineExamBatch> OnlineExam = (List<OnlineExamBatch>) onlineExamBatchRepository.findAll();
		return ResponseFormat.createFormat(OnlineExam, "Listed Successfully");
	}
	
	/*
	 * GET Method for getting OnlineExamBatch By BatchId
	 *
	 */
	@SuppressWarnings("unchecked")
	@GetMapping("/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long batchId) {
		List<OnlineExamBatch> onlineExamBatch = null;
		try{
//			onlineExamBatch = onlineExamBatchRepository.findOnlineExamBatchById(batchId);
			onlineExamBatch = (List<OnlineExamBatch>) onlineExamBatchRepository.findOnlineExamBatchByBatchId(batchId);
//			onlineExamBatch = (List<OnlineExamBatch>) onlineExamBatchRepository.findOnlineExamBatchByOnlineExamId(batchId);
		}catch(Exception e) {
			new ItemNotFoundException("OnlineExamBatch", "id", batchId);
		}
//				.orElseThrow(() -> new ItemNotFoundException("OnlineExamBatch", "id", onlineExamBatchId));
		return ResponseFormat.createFormat(onlineExamBatch, "Fetched Successfully");
	}

	/*
	 * Method for Creating OnlineExamBatch
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) OnlineExamBatch onlineExamBatch) {
		
//		Batch batch = batchRepository.findById(onlineExamBatch.getBatchId())
//				.orElseThrow(() -> new ItemNotFoundException("Batch", "id",onlineExamBatch.getBatchId()));
//		onlineExamBatch.setBatch(batch);
		
//		OnlineExam onlineExam = onlineExamRepository.findById(onlineExamBatch.getOnlineExamId())
//				.orElseThrow(() -> new ItemNotFoundException("OnlineExam", "id",onlineExamBatch.getOnlineExamId() ));
//		onlineExamBatch.setOnlineExam(onlineExam);
		
		OnlineExamBatch newOnlineExamBatch = (OnlineExamBatch) onlineExamBatchRepository.save(onlineExamBatch);
		return ResponseFormat.createFormat(newOnlineExamBatch, "Created Successfully.");
//		return ResponseFormat.createFormat(onlineExamBatch, "Created Successfully.");
	}

	/*
	 * Method for Updating OnlineExamBatch
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update
			(@PathVariable(value = "id") Long onlineExamBatchId,@Valid @RequestBody(required = false) OnlineExamBatch onlineExamBatch) {
		
		OnlineExamBatch newOnlineExamBatch = onlineExamBatchRepository.findById(onlineExamBatchId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineExamBatch", "id", onlineExamBatchId));
		
		Batch batch = batchRepository.findById(onlineExamBatch.getBatchId())
				.orElseThrow(() -> new ItemNotFoundException("Batch", "id",onlineExamBatch.getBatchId()));
		onlineExamBatch.setBatch(batch);
		
//		OnlineExam onlineExam = onlineExamRepository.findById(onlineExamBatch.getOnlineExamId())
//				.orElseThrow(() -> new ItemNotFoundException("OnlineExam", "id",onlineExamBatch.getOnlineExamId() ));
//		onlineExamBatch.setOnlineExam(onlineExam);
		
		onlineExamBatch.setId(onlineExamBatchId);
		onlineExamBatch.setCreatedAt(newOnlineExamBatch.getCreatedAt());

		OnlineExamBatch updatedOnlineExamBatch = onlineExamBatchRepository.save(onlineExamBatch);
		return ResponseFormat.createFormat(updatedOnlineExamBatch, "Updated Successfully");
	}

	/* DELETE Method for Delete OnlineExamBatch */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long onlineExamBatchId) {
		OnlineExamBatch newOnlineExamBatch = onlineExamBatchRepository.findById(onlineExamBatchId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineExamBatch", "id", onlineExamBatchId));

		onlineExamBatchRepository.delete(newOnlineExamBatch);
		return ResponseFormat.createFormat(newOnlineExamBatch, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OnlineExamBatch
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OnlineExamBatch> resultList = new ArrayList<OnlineExamBatch>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OnlineExamBatch.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"onlineExam.name","onlineExam.examType","onlineExam.examModel","batch.name"};
//			batchId onlineExam 
			resultList = (List<? extends OnlineExamBatch>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OnlineExamBatch Successfully. using " + text);
	}
}
