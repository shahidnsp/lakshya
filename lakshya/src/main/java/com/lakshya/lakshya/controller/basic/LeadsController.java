package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Chapter;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.EnquirySource;
import com.lakshya.lakshya.model.EnquiryStage;
import com.lakshya.lakshya.model.Lead;
import com.lakshya.lakshya.model.LeadStatus;
import com.lakshya.lakshya.model.School;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.EnquirySourceRepository;
import com.lakshya.lakshya.repository.EnquiryStageRepository;
import com.lakshya.lakshya.repository.LeadRepository;
import com.lakshya.lakshya.repository.LeadStatusRepository;
import com.lakshya.lakshya.repository.SchoolRepository;
import com.lakshya.lakshya.repository.UserRepository;


@RestController
@RequestMapping("/api/leads")
public class LeadsController {
	
	@Autowired
	LeadRepository leadRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	SchoolRepository schoolRepository;
	
	@Autowired
	LeadStatusRepository leadStatusRepository;
	
	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	
	@Autowired
	BranchRepository branchRepository;
	
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	EnquiryStageRepository enquiryStageRepository;
	
	/*
	 * GET Method for listing Leads 
	 *
	 */	
	@GetMapping
	public HashMap<String, Object> getAll(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<Lead> leads = leadRepository.findAll(paging);

		return ResponseFormat.createFormat(leads, "Listed Successfully.");
	}
	
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) Lead lead) {
		
		if(lead.getSchoolId()!=null) {		
			School school = schoolRepository.findById(lead.getSchoolId())
				.orElseThrow(() -> new ItemNotFoundException("School ", "id", lead.getSchoolId()));
			lead.setSchool(school);
		}
		
		if(lead.getEnquirySourceId()!=null) {	
			EnquirySource enquirySource = enquirySourceRepository.findById(lead.getEnquirySourceId())
					.orElseThrow(()->new ItemNotFoundException("EnquirySource", "id", lead.getEnquirySourceId()));
			lead.setEnquirySource(enquirySource);
		}
		
		
		EnquiryStage enquiryStage = enquiryStageRepository.findById(1L)
				.orElseThrow(() -> new ItemNotFoundException("EnquiryStage ", "id", 1L));
		lead.setEnquiryStage(enquiryStage);
		lead.setType("Lead");
		
		if(lead.getBranchId()!=null) {	
			Branch branch = branchRepository.findById(lead.getBranchId())
					.orElseThrow(() -> new ItemNotFoundException("Branch", "id", lead.getBranchId()));
			lead.setBranch(branch);
		}
		
		if(lead.getCourseId()!=null) {	
			Course course= courseRepository.findById(lead.getCourseId())
					.orElseThrow(() -> new ItemNotFoundException("Course", "id", lead.getCourseId()));
			lead.setCourse(course);
		}
		
		lead.setUser(this.getUser());
		
		Lead newlLead = (Lead) leadRepository.save(lead);
		return ResponseFormat.createFormat(newlLead, "Created Succefully.");
		
	}
	
	/*
	 * GET Method for fetch Leads By ID
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getLeadById(@PathVariable(value = "id") Long leadId) {
		
//		Lead lead = leadRepository.findById(leadId).orElseThrow(() -> new ItemNotFoundException("Lead", "id", leadId));
		Lead lead = null;
		try{
			lead = leadRepository.findOneById(leadId);
		}catch (Exception e) {
			new ItemNotFoundException("Lead", "id", leadId);
		}
//		System.out.println(lead);
		return ResponseFormat.createFormat(lead, "Listed Successfully");
	}
	
	
	/*
	 * PUT Method for Updating Leads
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long leadId,
			@Valid @RequestBody(required = false) Lead lead) {
		
		Lead newLead  = leadRepository.findById(leadId)
				.orElseThrow(()->new ItemNotFoundException("Lead", "id", leadId));
		
		School school = schoolRepository.findById(lead.getSchoolId())
				.orElseThrow(()->new ItemNotFoundException("School", "id", lead.getSchoolId()));
		lead.setSchool(school);
		
		EnquirySource enquirySource = enquirySourceRepository.findById(lead.getEnquirySourceId())
				.orElseThrow(()->new ItemNotFoundException("EnquirySource", "id", lead.getEnquirySourceId()));
		lead.setEnquirySource(enquirySource);
		
		Branch branch = branchRepository.findById(lead.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", lead.getBranchId()));
		lead.setBranch(branch);
		
		Course course= courseRepository.findById(lead.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", lead.getCourseId()));
		lead.setCourse(course);
		
		lead.setUser(this.getUser());
		lead.setUserId(lead.getUser().getId());
		lead.setId(leadId);
		lead.setCreatedAt(newLead.getCreatedAt());
		
		Lead updatedLead = leadRepository.save(lead);
		return ResponseFormat.createFormat(updatedLead, "Updated Successfully.");
	}
	
	/*
	 * DELETE Method for Delete Leads
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteBranch(@PathVariable(value = "id") Long leadId) {
		Lead leads = leadRepository.findById(leadId)
				.orElseThrow(() -> new ItemNotFoundException("Leads", "id", leadId));

		leadRepository.delete(leads);
		return ResponseFormat.createFormat(leads, "Deleted Successfully.");
	}
	
	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}

}
