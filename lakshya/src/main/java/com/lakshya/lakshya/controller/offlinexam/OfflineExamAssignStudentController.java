package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.model.offlineexam.OfflineExamAssignStudent;
import com.lakshya.lakshya.model.offlineexam.OfflineExamGrade;
import com.lakshya.lakshya.model.offlineexam.OfflineExamModel;
import com.lakshya.lakshya.offlinerepository.OfflineExamAssignStudentRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexamassignstudent")
public class OfflineExamAssignStudentController {
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	OfflineExamRepository offlineExamRepository;
	
	@Autowired
	OfflineExamAssignStudentRepository offlineExamAssignStudentRepository; 
	/*
	 * GET Method for listing OfflineExamAssignStudent
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExamAssignStudent> offlineExamList = (List<OfflineExamAssignStudent>) offlineExamAssignStudentRepository.findAll();
		return ResponseFormat.createFormat(offlineExamList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamAssignStudent
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExamAssignStudent offlineExamAssignStudent) {

		Student student =  studentRepository.findById(offlineExamAssignStudent.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student ", "id",offlineExamAssignStudent.getStudentId()));
		offlineExamAssignStudent.setStudent(student);
		
		OfflineExam offlineExamGrade = offlineExamRepository.findById(offlineExamAssignStudent.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam ", "id",offlineExamAssignStudent.getOfflineExamId()));				
		offlineExamAssignStudent.setOfflineExam(offlineExamGrade);		
		
		OfflineExamAssignStudent newOfflineExamAssignStudent = (OfflineExamAssignStudent) offlineExamAssignStudentRepository.save(offlineExamAssignStudent);
		return ResponseFormat.createFormat(newOfflineExamAssignStudent,"Created Successfully.");
	}

	/*
	 * Method for Updating OfflineExamAssignStudent
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamAssignStudentId,
			@Valid @RequestBody(required = false) OfflineExamAssignStudent offlineExamAssignStudent) {
		OfflineExamAssignStudent newOfflineExamAssignStudent = offlineExamAssignStudentRepository.findById(offlineExamAssignStudentId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamAssignStudent", "id", offlineExamAssignStudentId));
		
		OfflineExam offlineExamGrade = offlineExamRepository.findById(offlineExamAssignStudent.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam ", "id",offlineExamAssignStudent.getOfflineExamId()));				
		offlineExamAssignStudent.setOfflineExam(offlineExamGrade);		
		
		Student student =  studentRepository.findById(offlineExamAssignStudent.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student ", "id",offlineExamAssignStudent.getStudentId()));
		offlineExamAssignStudent.setStudent(student);
		
		offlineExamAssignStudent.setId(offlineExamAssignStudentId);
		offlineExamAssignStudent.setCreatedAt(newOfflineExamAssignStudent.getCreatedAt());				
		
		OfflineExamAssignStudent updateOfflineExamAssignStudent= offlineExamAssignStudentRepository.save(offlineExamAssignStudent);
		return ResponseFormat.createFormat(updateOfflineExamAssignStudent, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineExamAssignStudent */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamAssignStudentId) {
		OfflineExamAssignStudent newOfflineExamAssignStudent = offlineExamAssignStudentRepository.findById(offlineExamAssignStudentId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamAssignStudent", "id", offlineExamAssignStudentId));

		offlineExamAssignStudentRepository.delete(newOfflineExamAssignStudent);
		return ResponseFormat.createFormat(newOfflineExamAssignStudent, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExamAssignStudent
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineExamAssignStudent> resultList = new ArrayList<OfflineExamAssignStudent>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineExamAssignStudent.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"name","address","gender","phone1","phone2","phone3"};

			resultList = (List<? extends OfflineExamAssignStudent>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineExamAssignStudent Successfully. using " + text);
	}
}	
