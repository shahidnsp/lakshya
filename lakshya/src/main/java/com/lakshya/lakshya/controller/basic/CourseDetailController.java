package com.lakshya.lakshya.controller.basic;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.CourseDetail;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.CourseDetailRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.service.impl.ComboInterface;

@RestController
@RequestMapping("/api/coursedetail")
public class CourseDetailController {

	@Autowired
	CourseDetailRepository courseDetailRepository;
	
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	BranchRepository branchRepository;
	/*
	 * GET Method for listing Course Details
	 * 
	 */

	@GetMapping
	public HashMap<String, Object> getAll(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<CourseDetail> courseDetail = courseDetailRepository.findAll(paging);

		return ResponseFormat.createFormat(courseDetail, "Listed Successfully.");
	}
	
	/*
	 * GET Method for listing Course Details  By Id
	 *
	 */
	@GetMapping("/coursedetailforselectbox")
	public HashMap<String, Object> getCourseDetailsForCombo(@RequestParam Long branchId) {
		List<ComboInterface> courseDetails = courseDetailRepository.getCourseDetailsForCombo(branchId);
		return ResponseFormat.createFormat(courseDetails, "Listed Successfully");
	}
	
	/*
	 * GET Method list for Select Box
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getCourseDetailById(@PathVariable(value = "id") Long courseDetailId) {
		CourseDetail courseDetail = courseDetailRepository.findById(courseDetailId)
				.orElseThrow(() -> new ItemNotFoundException("CourseDetail", "id", courseDetailId));
		return ResponseFormat.createFormat(courseDetail, "Listed Successfully");
	}
	
	/*
	 * POST Method for Creating Course Details
	 * 
	 */
	@PostMapping
	public Object create(@Valid  @RequestBody(required = false) CourseDetail courseDetail) {

		Course course = courseRepository.findById(courseDetail.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseDetail.getCourseId()));
		courseDetail.setCourse(course);
		
		Branch branch = branchRepository.findById(courseDetail.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", courseDetail.getBranchId()));
		courseDetail.setBranch(branch);
		
		
		CourseDetail newcourseDetail = (CourseDetail) courseDetailRepository.save(courseDetail);
		return ResponseFormat.createFormat(newcourseDetail, "Created Successfully.");	
	}
	
	/*
	 * PUT Method for Updating courseDetail
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long courseDetailId,
			@Valid @RequestBody(required = false) CourseDetail courseDetail) {

		CourseDetail newcourseDetail = courseDetailRepository.findById(courseDetailId)
				.orElseThrow(() -> new ItemNotFoundException("CourseDetail", "id", courseDetailId));

		Course course = courseRepository.findById(courseDetail.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseDetail.getCourseId()));
		courseDetail.setCourse(course);
		
		Branch branch = branchRepository.findById(courseDetail.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", courseDetail.getBranchId()));
		courseDetail.setBranch(branch);
		
		courseDetail.setId(courseDetailId);
		courseDetail.setCreatedAt(newcourseDetail.getCreatedAt());
				
		CourseDetail updatedcourseDetail = courseDetailRepository.save(courseDetail);
		return ResponseFormat.createFormat(updatedcourseDetail, "Updated Successfully.");		
	}

	/*
	 * DELETE Method for Delete CourseDetail
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long chapterId) {
		CourseDetail courseDetail = courseDetailRepository.findById(chapterId)
				.orElseThrow(() -> new ItemNotFoundException("Chpater", "id", chapterId));

		courseDetailRepository.delete(courseDetail);
		return ResponseFormat.createFormat(courseDetail, "Deleted Successfully.");
	}
	
}
