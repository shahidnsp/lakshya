package com.lakshya.lakshya.controller.basic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.StudentDocument;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.StudentDocumentRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/studentdocument")
public class StudentDocumentController {
	@Autowired
	StudentDocumentRepository studentDocumentRepository; 

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	StudentRepository studentRepository;

	/*
	 * GET Method for listing Student Documents
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllStudentDocuments() {
		List<StudentDocument> studentDocument = (List<StudentDocument>) studentDocumentRepository.findAll();
		return ResponseFormat.createFormat(studentDocument, "Listed Successfully");
	}

	/*
	 * GET Method for listing Student Document Records By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getStudentDocumentById(@PathVariable(value = "id") Long studentDocumentId) {
		StudentDocument studentDocument = studentDocumentRepository.findById(studentDocumentId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Note", "id", studentDocumentId));
		return ResponseFormat.createFormat(studentDocument, "Listed Successfully");
	}

	/*
	 * Method for Creating Student Document
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) StudentDocument studentDocument) {
		
		Student newStudent = studentRepository.findById(studentDocument.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", studentDocument.getStudentId() ));
		studentDocument.setStudent(newStudent);
		
		studentDocument.setUser(this.getUser());
		
		StudentDocument newStudentDocument = (StudentDocument) studentDocumentRepository.save(studentDocument);
		return ResponseFormat.createFormat(newStudentDocument, "Created Successfully.");				
	}

	/*
	 * Method for Updating Student Document
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long studentDocumentId,@Valid @RequestBody(required = false) StudentDocument studentDocument) {
		
		StudentDocument newStudentDocument = studentDocumentRepository.findById(studentDocumentId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", studentDocumentId));
		
		Student newStudent = studentRepository.findById(studentDocument.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", studentDocument.getStudentId() ));
		studentDocument.setStudent(newStudent);
		
		studentDocument.setUser(this.getUser());
		
		studentDocument.setId(studentDocumentId);
		studentDocument.setCreatedAt(newStudentDocument.getCreatedAt());

		StudentDocument updatedStudentDocument = studentDocumentRepository.save(studentDocument);
		return ResponseFormat.createFormat(updatedStudentDocument, "Updated Successfully");
	}

	/* DELETE Method for Delete Student Document */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long studentDocumentId) {
		StudentDocument studentDocument = studentDocumentRepository.findById(studentDocumentId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", studentDocumentId));

		studentDocumentRepository.delete(studentDocument);
		return ResponseFormat.createFormat(studentDocument, "Deleted Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}

}
