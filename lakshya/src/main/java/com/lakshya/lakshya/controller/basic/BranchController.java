package com.lakshya.lakshya.controller.basic;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.BranchCategoryRepository;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;
import com.lakshya.lakshya.service.impl.ComboInterface;

/*
 * Controller For Branch 
 * 
 * Created By Shahid Neermunda
 * Date: 20/09/2019
 */

@RestController
@RequestMapping("/api/branch")
public class BranchController {

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	BranchCategoryRepository branchCategoryRepository;

	/*
	 * GET Method for listing Branch Categories
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAllBranch(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));		
		Page<Branch> branches = branchRepository.findAll(paging);		
		return ResponseFormat.createFormat(branches, "Listed Successfully.");

	}

	/*
	 * GET Method for listing Branch By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBranchById(@PathVariable(value = "id") Long branchId) {
		Branch newbranch = branchRepository.findById(branchId)
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", branchId));		
		return ResponseFormat.createFormat(newbranch, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Branch for Select Box
	 *
	 */
	@GetMapping("/branchforselectbox")
	public HashMap<String, Object> getBranchForCombo() {
		List<ComboInterface> branchLists = branchRepository.getBranchForCombo();
		return ResponseFormat.createFormat(branchLists, "Listed Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}

	/*
	 * POST Method for Creating Branch
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> createBranch(@Valid @RequestBody(required = false) Branch branch) {

		BranchCategory branchCategory = branchCategoryRepository.findById(branch.getBranchCategoryId())
				.orElseThrow(() -> new ItemNotFoundException("Branch Category", "id", branch.getBranchCategoryId()));// .getOne(branch.getBranchCategoryId());
		branch.setBranchCategory(branchCategory);

		branch.setUser(this.getUser());
		
		Branch newBranch = (Branch) branchRepository.save(branch);
		return ResponseFormat.createFormat(newBranch, "Created Successfully.");
	}

	/*
	 * PUT Method for Updating Branch
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateBranch(@PathVariable(value = "id") Long branchId,
			@Valid @RequestBody(required = false) Branch branch) {

		Branch newbranch = branchRepository.findById(branchId)
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", branchId));

		BranchCategory branchCategory = branchCategoryRepository.findById(branch.getBranchCategoryId())
				.orElseThrow(() -> new ItemNotFoundException("Branch Category", "id", branch.getBranchCategoryId()));// .getOne(branch.getBranchCategoryId());
		branch.setBranchCategory(branchCategory);

		//User user = userRepository.findById(branch.getUserId()).orElseThrow(() -> new ItemNotFoundException("User", "id", branch.getUserId()));
		
		branch.setUser(getUser());
		branch.setId(branchId);
		branch.setCreatedAt(newbranch.getCreatedAt());

		Branch updatedBranch = branchRepository.save(branch);
		return ResponseFormat.createFormat(updatedBranch, "Updated Successfully.");
	}

	/*
	 * DELETE Method for Delete Branch Categories
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteBranch(@PathVariable(value = "id") Long branchId) {
		Branch branch = branchRepository.findById(branchId)
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", branchId));

		branchRepository.delete(branch);
		return ResponseFormat.createFormat(branch, "Deleted Successfully.");
	}

	/*fbranchname
	 * PUT Method for Updating Branch Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeBranchStatus(@PathVariable(value = "id") Long branchId) {

		Branch branch = branchRepository.findById(branchId)
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", branchId));

		if (branch.getActive() == true)
			branch.setActive(false);
		else
			branch.setActive(true);

		Branch updatedBranch = branchRepository.save(branch);

		return ResponseFormat.createFormat(updatedBranch, "Status Updated Successfully.");
	}

	@PostMapping("/setselectedbranch")
	public Object setSelectBranch(HttpServletRequest request, @RequestBody Map<String, String> params) {

		Long branchId = Long.parseLong(params.get("branchId"));

		Branch branch = branchRepository.findById(branchId)
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", branchId));

		request.getSession().setAttribute("MY_BRANCH_ID", branch.getId());

		return ResponseFormat.createFormat(branch, "Branch Selected Successfully.");

	}

	@PostMapping("/getselectedbranch")
	public Object getSelectBranch(HttpServletRequest request) {

		// request.getSession().invalidate();

		HttpSession session = request.getSession(false);

		if (session != null) {

			Long branchId = (Long) request.getSession().getAttribute("MY_BRANCH_ID");

			Branch branch = branchRepository.findById(branchId)
					.orElseThrow(() -> new ItemNotFoundException("Branch", "id", branchId));

			return ResponseFormat.createFormat(branch, "List Selected Branch Successfully.");
		} else {
			throw new IllegalArgumentException("Branch Session not selected.");
		}

	}

	/*
	 * GET Method for Search in branch table
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends Branch> resultList = new ArrayList<Branch>();
		try {
			//Class type (required) for query execution
			Class<?> classType = Branch.class;
			//Fields need to be searched
			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
//			String[] fields = {"name","address","branchCategory.name"};
			resultList = (List<? extends Branch>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Branches Successfully. using " + text);
	}

}
