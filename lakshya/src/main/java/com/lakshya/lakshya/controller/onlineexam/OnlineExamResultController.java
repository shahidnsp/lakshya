package com.lakshya.lakshya.controller.onlineexam;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.onlineexam.OnlineExam;
import com.lakshya.lakshya.model.onlineexam.OnlineExamResult;
import com.lakshya.lakshya.model.onlineexam.OnlineExamResultList;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionPart;
import com.lakshya.lakshya.onlinerepository.OnlineExamRepository;
import com.lakshya.lakshya.onlinerepository.OnlineExamResultListRepository;
import com.lakshya.lakshya.onlinerepository.OnlineExamResultRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionPartRepository;
import com.lakshya.lakshya.repository.StudentRepository;

@RestController
@RequestMapping("/api/onlineexamresult")
public class OnlineExamResultController {
	/*
	 * Method for Creating OnlineExam
	 * 
	 */
	
	@Autowired
	OnlineExamResultRepository onlineExamResultRepository;
	
	@Autowired
	OnlineExamResultListRepository onlineExamResultListRepository;
	
	@Autowired
	OnlineQuestionPartRepository onlineQuestionPartRepository;
	
	@Autowired
	OnlineExamRepository onlineExamRepository;
	
	@Autowired
	StudentRepository studentRepository;

	@GetMapping("/onlineexamresultbystudent")
	public HashMap<String, Object> getMyOnlineExamResult(@RequestParam Long studentId) {

		List<OnlineExamResult> onlineExamResults=onlineExamResultRepository.findAllByStudent_Id(studentId);
		
		List<HashMap<String, Object>> resultLists = new ArrayList<HashMap<String, Object>>();
		for(OnlineExamResult onlineExamResult:onlineExamResults) {
			HashMap<String, Object> onlineList = new HashMap<String, Object>();
			onlineList.put("id", onlineExamResult.getId());
			
			OnlineExam newOnlineExam = onlineExamRepository.findById(onlineExamResult.getOnlineExamId())
					.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineExamResult.getOnlineExamId()));
			
			onlineList.put("onlineExamId", onlineExamResult.getOnlineExamId());
			onlineList.put("onlineExamName", newOnlineExam.getName());
			onlineList.put("noOfAnswered", onlineExamResult.getNoOfAnswered());
			onlineList.put("noOfUnAnswered", onlineExamResult.getNoOfUnAnswered());
			onlineList.put("noOfCorrect", onlineExamResult.getNoOfCorrect());
			onlineList.put("noOfInCorrect", onlineExamResult.getNoOfInCorrect());
			onlineList.put("totalCorrectMark", onlineExamResult.getTotalCorrectMark());
			onlineList.put("totalNegativeMark", onlineExamResult.getTotalNegativeMark());
			onlineList.put("totalMark", onlineExamResult.getTotalMark());
			
			resultLists.add(onlineList);
		}

		return ResponseFormat.createFormat(resultLists, "Listed Successfully.");
	}
	
	@GetMapping("/onlineexamresultbyexam")
	public HashMap<String, Object> getOnlineExamResultByExam(@RequestParam Long examId) {

		List<OnlineExamResult> onlineExamResults=onlineExamResultRepository.findAllByOnlineExamId(examId);
		
		List<HashMap<String, Object>> resultLists = new ArrayList<HashMap<String, Object>>();
		for(OnlineExamResult onlineExamResult:onlineExamResults) {
			HashMap<String, Object> onlineList = new HashMap<String, Object>();
			onlineList.put("id", onlineExamResult.getId());
			
			OnlineExam newOnlineExam = onlineExamRepository.findById(onlineExamResult.getOnlineExamId())
					.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineExamResult.getOnlineExamId()));
			
			Student student=onlineExamResult.getStudent();
			
			onlineList.put("id", onlineExamResult.getId());
			onlineList.put("onlineExamId", onlineExamResult.getOnlineExamId());
			onlineList.put("onlineExamName", newOnlineExam.getName());
			onlineList.put("studentName", student.getName());
			onlineList.put("noOfAnswered", onlineExamResult.getNoOfAnswered());
			onlineList.put("noOfUnAnswered", onlineExamResult.getNoOfUnAnswered());
			onlineList.put("noOfCorrect", onlineExamResult.getNoOfCorrect());
			onlineList.put("noOfInCorrect", onlineExamResult.getNoOfInCorrect());
			onlineList.put("totalCorrectMark", onlineExamResult.getTotalCorrectMark());
			onlineList.put("totalNegativeMark", onlineExamResult.getTotalNegativeMark());
			onlineList.put("totalMark", onlineExamResult.getTotalMark());
			
			resultLists.add(onlineList);
		}

		return ResponseFormat.createFormat(resultLists, "Listed Successfully.");
	}
	
	@GetMapping
	public HashMap<String, Object> getAllOnlineExamResult() {

		List<OnlineExamResult> onlineExamResults=(List<OnlineExamResult>) onlineExamResultRepository.findAll();
		

		return ResponseFormat.createFormat(onlineExamResults, "Listed Successfully.");
	}
	
	
	@PostMapping("/onlineexamresultbyexampdf")
   	public   ResponseEntity<InputStreamResource> getPDF(@RequestParam Long examId) throws FileNotFoundException {
    	try
        {
    		String examName="";
    		List<OnlineExamResult> onlineExamResults=onlineExamResultRepository.findAllByOnlineExamId(examId);
    		
    		List<HashMap<String, Object>> resultLists = new ArrayList<HashMap<String, Object>>();
    		for(OnlineExamResult onlineExamResult:onlineExamResults) {
    			HashMap<String, Object> onlineList = new HashMap<String, Object>();
    			onlineList.put("id", onlineExamResult.getId());
    			
    			OnlineExam newOnlineExam = onlineExamRepository.findById(onlineExamResult.getOnlineExamId())
    					.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineExamResult.getOnlineExamId()));
    			
    			Student student=onlineExamResult.getStudent();
    			
    			onlineList.put("id", onlineExamResult.getId());
    			onlineList.put("onlineExamId", onlineExamResult.getOnlineExamId());
    			onlineList.put("onlineExamName", newOnlineExam.getName());
    			onlineList.put("studentName", student.getName());
    			onlineList.put("noOfAnswered", onlineExamResult.getNoOfAnswered());
    			onlineList.put("noOfUnAnswered", onlineExamResult.getNoOfUnAnswered());
    			onlineList.put("noOfCorrect", onlineExamResult.getNoOfCorrect());
    			onlineList.put("noOfInCorrect", onlineExamResult.getNoOfInCorrect());
    			onlineList.put("totalCorrectMark", onlineExamResult.getTotalCorrectMark());
    			onlineList.put("totalNegativeMark", onlineExamResult.getTotalNegativeMark());
    			onlineList.put("totalMark", onlineExamResult.getTotalMark());
    			examName=newOnlineExam.getName();
    			resultLists.add(onlineList);
    		}
    		
            OutputStream file = new FileOutputStream(new File("HTMLtoPDF.pdf"));
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, file);
            StringBuilder htmlString = new StringBuilder();
            htmlString.append(new String("<html><head><style>#customers {border-collapse: collapse;width: 100%;font-size:11px;}#customers td, #customers th {border: 1px solid #ddd;padding: 5px;} #customers tr:nth-child(even){background-color: #f2f2f2;} #customers th {padding-top: 4px;padding-bottom: 4px;text-align: left;background-color: #4CAF50;color: white;}</style></head><body> "+examName+"<table id='customers'> "));
            
            htmlString.append(new String("<tr><th style='text-align:center;'>#</th><th>Name</th><th>Mark</th></tr>"));  
            int slno=1;
            for(HashMap<String, Object> resultList: resultLists) {
	            htmlString.append(new String("<tr><td style='text-align:center;'>"+slno+"</td><td>"+resultList.get("studentName")+"</td><td>"+resultList.get("totalMark")+"</td></tr>")); 
	            slno++;
            }
          
            htmlString.append(new String("</table></body></html>"));
                             
            document.open();
            InputStream is = new ByteArrayInputStream(htmlString.toString().getBytes());
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
            document.close();
            file.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    	
 
    	
    	MediaType mediaType = MediaType.parseMediaType("application/pdf");
 
        File file = new File("HTMLtoPDF.pdf");
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        
       
 
        return ResponseEntity.ok()
                // Content-Disposition
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                // Content-Type
                .contentType(mediaType)
                // Contet-Length
                .contentLength(file.length()) //
                .body(resource);
    	

   	}
	
	@PostMapping("/onlineexamresultanalysispdf") 
   	public  ResponseEntity<InputStreamResource> getOnlineExamResultAnalysisPDF(@RequestParam("resultId") Long resultId,@RequestParam("examId") Long examId) throws FileNotFoundException {
    	try
        {
    		String examName="";
    	
    		OnlineExamResult onlineExamResult = onlineExamResultRepository.findById(resultId)
					.orElseThrow(() -> new ItemNotFoundException("OnlineExamResult", "id", resultId));
    		
    		OnlineExam newOnlineExam = onlineExamRepository.findById(examId)
					.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", examId));
    		
    		Student student=onlineExamResult.getStudent();
    		
    		HashMap<String, Object> resultLists = new HashMap<String, Object>();
    		resultLists.put("onlineExamId", onlineExamResult.getOnlineExamId());
    		resultLists.put("onlineExamName", newOnlineExam.getName());
    		resultLists.put("studentName", student.getName());
    		resultLists.put("studentRollno", student.getRollno());
    		resultLists.put("noOfAnswered", onlineExamResult.getNoOfAnswered());
    		resultLists.put("noOfUnAnswered", onlineExamResult.getNoOfUnAnswered());
    		resultLists.put("noOfCorrect", onlineExamResult.getNoOfCorrect());
    		resultLists.put("noOfInCorrect", onlineExamResult.getNoOfInCorrect());
    		resultLists.put("totalCorrectMark", onlineExamResult.getTotalCorrectMark());
    		resultLists.put("totalNegativeMark", onlineExamResult.getTotalNegativeMark());
    		resultLists.put("totalMark", onlineExamResult.getTotalMark());
    		//resultLists.put("examMark", newOnlineExam.get);
    		
    		List<OnlineExamResultList> onlineExamResultLists=onlineExamResultListRepository.findAllByOnlineExamResultId(resultId);
    
    		List<HashMap<String, Object>> answersLists = new ArrayList<HashMap<String, Object>>();
    		for(OnlineExamResultList onlineExamResultList:onlineExamResultLists) {
    			OnlineQuestionPart onlineQuestionPart = onlineQuestionPartRepository.findById(onlineExamResultList.getOnlineQuestionPartId())
    					.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionPart", "id", onlineExamResultList.getOnlineQuestionPartId()));
    			
    			HashMap<String, Object> partList = new HashMap<String, Object>();
    			partList.put("questionpart", onlineQuestionPart.getQuestionText());
    			partList.put("questiontype", onlineQuestionPart.getQuestionType());
    			partList.put("markedanswer", onlineExamResultList.getMarkedAnswer());
    			partList.put("correctanswer", onlineExamResultList.getCorrectAnswer());
    			partList.put("iscorrect", onlineExamResultList.getIsCorrect());
    			partList.put("mark", onlineExamResultList.getMark());
    			answersLists.add(partList);
    		}
    		
    		resultLists.put("answers",answersLists);
    		
    		
            OutputStream file = new FileOutputStream(new File("HTMLtoPDF.pdf"));
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, file);
            StringBuilder htmlString = new StringBuilder();
            htmlString.append(new String("<html><head><style>#customers {border-collapse: collapse;width: 100%;font-size:11px;}#customers td, #customers th {border: 1px solid #ddd;padding: 5px;} #customers tr:nth-child(even){background-color: #f2f2f2;} #customers th {padding-top: 4px;padding-bottom: 4px;text-align: left;background-color: #4CAF50;color: white;}</style></head><body>ATTENDED QUESTION ANALYSIS"));
            
            //Header Table........................
            htmlString.append(new String("<table id='customers'> "));
            htmlString.append(new String("<tr><th>NAME</th><th>EXAM</th><th>MARK</th></tr>"));  
            htmlString.append(new String("<tr><td>"+resultLists.get("studentName")+" ("+resultLists.get("studentRollno")+")</td><td>"+resultLists.get("onlineExamName")+"</td><td>"+resultLists.get("totalMark")+"</td></tr>"));  
            htmlString.append(new String("</table>"));
            
            //Question Table........................
            htmlString.append(new String("<table id='customers'> "));
            htmlString.append(new String("<tr><th>#</th><th>QUESTION</th><th>CORRECT</th><th>MARKED</th><th>ISCORRECT</th></tr>"));  
           
            int slno=1;
            for(HashMap<String, Object> answersList:answersLists) {
            	  htmlString.append(new String("<tr><td>"+slno+"</td><td>"+answersList.get("questionpart")+"</td><td>"+answersList.get("correctanswer")+"</td><td>"+answersList.get("markedanswer")+"</td><td>"+answersList.get("iscorrect")+"</td></tr>"));  
            	  slno++;
            }
        	
			
            htmlString.append(new String("</table>"));
          
            htmlString.append(new String("</body></html>"));
                             
            document.open();
            InputStream is = new ByteArrayInputStream(htmlString.toString().getBytes());
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
            document.close();
            file.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    	
 
    	
    	MediaType mediaType = MediaType.parseMediaType("application/pdf");
 
        File file = new File("HTMLtoPDF.pdf");
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        
       
 
        return ResponseEntity.ok()
                // Content-Disposition
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                // Content-Type
                .contentType(mediaType)
                // Contet-Length
                .contentLength(file.length()) //
                .body(resource);
    	

   	}
	
	
}
