package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Chapter;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.model.offlineexam.OfflineExamChapter;
import com.lakshya.lakshya.model.offlineexam.OfflineExamGrade;
import com.lakshya.lakshya.model.offlineexam.OfflineExamModel;
import com.lakshya.lakshya.offlinerepository.OfflineExamChapterRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.repository.ChapterRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexamchapter")
public class OfflineExamChapterController {
	@Autowired
	ChapterRepository chapterRepository;
	
	@Autowired
	OfflineExamRepository offlineExamRepository;
	
	@Autowired
	OfflineExamChapterRepository offlineExamChapterRepository;  
	/*
	 * GET Method for listing OfflineExamChapter
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExamChapter> offlineExamChapterList = (List<OfflineExamChapter>) offlineExamChapterRepository.findAll();
		return ResponseFormat.createFormat(offlineExamChapterList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamChapter
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExamChapter offlineExamChapter) {

		OfflineExam newOfflineExam= offlineExamRepository.findById(offlineExamChapter.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineExamChapter.getOfflineExamId()));
		offlineExamChapter.setOfflineExam(newOfflineExam );
		
		Chapter newChapter= chapterRepository.findById(offlineExamChapter.getChapterId())
				.orElseThrow(() -> new ItemNotFoundException("Chapter", "id",offlineExamChapter.getChapterId()));
		offlineExamChapter.setChapter(newChapter);
		
		OfflineExamChapter newOfflineExamChapter = (OfflineExamChapter) offlineExamChapterRepository.save(offlineExamChapter);
		return ResponseFormat.createFormat(newOfflineExamChapter,"Created Successfully.");
	}

	/*
	 * Method for Updating OfflineExamChapter
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamChapterId,
			@Valid @RequestBody(required = false) OfflineExamChapter offlineExamChapter) {
		OfflineExamChapter newOfflineExamChapter = offlineExamChapterRepository.findById(offlineExamChapterId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamChapter", "id", offlineExamChapterId));
		
		OfflineExam newOfflineExam= offlineExamRepository.findById(offlineExamChapter.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineExamChapter.getOfflineExamId()));
		offlineExamChapter.setOfflineExam(newOfflineExam );
		
		Chapter newChapter= chapterRepository.findById(offlineExamChapter.getChapterId())
				.orElseThrow(() -> new ItemNotFoundException("Chapter", "id",offlineExamChapter.getChapterId()));
		offlineExamChapter.setChapter(newChapter);
		
		offlineExamChapter.setId(offlineExamChapterId);
		offlineExamChapter.setCreatedAt(newOfflineExamChapter.getCreatedAt());				
		
		OfflineExamChapter updateOfflineExamChapter= offlineExamChapterRepository.save(offlineExamChapter);
		return ResponseFormat.createFormat(updateOfflineExamChapter, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineExamChapter */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamChapterId) {
		OfflineExamChapter newOfflineExamChapter= offlineExamChapterRepository.findById(offlineExamChapterId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamChapter", "id", offlineExamChapterId));

		offlineExamChapterRepository.delete(newOfflineExamChapter);
		return ResponseFormat.createFormat(newOfflineExamChapter, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExamChapter
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineExamChapter> resultList = new ArrayList<OfflineExamChapter>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineExamChapter.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"name","address","gender","phone1","phone2","phone3"};

			resultList = (List<? extends OfflineExamChapter>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineExamChapter Successfully. using " + text);
	}
}
