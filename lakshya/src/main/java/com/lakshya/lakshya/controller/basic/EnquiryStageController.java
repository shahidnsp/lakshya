package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.EnquiryStage;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.EnquiryStageRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.impl.ComboInterface;


/*
 * Controller For Enquiry Source 
 * 
 * Created By Shahid Neermunda
 * Date: 24/09/2019
 */

@RestController
@RequestMapping("/api/enquirystage")
public class EnquiryStageController {

	@Autowired
	EnquiryStageRepository enquiryStageRepository;

	@Autowired
	private UserRepository userRepository;

	/*
	 * GET Method for listing Enquiry Stage
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAllEnquiryStage(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<EnquiryStage> enquiryStages = enquiryStageRepository.findAll(paging);

		return ResponseFormat.createFormat(enquiryStages, "Listed Successfully.");

	}
	
	/*
	 * GET Method for listing getEnquiryStageForCombo
	 * 
	 */
	@GetMapping("/getEnquiryStageForCombo")
	public HashMap<String, Object> getEnquiryStageForCombo() {

		List<ComboInterface> enquiryStages = enquiryStageRepository.getEnquiryStageForCombo();

		return ResponseFormat.createFormat(enquiryStages, "Listed Successfully.");
	}
	
	/*
	 * GET Method for listing Chapter  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBranchById(@PathVariable(value = "id") Long enquiryStageId) {
		EnquiryStage newEnquiryStage = enquiryStageRepository.findById(enquiryStageId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Stage", "id", enquiryStageId));
		return ResponseFormat.createFormat(newEnquiryStage, "Listed Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}

	/*
	 * POST Method for Creating Enquiry Stage
	 * 
	 */
	@PostMapping
	public Object createEnquiryStage(@Valid @RequestBody(required = false) EnquiryStage enquiryStage) {

		enquiryStage.setUser(this.getUser());
		
		EnquiryStage newEnquiryStage = (EnquiryStage) enquiryStageRepository.save(enquiryStage);
		return ResponseFormat.createFormat(newEnquiryStage, "Created Successfully.");		
	}

	/*
	 * PUT Method for Updating Enquiry Stage
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateEnquiryStage(@PathVariable(value = "id") Long enquiryStageId,
			@Valid @RequestBody(required = false) EnquiryStage enquiryStage) {

		EnquiryStage newEnquiryStage = enquiryStageRepository.findById(enquiryStageId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Stage", "id", enquiryStageId));
		
		enquiryStage.setUser(this.getUser());

		enquiryStage.setId(enquiryStageId);
		enquiryStage.setCreatedAt(newEnquiryStage.getCreatedAt());
		
		/*newEnquiryStage.setName(enquiryStage.getName());
		newEnquiryStage.setColor(enquiryStage.getColor());
		newEnquiryStage.setRemark(enquiryStage.getRemark());*/

		EnquiryStage updateEnquiryStage = enquiryStageRepository.save(enquiryStage);		
		return ResponseFormat.createFormat(updateEnquiryStage, "Updated Successfully.");
	}

	/*
	 * DELETE Method for Delete Enquiry Stage
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteEnquiryStage(@PathVariable(value = "id") Long enquiryStageId) {
		EnquiryStage enquiryStage = enquiryStageRepository.findById(enquiryStageId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Stage", "id", enquiryStageId));
		if(enquiryStage.getIsRemovable()) {
			enquiryStageRepository.delete(enquiryStage);
		}else {
			return ResponseFormat.createFormat(enquiryStage, "Read only Data..First 3 Data are read only");
		}
		return ResponseFormat.createFormat(enquiryStage, "Deleted Successfully.");

	}

	/*
	 * PUT Method for Updating Enquiry Stage Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeEnquiryStageStatus(@PathVariable(value = "id") Long enquiryStageId) {

		EnquiryStage enquiryStage = enquiryStageRepository.findById(enquiryStageId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Stage", "id", enquiryStageId));

		if (enquiryStage.getActive() == true)
			enquiryStage.setActive(false);
		else
			enquiryStage.setActive(true);

		EnquiryStage updateEnquiryStage = enquiryStageRepository.save(enquiryStage);

		return ResponseFormat.createFormat(updateEnquiryStage, "Status Updated Successfully.");
	}

}