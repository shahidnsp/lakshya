package com.lakshya.lakshya.controller.basic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.AcademicYearRepository;
import com.lakshya.lakshya.repository.BatchRepository;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;
import com.lakshya.lakshya.service.impl.ComboInterface;

@RestController
@RequestMapping("/api/batch")
public class BatchController {

	@Autowired
	BatchRepository batchRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	AcademicYearRepository academicYearRepository;

	@Autowired
	UserRepository userRepository;

	/*
	 * GET Method for listing Batch All
	 * 
	 */

	@GetMapping("/all")
	public HashMap<String, Object> getAllBranch() {
		List<Batch> batches = (List<Batch>) batchRepository.findAll();
		return ResponseFormat.createFormat(batches, "Listed Succefully");
	}
	
	@GetMapping("/batchforselect")
	public HashMap<String, Object> getBatchForCombo(@RequestParam Long courseId,@RequestParam Long branchId) {
		List<ComboInterface> batches = (List<ComboInterface>) batchRepository.getBatchForCombo(courseId, branchId);
		return ResponseFormat.createFormat(batches, "Listed Succefully");
	}
	
	/*
	 * GET Method for listing Batch
	 * 
	 */

	@GetMapping
	public HashMap<String, Object> getAll(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		
		Page<Batch> batches = batchRepository.findAll(paging);

		return ResponseFormat.createFormat(batches, "Listed Succefully");
	}

	/*
	 * GET Method for listing Batch  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBatchById(@PathVariable(value = "id") Long batchId) {
		Batch newBatch = batchRepository.findById(batchId)
				.orElseThrow(() -> new ItemNotFoundException("Batch", "id", batchId));
		return ResponseFormat.createFormat(newBatch, "Listed Successfully");
	}
	
	/*
	 * POST Method for Creating Batch
	 * 
	 */
	@PostMapping
	public Object create(@Valid @RequestBody(required = false) Batch batch) {
		try{
			
			Branch branch = branchRepository.findById(batch.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", batch.getBranchId()));
		batch.setBranch(branch);

		Course course = courseRepository.findById(batch.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", batch.getCourseId()));
		batch.setCourse(course);

		AcademicYear academicYear = academicYearRepository.findById(batch.getAcademicYearId())
				.orElseThrow(() -> new ItemNotFoundException("AcademicYear", "id", batch.getAcademicYearId()));
		batch.setAcademicYear(academicYear);
				
		// batch.setUser(getUser()); //TODO Remove Comment Line

		Batch newBatch = (Batch) batchRepository.save(batch);
		return ResponseFormat.createFormat(newBatch, "Created Successfully.");
		}catch (Exception e) {
			return ResponseFormat.createFormat("batch", "Constraint violation Batch Not Created.");
		}
	}

	/*
	 * PUT Method for Creating Batch
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long batchId,
			@Valid @RequestBody(required = false) Batch batch) {
		Batch newBatch = batchRepository.findById(batchId)
				.orElseThrow(() -> new ItemNotFoundException("Batch", "id", batchId));
		Branch branch = branchRepository.findById(batch.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id", batch.getBranchId()));
		newBatch.setBranch(branch);

		Course course = courseRepository.findById(batch.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", batch.getCourseId()));
		newBatch.setCourse(course);
		// newbranch.setUser(getUser()); //TODO Remove Comment Line

		newBatch.setName(batch.getName());
		newBatch.setCode(batch.getCode());
		newBatch.setRemark(batch.getRemark());

		Batch updateBatch = batchRepository.save(newBatch);

		return ResponseFormat.createFormat(updateBatch, "Updated Successfully.");

	}

	/*
	 * DELETE Method for Delete Branch Categories
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long batchId) {
		Batch batch = batchRepository.findById(batchId)
				.orElseThrow(() -> new ItemNotFoundException("Batch", "id", batchId));

		batchRepository.delete(batch);
		return ResponseFormat.createFormat(batch, "Deleted Successfully.");

	}

	/*
	 * PUT Method for Updating Branch Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeBatchStatus(@PathVariable(value = "id") Long batchId) {

		Batch batch = batchRepository.findById(batchId)
				.orElseThrow(() -> new ItemNotFoundException("Batch", "id", batchId));

		if (batch.getActive() == true)
			batch.setActive(false);
		else
			batch.setActive(true);

		Batch updatedBatch = batchRepository.save(batch);

		return ResponseFormat.createFormat(updatedBatch, "Status Updated Successfully.");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}
	
	/*
	 * GET Method for Search in Batch table
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends Batch> resultList = new ArrayList<Batch>();
		try {			

			//Class type (required) for query execution
			Class<?> classType = Batch.class;
			//Fields need to be searched
			String[] fields = {"name","remark","code","branch.name"};
			
			resultList = (List<? extends Batch>) searchservice.search(text,classType,resultList,fields);
//			searchservice.printClassName(classType);
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Batch Successfully. using " + text);
	}
}
