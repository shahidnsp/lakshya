package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.offlineexam.ExamGroup;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.model.offlineexam.OfflineExamKey;
import com.lakshya.lakshya.model.offlineexam.OfflineExamMainGroup;
import com.lakshya.lakshya.offlinerepository.OfflineExamKeyRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamMainGroupRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexamkey")
public class OfflineExamKeyController {
	@Autowired
	OfflineExamKeyRepository offlineExamKeyRepository;
	
	@Autowired
	OfflineExamRepository offlineExamRepository;
	
	@Autowired
	OfflineExamMainGroupRepository offlineExamMainGroupRepository;
	/*
	 * GET Method for listing OfflineExamKey
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExamKey> examGroupList = (List<OfflineExamKey>)offlineExamKeyRepository.findAll();
		return ResponseFormat.createFormat(examGroupList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamKey
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExamKey offlineExamKey) {
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineExamKey.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineExamKey.getOfflineExamId()));
		offlineExamKey.setOfflineExam(offlineExam );  	
		
		OfflineExamMainGroup offlineExamMainGroup = offlineExamMainGroupRepository.findById(offlineExamKey.getOfflineExamMainGroupId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id",offlineExamKey.getOfflineExamMainGroupId()));
		offlineExamKey.setOfflineExamMainGroup(offlineExamMainGroup);  	
		
		OfflineExamKey newOfflineExamKey = (OfflineExamKey) offlineExamKeyRepository.save(offlineExamKey);
		return ResponseFormat.createFormat(newOfflineExamKey,"Created Successfully.");
		
	}

	/*
	 * Method for Updating OfflineExamKey
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamKeyId,
			@Valid @RequestBody(required = false) OfflineExamKey offlineExamKey) {
		
		OfflineExamKey newOfflineExamKey = offlineExamKeyRepository.findById(offlineExamKeyId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamKey", "id",offlineExamKeyId));		  	
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineExamKey.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineExamKey.getOfflineExamId()));
		offlineExamKey.setOfflineExam(offlineExam );  	
		
		OfflineExamMainGroup offlineExamMainGroup = offlineExamMainGroupRepository.findById(offlineExamKey.getOfflineExamMainGroupId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id",offlineExamKey.getOfflineExamMainGroupId()));
		offlineExamKey.setOfflineExamMainGroup(offlineExamMainGroup);  	
		
		offlineExamKey.setId(offlineExamKeyId);
		offlineExamKey.setCreatedAt(newOfflineExamKey.getCreatedAt());				
		
		OfflineExamKey updatedOfflineExamKey = offlineExamKeyRepository.save(offlineExamKey);
		return ResponseFormat.createFormat(updatedOfflineExamKey, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineExamKey */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamKeyId) {
		OfflineExamKey newOfflineExamKey = offlineExamKeyRepository.findById(offlineExamKeyId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamKey", "id", offlineExamKeyId));

		offlineExamKeyRepository.delete(newOfflineExamKey);
		return ResponseFormat.createFormat(newOfflineExamKey, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExamKey
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineExamKey> resultList = new ArrayList<OfflineExamKey>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineExamKey.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"type","file_name","original_name"};

			resultList = (List<? extends OfflineExamKey>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineExamKey Successfully. using " + text);
	}
}
