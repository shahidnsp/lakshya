package com.lakshya.lakshya.controller.basic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.LossReason;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.BranchCategoryRepository;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.SubjectRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

/*
 * Controller For Subject 
 * 
 * Created By Shahid Neermunda
 * Date: 24/09/2019
 */

@RestController
@RequestMapping("/api/subject")
public class SubjectController {

	@Autowired
	SubjectRepository subjectRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	BranchRepository branchRepository;

	/*
	 * GET Method for listing Subject
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAllBranchCategory(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<Subject> subjects = subjectRepository.findAll(paging);

		return ResponseFormat.createFormat(subjects, "Listed Successfully.");

	}
	
	/*
	 * GET Method for listing all Subject
	 * 
	 */
	@GetMapping("/all")
	public HashMap<String, Object> getAllBranchCategory() {
		List<Subject> subjects = (List<Subject>) subjectRepository.findAll();

		return ResponseFormat.createFormat(subjects, "Listed Successfully.");

	}
	
	/*
	 * GET Method for listing Chapter  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBranchById(@PathVariable(value = "id") Long subjectId) {
		Subject newSubject = subjectRepository.findById(subjectId)
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id", subjectId));
		return ResponseFormat.createFormat(newSubject, "Listed Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}

	/*
	 * POST Method for Creating Subject
	 * 
	 */
	@PostMapping
	public Object createSubject(@Valid @RequestBody(required = false) Subject subject) {

		Course course = courseRepository.findById(subject.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", subject.getCourseId()));
		subject.setCourse(course);

		if (subject.getBranchId() == null) {
			subject.setSubjectFrom("Main");
		} else {
			Branch branch = branchRepository.findById(subject.getBranchId())
					.orElseThrow(() -> new ItemNotFoundException("Branch", "id", subject.getBranchId()));
			subject.setBranch(branch);
		}


		subject.setUser(this.getUser());
		
		Subject newSubject = (Subject) subjectRepository.save(subject);
		return ResponseFormat.createFormat(newSubject, "Created Successfully.");
		
	}

	/*
	 * PUT Method for Updating Branch
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateSubject(@PathVariable(value = "id") Long subjectId,
			@Valid @RequestBody(required = false) Subject subject) {

		Subject newSubject = subjectRepository.findById(subjectId)
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id", subjectId));

		Course course = courseRepository.findById(subject.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", subject.getCourseId()));
		subject.setCourse(course);

		if (subject.getBranchId() == null) {
			subject.setSubjectFrom("Main");
		} else {
			Branch branch = branchRepository.findById(subject.getBranchId())
					.orElseThrow(() -> new ItemNotFoundException("Branch", "id", subject.getBranchId()));
			subject.setBranch(branch);
		}

		subject.setUser(this.getUser());

		subject.setId(subjectId);
		subject.setCreatedAt(newSubject.getCreatedAt());
		
		/*newSubject.setName(subject.getName());
		newSubject.setCode(subject.getCode());
		newSubject.setHours(subject.getHours());
		newSubject.setRemark(subject.getRemark());*/

		Subject updatedSubject = subjectRepository.save(subject);
		return ResponseFormat.createFormat(updatedSubject, "Updated Successfully.");
	}

	/*
	 * DELETE Method for Delete Branch Categories
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteSubject(@PathVariable(value = "id") Long subjectId) {
		Subject subject = subjectRepository.findById(subjectId)
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id", subjectId));

		subjectRepository.delete(subject);
		return ResponseFormat.createFormat(subject, "Deleted Successfully.");

	}

	/*
	 * PUT Method for Updating Subject Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeSubjectStatus(@PathVariable(value = "id") Long subjectId) {

		Subject subject = subjectRepository.findById(subjectId)
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id", subjectId));

		if (subject.getActive() == true)
			subject.setActive(false);
		else
			subject.setActive(true);

		Subject updatedSubject = subjectRepository.save(subject);

		return ResponseFormat.createFormat(updatedSubject, "Status Updated Successfully.");
	}

	/*
	 * GET Method for Search in Subject table
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends Subject> resultList = new ArrayList<Subject>();
		try {
			//Class type (required) for query execution
			Class<?> classType = Subject.class;
			//Fields need to be searched
			String[] fields = {"name","remark"};
			
			resultList = (List<? extends Subject>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Subjects Successfully. using " + text);
	}
}
