package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineAnswerSheetOMR;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.offlinerepository.OfflineAnswerSheetOMRRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.SubjectRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineanswersheetomr")
public class OfflineAnswerSheetOMRController {
	
	@Autowired
	OfflineAnswerSheetOMRRepository offlineAnswerSheetOMRRepository; 
	
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	SubjectRepository subjectRepository;	
	
	@Autowired
	OfflineExamRepository offlineExamRepository;
		
	@Autowired
	UserRepository userRepository;
	
	/*
	 * GET Method for listing OfflineAnswerSheetOMR
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineAnswerSheetOMR> examGroupList = (List<OfflineAnswerSheetOMR>)offlineAnswerSheetOMRRepository.findAll();
		return ResponseFormat.createFormat(examGroupList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineAnswerSheetOMR
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineAnswerSheetOMR offlineAnswerSheetOMR) {
		
		Student student= studentRepository.findById(offlineAnswerSheetOMR.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student", "id",offlineAnswerSheetOMR.getStudentId()));
		offlineAnswerSheetOMR.setStudent(student); 
		
		Subject subject= subjectRepository.findById(offlineAnswerSheetOMR.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id",offlineAnswerSheetOMR.getSubjectId()));
		offlineAnswerSheetOMR.setSubject(subject); 
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineAnswerSheetOMR.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineAnswerSheetOMR.getOfflineExamId()));
		offlineAnswerSheetOMR.setOfflineExam(offlineExam );  	
		
		User user= userRepository.findById(offlineAnswerSheetOMR.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineAnswerSheetOMR.getUserId()));
		offlineAnswerSheetOMR.setUser(user); 		

		OfflineAnswerSheetOMR newOfflineAnswerSheetOMR = (OfflineAnswerSheetOMR) offlineAnswerSheetOMRRepository.save(offlineAnswerSheetOMR);
		return ResponseFormat.createFormat(newOfflineAnswerSheetOMR,"Created Successfully.");
	}

	/*
	 * Method for Updating OfflineAnswerSheetOMR
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineAnswerSheetOMRId,
			@Valid @RequestBody(required = false) OfflineAnswerSheetOMR offlineAnswerSheetOMR) {
		
		OfflineAnswerSheetOMR newOfflineAnswerSheetOMR = offlineAnswerSheetOMRRepository.findById(offlineAnswerSheetOMRId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineAnswerSheetOMR", "id",offlineAnswerSheetOMRId));
		
		Student student= studentRepository.findById(offlineAnswerSheetOMR.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student", "id",offlineAnswerSheetOMR.getStudentId()));
		offlineAnswerSheetOMR.setStudent(student); 
		
		Subject subject= subjectRepository.findById(offlineAnswerSheetOMR.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id",offlineAnswerSheetOMR.getSubjectId()));
		offlineAnswerSheetOMR.setSubject(subject); 
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineAnswerSheetOMR.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineAnswerSheetOMR.getOfflineExamId()));
		offlineAnswerSheetOMR.setOfflineExam(offlineExam);  	
		
		User user= userRepository.findById(offlineAnswerSheetOMR.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineAnswerSheetOMR.getUserId()));
		offlineAnswerSheetOMR.setUser(user); 	
		
		offlineAnswerSheetOMR.setId(offlineAnswerSheetOMRId);
		offlineAnswerSheetOMR.setCreatedAt(newOfflineAnswerSheetOMR.getCreatedAt());				
		
		OfflineAnswerSheetOMR updatedOfflineAnswerSheetOMR = offlineAnswerSheetOMRRepository.save(offlineAnswerSheetOMR);
		return ResponseFormat.createFormat(updatedOfflineAnswerSheetOMR, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineAnswerSheetOMR */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineAnswerSheetOMRId) {
		OfflineAnswerSheetOMR newOfflineAnswerSheetOMR = offlineAnswerSheetOMRRepository.findById(offlineAnswerSheetOMRId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineAnswerSheetOMR", "id", offlineAnswerSheetOMRId));

		offlineAnswerSheetOMRRepository.delete(newOfflineAnswerSheetOMR);
		return ResponseFormat.createFormat(newOfflineAnswerSheetOMR, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineAnswerSheetOMR
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineAnswerSheetOMR> resultList = new ArrayList<OfflineAnswerSheetOMR>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineAnswerSheetOMR.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"vercode","sub_total_string"};

			resultList = (List<? extends OfflineAnswerSheetOMR>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineAnswerSheetOMR Successfully. using " + text);
	}
	
	
}
