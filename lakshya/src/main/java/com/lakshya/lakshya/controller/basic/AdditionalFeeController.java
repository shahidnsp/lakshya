package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.AdditionalFee;
import com.lakshya.lakshya.model.BasicSetting;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.StudentAdmission;
import com.lakshya.lakshya.repository.AdditionalFeeRepository;

@RestController
@RequestMapping("/api/additionalfee")
public class AdditionalFeeController {
	@Autowired
	AdditionalFeeRepository additionalFeeRepository;

	/*
	 * GET Method for get all Additional fees
	 * 
	 */

	@GetMapping
	public HashMap<String, Object> getAll(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<AdditionalFee> additionalfees = additionalFeeRepository.findAll(paging);

		return ResponseFormat.createFormat(additionalfees, "Listed Succefully");
	}
	
	/*
	 * GET Method for listing StudentAdmission  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getStudentAdmissionById(@PathVariable(value = "id") Long additinalFeeId) {
		AdditionalFee additionalFee = additionalFeeRepository.findById(additinalFeeId)
				.orElseThrow(() -> new ItemNotFoundException("AdditionalFee", "id", additinalFeeId));
		return ResponseFormat.createFormat(additionalFee, "Listed Successfully");
	}

	/*
	 * POST Method for Creating Additional fees
	 * 
	 */
	@PostMapping
	public Object create(@Valid @RequestBody(required = false) AdditionalFee additionalFee) {

		AdditionalFee newAdditionalFee = (AdditionalFee) additionalFeeRepository.save(additionalFee);
		return ResponseFormat.createFormat(newAdditionalFee, "Created Successfully.");

	}

	/*
	 * PUT Method for updating Additoinal fee
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long additinalFeeId,
			@Valid @RequestBody(required = false) AdditionalFee additionalFee) {

		AdditionalFee newAdditionalFee = additionalFeeRepository.findById(additinalFeeId)
				.orElseThrow(() -> new ItemNotFoundException("Additional fess", "id", additinalFeeId));

		newAdditionalFee.setName(additionalFee.getName());
		newAdditionalFee.setAmount(additionalFee.getAmount());
		newAdditionalFee.setRemark(additionalFee.getRemark());

		AdditionalFee updateAdditionalFee = additionalFeeRepository.save(newAdditionalFee);

		return ResponseFormat.createFormat(updateAdditionalFee, "Updated Successfully.");

	}

	/*
	 * DELETE Method for Delete Addiotnal Fees
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long additinalFeeId) {
		AdditionalFee additionalFee = additionalFeeRepository.findById(additinalFeeId)
				.orElseThrow(() -> new ItemNotFoundException("Additional Fee", "id", additinalFeeId));

		additionalFeeRepository.delete(additionalFee);
		return ResponseFormat.createFormat(null, "Deleted Successfully.");

	}

}
