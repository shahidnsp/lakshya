package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineExamMainGroup;
import com.lakshya.lakshya.offlinerepository.OfflineExamMainGroupRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexammaingroup")
public class OfflineExamMainGroupController {
	@Autowired
	OfflineExamMainGroupRepository offlineExamMainGroupRepository;
	
	@Autowired
	UserRepository userRepository;

	/*
	 * GET Method for listing OfflineExamMainGroup
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExamMainGroup> offlineExamMainGroupList = (List<OfflineExamMainGroup>) offlineExamMainGroupRepository.findAll();
		return ResponseFormat.createFormat(offlineExamMainGroupList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamMainGroup
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExamMainGroup offlineExamMainGroup) {

		User newUser = userRepository.findById(offlineExamMainGroup.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamMainGroup.getUserId()));
		offlineExamMainGroup.setUser(newUser);
		
		OfflineExamMainGroup newOfflineExamMainGroup = (OfflineExamMainGroup) offlineExamMainGroupRepository.save(offlineExamMainGroup);
		return ResponseFormat.createFormat(newOfflineExamMainGroup,"Created Successfully.");		
	}

	/*
	 * Method for Updating OfflineExamMainGroup
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamMainGroupId,
			@Valid @RequestBody(required = false) OfflineExamMainGroup offlineExamMainGroup) {
		OfflineExamMainGroup newOfflineExamMainGroup = offlineExamMainGroupRepository.findById(offlineExamMainGroupId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id", offlineExamMainGroupId));
		
		User newUser = userRepository.findById(offlineExamMainGroup.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamMainGroup.getUserId()));
		offlineExamMainGroup.setUser(newUser);
		
		offlineExamMainGroup.setId(offlineExamMainGroupId);
		offlineExamMainGroup.setCreatedAt(newOfflineExamMainGroup.getCreatedAt());				
		
		OfflineExamMainGroup updatedOfflineExamMainGroup= offlineExamMainGroupRepository.save(offlineExamMainGroup);
		return ResponseFormat.createFormat(updatedOfflineExamMainGroup, "Updated Successfully");
	}

	/* DELETE Method for Delete OfflineExamMainGroup */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamMainGroupId) {
		OfflineExamMainGroup newOfflineExamMainGroup = offlineExamMainGroupRepository.findById(offlineExamMainGroupId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id", offlineExamMainGroupId));

		offlineExamMainGroupRepository.delete(newOfflineExamMainGroup);
		return ResponseFormat.createFormat(newOfflineExamMainGroup, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExamMainGroup
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineExamMainGroup> resultList = new ArrayList<OfflineExamMainGroup>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineExamMainGroup.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"name"};

			resultList = (List<? extends OfflineExamMainGroup>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Exam Groups Successfully. using " + text);
	}
}
