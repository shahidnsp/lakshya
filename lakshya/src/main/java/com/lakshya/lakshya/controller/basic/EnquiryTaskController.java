package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Chapter;
import com.lakshya.lakshya.model.EnquiryTask;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.EnquiryTaskRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/enquirytask")
public class EnquiryTaskController {
	
	@Autowired
	EnquiryTaskRepository enquiryTaskRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	StudentRepository studentRepository;
	
	/*
	 * GET Method for listing Enquiry Task 
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllTask() {
		List<EnquiryTask> enquiryTask = (List<EnquiryTask>) enquiryTaskRepository.findAll();
		return ResponseFormat.createFormat(enquiryTask, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Enquiry Task
	 *
	 */
	@GetMapping("/duetasklist")
	public HashMap<String, Object> getAllDueTaskByUser(@RequestParam Long studentId) {
		User user=getUser();
		List<EnquiryTask> enquiryTask = (List<EnquiryTask>) enquiryTaskRepository.findAllByStudentIdAndUserIdAndIsnoted(studentId, user.getId(), false);
		return ResponseFormat.createFormat(enquiryTask, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Enquiry Task
	 *
	 */
	@GetMapping("/followuplists")
	public HashMap<String, Object> getAllFollowUpByUser() {
		User user=this.getUser();
		List<EnquiryTask> enquiryTask = (List<EnquiryTask>) enquiryTaskRepository.findAllByUserIdAndIsnoted(user.getId(), false);
		return ResponseFormat.createFormat(enquiryTask, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Enquiry Task
	 *
	 */
	@PutMapping("changeenqurytaskstatus/{id}")
	public HashMap<String, Object> changeEnquryTaskStatus(@PathVariable(value = "id") Long enquiryTaskId) {
		EnquiryTask newEnquiryTask = enquiryTaskRepository.findById(enquiryTaskId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", enquiryTaskId));
		
		if (newEnquiryTask.getIsnoted() == true)
			newEnquiryTask.setIsnoted(false);
		else
			newEnquiryTask.setIsnoted(true);

		EnquiryTask updatedEnquiryTask = enquiryTaskRepository.save(newEnquiryTask);
		
		return ResponseFormat.createFormat(updatedEnquiryTask, "Listed Successfully");
	}

	/*
	 * GET Method for listing Enquiry Task Records By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getStudentDocumentById(@PathVariable(value = "id") Long enquiryTaskId) {
		EnquiryTask enquiryTask = enquiryTaskRepository.findById(enquiryTaskId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Note", "id", enquiryTaskId));
		return ResponseFormat.createFormat(enquiryTask, "Listed Successfully");
	}

	/*
	 * Method for Creating Enquiry Task
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) EnquiryTask enquiryTask) {
		
		Student newStudent = studentRepository.findById(enquiryTask.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", enquiryTask.getStudentId()));
		enquiryTask.setStudent(newStudent);
		
		User user = userRepository.findById(enquiryTask.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id", enquiryTask.getUserId()));
		enquiryTask.setUser(user);
		
		EnquiryTask newEnquiryTask = (EnquiryTask) enquiryTaskRepository.save(enquiryTask);
		return ResponseFormat.createFormat(newEnquiryTask, "Created Succefully.");
	}

	/*
	 * Method for Updating Enquiry Task
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long enquiryTaskId,
			@Valid @RequestBody(required = false) EnquiryTask enquiryTask) {
		
		EnquiryTask newEnquiryTask = enquiryTaskRepository.findById(enquiryTaskId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", enquiryTaskId));
		
		Student newStudent = studentRepository.findById(enquiryTask.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", enquiryTask.getStudentId()));
		enquiryTask.setStudent(newStudent);
		
		enquiryTask.setUser(this.getUser());
		
		enquiryTask.setId(enquiryTaskId);
		enquiryTask.setCreatedAt(newEnquiryTask.getCreatedAt());

		EnquiryTask updatedEnquiryTask = enquiryTaskRepository.save(enquiryTask);
		return ResponseFormat.createFormat(updatedEnquiryTask, "Updated Succefully");
	}

	/* DELETE Method for Delete Enquiry Task */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long enquiryTaskId) {
		EnquiryTask enquiryTask = enquiryTaskRepository.findById(enquiryTaskId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", enquiryTaskId));

		enquiryTaskRepository.delete(enquiryTask);
		return ResponseFormat.createFormat(enquiryTask, "Deleted Successfully");
	}
	
	

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}
}
