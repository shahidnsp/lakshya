package com.lakshya.lakshya.controller.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.repository.BranchCategoryRepository;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.service.HibernateSearchService;
import com.lakshya.lakshya.service.impl.ComboInterface;
import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;

/*
 * Controller For Branch Category
 * 
 * Created By Shahid Neermunda
 * Date: 17/09/2019
 */

@RestController
@RequestMapping("/api/branchcategory")
public class BranchCategoryController {

	@Autowired
	BranchCategoryRepository branchCategoryRepository;

	/*
	 * GET Method for listing Branch Categories
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAllBranchCategory() {

		List<BranchCategory> branches = (List<BranchCategory>) branchCategoryRepository.findAll();

		return ResponseFormat.createFormat(branches, "Listed Successfully.");
	}

	/*
	 * GET Method for listing Batch Categories By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBatchById(@PathVariable(value = "id") Long categoryId) {
		BranchCategory category = branchCategoryRepository.findById(categoryId)
				.orElseThrow(() -> new ItemNotFoundException("BranchCategory", "id", categoryId));
		return ResponseFormat.createFormat(category, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Batch Categories By Id
	 *
	 */
	@GetMapping("/branchcategoryforselectbox")
	public HashMap<String, Object> getBranchCategoryForCombo() {
		List<ComboInterface> category = branchCategoryRepository.getBranchCategoryForCombo();
		return ResponseFormat.createFormat(category, "Listed Successfully");
	}

	/*
	 * POST Method for Creating Branch Categories
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> createBranchCategory(
			@Valid @RequestBody(required = false) BranchCategory branchCategory) {

		BranchCategory newbranchCategory = (BranchCategory) branchCategoryRepository.save(branchCategory);
		return ResponseFormat.createFormat(newbranchCategory, "Created Successfully.");

	}

	/*
	 * PUT Method for Updating Branch Categories
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateBranchCategory(@PathVariable(value = "id") Long categoryId,
			@Valid @RequestBody(required = false) BranchCategory branchCategory) {

		BranchCategory category = branchCategoryRepository.findById(categoryId)
				.orElseThrow(() -> new ItemNotFoundException("Category", "id", categoryId));

		category.setName(branchCategory.getName());
		category.setRemark(branchCategory.getRemark());

		BranchCategory updatedCategory = branchCategoryRepository.save(category);

		return ResponseFormat.createFormat(updatedCategory, "Updated Successfully.");
	}

	/*
	 * DELETE Method for Delete Branch Categories
	 * 
	 */
	@Autowired
	BranchRepository branchRepository;

	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteBranchCategory(@PathVariable(value = "id") Long categoryId) {
		BranchCategory category = branchCategoryRepository.findById(categoryId)
				.orElseThrow(() -> new ItemNotFoundException("Category", "id", categoryId));
		
		/*List<Branch> branchlist = new ArrayList<Branch>();
		branchlist = branchRepository.getBranchByBranchCategoryId(categoryId);
		for (Branch branch : branchlist) {
			System.out.println("Name  : "+ branch.getName());
			branchRepository.delete(branch);
		}*/

		branchCategoryRepository.delete(category);
		return ResponseFormat.createFormat(category, "Deleted Successfully.");
	}

	/*
	 * PUT Method for Updating Branch Categories Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeBranchCategoryStatus(@PathVariable(value = "id") Long categoryId) {

		BranchCategory category = branchCategoryRepository.findById(categoryId)
				.orElseThrow(() -> new ItemNotFoundException("Category", "id", categoryId));

		if (category.getActive() == true)
			category.setActive(false);
		else
			category.setActive(true);

		BranchCategory updatedCategory = branchCategoryRepository.save(category);

		return ResponseFormat.createFormat(updatedCategory, "Status Updated Successfully.");
	}
	
	
	/*
	 * GET Method for Search in branch category table
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends BranchCategory> resultList = new ArrayList<BranchCategory>();
		try {			

			//Class type (required) for query execution
			Class<?> classType = BranchCategory.class;
			//Fields need to be searched
			String[] fields = {"name","remark"};
			resultList = (List<? extends BranchCategory>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Branch Category Successfully. using " + text);
	}
	
	

}
