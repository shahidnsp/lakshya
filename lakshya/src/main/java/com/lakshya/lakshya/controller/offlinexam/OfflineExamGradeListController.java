package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.model.offlineexam.OfflineExamGrade;
import com.lakshya.lakshya.model.offlineexam.OfflineExamGradeList;
import com.lakshya.lakshya.model.offlineexam.OfflineExamModel;
import com.lakshya.lakshya.offlinerepository.OfflineExamGradeListRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamGradeRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexamgradelist")
public class OfflineExamGradeListController {
	
	@Autowired
	OfflineExamGradeListRepository offlineExamGradeListRepository;
	
	@Autowired
	OfflineExamGradeRepository offlineExamGradeRepository;
	/*
	 * GET Method for listing OfflineExamGradeList
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExamGradeList> offlineExamGradeList = (List<OfflineExamGradeList>) offlineExamGradeListRepository.findAll();
		return ResponseFormat.createFormat(offlineExamGradeList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamGradeList
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExamGradeList offlineExamGradeList) {
		
		
		OfflineExamGrade offlineExamGrade = offlineExamGradeRepository.findById(offlineExamGradeList.getOfflineExamGradeId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamGrade ", "id",offlineExamGradeList.getOfflineExamGradeId()));				
		offlineExamGradeList.setOfflineExamGrade(offlineExamGrade);		
		
		OfflineExamGradeList newOfflineExamGradeList = (OfflineExamGradeList) offlineExamGradeListRepository.save(offlineExamGradeList);
		return ResponseFormat.createFormat(newOfflineExamGradeList,"Created Successfully.");
	}

	/*
	 * Method for Updating OfflineExamGradeList
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamGradeListId,
			@Valid @RequestBody(required = false) OfflineExamGradeList offlineExamGradeList) {
		OfflineExamGradeList newOfflineExamGradeList = offlineExamGradeListRepository.findById(offlineExamGradeList.getOfflineExamGradeId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamGradeList", "id", offlineExamGradeListId));
		
		OfflineExamGrade offlineExamGrade = offlineExamGradeRepository.findById(offlineExamGradeList.getOfflineExamGradeId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamGrade ", "id",offlineExamGradeList.getOfflineExamGradeId()));				
		offlineExamGradeList.setOfflineExamGrade(offlineExamGrade);		
				
		offlineExamGradeList.setId(offlineExamGradeListId);
		offlineExamGradeList.setCreatedAt(newOfflineExamGradeList.getCreatedAt());				
		
		OfflineExamGradeList updateOfflineExamGradeList = offlineExamGradeListRepository.save(offlineExamGradeList);
		return ResponseFormat.createFormat(updateOfflineExamGradeList, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineExamGradeList */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamGradeListId) {
		OfflineExamGradeList newOfflineExamGradeList= offlineExamGradeListRepository.findById(offlineExamGradeListId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamGradeList", "id", offlineExamGradeListId));

		offlineExamGradeListRepository.delete(newOfflineExamGradeList);
		return ResponseFormat.createFormat(newOfflineExamGradeList, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExamGradeList
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineExamGradeList> resultList = new ArrayList<OfflineExamGradeList>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineExamGradeList.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"code","to_ending","from_starting"};

			resultList = (List<? extends OfflineExamGradeList>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineExamGradeList Successfully. using " + text);
	}
}
