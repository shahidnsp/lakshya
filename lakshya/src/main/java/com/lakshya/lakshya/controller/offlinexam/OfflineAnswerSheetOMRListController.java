package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineAnswerSheetOMR;
import com.lakshya.lakshya.model.offlineexam.OfflineAnswerSheetOMRList;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.offlinerepository.OfflineAnswerSheetOMRListRepository;
import com.lakshya.lakshya.offlinerepository.OfflineAnswerSheetOMRRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.SubjectRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineanswersheetomrlist")
public class OfflineAnswerSheetOMRListController {
	
	@Autowired
	OfflineAnswerSheetOMRListRepository offlineAnswerSheetOMRListRepository;
	
	@Autowired
	OfflineAnswerSheetOMRRepository offlineAnswerSheetOMRRepository; 
			
	@Autowired
	SubjectRepository subjectRepository;	
			
	/*
	 * GET Method for listing OfflineAnswerSheetOMRList
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineAnswerSheetOMRList> offlineAnswerSheetOMRList = (List<OfflineAnswerSheetOMRList>)offlineAnswerSheetOMRListRepository.findAll();
		return ResponseFormat.createFormat(offlineAnswerSheetOMRList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineAnswerSheetOMRList
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineAnswerSheetOMRList offlineAnswerSheetOMRList) {
		
		OfflineAnswerSheetOMR offlineAnswerSheetOMR = offlineAnswerSheetOMRRepository.findById(offlineAnswerSheetOMRList.getOfflineAnswerSheetOMRId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineAnswerSheetOMR", "id",offlineAnswerSheetOMRList.getOfflineAnswerSheetOMRId()));
		offlineAnswerSheetOMRList.setOfflineAnswerSheetOMR(offlineAnswerSheetOMR);
		
		Subject subject= subjectRepository.findById(offlineAnswerSheetOMRList.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id",offlineAnswerSheetOMRList.getSubjectId()));
		offlineAnswerSheetOMRList.setSubject(subject); 		

		OfflineAnswerSheetOMRList newOfflineAnswerSheetOMRList = (OfflineAnswerSheetOMRList) offlineAnswerSheetOMRListRepository.save(offlineAnswerSheetOMRList);
		return ResponseFormat.createFormat(newOfflineAnswerSheetOMRList,"Created Successfully.");
	}

	/*
	 * Method for Updating OfflineAnswerSheetOMRList
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineAnswerSheetOMRListId,
			@Valid @RequestBody(required = false) OfflineAnswerSheetOMRList offlineAnswerSheetOMRList) {
		
		OfflineAnswerSheetOMRList newOfflineAnswerSheetOMRList = offlineAnswerSheetOMRListRepository.findById(offlineAnswerSheetOMRListId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineAnswerSheetOMRList", "id",offlineAnswerSheetOMRListId));
		
		OfflineAnswerSheetOMR offlineAnswerSheetOMR = offlineAnswerSheetOMRRepository.findById(offlineAnswerSheetOMRList.getOfflineAnswerSheetOMRId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineAnswerSheetOMR", "id",offlineAnswerSheetOMRList.getOfflineAnswerSheetOMRId()));
		offlineAnswerSheetOMRList.setOfflineAnswerSheetOMR(offlineAnswerSheetOMR);
		
		Subject subject= subjectRepository.findById(offlineAnswerSheetOMRList.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id",offlineAnswerSheetOMRList.getSubjectId()));
		offlineAnswerSheetOMRList.setSubject(subject); 		
		
		offlineAnswerSheetOMRList.setId(offlineAnswerSheetOMRListId);
		offlineAnswerSheetOMRList.setCreatedAt(newOfflineAnswerSheetOMRList.getCreatedAt());				
		
		OfflineAnswerSheetOMRList updatedOfflineAnswerSheetOMRList = offlineAnswerSheetOMRListRepository.save(offlineAnswerSheetOMRList);
		return ResponseFormat.createFormat(updatedOfflineAnswerSheetOMRList, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineAnswerSheetOMRList */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineAnswerSheetOMRListId) {
		OfflineAnswerSheetOMRList newOfflineAnswerSheetOMRList = offlineAnswerSheetOMRListRepository.findById(offlineAnswerSheetOMRListId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineAnswerSheetOMRList", "id", offlineAnswerSheetOMRListId));

		offlineAnswerSheetOMRListRepository.delete(newOfflineAnswerSheetOMRList);
		return ResponseFormat.createFormat(newOfflineAnswerSheetOMRList, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineAnswerSheetOMRList
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineAnswerSheetOMRList> resultList = new ArrayList<OfflineAnswerSheetOMRList>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineAnswerSheetOMRList.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"name","address","gender","phone1","phone2","phone3"};

			resultList = (List<? extends OfflineAnswerSheetOMRList>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineAnswerSheetOMRList Successfully. using " + text);
	}
}
