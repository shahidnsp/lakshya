package com.lakshya.lakshya.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import com.lakshya.lakshya.config.AuthorizationServerConfig;
import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.config.UserHelper;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.UserAssignedStudent;
import com.lakshya.lakshya.repository.AcademicYearRepository;
import com.lakshya.lakshya.repository.BatchRepository;
import com.lakshya.lakshya.repository.BranchCategoryRepository;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserAssignedStudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	BranchRepository branchRepository;
	
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	AcademicYearRepository academicYearRepository;
	
	@Autowired
	BranchCategoryRepository branchCategoryRepository;
	
	@Autowired
	BatchRepository batchRepository;
	
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	UserAssignedStudentRepository userAssignedStudentRepository;
	

	private BCryptPasswordEncoder bCryptPasswordEncoder;

	/*
	 * public UserController(UserRepository userRepository,BCryptPasswordEncoder
	 * bCryptPasswordEncoder) { this.userRepository = userRepository;
	 * this.bCryptPasswordEncoder = bCryptPasswordEncoder; }
	 */

	@GetMapping("/user")
	// @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
	public List<User> getAllCategory() {
		return (List<User>) userRepository.findAll();

	}
	
	@GetMapping("/text")
	public Object getTest() {
		return "Working";

	}
	
	
	@GetMapping("/user/userdetails")
	public HashMap<String, Object> getUserDetails() {
		
	
		User currentUser=userRepository.findById(1L).orElseThrow(() -> new ItemNotFoundException("User", "id", 1L));
	    HashMap<String, Object> map = new HashMap<>();
	    //map.put("key1", "value1");
	    map.put("profile", currentUser);
	    map.put("pages", UserHelper.pages("1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111",false));
	    map.put("permissions", UserHelper.pages("1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111,1111",true));
	    
	    map.put("branches",branchRepository.findAll());
	    
	    return ResponseFormat.createFormat(map, "Listed Successfully");
	   
	}
	
	
	@GetMapping("/user/getallottedbranches")
	public HashMap<String, Object> getMyAllotedBranches() {
		
		List<Branch>  branches = (List<Branch>) branchRepository.findAll();		
		return ResponseFormat.createFormat(branches, "Listed Successfully.");
	   
	}
	
	@GetMapping("/user/userlist")
	public HashMap<String, Object> getUserList() {
		
		List<User>  users = (List<User>) userRepository.findAllByTypeofuser("Admin");		
		return ResponseFormat.createFormat(users, "Listed Successfully.");
	   
	}
	

	@GetMapping("/createuser")
	public User createUser() {
		bCryptPasswordEncoder = new BCryptPasswordEncoder();
	User user = new User("Administrator", "admin@admin.com", "admin", bCryptPasswordEncoder.encode("admin"), true,"", "", "", 1L, "profile.png", "Admin");
//	User user = new User("first", "first@first.com", "first", bCryptPasswordEncoder.encode("first"), true,"first", "first", "first", 1L, "profile2.png", "first");
	return (User) userRepository.save(user);

	}
	
	@GetMapping("/createstudentuser")
	public Object createStudentUser() {
		
		//Add Student User..............
		bCryptPasswordEncoder = new BCryptPasswordEncoder();
		User user = new User("Student", "student@student.com", "student", bCryptPasswordEncoder.encode("student"), false,"", "", "", 1L, "profile.png", "Student");
		User newuser= (User) userRepository.save(user);
		
		//Academic Year
		AcademicYear academicyear=new AcademicYear("2019-2020","",newuser);
		AcademicYear newacademicyear=academicYearRepository.save(academicyear);
		
		//Add Branch Category
		BranchCategory branchCategory=new BranchCategory("New Branch", "No remark");
		BranchCategory newbranchCategory=branchCategoryRepository.save(branchCategory);
		
		//Add Branch
		Branch branch=new Branch("Manjeri", "M2", "Manjei", "Manjeri", "Manjeri", "Kerala", "676121", newbranchCategory);
		Branch newbranch=branchRepository.save(branch);
		
		//Add Course
		Course course=new Course("Physics", "P", "remark", newbranch);
		Course newcourse=courseRepository.save(course);
		
		//Add Batch
		Batch batch=new Batch("B2 Batch", "B2", "remark", newbranch, newcourse, newacademicyear);
		Batch newbatch=batchRepository.save(batch);
		
		
		//Add Student...................
		Student student=new Student();
		student.setName("Chandrasekhar Azad");
		student.setRollno("R110");
		student.setPhoto("profile.png");
		student.setGender("Male");
		student.setReligion("Hindu");
		student.setCaste("Ezhava");
		student.setAdharno("784545454545");
		//student.setDateofbirth("25/10/1993");
		student.setAddress("Manjeri, Malappuram");
		student.setLandmark("Manjeri");
		student.setCity("Manjeri");
		student.setPincode("676121");
		student.setEmail("abc@gmail.com");
		student.setUser(newuser);
		student.setBatch(newbatch); 
		student.setBranch(newbranch);
		Student newstudent= (Student) studentRepository.save(student);
		
		//Add User Assigned Student...............
		UserAssignedStudent userAssignedStudent=new UserAssignedStudent(newuser, newstudent);
		userAssignedStudentRepository.save(userAssignedStudent);
		
		
		return newuser;
	}
	
	@GetMapping("/test")
	public HashMap<String, Object> get() {
	    HashMap<String, Object> map = new HashMap<>();
	    map.put("key1", "value1");
	    map.put("results", userRepository.findAll());
	    return map;
	}
	
	
	/*
	 * GET Method for listing Student Academic Record By Id
	 *
	 */
	@PostMapping("studentLogin")
	public HashMap<String, Object> studentLogin(@RequestParam String username,@RequestParam String password) {
		
		User user=userRepository.findByUsername(username);
		bCryptPasswordEncoder = new BCryptPasswordEncoder();
		
		HashMap<String, Object> map = new HashMap<>();
		if(bCryptPasswordEncoder.matches(password, user.getPassword())) {
			
			UserDetails userDetails= new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority());
			
		
		    map.put("username", username);
		    map.put("password", password);
		    map.put("user", userDetails);

		    return map;
		}
		
		 map.put("error", "invalid_grant");
		 map.put("error_description", "Bad credentials");
		 return map;
	}
	
	
	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}
	
	
	
	
}
