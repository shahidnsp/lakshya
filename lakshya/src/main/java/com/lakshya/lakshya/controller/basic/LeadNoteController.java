package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.EnquiryNote;
import com.lakshya.lakshya.model.Lead;
import com.lakshya.lakshya.model.LeadNote;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.EnquiryNoteRepository;
import com.lakshya.lakshya.repository.LeadNoteRepository;
import com.lakshya.lakshya.repository.LeadRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/leadnote")
public class LeadNoteController {
	@Autowired
	LeadNoteRepository leadNoteRepository;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	LeadRepository leadRepository;

	
	/*
	 * GET Method for listing Student Academic Records
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllLeadNote() {
		List<LeadNote> leadNote = (List<LeadNote>) leadNoteRepository.findAll();
		return ResponseFormat.createFormat(leadNote, "Listed Successfully");
	}

	/*
	 * GET Method for listing Student Academic Record By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getLeadNoteById(@PathVariable(value = "id") Long leadNoteId) {
		LeadNote leadNote = leadNoteRepository.findById(leadNoteId)
				.orElseThrow(() -> new ItemNotFoundException("Lead Note", "id", leadNoteId));
		return ResponseFormat.createFormat(leadNote, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Student Academic Record By Id
	 *
	 */
	@GetMapping("/getleadnotebyleadid")
	public HashMap<String, Object> getLeadNoteByStudentId(@RequestParam Long leadId) {
		List<LeadNote> leadNotes = leadNoteRepository.findAllByLeadId(leadId);
		return ResponseFormat.createFormat(leadNotes, "Listed Successfully");
	}

	/*
	 * Method for Creating Student Academic Record
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@RequestBody(required = false) LeadNote leadNote) {
		
		Lead newLead = leadRepository.findById(leadNote.getLeadId())
				.orElseThrow(() -> new ItemNotFoundException("Lead with Id", "id", leadNote.getLeadId()));
		leadNote.setLead(newLead);
	
		leadNote.setUser(getUser());
		
		LeadNote newLeadNote = (LeadNote) leadNoteRepository.save(leadNote);
		return ResponseFormat.createFormat(newLeadNote, "Created Successfully.");
	}

	/*
	 * Method for Updating Student Academic Record
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long leadNoteId,
			@Valid @RequestBody(required = false) LeadNote leadNote) {
		
		LeadNote newLeadNote = leadNoteRepository.findById(leadNoteId)
				.orElseThrow(() -> new ItemNotFoundException("Lead Note", "id", leadNoteId));
		
		Lead newLead = leadRepository.findById(leadNote.getLeadId())
				.orElseThrow(() -> new ItemNotFoundException("Lead with Id", "id", leadNote.getLeadId()));
		leadNote.setLead(newLead);
		
		leadNote.setUser(getUser());
		
		leadNote.setId(leadNoteId);
		leadNote.setCreatedAt(newLeadNote.getCreatedAt());

		LeadNote updatedLeadNote = leadNoteRepository.save(leadNote);
		return ResponseFormat.createFormat(updatedLeadNote, "Updated Successfully");
	}

	/* DELETE Method for Delete Enquiry Collaborator */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long enquiryNoteId) {
		LeadNote newLeadNote = leadNoteRepository.findById(enquiryNoteId)
				.orElseThrow(() -> new ItemNotFoundException("Lead Note", "id", enquiryNoteId));

		leadNoteRepository.delete(newLeadNote);
		return ResponseFormat.createFormat(newLeadNote, "Deleted Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}
}
