package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineExamModel;
import com.lakshya.lakshya.offlinerepository.OfflineExamModelRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexammodel")
public class OfflineExamModelController {
	@Autowired
	OfflineExamModelRepository offlineExamModelRepository; 

	@Autowired
	UserRepository userRepository;
	
	/*
	 * GET Method for listing OfflineExamModel
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExamModel> offlineExamModelList = (List<OfflineExamModel>) offlineExamModelRepository.findAll();
		return ResponseFormat.createFormat(offlineExamModelList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamModel
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExamModel offlineExamModel) {
		
		User newUser = userRepository.findById(offlineExamModel.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamModel.getUserId()));
		offlineExamModel.setUser(newUser);
		
		OfflineExamModel newOfflineExamModel = (OfflineExamModel) offlineExamModelRepository.save(offlineExamModel);
		return ResponseFormat.createFormat(newOfflineExamModel,"Created Successfully.");

	}

	/*
	 * Method for Updating OfflineExamModel
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamModelId,
			@Valid @RequestBody(required = false) OfflineExamModel offlineExamModel) {
		OfflineExamModel newOfflineExamModel = offlineExamModelRepository.findById(offlineExamModelId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id", offlineExamModelId));
		
		User newUser = userRepository.findById(offlineExamModel.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamModel.getUserId()));
		offlineExamModel.setUser(newUser);
		
		offlineExamModel.setId(offlineExamModelId);
		offlineExamModel.setCreatedAt(newOfflineExamModel.getCreatedAt());				
		
		OfflineExamModel updatedOfflineExamModel= offlineExamModelRepository.save(offlineExamModel);
		return ResponseFormat.createFormat(updatedOfflineExamModel, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineExamModel */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamModelId) {
		OfflineExamModel newOfflineExamModel = offlineExamModelRepository.findById(offlineExamModelId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamModel", "id", offlineExamModelId));

		offlineExamModelRepository.delete(newOfflineExamModel);
		return ResponseFormat.createFormat(newOfflineExamModel, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExamModel
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineExamModel> resultList = new ArrayList<OfflineExamModel>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineExamModel.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"model_name"};

			resultList = (List<? extends OfflineExamModel>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineExamModel Successfully. using " + text);
	}
}
