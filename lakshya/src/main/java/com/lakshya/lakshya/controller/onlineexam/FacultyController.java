package com.lakshya.lakshya.controller.onlineexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.onlineexam.Faculty;
import com.lakshya.lakshya.repository.FacultiesRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/faculty")
public class FacultyController {
	@Autowired
	FacultiesRepository facultiesRepository; 

	/*
	 * GET Method for listing all Faculties
	 *
	 */
//	@GetMapping("/all")
	@GetMapping
	public HashMap<String, Object> getFaculties() {
		List<Faculty> faculties = (List<Faculty>) facultiesRepository.findAll();
		return ResponseFormat.createFormat(faculties, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Faculties
	 *
	 */
//	@GetMapping
//	public HashMap<String, Object> getFaculties(@RequestParam(defaultValue = "0") Integer pageNo,
//			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {
//		
//		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
//		Page<Faculty> faculties = facultiesRepository.findAll(paging);
//		return ResponseFormat.createFormat(faculties, "Listed Successfully");
//	}
	
	/*
	 * GET Method for listing Faculty By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long FacultiesId) {
		Faculty faculty = facultiesRepository.findById(FacultiesId)
				.orElseThrow(() -> new ItemNotFoundException("Faculty", "id", FacultiesId));
		return ResponseFormat.createFormat(faculty, "Fetched Successfully");
	}

	/*
	 * Method for Creating Faculties
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) Faculty faculties) {
		Faculty newFaculties = (Faculty) facultiesRepository.save(faculties);
		return ResponseFormat.createFormat(newFaculties,"Created Successfully.");
	}

	/*
	 * Method for Updating Faculties
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long FacultiesId,
			@Valid @RequestBody(required = false) Faculty faculties) {
		Faculty newFaculties = facultiesRepository.findById(FacultiesId)
				.orElseThrow(() -> new ItemNotFoundException("Faculties", "id", FacultiesId));
		
		faculties.setId(FacultiesId);
		faculties.setCreatedAt(newFaculties.getCreatedAt());				
		
		Faculty updatedFaculties = facultiesRepository.save(faculties);
		return ResponseFormat.createFormat(updatedFaculties, "Updated Successfully");
	}

	/* DELETE Method for Delete Faculties */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long facultiesId) {
		Faculty newFaculties = facultiesRepository.findById(facultiesId)
				.orElseThrow(() -> new ItemNotFoundException("Faculties", "id", facultiesId));

		facultiesRepository.delete(newFaculties);
		return ResponseFormat.createFormat(newFaculties, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in Faculty
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends Faculty> resultList = new ArrayList<Faculty>();
		try {
			//Class type (required) for query execution
			Class<?> classType = Faculty.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"name","address","phone1","phone2"};

			resultList = (List<? extends Faculty>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Faculties Successfully. using " + text);
	}
}
