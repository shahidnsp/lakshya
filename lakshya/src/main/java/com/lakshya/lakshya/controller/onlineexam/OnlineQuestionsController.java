package com.lakshya.lakshya.controller.onlineexam;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.controller.basic.UploadController;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.onlineexam.Faculty;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestion;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionOption;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionPart;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionTopic;
import com.lakshya.lakshya.model.onlineexam.QuestionYear;
import com.lakshya.lakshya.onlinerepository.OnlineExamQuestionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineExamRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionOptionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionPartRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionTopicRepository;
import com.lakshya.lakshya.property.FileStorageProperties;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.FacultiesRepository;
import com.lakshya.lakshya.repository.QuestionYearRepository;
import com.lakshya.lakshya.repository.SubjectRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.FileStorageService;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/onlinequestion")
public class OnlineQuestionsController {

	private static final Logger logger = LoggerFactory.getLogger(OnlineQuestionsController.class);

	@Autowired
	OnlineQuestionRepository onlineQuestionRepository;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	OnlineQuestionOptionRepository onlineQuestionOptionRepository;

	@Autowired
	SubjectRepository subjectRepository;

	@Autowired
	FacultiesRepository facultiesRepository;

	@Autowired
	QuestionYearRepository questionYearRepository;

	@Autowired
	OnlineQuestionPartRepository onlineQuestionPartRepository;

	@Autowired
	OnlineExamQuestionRepository onlineExamQuestionRepository;

	@Autowired
	OnlineExamRepository onlineExamRepository;

	@Autowired
	OnlineQuestionTopicRepository onlineQuestionTopicRepository;

	@Autowired
	private FileStorageService fileStorageService;

	/*
	 * GET Method for listing Online Question
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getQuestionYears() {
		// List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>)
		// onlineQuestionRepository.findAll();
		List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
				.findAllByOrderByCreatedAtDesc();
		return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
	}

	/*
	 * GET Method for listing Online Question
	 *
	 */
	@GetMapping("/search")
	public HashMap<String, Object> getQuestionBySearch(
			@RequestParam(defaultValue = "0", value = "courseId") Long courseId,
			@RequestParam(defaultValue = "0", value = "subjectId") Long subjectId,
			@RequestParam(defaultValue = "0", value = "facultyId") Long facultyId,
			@RequestParam(defaultValue = "0", value = "questionYearId") Long questionYearId,
			@RequestParam(defaultValue = "0", value = "questionFor") String questionFor,
			@RequestParam(defaultValue = "0", value = "deficultyLevel") String deficultyLevel,
			@RequestParam(defaultValue = "0", value = "format") String format) {

		if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByOrderByCreatedAtDesc();
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdOrderByCreatedAtDesc(courseId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId != 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndSubjectIdOrderByCreatedAtDesc(courseId, subjectId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId != 0 && facultyId != 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndSubjectIdAndFacultyIdOrderByCreatedAtDesc(courseId, subjectId, facultyId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndSubjectIdAndFacultyIdAndQuestionYearIdOrderByCreatedAtDesc(courseId, subjectId,
							facultyId, questionYearId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndSubjectIdAndFacultyIdAndQuestionYearIdAndQuestionForOrderByCreatedAtDesc(
							courseId, subjectId, facultyId, questionYearId, questionFor);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && !questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndSubjectIdAndFacultyIdAndQuestionYearIdAndQuestionForAndDifficultyLevelOrderByCreatedAtDesc(
							courseId, subjectId, facultyId, questionYearId, questionFor, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId == 0 && facultyId != 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndFacultyIdOrderByCreatedAtDesc(courseId, facultyId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId == 0 && facultyId == 0 && questionYearId != 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndQuestionYearIdOrderByCreatedAtDesc(courseId, questionYearId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndQuestionForOrderByCreatedAtDesc(courseId, questionFor);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndQuestionForOrderByCreatedAtDesc(courseId, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndFormatOrderByCreatedAtDesc(courseId, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		}

		else if (courseId == 0 && subjectId != 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdOrderByCreatedAtDesc(subjectId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdOrderByCreatedAtDesc(subjectId, facultyId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId == 0 && questionYearId != 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndQuestionYearIdOrderByCreatedAtDesc(subjectId, questionYearId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId == 0 && questionYearId == 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndQuestionForOrderByCreatedAtDesc(subjectId, questionFor);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndDifficultyLevelOrderByCreatedAtDesc(subjectId, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFormatOrderByCreatedAtDesc(subjectId, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdAndQuestionYearIdOrderByCreatedAtDesc(subjectId, facultyId,
							questionYearId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId == 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdAndQuestionForOrderByCreatedAtDesc(subjectId, facultyId,
							questionFor);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId == 0 && questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdAndDifficultyLevelOrderByCreatedAtDesc(subjectId, facultyId,
							deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdAndFormatOrderByCreatedAtDesc(subjectId, facultyId, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdAndQuestionYearIdAndQuestionForOrderByCreatedAtDesc(subjectId,
							facultyId, questionYearId, questionFor);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdAndQuestionYearIdAndDifficultyLevelOrderByCreatedAtDesc(subjectId,
							facultyId, questionYearId, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdAndQuestionYearIdAndFormatOrderByCreatedAtDesc(subjectId, facultyId,
							questionYearId, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && !questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdAndQuestionYearIdAndQuestionForAndDifficultyLevelOrderByCreatedAtDesc(
							subjectId, facultyId, questionYearId, questionFor, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllBySubjectIdAndFacultyIdAndQuestionYearIdAndQuestionForAndFormatOrderByCreatedAtDesc(
							subjectId, facultyId, questionYearId, questionFor, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId != 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdAndQuestionYearIdOrderByCreatedAtDesc(facultyId, questionYearId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId == 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdAndQuestionForOrderByCreatedAtDesc(facultyId, questionFor);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId == 0 && questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdAndDifficultyLevelOrderByCreatedAtDesc(facultyId, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdAndFormatOrderByCreatedAtDesc(facultyId, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId != 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdAndQuestionYearIdAndQuestionForOrderByCreatedAtDesc(facultyId, questionYearId,
							questionFor);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId != 0 && questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdAndQuestionYearIdAndDifficultyLevelOrderByCreatedAtDesc(facultyId,
							questionYearId, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId != 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdAndQuestionYearIdAndFormatOrderByCreatedAtDesc(facultyId, questionYearId,
							format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId != 0 && !questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdAndQuestionYearIdAndQuestionForAndDifficultyLevelOrderByCreatedAtDesc(facultyId,
							questionYearId, questionFor, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId != 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdAndQuestionYearIdAndQuestionForAndFormatOrderByCreatedAtDesc(facultyId,
							questionYearId, questionFor, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId != 0 && subjectId != 0 && facultyId != 0 && questionYearId != 0 && !questionFor.equals("0")
				&& !deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByCourseIdAndSubjectIdAndFacultyIdAndQuestionYearIdAndQuestionForAndDifficultyLevelAndFormatOrderByCreatedAtDesc(
							courseId, subjectId, facultyId, questionYearId, questionFor, deficultyLevel, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId != 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFacultyIdOrderByCreatedAtDesc(facultyId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		}

		else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId != 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionYearIdOrderByCreatedAtDesc(questionYearId);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId != 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionYearIdAndQuestionForOrderByCreatedAtDesc(questionYearId, questionFor);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId != 0 && questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionYearIdAndDifficultyLevelOrderByCreatedAtDesc(questionYearId, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId != 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionYearIdAndFormatOrderByCreatedAtDesc(questionYearId, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId != 0 && !questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionYearIdAndQuestionForAndDifficultyLevelOrderByCreatedAtDesc(questionYearId,
							questionFor, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId != 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionYearIdAndQuestionForAndFormatOrderByCreatedAtDesc(questionYearId, questionFor,
							format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId != 0 && !questionFor.equals("0")
				&& !deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionYearIdAndQuestionForAndDifficultyLevelAndFormatOrderByCreatedAtDesc(
							questionYearId, questionFor, deficultyLevel, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		}

		else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionForOrderByCreatedAtDesc(questionFor);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && !questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionForAndDifficultyLevelOrderByCreatedAtDesc(questionFor, deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && !questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionForAndFormatOrderByCreatedAtDesc(questionFor, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && !questionFor.equals("0")
				&& !deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByQuestionForAndDifficultyLevelAndFormatOrderByCreatedAtDesc(questionFor, deficultyLevel,
							format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		}

		else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& !deficultyLevel.equals("0") && format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByDifficultyLevelOrderByCreatedAtDesc(deficultyLevel);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		} else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& !deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByDifficultyLevelAndFormatOrderByCreatedAtDesc(deficultyLevel, format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		}

		else if (courseId == 0 && subjectId == 0 && facultyId == 0 && questionYearId == 0 && questionFor.equals("0")
				&& deficultyLevel.equals("0") && !format.equals("0")) {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByFormatOrderByCreatedAtDesc(format);
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		}

		else {
			List<OnlineQuestion> onlineQuestion = (List<OnlineQuestion>) onlineQuestionRepository
					.findAllByOrderByCreatedAtDesc();
			return ResponseFormat.createFormat(onlineQuestion, "Listed Successfully");
		}

		// return ResponseFormat.createFormat(null, "Listed Successfully");
	}

	/*
	 * GET Method for getting Online Question by Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getOnlineQuestionById(@PathVariable(value = "id") Long onlineQuestionId) {

		OnlineQuestion onlineQuestion = onlineQuestionRepository.findById(onlineQuestionId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineQuestionId));

		return ResponseFormat.createFormat(onlineQuestion, "Created Successfully.");
	}

	/*
	 * Method for Creating Online Question
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) OnlineQuestion onlineQuestion) {
		OnlineQuestion newOnlineQuestion;

		Course course = courseRepository.findById(onlineQuestion.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", onlineQuestion.getCourseId()));
		onlineQuestion.setCourse(course);

		Subject subject = subjectRepository.findById(onlineQuestion.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id", onlineQuestion.getSubjectId()));
		onlineQuestion.setSubject(subject);

		Faculty faculty = facultiesRepository.findById(onlineQuestion.getFacultyId())
				.orElseThrow(() -> new ItemNotFoundException("Faculty", "id", onlineQuestion.getFacultyId()));
		onlineQuestion.setFaculty(faculty);

		QuestionYear questionYear = questionYearRepository.findById(onlineQuestion.getQuestionYearId())
				.orElseThrow(() -> new ItemNotFoundException("QuestionYear", "id", onlineQuestion.getQuestionYearId()));
		onlineQuestion.setQuestionYear(questionYear);

//		OnlineQuestionTopic onlineQuestionTopic = onlineQuestionTopicRepository.findById(onlineQuestion.getOnlineQuestionTopicId())
//				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionTopic", "id", onlineQuestion.getOnlineQuestionTopicId()));
//		onlineQuestion.setOnlineQuestionTopic(onlineQuestionTopic);

		newOnlineQuestion = (OnlineQuestion) onlineQuestionRepository.save(onlineQuestion);
		return ResponseFormat.createFormat(newOnlineQuestion, "Created Successfully.");
//		return ResponseFormat.createFormat(onlineQuestion, "Created Successfully.");
	}

	/**
	 * Method for Creating Paragraph Online Question
	 * 
	 * @param data
	 * @return object
	 * 
	 */
	@PostMapping("/paragraph")
	public HashMap<String, Object> addParaQuestion(@Valid @RequestBody(required = false) Map<Object, Object> data) {

		Long onlineQuestionTopicId = null;
		OnlineQuestionPart questionPart = null;
		OnlineQuestionOption questionOption = null;
		HashMap<String, Object> result = new HashMap<String, Object>();

		String format = (String) data.get("format");
		System.out.println(format);
		if (format.equals("Paragraph")) {
			OnlineQuestionTopic onlineQuestionTopic = new OnlineQuestionTopic(data.get("content").toString());

			OnlineQuestionTopic newonOnlineQuestionTopic = (OnlineQuestionTopic) onlineQuestionTopicRepository
					.save(onlineQuestionTopic);
			onlineQuestionTopicId = onlineQuestionTopic.getId();

			ArrayList<String> onlineQuestions = (ArrayList<String>) data.get("onlineQuestionPart");

			Map<String, Object>[] onlineQuestionsMaps = onlineQuestions.toArray(new HashMap[onlineQuestions.size()]);

			for (Map<String, Object> objectlist : onlineQuestionsMaps) {
				OnlineQuestion newOnlineQuestion = new OnlineQuestion();
				questionPart = new OnlineQuestionPart();

//				newOnlineQuestion.setOnlineQuestionTopicId(onlineQuestionTopicId);
				newOnlineQuestion.setOnlineQuestionTopic(onlineQuestionTopic);

				Long subjectId = Long.valueOf((String) data.get("subjectId").toString());
				Subject subject = subjectRepository.findById(subjectId)
						.orElseThrow(() -> new ItemNotFoundException("Subject", "id", subjectId));
				newOnlineQuestion.setSubject(subject);

				Long courseId = Long.valueOf((String) data.get("courseId").toString());
				Course course = courseRepository.findById(courseId)
						.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseId));
				newOnlineQuestion.setCourse(course);

				Long facultyId = Long.valueOf((String) data.get("facultyId").toString());
				Faculty faculty = facultiesRepository.findById(facultyId)
						.orElseThrow(() -> new ItemNotFoundException("Faculty", "id", facultyId));
				newOnlineQuestion.setFaculty(faculty);

				Long questioYearId = Long.valueOf((String) data.get("questionYearId").toString());
				QuestionYear questionYear = questionYearRepository.findById(questioYearId)
						.orElseThrow(() -> new ItemNotFoundException("Question Year", "id", questioYearId));
				newOnlineQuestion.setQuestionYear(questionYear);

				newOnlineQuestion.setDifficultyLevel((String) data.get("difficultyLevel").toString());
				newOnlineQuestion.setQuestionFor((String) data.get("questionFor").toString());
				newOnlineQuestion.setFormat((String) data.get("format").toString());

//				Question Part Area
				questionPart.setQuestionType((String) objectlist.get("questionType"));
				questionPart.setQuestionText((String) objectlist.get("questionText"));

				String mark = (String) objectlist.get("mark");
				questionPart.setMark(Float.parseFloat(mark));

				Set<OnlineQuestionOption> onlineQuestionOptionsSet = new HashSet<OnlineQuestionOption>();

				ArrayList<String> questionOptions = (ArrayList<String>) objectlist.get("onlineQuestionOptions");
				Map<String, Object>[] questionMaps = questionOptions.toArray(new HashMap[questionOptions.size()]);

				for (Map<String, Object> option : questionMaps) {
					questionOption = new OnlineQuestionOption();

					questionOption.setAnswerValue((String) option.get("answerValue"));

					Boolean isCorrect = (Boolean) option.get("isCorrect");
					questionOption.setIsCorrect(isCorrect);

					onlineQuestionOptionsSet.add(questionOption);
				}

				questionPart.setOnlineQuestionOptions(onlineQuestionOptionsSet);

				Set<OnlineQuestionPart> questionPartSet = new HashSet<OnlineQuestionPart>();
				questionPartSet.add(questionPart);

				newOnlineQuestion.setOnlineQuestionPart(questionPartSet);

				OnlineQuestion newOuestion = (OnlineQuestion) onlineQuestionRepository.save(newOnlineQuestion);

			}

			result.put("questions", onlineQuestions.size());
			result.put("online_qustion_topic_id", onlineQuestionTopicId);

		}
		return ResponseFormat.createFormat(result, "Added to Online Exam");

	}

	/**
	 * Method for Updating Paragraph Online Question
	 * 
	 * @param data
	 * @return object
	 */
	
//	@PutMapping("/paragraph/{id}")
//	public HashMap<String, Object> updateParaQuestion(@PathVariable(value = "id") Long onlineQuestionId,
//			@Valid @RequestBody(required = false) OnlineQuestion onlineQuestion) {
//
//		OnlineQuestion newOnlineQuestion = onlineQuestionRepository.findById(onlineQuestionId)
//				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineQuestionId));
//
//		String format = (String) onlineQuestion.getFormat();
//
//		if (format.equals("Paragraph")) {
//
//			Subject subject = subjectRepository.findById((Long) onlineQuestion.getSubjectId())
//					.orElseThrow(() -> new ItemNotFoundException("Subject", "id", onlineQuestion.getSubjectId()));
//			onlineQuestion.setSubject(subject);
//
//			Course course = courseRepository.findById((Long) onlineQuestion.getCourseId())
//					.orElseThrow(() -> new ItemNotFoundException("Course", "id", onlineQuestion.getCourseId()));
//			onlineQuestion.setCourse(course);
//
//			Faculty faculty = facultiesRepository.findById((Long) onlineQuestion.getFacultyId())
//					.orElseThrow(() -> new ItemNotFoundException("Faculty", "id", onlineQuestion.getFacultyId()));
//			onlineQuestion.setFaculty(faculty);
//
//			QuestionYear questionYear = questionYearRepository.findById((Long) onlineQuestion.getQuestionYearId())
//					.orElseThrow(
//							() -> new ItemNotFoundException("QuestionYear", "id", onlineQuestion.getQuestionYearId()));
//			onlineQuestion.setQuestionYear(questionYear);
//
//			OnlineQuestionTopic onlineQuestionTopic = onlineQuestionTopicRepository
//					.findById((Long) onlineQuestion.getOnlineQuestionTopicId())
//					.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionTopic", "id",
//							(Long) onlineQuestion.getOnlineQuestionTopicId()));
//			onlineQuestionTopic.setContent(onlineQuestion.getOnlineQuestionTopic().getContent());
//
//			OnlineQuestionTopic newOnlineQuestionTopic = onlineQuestionTopicRepository.save(onlineQuestionTopic);
//			onlineQuestion.setOnlineQuestionTopic(newOnlineQuestionTopic);
//
//			Set<OnlineQuestionPart> onlineQuestionPartSet = (Set<OnlineQuestionPart>) onlineQuestion
//					.getOnlineQuestionPart();
//			Set<OnlineQuestionPart> updatedOnlineQuestionPartSet = new HashSet<OnlineQuestionPart>();
//
//			for (OnlineQuestionPart onlineQuestionPartObject : onlineQuestionPartSet) {
//
//				OnlineQuestionPart onlineQuestionPart = onlineQuestionPartRepository
//						.findById(onlineQuestionPartObject.getId())
//						.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionPart", "id",
//								onlineQuestionPartObject.getId()));
//
//				Set<OnlineQuestionOption> onlineQuestionOptionSet = onlineQuestionPartObject.getOnlineQuestionOptions();
//				Set<OnlineQuestionOption> updatedOnlineQuestionOptionSet = new HashSet<OnlineQuestionOption>();
//
//				for (OnlineQuestionOption onlineQuestionOptionObject : onlineQuestionOptionSet) {
//					OnlineQuestionOption onlineQuestionOption = onlineQuestionOptionRepository
//							.findById(onlineQuestionOptionObject.getId())
//							.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionOption", "id",
//									onlineQuestionOptionObject.getId()));
//					onlineQuestionOptionObject.setCreatedAt(onlineQuestionOption.getCreatedAt());
//
//					OnlineQuestionOption updatedOnlineQuestionOption = onlineQuestionOptionRepository
//							.save(onlineQuestionOptionObject);
//					updatedOnlineQuestionOptionSet.add(updatedOnlineQuestionOption);
//				}
//
//				onlineQuestionPartObject.setOnlineQuestionOptions(updatedOnlineQuestionOptionSet);
//				onlineQuestionPartObject.setCreatedAt(onlineQuestionPart.getCreatedAt());
//
//				OnlineQuestionPart updatedOnlineQuestionPart = onlineQuestionPartRepository
//						.save(onlineQuestionPartObject);
//				updatedOnlineQuestionPartSet.add(updatedOnlineQuestionPart);
//			}
//			onlineQuestion.setOnlineQuestionPart(updatedOnlineQuestionPartSet);
//		}
//
//		onlineQuestion.setCreatedAt(newOnlineQuestion.getCreatedAt());
//		OnlineQuestion updatedOnlineQuestion = onlineQuestionRepository.save(onlineQuestion);
//		return ResponseFormat.createFormat(updatedOnlineQuestion, "Updated Successfully");
//	}
	
	
	@PutMapping("/paragraph/{id}")
	public HashMap<String, Object> updateParaQuestion(@PathVariable(value = "id") Long onlineQuestionId,
			@Valid @RequestBody(required = false) Map<Object, Object> data) {

		Long onlineQuestionTopicId = null;
		OnlineQuestionPart questionPart = null;
		OnlineQuestionOption questionOption = null;
		HashMap<String, Object> result = new HashMap<String, Object>();

		String format = (String) data.get("format");
		System.out.println(format);
		if (format.equals("Paragraph")) {
			OnlineQuestionTopic onlineQuestionTopic = new OnlineQuestionTopic(data.get("content").toString());

			OnlineQuestionTopic newonOnlineQuestionTopic = (OnlineQuestionTopic) onlineQuestionTopicRepository
					.save(onlineQuestionTopic);
			onlineQuestionTopicId = onlineQuestionTopic.getId();

			ArrayList<String> onlineQuestions = (ArrayList<String>) data.get("onlineQuestionPart");

			Map<String, Object>[] onlineQuestionsMaps = onlineQuestions.toArray(new HashMap[onlineQuestions.size()]);

			for (Map<String, Object> objectlist : onlineQuestionsMaps) {
				// OnlineQuestion newOnlineQuestion = new OnlineQuestion();
				OnlineQuestion newOnlineQuestion = onlineQuestionRepository.findById(onlineQuestionId)
						.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineQuestionId));
				questionPart = new OnlineQuestionPart();

//				newOnlineQuestion.setOnlineQuestionTopicId(onlineQuestionTopicId);
				newOnlineQuestion.setOnlineQuestionTopic(onlineQuestionTopic);

				Long subjectId = Long.valueOf((String) data.get("subjectId").toString());
				Subject subject = subjectRepository.findById(subjectId)
						.orElseThrow(() -> new ItemNotFoundException("Subject", "id", subjectId));
				newOnlineQuestion.setSubject(subject);

				Long courseId = Long.valueOf((String) data.get("courseId").toString());
				Course course = courseRepository.findById(courseId)
						.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseId));
				newOnlineQuestion.setCourse(course);

				Long facultyId = Long.valueOf((String) data.get("facultyId").toString());
				Faculty faculty = facultiesRepository.findById(facultyId)
						.orElseThrow(() -> new ItemNotFoundException("Faculty", "id", facultyId));
				newOnlineQuestion.setFaculty(faculty);

				Long questioYearId = Long.valueOf((String) data.get("questionYearId").toString());
				QuestionYear questionYear = questionYearRepository.findById(questioYearId)
						.orElseThrow(() -> new ItemNotFoundException("Question Year", "id", questioYearId));
				newOnlineQuestion.setQuestionYear(questionYear);

				newOnlineQuestion.setDifficultyLevel((String) data.get("difficultyLevel").toString());
				newOnlineQuestion.setQuestionFor((String) data.get("questionFor").toString());
				newOnlineQuestion.setFormat((String) data.get("format").toString());

//				Question Part Area
				questionPart.setQuestionType((String) objectlist.get("questionType"));
				questionPart.setQuestionText((String) objectlist.get("questionText"));
//				questionPart.setMark((float) objectlist.get("questionText"));
				String mark = (String) objectlist.get("mark");
				questionPart.setMark(Float.parseFloat(mark));

				Set<OnlineQuestionOption> onlineQuestionOptionsSet = new HashSet<OnlineQuestionOption>();

				ArrayList<String> questionOptions = (ArrayList<String>) objectlist.get("onlineQuestionOptions");
				Map<String, Object>[] questionMaps = questionOptions.toArray(new HashMap[questionOptions.size()]);

				for (Map<String, Object> option : questionMaps) {
					questionOption = new OnlineQuestionOption();

					questionOption.setAnswerValue((String) option.get("answerValue"));

					Boolean isCorrect = (Boolean) option.get("isCorrect");
					questionOption.setIsCorrect(isCorrect);

					onlineQuestionOptionsSet.add(questionOption);
				}

				questionPart.setOnlineQuestionOptions(onlineQuestionOptionsSet);

				Set<OnlineQuestionPart> questionPartSet = new HashSet<OnlineQuestionPart>();
				questionPartSet.add(questionPart);

				newOnlineQuestion.setOnlineQuestionPart(questionPartSet);

				OnlineQuestion newOuestion = (OnlineQuestion) onlineQuestionRepository.save(newOnlineQuestion);

			}

			result.put("questions", onlineQuestions.size());
			result.put("online_qustion_topic_id", onlineQuestionTopicId);

		}

//		return ResponseFormat.createFormat(result, "Added to Online Exam");
		return ResponseFormat.createFormat(data, "Updated to Online Exam");

	}

	/*
	 * Method for Updating Online Question
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long onlineQuestionId,
			@Valid @RequestBody(required = false) OnlineQuestion onlineQuestion) {
		OnlineQuestion newOnlineQuestion = onlineQuestionRepository.findById(onlineQuestionId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineQuestionId));

		Course course = courseRepository.findById(onlineQuestion.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", onlineQuestion.getCourseId()));
		onlineQuestion.setCourse(course);

		Subject subject = subjectRepository.findById(onlineQuestion.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id", onlineQuestion.getSubjectId()));
		onlineQuestion.setSubject(subject);

		Faculty faculty = facultiesRepository.findById(onlineQuestion.getFacultyId())
				.orElseThrow(() -> new ItemNotFoundException("Faculty", "id", onlineQuestion.getFacultyId()));
		onlineQuestion.setFaculty(faculty);

		QuestionYear questionYear = questionYearRepository.findById(onlineQuestion.getQuestionYearId())
				.orElseThrow(() -> new ItemNotFoundException("QuestionYear", "id", onlineQuestion.getQuestionYearId()));
		onlineQuestion.setQuestionYear(questionYear);

		onlineQuestion.setId(onlineQuestionId);
		onlineQuestion.setCreatedAt(newOnlineQuestion.getCreatedAt());

		OnlineQuestion updatedOnlineQuestion = onlineQuestionRepository.save(onlineQuestion);
		return ResponseFormat.createFormat(updatedOnlineQuestion, "Updated Successfully");
	}

	/* DELETE Method for Delete Online Question */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long onlineQuestionId) {

		OnlineQuestion newOnlineQuestion = onlineQuestionRepository.findById(onlineQuestionId)
				.orElseThrow(() -> new ItemNotFoundException("Online Question", "id", onlineQuestionId));
		Set<OnlineQuestionPart> onlineQuestionPartList = newOnlineQuestion.getOnlineQuestionPart();
		for (OnlineQuestionPart onlineQuestionPart : onlineQuestionPartList) {
			Set<OnlineQuestionOption> onlineQuestionOptionList = onlineQuestionPart.getOnlineQuestionOptions();
			for (OnlineQuestionOption onlineQuestionOption : onlineQuestionOptionList) {
				onlineQuestionOptionRepository.deleteById(onlineQuestionOption.getId());
			}
			onlineQuestionPartRepository.deleteById(onlineQuestionPart.getId());
		}
		onlineQuestionRepository.delete(newOnlineQuestion);

		return ResponseFormat.createFormat(newOnlineQuestion, "Deleted Successfully");

//		OnlineQuestion newOnlineQuestion = onlineQuestionRepository.findById(onlineQuestionId)
//				.orElseThrow(() -> new ItemNotFoundException("Online Question", "id", onlineQuestionId));
//		onlineQuestionRepository.delete(newOnlineQuestion);
//		return ResponseFormat.createFormat(newOnlineQuestion, "Deleted Successfully");
	}

	/*
	 * GET Method for Search in OnlineQuestion
	 *
	 */

	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")

	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OnlineQuestion> resultList = new ArrayList<OnlineQuestion>();
		try {
			// Class type (required) for query execution
			Class<?> classType = OnlineQuestion.class;
			// Fields need to be searched
			// String[] fields =
			// {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = { "difficultyLevel", "format", "course.name", "subject.name", "faculty.name",
					"questionYear.name", "onlineQuestionPart.question_text" };
//					"solutions",	"format",

			resultList = (List<? extends OnlineQuestion>) searchservice.search(text, classType, resultList, fields);
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OnlineQuestions Successfully. using " + text);
	}

	// method for uploading single file to store into database table NB : coloums
	// are considered as array
	@PostMapping("/import")
	public HashMap<String, Object> importQuestions(@RequestParam("file") MultipartFile file) throws IOException {
		OnlineQuestion question = null;
		OnlineQuestionPart questionPart = null;
		OnlineQuestionOption onlineQuestionOption1 = null, onlineQuestionOption2 = null, onlineQuestionOption3 = null,
				onlineQuestionOption4 = null;
		Sheet worksheet = null;
		Workbook workbook = null;
		String difficultyLevel, type = null, questionText;

		String fileExtensionName = file.getOriginalFilename().substring(file.getOriginalFilename().indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			workbook = new XSSFWorkbook(file.getInputStream());
			worksheet = (XSSFSheet) workbook.getSheetAt(0);
		} else if (fileExtensionName.equals(".xls")) {
			workbook = new HSSFWorkbook(file.getInputStream());
			worksheet = (HSSFSheet) workbook.getSheetAt(0);
		}

		for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
			Row row = worksheet.getRow(i);
			question = new OnlineQuestion();
			questionPart = new OnlineQuestionPart();
			try {
				Long courseId = Long.valueOf((long) row.getCell(1).getNumericCellValue());
				Course course = courseRepository.findById(courseId)
						.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseId));
				question.setCourse(course);
			} catch (NullPointerException e) {
				System.out.println("1");
				question.setCourseId(null);
			}

			try {
				Long subjectId = Long.valueOf((long) row.getCell(2).getNumericCellValue());
				Subject subject = subjectRepository.findById(subjectId)
						.orElseThrow(() -> new ItemNotFoundException("Subject", "id", subjectId));
				question.setSubject(subject);
			} catch (IllegalStateException ex) {
				System.out.println("2");
				question.setSubjectId(null);
			}

			try {
				Long facultyId = Long.valueOf((long) row.getCell(3).getNumericCellValue());
				Faculty faculty = facultiesRepository.findById(facultyId)
						.orElseThrow(() -> new ItemNotFoundException("Faculty", "id", facultyId));
				question.setFaculty(faculty);
			} catch (NullPointerException e) {
				System.out.println("3");
				question.setFacultyId(null);
			}

			try {
				Long questionYearId = Long.valueOf((long) row.getCell(4).getNumericCellValue());
				QuestionYear questionYear = questionYearRepository.findById(questionYearId)
						.orElseThrow(() -> new ItemNotFoundException("QuestionYear", "id", questionYearId));
				question.setQuestionYear(questionYear);
			} catch (NullPointerException e) {
				System.out.println("4");
				question.setQuestionYearId(null);
			}

			try {
				difficultyLevel = row.getCell(5).getStringCellValue();
				question.setDifficultyLevel(difficultyLevel);
			} catch (NullPointerException e) {
				System.out.println("5");
				question.setDifficultyLevel(null);
			}

			try {
				type = row.getCell(6).getStringCellValue();
				questionPart.setQuestionType(type);
			} catch (Exception e) {
				System.out.println("6");
				questionPart.setQuestionType(null);
			}

			try {
				questionText = row.getCell(7).getStringCellValue();
				questionPart.setQuestionText(questionText);
			} catch (NullPointerException e) {
				System.out.println("7");
				questionPart.setQuestionText(null);
			}

			onlineQuestionOption1 = new OnlineQuestionOption();
			onlineQuestionOption2 = new OnlineQuestionOption();
			onlineQuestionOption3 = new OnlineQuestionOption();
			onlineQuestionOption4 = new OnlineQuestionOption();

			try {
				onlineQuestionOption1.setAnswerValue(row.getCell(8).getStringCellValue());
			} catch (NullPointerException e) {
				System.out.println("8");
				onlineQuestionOption1.setAnswerValue(null);
			}

			try {
				onlineQuestionOption2.setAnswerValue(row.getCell(9).getStringCellValue());
			} catch (NullPointerException e) {
				System.out.println("9");
				onlineQuestionOption2.setAnswerValue(null);
			}

			try {
				onlineQuestionOption3.setAnswerValue(row.getCell(10).getStringCellValue());
			} catch (NullPointerException e) {
				System.out.println("10");
				onlineQuestionOption3.setAnswerValue(null);
			}

			try {
				onlineQuestionOption4.setAnswerValue(row.getCell(11).getStringCellValue());
			} catch (NullPointerException e) {
				System.out.println("11");
				onlineQuestionOption4.setAnswerValue(null);
			}

			try {
				String isCorrect = row.getCell(11).getStringCellValue().toLowerCase();
				if (isCorrect.equals(onlineQuestionOption1.getAnswerValue())) {
					onlineQuestionOption1.setIsCorrect(true);
				} else if (isCorrect.equals(onlineQuestionOption2.getAnswerValue())) {
					onlineQuestionOption2.setIsCorrect(true);
				} else if (isCorrect.equals(onlineQuestionOption3.getAnswerValue())) {
					onlineQuestionOption3.setIsCorrect(true);
				} else if (isCorrect.equals(onlineQuestionOption4.getAnswerValue())) {
					onlineQuestionOption4.setIsCorrect(true);
				} else {
					System.out.println(
							"No options selected true for question number : " + row.getCell(0).getNumericCellValue());
				}
			} catch (NullPointerException e) {
				System.out.println("12");
				question.setDifficultyLevel(null);
			}

			try {
				question.setFormat("single");
				try {

					Set<OnlineQuestionOption> onlineQuestionOptionsSet = new HashSet<OnlineQuestionOption>();
					onlineQuestionOptionsSet.add(onlineQuestionOption1);
					onlineQuestionOptionsSet.add(onlineQuestionOption2);
					onlineQuestionOptionsSet.add(onlineQuestionOption3);
					onlineQuestionOptionsSet.add(onlineQuestionOption4);
					questionPart.setOnlineQuestionOptions(onlineQuestionOptionsSet);

					Set<OnlineQuestionPart> questionPartSet = new HashSet<OnlineQuestionPart>();
					questionPartSet.add(questionPart);
					question.setOnlineQuestionPart(questionPartSet);

				} catch (Exception e) {
					System.out.println("Not saved " + e);
				}

			} catch (Exception e) {
				System.out.println("Error occured : " + e);
			}
		}

		System.out.println("File Imported");
		Map<String, Object> output = new HashMap<>();
		output.put("filename ", file.getOriginalFilename());
		return ResponseFormat.createFormat(output, "Imported Successfully.");
	}

	@Autowired
	FileStorageProperties fileStorageProperties;

	/*
	 * GET method for download Online Question Excel Format
	 */
	@GetMapping("/getformat")
	public ResponseEntity<InputStreamResource> getExcelFormat() throws IOException {
		HttpHeaders headers = new HttpHeaders();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		String fileName = "/onlineQuestionFormat.xlsx";
		Path path = Paths.get(fileStorageProperties.getFormatDir());
		File file = new File(path.toString() + fileName);

		FileInputStream is = new FileInputStream(file);
		InputStreamResource isr = new InputStreamResource(is);
		MediaType mediaType = MediaType.parseMediaType("application/vnd.ms-excel");

		return ResponseEntity.ok().contentLength(file.length())
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=onlineQuestionFormat.xlsx")
				.contentType(mediaType).body(isr);
	}

}
