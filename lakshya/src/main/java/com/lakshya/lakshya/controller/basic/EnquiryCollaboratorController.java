package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.EnquiryCollaborator;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.EnquiryCollaboratorRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/enquirycollaborator")
public class EnquiryCollaboratorController {
	@Autowired
	EnquiryCollaboratorRepository enquiryCollaboratorRepository;

	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	UserRepository userRepository;
		

	/*
	 * GET Method for listing Enquiry Collaborators
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllEnquiryCollaborators(@RequestParam("studentId") Long studentId) {
		List<EnquiryCollaborator> enquiryCollaborators = (List<EnquiryCollaborator>) enquiryCollaboratorRepository.findAllByStudentId(studentId);
		return ResponseFormat.createFormat(enquiryCollaborators, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Enquiry Collaborators By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getAcademicYearById(@PathVariable(value = "id") Long EnquiryCollaboratorId) {
		EnquiryCollaborator enquiryCollaborator = enquiryCollaboratorRepository.findById(EnquiryCollaboratorId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Collaborator", "id", EnquiryCollaboratorId));
		return ResponseFormat.createFormat(enquiryCollaborator, "Listed Successfully");
	}

	/*
	 * Method for Creating Enquiry Collaborator
	 * 
	 */
	@PostMapping
	public HashMap create(@Valid @RequestBody(required = false) EnquiryCollaborator enquiryCollaborator) {

		User user = userRepository.findById(enquiryCollaborator.getUserId()).orElseThrow(() 
				-> new ItemNotFoundException("User ", "id", enquiryCollaborator.getUserId()));
		enquiryCollaborator.setUser(user);

		Student student = studentRepository.findById(enquiryCollaborator.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student ", "id", enquiryCollaborator.getStudentId()));
		enquiryCollaborator.setStudent(student);

		EnquiryCollaborator newEnquiryCollaborator = (EnquiryCollaborator) enquiryCollaboratorRepository
				.save(enquiryCollaborator);
		return ResponseFormat.createFormat(newEnquiryCollaborator, "Created Successfully.");
	}

	/* DELETE Method for Delete Enquiry Collaborator */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long enquiryCollaboratorId) {

		EnquiryCollaborator newEnquiryCollaborator = enquiryCollaboratorRepository.findById(enquiryCollaboratorId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Collaborator", "id", enquiryCollaboratorId));

		enquiryCollaboratorRepository.delete(newEnquiryCollaborator);
		return ResponseFormat.createFormat(newEnquiryCollaborator, "Deleted Successfully");
	}
	
	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}
}
