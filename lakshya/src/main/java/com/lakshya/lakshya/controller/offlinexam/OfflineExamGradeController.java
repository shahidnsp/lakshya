package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineExamGrade;
import com.lakshya.lakshya.offlinerepository.OfflineExamGradeRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexamgrade")
public class OfflineExamGradeController {
	@Autowired
	OfflineExamGradeRepository offlineExamGradeRepository; 

	@Autowired
	UserRepository userRepository;
	
	/*
	 * GET Method for listing OfflineExamGrade
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExamGrade> offlineExamGradeList = (List<OfflineExamGrade>) offlineExamGradeRepository.findAll();
		return ResponseFormat.createFormat(offlineExamGradeList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamGrade
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExamGrade offlineExamGrade) {
		
		User newUser = userRepository.findById(offlineExamGrade.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamGrade.getUserId()));
		offlineExamGrade.setUser(newUser);
		
		OfflineExamGrade newOfflineExamGrade = (OfflineExamGrade) offlineExamGradeRepository.save(offlineExamGrade);
		return ResponseFormat.createFormat(newOfflineExamGrade,"Created Successfully.");

	}

	/*
	 * Method for Updating OfflineExamGrade
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamGradeId,
			@Valid @RequestBody(required = false) OfflineExamGrade offlineExamGrade) {
		OfflineExamGrade newOfflineExamGrade = offlineExamGradeRepository.findById(offlineExamGradeId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMainGroup", "id", offlineExamGradeId));
		
		User newUser = userRepository.findById(offlineExamGrade.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamGrade.getUserId()));
		offlineExamGrade.setUser(newUser);
		
		offlineExamGrade.setId(offlineExamGradeId);
		offlineExamGrade.setCreatedAt(newOfflineExamGrade.getCreatedAt());				
		
		OfflineExamGrade updatedOfflineExamGrade= offlineExamGradeRepository.save(offlineExamGrade);
		return ResponseFormat.createFormat(updatedOfflineExamGrade, "Updated Successfully");
	}

	/* DELETE Method for Delete OfflineExamGrade */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamGradeId) {
		OfflineExamGrade newOfflineExamGrade= offlineExamGradeRepository.findById(offlineExamGradeId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamGrade", "id", offlineExamGradeId));

		offlineExamGradeRepository.delete(newOfflineExamGrade);
		return ResponseFormat.createFormat(newOfflineExamGrade, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExamGrade
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineExamGrade> resultList = new ArrayList<OfflineExamGrade>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineExamGrade.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"name"};

			resultList = (List<? extends OfflineExamGrade>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineExamGrade Successfully. using " + text);
	}
}
