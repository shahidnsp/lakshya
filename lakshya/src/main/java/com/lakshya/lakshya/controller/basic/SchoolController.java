package com.lakshya.lakshya.controller.basic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.LossReason;
import com.lakshya.lakshya.model.School;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.LossReasonRepository;
import com.lakshya.lakshya.repository.SchoolRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

/*
 * Controller For Enquiry Source 
 * 
 * Created By Shahid Neermunda
 * Date: 06/01/2020
 */

@RestController
@RequestMapping("/api/school")
public class SchoolController {
	@Autowired
	SchoolRepository schoolRepository;

	/*
	 * GET Method for listing school
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAllSchool() {

		Iterable<School> schools = schoolRepository.findAll();
		
		return ResponseFormat.createFormat(schools, "Listed Successfully.");

	}
	
	/*
	 * GET Method for listing School  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBranchById(@PathVariable(value = "id") Long schoolId) {
		School school = schoolRepository.findById(schoolId)
				.orElseThrow(() -> new ItemNotFoundException("School", "id", schoolId));
		return ResponseFormat.createFormat(school, "Listed Successfully");
	}

	/*
	 * POST Method for Creating School
	 * 
	 */
	@PostMapping
	public Object createSchool(@Valid @RequestBody(required = false) School school) {
		School newSchool = (School) schoolRepository.save(school);
		return ResponseFormat.createFormat(newSchool, "Created Successfully.");
	}

	/*
	 * PUT Method for Updating School
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateSchool(@PathVariable(value = "id") Long schoolId,
			@Valid @RequestBody(required = false) School school) {

		School newSchool = schoolRepository.findById(schoolId)
				.orElseThrow(() -> new ItemNotFoundException("School", "id", schoolId));
		
		school.setId(schoolId);
		school.setCreatedAt(newSchool.getCreatedAt());
		
		School updateSchool = schoolRepository.save(school);

		return ResponseFormat.createFormat(updateSchool, "Updated Successfully.");
	}

	/*
	 * DELETE Method for Delete Loss Reason
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteLossReason(@PathVariable(value = "id") Long schoolId) {
		School school = schoolRepository.findById(schoolId)
				.orElseThrow(() -> new ItemNotFoundException("School", "id", schoolId));

		schoolRepository.delete(school);
		return ResponseFormat.createFormat(school, "Deleted Successfully.");

	}
	
}
