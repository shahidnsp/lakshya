package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.model.offlineexam.OfflineExamGrade;
import com.lakshya.lakshya.model.offlineexam.OfflineExamModel;
import com.lakshya.lakshya.offlinerepository.OfflineExamGradeRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamModelRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.repository.AcademicYearRepository;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.SubjectRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexam")
public class OfflineExamController {
	@Autowired
	OfflineExamRepository offlineExamRepository; 

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	OfflineExamGradeRepository offlineExamGradeRepository;
	
	@Autowired
	OfflineExamModelRepository offlineExamModelRepository;
	
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	SubjectRepository subjectRepository;
	
	@Autowired
	BranchRepository branchRepository;
	
	@Autowired
	AcademicYearRepository academicYearRepository;
		
	/*
	 * GET Method for listing OfflineExam
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExam> offlineExamList = (List<OfflineExam>) offlineExamRepository.findAll();
		return ResponseFormat.createFormat(offlineExamList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExam
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExam offlineExam) {

		Course course = courseRepository.findById(offlineExam.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id",offlineExam.getCourseId()));
		offlineExam.setCourse(course);
		
		Subject subject = subjectRepository.findById(offlineExam.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id",offlineExam.getSubjectId()));
		offlineExam.setSubject(subject);
		
		Branch branch = branchRepository.findById(offlineExam.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id",offlineExam.getBranchId()));
		offlineExam.setBranch(branch);
		
		AcademicYear academicyear = academicYearRepository.findById(offlineExam.getAcademicYearId())
				.orElseThrow(() -> new ItemNotFoundException("AcademicYear ", "id",offlineExam.getAcademicYearId()));
		offlineExam.setAcademicYear(academicyear);
		
		OfflineExamGrade offlineExamGrade = offlineExamGradeRepository.findById(offlineExam.getOfflineExamGradeId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamGrade ", "id",offlineExam.getOfflineExamGradeId()));				
		offlineExam.setOfflineExamGrade(offlineExamGrade);		
		
		OfflineExamModel offlineExamModel = offlineExamModelRepository.findById(offlineExam.getOfflineExamModelId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamModel ", "id",offlineExam.getOfflineExamModelId()));
		offlineExam.setOfflineExamModel(offlineExamModel);
		
		User newUser = userRepository.findById(offlineExam.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamGrade.getUserId()));
		offlineExam.setUser(newUser);
		
		OfflineExam newOfflineExam = (OfflineExam) offlineExamRepository.save(offlineExam);
		return ResponseFormat.createFormat(newOfflineExam,"Created Successfully.");
//		return ResponseFormat.createFormat(offlineExam,"Created Successfully.");
	}

	/*
	 * Method for Updating OfflineExamGrade
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamId,
			@Valid @RequestBody(required = false) OfflineExam offlineExam) {
		OfflineExam newOfflineExam = offlineExamRepository.findById(offlineExamId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id", offlineExamId));
		
		Course course = courseRepository.findById(offlineExam.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id",offlineExam.getCourseId()));
		offlineExam.setCourse(course);
		
		Subject subject = subjectRepository.findById(offlineExam.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id",offlineExam.getSubjectId()));
		offlineExam.setSubject(subject);
		
		Branch branch = branchRepository.findById(offlineExam.getBranchId())
				.orElseThrow(() -> new ItemNotFoundException("Branch", "id",offlineExam.getBranchId()));
		offlineExam.setBranch(branch);
		
		AcademicYear academicyear = academicYearRepository.findById(offlineExam.getAcademicYearId())
				.orElseThrow(() -> new ItemNotFoundException("AcademicYear ", "id",offlineExam.getAcademicYearId()));
		offlineExam.setAcademicYear(academicyear);
		
		OfflineExamGrade offlineExamGrade = offlineExamGradeRepository.findById(offlineExam.getOfflineExamGradeId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamGrade ", "id",offlineExam.getOfflineExamGradeId()));				
		offlineExam.setOfflineExamGrade(offlineExamGrade);		
		
		OfflineExamModel offlineExamModel = offlineExamModelRepository.findById(offlineExam.getOfflineExamModelId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamModel ", "id",offlineExam.getOfflineExamModelId()));
		offlineExam.setOfflineExamModel(offlineExamModel);
		
		User newUser = userRepository.findById(offlineExam.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamGrade.getUserId()));
		offlineExam.setUser(newUser);
		
		offlineExam.setId(offlineExamId);
		offlineExam.setCreatedAt(newOfflineExam.getCreatedAt());				
		
		OfflineExam updateOfflineExam= offlineExamRepository.save(offlineExam);
		return ResponseFormat.createFormat(updateOfflineExam, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineExam */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamId) {
		OfflineExam newOfflineExam= offlineExamRepository.findById(offlineExamId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id", offlineExamId));

		offlineExamRepository.delete(newOfflineExam);
		return ResponseFormat.createFormat(newOfflineExam, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExam
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineExam> resultList = new ArrayList<OfflineExam>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineExam.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"name","exam_type"};

			resultList = (List<? extends OfflineExam>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineExam Successfully. using " + text);
	}
}
