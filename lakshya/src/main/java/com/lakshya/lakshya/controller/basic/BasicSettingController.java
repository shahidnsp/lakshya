package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.BasicSetting;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.repository.BasicSettingRepository;

@RestController
@RequestMapping("/api/basicsetttings")
public class BasicSettingController {

	@Autowired
	BasicSettingRepository basicSettingRepository;

	/*
	 * GET Method for get last basic settings data
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> get() {

		BasicSetting basicSetting = basicSettingRepository.findTopByOrderByIdDesc();

		return ResponseFormat.createFormat(basicSetting, "Listed Successfully.");

	}

	/*
	 * POST Method for Creating Basic Settings
	 * 
	 */
	@PostMapping
	public Object create(@Valid @RequestBody(required=false) BasicSetting basicSetting) {

		BasicSetting newBasicSetting = (BasicSetting) basicSettingRepository.save(basicSetting);
		return ResponseFormat.createFormat(basicSetting, "Created Successfully.");

	}

	/*
	 * PUT Method for Updating Basic Settings
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long basicSettingId,
			@Valid @RequestBody(required = false) BasicSetting basicSetting) {

		BasicSetting newBasicSetting = basicSettingRepository.findById(basicSettingId)
				.orElseThrow(() -> new ItemNotFoundException("Basic Settings", "id", basicSettingId));

		newBasicSetting.setFirmName(basicSetting.getFirmName());
		newBasicSetting.setLogo(basicSetting.getLogo());
		newBasicSetting.setEmail(basicSetting.getEmail());
		newBasicSetting.setWebsite(basicSetting.getWebsite());

		BasicSetting updBasicSetting = basicSettingRepository.save(newBasicSetting);

		return ResponseFormat.createFormat(updBasicSetting, "Updated Successfully.");
	}

	/*
	 * DELETE Method for Delete basic Settings
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long basicSettingId) {
		BasicSetting basicSetting = basicSettingRepository.findById(basicSettingId)
				.orElseThrow(() -> new ItemNotFoundException("Basic Settingd", "id", basicSettingId));

		basicSettingRepository.delete(basicSetting);
		return ResponseFormat.createFormat(null, "Deleted Successfully.");

	}

}
