package com.lakshya.lakshya.controller.onlineexam;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestion;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionSection;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionSectionItem;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionSectionItemRepository;
import com.lakshya.lakshya.onlinerepository.OnlineQuestionSectionRepository;

@RestController
@RequestMapping("/api/onlinequestionsection")
public class OnlineQuestionSectionItemController {

	@Autowired
	OnlineQuestionSectionItemRepository onlineQuestionSectionItemRepository;

	@Autowired
	OnlineQuestionRepository onlineQuestionRepository;

	@Autowired
	OnlineQuestionSectionRepository onlineQuestionSectionRepository;

	/*
	 * GET Method for listing Online Question Section Item
	 *
	 */
	@GetMapping("/item")
	public HashMap<String, Object> getOnlineQuestionSectionItem() {
		List<OnlineQuestionSectionItem> onlineQuestionSectionItemList = (List<OnlineQuestionSectionItem>) onlineQuestionSectionItemRepository
				.findAll();
		return ResponseFormat.createFormat(onlineQuestionSectionItemList, "Listed Successfully");
	}

	/*
	 * GET Method for listing Online Question Section Item
	 *
	 */
	@GetMapping("/item/{id}")
	public HashMap<String, Object> getOnlineQuestionSectionItemById(
			@PathVariable(value = "id") Long onlineQuestionSectionItemId) {
		OnlineQuestionSectionItem onlineQuestionSectionItem = (OnlineQuestionSectionItem) onlineQuestionSectionItemRepository
				.findOneById(onlineQuestionSectionItemId);
		return ResponseFormat.createFormat(onlineQuestionSectionItem, "Fetched Successfully");
	}

	/*
	 * Method for Creating Online Question Section Item
	 * 
	 */
	@PostMapping("/item")
	public HashMap<String, Object> createOnlineQuestionSectionItem(
			@Valid @RequestBody(required = false) OnlineQuestionSectionItem onlineQuestionSectionItem) {

		/*Set<OnlineQuestion> onlineQuestionSet = new HashSet<OnlineQuestion>();

		OnlineQuestionSection onlineQuestionSection = onlineQuestionSectionRepository
				.findById(onlineQuestionSectionItem.getOnlineQuestionSectionId())
				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionSection", "id",
						onlineQuestionSectionItem.getOnlineQuestionSectionId()));
		onlineQuestionSectionItem.setOnlineQuestionSection(onlineQuestionSection);

		for (Long onlineQuestionId : onlineQuestionSectionItem.getOnlineQuestionArray()) {
			try {
				OnlineQuestion onlineQuestion = onlineQuestionRepository.findById(onlineQuestionId)
						.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineQuestionId));
				onlineQuestionSet.add(onlineQuestion);
			} catch (Exception e) {
				System.out.println("No Question with Id : " + onlineQuestionId);
			}
		}
		onlineQuestionSectionItem.setOnlineQuestionSet(onlineQuestionSet);

		OnlineQuestionSectionItem newOnlineQuestionSectionItem = onlineQuestionSectionItemRepository
				.save(onlineQuestionSectionItem);
		return ResponseFormat.createFormat(newOnlineQuestionSectionItem, "Created Successfully.");*/
		
		return ResponseFormat.createFormat(null, "Created Successfully.");
	}

	/*
	 * Method for Updating Online Question Section Item
	 * 
	 */
	@PutMapping("/item/{id}")
	public HashMap<String, Object> updateOnlineQuestionSectionItem(
			@PathVariable(value = "id") Long onlineQuestionSectionItemId,
			@Valid @RequestBody(required = false) OnlineQuestionSectionItem onlineQuestionSectionItem) {

		/*Set<OnlineQuestion> onlineQuestionSet = new HashSet<OnlineQuestion>();

		OnlineQuestionSectionItem newOnlineQuestionSectionItem = onlineQuestionSectionItemRepository
				.findById(onlineQuestionSectionItemId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionSectionItem", "id",
						onlineQuestionSectionItemId));

		OnlineQuestionSection onlineQuestionSection = onlineQuestionSectionRepository
				.findById(onlineQuestionSectionItem.getOnlineQuestionSectionId())
				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionSection", "id",
						onlineQuestionSectionItem.getOnlineQuestionSectionId()));
		onlineQuestionSectionItem.setOnlineQuestionSection(onlineQuestionSection);

		for (Long onlineQuestionId : onlineQuestionSectionItem.getOnlineQuestionArray()) {
			try {
				OnlineQuestion onlineQuestion = onlineQuestionRepository.findById(onlineQuestionId)
						.orElseThrow(() -> new ItemNotFoundException("OnlineQuestion", "id", onlineQuestionId));
				onlineQuestionSet.add(onlineQuestion);
			} catch (Exception e) {
				System.out.println("No Question with Id : " + onlineQuestionId);
			}
		}
		onlineQuestionSectionItem.setOnlineQuestionSet(onlineQuestionSet);

		onlineQuestionSectionItem.setId(onlineQuestionSectionItemId);
		onlineQuestionSectionItem.setCreatedAt(newOnlineQuestionSectionItem.getCreatedAt());


		return ResponseFormat.createFormat(onlineQuestionSectionItem, "Updated Successfully");*/
		
		return ResponseFormat.createFormat(null, "Updated Successfully");
	}

	/*
	 * DELETE Method for Delete Online Question Section Item
	 */
	@DeleteMapping("/item/{id}")
	public HashMap<String, Object> deleteOnlineQuestionSectionItem(
			@PathVariable(value = "id") Long onlineQuestionSectionItemId) {
		OnlineQuestionSectionItem newOnlineQuestionSectionItem = onlineQuestionSectionItemRepository
				.findById(onlineQuestionSectionItemId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionSectionItem", "id",
						onlineQuestionSectionItemId));

		onlineQuestionSectionItemRepository.delete(newOnlineQuestionSectionItem);
		return ResponseFormat.createFormat(newOnlineQuestionSectionItem, "Deleted Successfully");
	}

	/*
	 * 
	 * 
	 * Online Question Section
	 * 
	 * 
	 * 
	 */

	/*
	 * GET Method for listing Online Question Section
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getOnlineQuestionSection() {
		List<OnlineQuestionSection> onlineQuestionSectionList = (List<OnlineQuestionSection>) onlineQuestionSectionRepository
				.findAll();
		return ResponseFormat.createFormat(onlineQuestionSectionList, "Listed Successfully");
	}

	/*
	 * GET Method for listing Online Question Section
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getOnlineQuestionSectionById(
			@PathVariable(value = "id") Long onlineQuestionSectionId) {
		OnlineQuestionSection onlineQuestionSection = (OnlineQuestionSection) onlineQuestionSectionRepository
				.findOneById(onlineQuestionSectionId);
		return ResponseFormat.createFormat(onlineQuestionSection, "Fetched Successfully");
	}

	/*
	 * POST Method for Creating Online Question Section
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> createOnlineQuestionSection(
			@Valid @RequestBody(required = false) OnlineQuestionSection onlineQuestionSection) {

		OnlineQuestionSection newOnlineQuestionSection = onlineQuestionSectionRepository.save(onlineQuestionSection);
		return ResponseFormat.createFormat(newOnlineQuestionSection, "Created Successfully");
	}

	/*
	 * PUT Method for Updating Online Question Section
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateOnlineQuestionSection(@PathVariable(value = "id") Long onlineQuestionSectionId,
			@Valid @RequestBody(required = false) OnlineQuestionSection onlineQuestionSection) {

		OnlineQuestionSection newOnlineQuestionSection = onlineQuestionSectionRepository
				.findById(onlineQuestionSectionId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionSection", "id", onlineQuestionSectionId));

		onlineQuestionSection.setCreatedAt(newOnlineQuestionSection.getCreatedAt());
		OnlineQuestionSection updatedOnlineQuestionSection = onlineQuestionSectionRepository
				.save(onlineQuestionSection);
		return ResponseFormat.createFormat(updatedOnlineQuestionSection, "Updated Successfully");
	}

	/*
	 * DELETE Method for Delete Online Question Section
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteonlineQuestionSectionById(
			@PathVariable(value = "id") Long onlineQuestionSectionId) {
		OnlineQuestionSection newOnlineQuestionSection = onlineQuestionSectionRepository
				.findById(onlineQuestionSectionId)
				.orElseThrow(() -> new ItemNotFoundException("OnlineQuestionSection", "id", onlineQuestionSectionId));

		onlineQuestionSectionRepository.delete(newOnlineQuestionSection);
		return ResponseFormat.createFormat(newOnlineQuestionSection, "Deleted Successfully");
	}
}
