package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Chapter;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.CourseFeeStructure;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.repository.CourseFeeStructureRepository;
import com.lakshya.lakshya.repository.CourseRepository;

@RestController
@RequestMapping("/api/coursefeestructure")
public class CourseFeeStructureController {
	@Autowired
	CourseFeeStructureRepository courseFeeStructureRepository;

	@Autowired
	CourseRepository courseRepository;

	/*
	 * GET Method for listing all courses fee strcures
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAll(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<CourseFeeStructure> coursefeestr = courseFeeStructureRepository.findAll(paging);
		return ResponseFormat.createFormat(coursefeestr, "Listed Successfully");
	}

	/*
	 * POST Method for Create curse
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) CourseFeeStructure courseFeeStructure) {

		Course course = courseRepository.findById(courseFeeStructure.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course", "id", courseFeeStructure.getCourseId()));
		courseFeeStructure.SetCourse(course);

		CourseFeeStructure newFee = (CourseFeeStructure) courseFeeStructureRepository.save(courseFeeStructure);
		return ResponseFormat.createFormat(newFee, "Created successfully");
	}

	/*
	 * PUT Method for Updating Chapter
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long courseFeeStructureId,
			@Valid @RequestBody(required = false) CourseFeeStructure courseFeeStructure) {

		CourseFeeStructure newCourseFeeStructure = courseFeeStructureRepository.findById(courseFeeStructureId)
				.orElseThrow(() -> new ItemNotFoundException("Course fee structure", "id", courseFeeStructureId));
			
		Course course = courseRepository.findById(courseFeeStructure.getCourseId())
				.orElseThrow(() -> new ItemNotFoundException("Course ", "id", courseFeeStructure.getCourse()));
		courseFeeStructure.SetCourse(course);

		courseFeeStructure.setId(courseFeeStructureId);
		courseFeeStructure.setCreatedAt(newCourseFeeStructure.getCreatedAt());
				
		/*newCourseFeeStructure.setName(courseFeeStructure.getName());
		newCourseFeeStructure.setAmount(courseFeeStructure.getAmount());
		newCourseFeeStructure.setDuration(courseFeeStructure.getDuration());
		newCourseFeeStructure.setNoofemi(courseFeeStructure.getNoofemi());
		newCourseFeeStructure.setAfterEvery(courseFeeStructure.getAfterEvery());
		newCourseFeeStructure.setDayOfMonth(courseFeeStructure.getDayofmonth());*/
		
		CourseFeeStructure updatedcoursCourseFeeStructure = courseFeeStructureRepository.save(courseFeeStructure);

		return ResponseFormat.createFormat(updatedcoursCourseFeeStructure, "Updated Successfully.");
	}

	/*
	 * DELETE Method for Delete Chapter
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long courseFeeStructureId) {
		CourseFeeStructure courseFeeStructure = courseFeeStructureRepository.findById(courseFeeStructureId)
				.orElseThrow(() -> new ItemNotFoundException("Course Fee Structure", "id", courseFeeStructureId));

		courseFeeStructureRepository.delete(courseFeeStructure);
		return ResponseFormat.createFormat(courseFeeStructure, "Deleted Successfully.");

	}
}
