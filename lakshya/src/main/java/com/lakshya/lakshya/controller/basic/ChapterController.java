package com.lakshya.lakshya.controller.basic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Chapter;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.ChapterRepository;
import com.lakshya.lakshya.repository.SubjectRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/chapter")
public class ChapterController {

	@Autowired
	ChapterRepository chapterRepository;

	@Autowired
	SubjectRepository subjectRepository;

	@Autowired
	UserRepository userRepository;

	/*
	 * GET Method for listing Branch Categories
	 * 
	 */

	@GetMapping
	public HashMap<String, Object> getAll(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<Chapter> chapters = chapterRepository.findAll(paging);

		return ResponseFormat.createFormat(chapters, "Listed Successfully.");
	}
	
	/*
	 * GET Method for listing Chapter  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBranchById(@PathVariable(value = "id") Long chapterId) {
		Chapter newChapter = chapterRepository.findById(chapterId)
				.orElseThrow(() -> new ItemNotFoundException("Chapter", "id", chapterId));
		return ResponseFormat.createFormat(newChapter, "Listed Successfully");
	}

	/*
	 * POST Method for Creating Chapter
	 * 
	 */
	@PostMapping
	public Object create(@Valid @RequestBody(required = false) Chapter chapter) {

		Subject subject = subjectRepository.findById(chapter.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id", chapter.getSubjectId()));
		chapter.setSubject(subject);
		
		//User user = userRepository.findById(chapter.getUserId()).orElseThrow(() -> new ItemNotFoundException("User", "id", chapter.getUserId()));
		
		chapter.setUser(this.getUser());

		Chapter newChapter = (Chapter) chapterRepository.save(chapter);
		return ResponseFormat.createFormat(newChapter, "Created Successfully.");	
	}

	/*
	 * PUT Method for Updating Chapter
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long chapterId,
			@Valid @RequestBody(required = false) Chapter chapter) {

		Chapter newChapter = chapterRepository.findById(chapterId)
				.orElseThrow(() -> new ItemNotFoundException("Chapter", "id", chapterId));

		Subject subject = subjectRepository.findById(chapter.getSubjectId())
				.orElseThrow(() -> new ItemNotFoundException("Subject", "id", chapter.getSubjectId()));
		chapter.setSubject(subject);

		//User user = userRepository.findById(chapter.getUserId()).orElseThrow(() -> new ItemNotFoundException("User", "id", chapter.getUserId()));
		
		chapter.setUser(this.getUser());
		
		chapter.setId(chapterId);
		chapter.setCreatedAt(newChapter.getCreatedAt());
				
		Chapter updatedcChapter = chapterRepository.save(chapter);
		return ResponseFormat.createFormat(updatedcChapter, "Updated Successfully.");		
	}

	/*
	 * DELETE Method for Delete Chapter
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long chapterId) {
		Chapter chapter = chapterRepository.findById(chapterId)
				.orElseThrow(() -> new ItemNotFoundException("Chpater", "id", chapterId));

		chapterRepository.delete(chapter);
		return ResponseFormat.createFormat(chapter, "Deleted Successfully.");

	}

	/*
	 * PUT Method for Updating Branch Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeChapterStatus(@PathVariable(value = "id") Long chapterId) {

		Chapter chapter = chapterRepository.findById(chapterId)
				.orElseThrow(() -> new ItemNotFoundException("Chapter", "id", chapterId));

		if (chapter.getActive() == true)
			chapter.setActive(false);
		else
			chapter.setActive(true);

		Chapter updatedChapter = chapterRepository.save(chapter);

		return ResponseFormat.createFormat(updatedChapter, "Status Updated Successfully.");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}
	
	/*
	 * GET Method for Search in Chapter table
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends Chapter> resultList = new ArrayList<Chapter>();
		try {
			//Class type (required) for query execution
			Class<?> classType = Chapter.class;
			//Fields need to be searched
			String[] fields = {"name","remark"};
			
			resultList = (List<? extends Chapter>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Chapters Successfully. using " + text);
	}
}
