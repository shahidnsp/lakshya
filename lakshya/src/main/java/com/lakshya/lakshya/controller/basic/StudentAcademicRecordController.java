package com.lakshya.lakshya.controller.basic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.School;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.StudentAcademicRecord;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.SchoolRepository;
import com.lakshya.lakshya.repository.StudentAcademicRecordRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/studentacademicrecord")
public class StudentAcademicRecordController {
	@Autowired
	StudentAcademicRecordRepository studentAcademicRecordRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	SchoolRepository schoolRepository;
	
	/*
	 * GET Method for listing Student Academic Records
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllStudentAcademicRecord(@RequestParam Long studentId) {
		List<StudentAcademicRecord> studentAcademicRecord = (List<StudentAcademicRecord>) studentAcademicRecordRepository.findAllByStudentId(studentId);
		return ResponseFormat.createFormat(studentAcademicRecord, "Listed Successfully");
	}

	/*
	 * GET Method for listing Student Academic Record By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getStudentAcademicRecordById(@PathVariable(value = "id") Long studentAcademicRecordId) {
		StudentAcademicRecord studentAcademicRecord = studentAcademicRecordRepository.findById(studentAcademicRecordId)
				.orElseThrow(() -> new ItemNotFoundException("Student Academic Record", "id", studentAcademicRecordId));
		return ResponseFormat.createFormat(studentAcademicRecord, "Listed Successfully");
	}

	/*
	 * Method for Creating Student Academic Record
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(
			@Valid @RequestBody(required = false) StudentAcademicRecord studentAcademicRecord) {
				
		Student newStudent = studentRepository.findById(studentAcademicRecord.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", studentAcademicRecord.getStudentId()));
		studentAcademicRecord.setStudent(newStudent);
		
		School newSchool = schoolRepository.findById(studentAcademicRecord.getSchoolId())
				.orElseThrow(() -> new ItemNotFoundException("School", "id", studentAcademicRecord.getSchoolId()));
		studentAcademicRecord.setSchool(newSchool);
		
		studentAcademicRecord.setUser(this.getUser());
		
		StudentAcademicRecord newStudentAcademicRecord = (StudentAcademicRecord) studentAcademicRecordRepository
				.save(studentAcademicRecord);		
		return ResponseFormat.createFormat(newStudentAcademicRecord, "Created Successfully.");		
	}

	/*
	 * Method for Updating Student Academic Record
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long studentAcademicRecordId,
			@Valid @RequestBody(required = false) StudentAcademicRecord studentAcademicRecord) {
		
		StudentAcademicRecord newStudentAcademicRecord = studentAcademicRecordRepository.findById(studentAcademicRecordId)
				.orElseThrow(() -> new ItemNotFoundException("StudentAcademicRecord", "id", studentAcademicRecordId));
		
		Student newStudent = studentRepository.findById(studentAcademicRecord.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", studentAcademicRecord.getStudentId()));
		studentAcademicRecord.setStudent(newStudent);
		
		School newSchool = schoolRepository.findById(studentAcademicRecord.getSchoolId())
				.orElseThrow(() -> new ItemNotFoundException("School", "id", studentAcademicRecord.getSchoolId()));
		studentAcademicRecord.setSchool(newSchool);
		
		studentAcademicRecord.setUser(this.getUser());
		
		studentAcademicRecord.setId(studentAcademicRecordId);
		studentAcademicRecord.setCreatedAt(newStudentAcademicRecord.getCreatedAt());

		StudentAcademicRecord updatedstudentAcademicRecord = studentAcademicRecordRepository.save(studentAcademicRecord);
		return ResponseFormat.createFormat(updatedstudentAcademicRecord, "Updated Successfully");
	}

	/* DELETE Method for Delete Enquiry Collaborator */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long studentAcademicRecordId) {
		StudentAcademicRecord studentAcademicRecord = studentAcademicRecordRepository.findById(studentAcademicRecordId)
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", studentAcademicRecordId));

		studentAcademicRecordRepository.delete(studentAcademicRecord);
		return ResponseFormat.createFormat(studentAcademicRecord, "Deleted Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);
		return user;
	}

}
