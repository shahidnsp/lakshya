package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.model.offlineexam.OfflineExamAssignBatch;
import com.lakshya.lakshya.model.offlineexam.OfflineExamGrade;
import com.lakshya.lakshya.model.offlineexam.OfflineExamModel;
import com.lakshya.lakshya.offlinerepository.OfflineExamAssignBatchRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.repository.BatchRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexamassignbatch")
public class OfflineExamAssignBatchController {
	
	@Autowired
	BatchRepository batchRepository;
	
	@Autowired
	OfflineExamRepository offlineExamRepository;
	
	@Autowired
	OfflineExamAssignBatchRepository offlineExamAssignBatchRepository;
	/*
	 * GET Method for listing OfflineExamAssignBatch
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExamAssignBatch> offlineExamAssignBatchList = (List<OfflineExamAssignBatch>) offlineExamAssignBatchRepository.findAll();
		return ResponseFormat.createFormat(offlineExamAssignBatchList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamAssignBatch
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExamAssignBatch offlineExamAssignBatch) {
		
		Batch batch = batchRepository.findById(offlineExamAssignBatch.getBatchId())
				.orElseThrow(() -> new ItemNotFoundException("Batch", "id",offlineExamAssignBatch.getBatchId()));
		offlineExamAssignBatch.setBatch(batch);
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineExamAssignBatch.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineExamAssignBatch.getOfflineExamId()));				
		offlineExamAssignBatch.setOfflineExam(offlineExam);		
		
		OfflineExamAssignBatch newOfflineExamAssignBatch = (OfflineExamAssignBatch) offlineExamAssignBatchRepository.save(offlineExamAssignBatch);
		return ResponseFormat.createFormat(newOfflineExamAssignBatch,"Created Successfully.");
	}

	/*
	 * Method for Updating OfflineExamAssignBatch
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamAssignBatchId,
			@Valid @RequestBody(required = false) OfflineExamAssignBatch offlineExamAssignBatch) {
		
		OfflineExamAssignBatch newOfflineExamAssignBatch = offlineExamAssignBatchRepository.findById(offlineExamAssignBatchId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamAssignBatch", "id", offlineExamAssignBatchId)); 
		
		Batch batch = batchRepository.findById(offlineExamAssignBatch.getBatchId())
				.orElseThrow(() -> new ItemNotFoundException("Batch", "id",offlineExamAssignBatch.getBatchId()));
		offlineExamAssignBatch.setBatch(batch);
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineExamAssignBatch.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id", offlineExamAssignBatch.getOfflineExamId()));
		offlineExamAssignBatch.setOfflineExam(offlineExam);
		
		offlineExamAssignBatch.setId(offlineExamAssignBatchId);
		offlineExamAssignBatch.setCreatedAt(newOfflineExamAssignBatch.getCreatedAt());				
		
		OfflineExamAssignBatch updateOfflineExamAssignBatch= offlineExamAssignBatchRepository.save(offlineExamAssignBatch);
		return ResponseFormat.createFormat(updateOfflineExamAssignBatch, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineExamAssignBatch */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamAssignBatchId) {
		OfflineExamAssignBatch newOfflineExamAssignBatch= offlineExamAssignBatchRepository.findById(offlineExamAssignBatchId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id", offlineExamAssignBatchId));

		offlineExamAssignBatchRepository.delete(newOfflineExamAssignBatch);
		return ResponseFormat.createFormat(newOfflineExamAssignBatch, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExamAssignBatch
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends OfflineExamAssignBatch> resultList = new ArrayList<OfflineExamAssignBatch>();
		try {
			//Class type (required) for query execution
			Class<?> classType = OfflineExamAssignBatch.class;
			//Fields need to be searched
//			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = {"name","address","gender","phone1","phone2","phone3"};

			resultList = (List<? extends OfflineExamAssignBatch>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed OfflineExamAssignBatch Successfully. using " + text);
	}	
}
