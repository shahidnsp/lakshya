package com.lakshya.lakshya.controller.offlinexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.offlineexam.OfflineExam;
import com.lakshya.lakshya.model.offlineexam.OfflineExamKey;
import com.lakshya.lakshya.model.offlineexam.OfflineExamMainGroup;
import com.lakshya.lakshya.model.offlineexam.OfflineExamMark;
import com.lakshya.lakshya.offlinerepository.OfflineExamKeyRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamMainGroupRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamMarkRepository;
import com.lakshya.lakshya.offlinerepository.OfflineExamRepository;
import com.lakshya.lakshya.repository.BatchRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/offlineexammark")
public class OfflineExamMarkController {
	@Autowired
	OfflineExamMarkRepository offlineExamMarkRepository;
	
	@Autowired
	OfflineExamRepository offlineExamRepository;
	
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	BatchRepository batchRepository;	
	
	@Autowired
	UserRepository userRepository;
	/*
	 * GET Method for listing OfflineExamMark
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAll() {
		List<OfflineExamMark> examGroupList = (List<OfflineExamMark>)offlineExamMarkRepository.findAll();
		return ResponseFormat.createFormat(examGroupList, "Listed Successfully");
	}

	/*
	 * Method for Creating OfflineExamMark
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required=false) OfflineExamMark offlineExamMark) {
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineExamMark.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineExamMark.getOfflineExamId()));
		offlineExamMark.setOfflineExam(offlineExam );  	
		
		Student student= studentRepository.findById(offlineExamMark.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student", "id",offlineExamMark.getStudentId()));
		offlineExamMark.setStudent(student); 
		
		Batch batch= batchRepository.findById(offlineExamMark.getBatchId())
				.orElseThrow(() -> new ItemNotFoundException("Batch", "id",offlineExamMark.getBatchId()));
		offlineExamMark.setBatch(batch); 
		
		User user= userRepository.findById(offlineExamMark.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamMark.getUserId()));
		offlineExamMark.setUser(user); 		

		OfflineExamMark newOfflineExamMark = (OfflineExamMark) offlineExamMarkRepository.save(offlineExamMark);
		return ResponseFormat.createFormat(newOfflineExamMark,"Created Successfully.");
		
	}

	/*
	 * Method for Updating OfflineExamMark
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long offlineExamMarkId,
			@Valid @RequestBody(required = false) OfflineExamMark offlineExamMark) {
		
		OfflineExamMark newOfflineExamMark= offlineExamMarkRepository.findById(offlineExamMarkId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamMark", "id",offlineExamMarkId));
		
		OfflineExam offlineExam = offlineExamRepository.findById(offlineExamMark.getOfflineExamId())
				.orElseThrow(() -> new ItemNotFoundException("OfflineExam", "id",offlineExamMark.getOfflineExamId()));
		offlineExamMark.setOfflineExam(offlineExam );  	
		
		Student student= studentRepository.findById(offlineExamMark.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student", "id",offlineExamMark.getStudentId()));
		offlineExamMark.setStudent(student); 
		
		Batch batch= batchRepository.findById(offlineExamMark.getBatchId())
				.orElseThrow(() -> new ItemNotFoundException("Batch", "id",offlineExamMark.getBatchId()));
		offlineExamMark.setBatch(batch); 
		
		User user= userRepository.findById(offlineExamMark.getUserId())
				.orElseThrow(() -> new ItemNotFoundException("User", "id",offlineExamMark.getUserId()));
		offlineExamMark.setUser(user); 		
		
		offlineExamMark.setId(offlineExamMarkId);
		offlineExamMark.setCreatedAt(newOfflineExamMark.getCreatedAt());				
		
		OfflineExamMark updatedOfflineExamMark = offlineExamMarkRepository.save(offlineExamMark);
		return ResponseFormat.createFormat(updatedOfflineExamMark, "Updated Successfully");		
	}

	/* DELETE Method for Delete OfflineExamMark */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long offlineExamMarkId) {
		OfflineExamMark newOfflineExamMark = offlineExamMarkRepository.findById(offlineExamMarkId)
				.orElseThrow(() -> new ItemNotFoundException("OfflineExamKey", "id", offlineExamMarkId));

		offlineExamMarkRepository.delete(newOfflineExamMark);
		return ResponseFormat.createFormat(newOfflineExamMark, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in OfflineExamMark
	 *
	 */
//	@Autowired
//	private HibernateSearchService searchservice;
//
//	@SuppressWarnings("unchecked")
//	@GetMapping("/search/{text}")
//	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
//		List<? extends OfflineExamMark> resultList = new ArrayList<OfflineExamMark>();
//		try {
//			//Class type (required) for query execution
//			Class<?> classType = OfflineExamMark.class;
//			//Fields need to be searched
////			String[] fields = {"name","address","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
//			String[] fields = {"name","address","gender","phone1","phone2","phone3"};
//
//			resultList = (List<? extends OfflineExamMark>) searchservice.search(text,classType,resultList,fields);			
//		} catch (Exception ex) {
//			System.out.println("Error " + ex);
//		}
//		return ResponseFormat.createFormat(resultList, "Listed OfflineExamMark Successfully. using " + text);
//	}
}
