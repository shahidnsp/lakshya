package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.CourseDetail;
import com.lakshya.lakshya.model.EnquiryList;
import com.lakshya.lakshya.model.EnquirySource;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.repository.AcademicYearRepository;
import com.lakshya.lakshya.repository.CourseDetailRepository;
import com.lakshya.lakshya.repository.EnquiryListRepository;
import com.lakshya.lakshya.repository.EnquirySourceRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/enquirylist")
public class EnquiryListController {
	@Autowired
	EnquiryListRepository enquiryListRepository;
	
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	AcademicYearRepository academicYearRepository;
	
	@Autowired
	CourseDetailRepository courseDetailRepository;
	
	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	
	@Autowired
	UserRepository userRepository;
	
	
	/*
	 * GET Method for listing EnquiryList  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getEnquiryById(@PathVariable(value = "id") Long enquiryListId) {
		EnquiryList enquiryList = enquiryListRepository.findById(enquiryListId)
				.orElseThrow(() -> new ItemNotFoundException("EnquiryList", "id", enquiryListId));
		return ResponseFormat.createFormat(enquiryList, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing EnquiryList  By Student Id
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getEnquiryByStudentId(@RequestParam Long studentId) {
		
		List<EnquiryList> enquiryList = enquiryListRepository.findAllByStudentId(studentId);
		return ResponseFormat.createFormat(enquiryList, "Listed Successfully");
	}
	
	/*
	 * POST Method for Creating EnquiryList
	 * 
	 */
	@PostMapping
	public Object create(@Valid @RequestBody(required = false) EnquiryList enquiryList) {

		Student student = studentRepository.findById(enquiryList.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", enquiryList.getStudentId()));
		enquiryList.setStudent(student);
		
		AcademicYear academicYear = academicYearRepository.findById(enquiryList.getAcademicYearId())
				.orElseThrow(() -> new ItemNotFoundException("AcademicYear", "id", enquiryList.getAcademicYearId()));
		enquiryList.setAcademicYear(academicYear);
		
		CourseDetail courseDetail = courseDetailRepository.findById(enquiryList.getCourseDetailId())
				.orElseThrow(() -> new ItemNotFoundException("CourseDetail", "id", enquiryList.getCourseDetailId()));
		enquiryList.setCourseDetail(courseDetail);
		
		EnquirySource enquirySource = enquirySourceRepository.findById(enquiryList.getEnquirySourceId())
				.orElseThrow(() -> new ItemNotFoundException("EnquirySource", "id", enquiryList.getEnquirySourceId()));
		enquiryList.setEnquirySource(enquirySource);
		
		EnquiryList newenquiryList = (EnquiryList) enquiryListRepository.save(enquiryList);
		return ResponseFormat.createFormat(newenquiryList, "Created Successfully.");	
	}
	
	/*
	 * PUT Method for Updating EnquiryList
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long enquiryListId,
			@Valid @RequestBody(required = false) EnquiryList enquiryList) {

		EnquiryList newEnquiryList = enquiryListRepository.findById(enquiryListId)
				.orElseThrow(() -> new ItemNotFoundException("EnquiryList", "id", enquiryListId));

		Student student = studentRepository.findById(enquiryList.getStudentId())
				.orElseThrow(() -> new ItemNotFoundException("Student", "id", enquiryList.getStudentId()));
		enquiryList.setStudent(student);
		
		AcademicYear academicYear = academicYearRepository.findById(enquiryList.getAcademicYearId())
				.orElseThrow(() -> new ItemNotFoundException("AcademicYear", "id", enquiryList.getAcademicYearId()));
		enquiryList.setAcademicYear(academicYear);
		
		CourseDetail courseDetail = courseDetailRepository.findById(enquiryList.getCourseDetailId())
				.orElseThrow(() -> new ItemNotFoundException("CourseDetail", "id", enquiryList.getCourseDetailId()));
		enquiryList.setCourseDetail(courseDetail);
		
		EnquirySource enquirySource = enquirySourceRepository.findById(enquiryList.getEnquirySourceId())
				.orElseThrow(() -> new ItemNotFoundException("CourseDetail", "id", enquiryList.getEnquirySourceId()));
		enquiryList.setEnquirySource(enquirySource);

		
		enquiryList.setId(enquiryListId);
		enquiryList.setCreatedAt(newEnquiryList.getCreatedAt());
				
		EnquiryList updatedEnquiryList = enquiryListRepository.save(enquiryList);
		return ResponseFormat.createFormat(updatedEnquiryList, "Updated Successfully.");		
	}
	
	/*
	 * DELETE Method for Delete EnquiryList
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long enquiryListId) {
		EnquiryList enquiryList = enquiryListRepository.findById(enquiryListId)
				.orElseThrow(() -> new ItemNotFoundException("EnquiryList", "id", enquiryListId));

		enquiryListRepository.delete(enquiryList);
		return ResponseFormat.createFormat(enquiryList, "Deleted Successfully.");
	}
}
