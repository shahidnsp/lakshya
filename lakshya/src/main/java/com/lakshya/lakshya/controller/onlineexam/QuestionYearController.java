package com.lakshya.lakshya.controller.onlineexam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestion;
import com.lakshya.lakshya.model.onlineexam.QuestionYear;
import com.lakshya.lakshya.repository.QuestionYearRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

@RestController
@RequestMapping("/api/questionyear")
public class QuestionYearController {
	@Autowired
	QuestionYearRepository questionYearsRepository;

	/*
	 * GET Method for listing all QuestionYears
	 *
	 */
//	@GetMapping("/all")
	@GetMapping
	public HashMap<String, Object> getQuestionYears() {
		List<QuestionYear> questionYears = (List<QuestionYear>) questionYearsRepository.findAll();
		return ResponseFormat.createFormat(questionYears, "Listed Successfully");
	}
	
	/*
	 * GET Method for listing Faculties
	 *
	 */
//	@GetMapping
//	public HashMap<String, Object> getFaculties(@RequestParam(defaultValue = "0") Integer pageNo,
//			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {
//		
//		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
//		Page<QuestionYear> questionYears = questionYearsRepository.findAll(paging);
//		return ResponseFormat.createFormat(questionYears, "Listed Successfully");
//	}
	
	/*
	 * GET Method for getting QuestionYears By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long QuestionYearsId) {
		QuestionYear questionYear = questionYearsRepository.findById(QuestionYearsId)
				.orElseThrow(() -> new ItemNotFoundException("QuestionYears", "id", QuestionYearsId));
		return ResponseFormat.createFormat(questionYear, "Fetched Successfully");
	}

	/*
	 * Method for Creating QuestionYears
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) QuestionYear questionYears) {
		QuestionYear newQuestionYears = (QuestionYear) questionYearsRepository.save(questionYears);
		return ResponseFormat.createFormat(newQuestionYears, "Created Successfully.");
	}

	/*
	 * Method for Updating QuestionYears
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long QuestionYearsId,
			@Valid @RequestBody(required = false) QuestionYear questionYears) {
		QuestionYear newQuestionYears = questionYearsRepository.findById(QuestionYearsId)
				.orElseThrow(() -> new ItemNotFoundException("QuestionYears", "id", QuestionYearsId));

		questionYears.setId(QuestionYearsId);
		questionYears.setCreatedAt(newQuestionYears.getCreatedAt());

		QuestionYear updatedQuestionYears = questionYearsRepository.save(questionYears);
		return ResponseFormat.createFormat(updatedQuestionYears, "Updated Successfully");
	}

	/* DELETE Method for Delete QuestionYears */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long QuestionYearsId) {
		QuestionYear newQuestionYears = questionYearsRepository.findById(QuestionYearsId)
				.orElseThrow(() -> new ItemNotFoundException("QuestionYears", "id", QuestionYearsId));

		questionYearsRepository.delete(newQuestionYears);
		return ResponseFormat.createFormat(newQuestionYears, "Deleted Successfully");
	}
	
	/*
	 * GET Method for Search in QuestionYear
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends QuestionYear> resultList = new ArrayList<QuestionYear>();
		try {
			// Class type (required) for query execution
			Class<?> classType = QuestionYear.class;
			// Fields need to be searched
//			String[] fields = {"name","remark","branchPhones.phone","branchLandLines.phone","branchCategory.name"};
			String[] fields = { "name","remark"};

			resultList = (List<? extends QuestionYear>) searchservice.search(text, classType, resultList, fields);
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed QuestionYears Successfully. using " + text);
	}

}
