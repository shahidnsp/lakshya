package com.lakshya.lakshya.controller.basic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.EnquirySMS;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.EnquirySMSRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/enquirysms")
public class EnquirySMSController {
	@Autowired
	EnquirySMSRepository enquirySMSRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	StudentRepository studentRepository;

	/*
	 * GET Method for listing Enquiry Collaborators
	 *
	 */
	@GetMapping
	public HashMap<String, Object> getAllEnquirySMS() {
		List<EnquirySMS> enquirySMS = (List<EnquirySMS>) enquirySMSRepository.findAll();
		return ResponseFormat.createFormat(enquirySMS, "Listed Successfully");
	}

	/*
	 * GET Method for listing Enquiry Collaborators By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getEnquiryCollaboratorsById(@PathVariable(value = "id") Long enquirySMSId) {
		EnquirySMS enquirySMS = enquirySMSRepository.findById(enquirySMSId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Collaborator", "id", enquirySMSId));
		return ResponseFormat.createFormat(enquirySMS, "Listed Successfully");
	}

	/*
	 * Method for Creating Enquiry Collaborator
	 * 
	 */

	@PostMapping
	public HashMap<String, Object> create(@Valid @RequestBody(required = false) EnquirySMS enquirySMS) {

		Student newStudent = studentRepository.findById(enquirySMS.getStudent_Id())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", enquirySMS.getStudent_Id()));
		enquirySMS.setStudent(newStudent);
		
		//User newuser = userRepository.findById(enquirySMS.getUser_Id()).orElseThrow(() -> new ItemNotFoundException("User with Id", "id", enquirySMS.getUser_Id()));
		enquirySMS.setUser(this.getUser());

		EnquirySMS newEnquirySMS = (EnquirySMS) enquirySMSRepository.save(enquirySMS);
		return ResponseFormat.createFormat(newEnquirySMS, "Created Successfully.");
	}

	/*
	 * Method for Updating Enquiry Collaborator
	 * 
	 */

	@PutMapping("/{id}")
	public HashMap<String, Object> update(@PathVariable(value = "id") Long enquirySMSId,
			@Valid @RequestBody(required = false) EnquirySMS enquirySMS) {

		EnquirySMS newEnquirySMS = enquirySMSRepository.findById(enquirySMSId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", enquirySMSId));

		Student newStudent = studentRepository.findById(enquirySMS.getStudent_Id())
				.orElseThrow(() -> new ItemNotFoundException("Student with Id", "id", enquirySMS.getStudent_Id()));
		enquirySMS.setStudent(newStudent);

		//User newUser = userRepository.findById(enquirySMS.getUser_Id()).orElseThrow(() -> new ItemNotFoundException("User with Id", "id", enquirySMS.getUser_Id()));
		enquirySMS.setUser(this.getUser());

		enquirySMS.setId(enquirySMSId);
		enquirySMS.setCreatedAt(newEnquirySMS.getCreatedAt());

		EnquirySMS updatedEnquirySMS = enquirySMSRepository.save(enquirySMS);
		return ResponseFormat.createFormat(updatedEnquirySMS, "Updated Successfully");
	}

	/* DELETE Method for Delete Enquiry Collaborator */

	@DeleteMapping("/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long enquirySMSId) {
		EnquirySMS enquirySMS = enquirySMSRepository.findById(enquirySMSId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry CollaboratorId", "id", enquirySMSId));

		enquirySMSRepository.delete(enquirySMS);
		return ResponseFormat.createFormat(enquirySMS, "Deleted Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);
		return user;
	}
}
