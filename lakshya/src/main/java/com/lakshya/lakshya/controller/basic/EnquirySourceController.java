package com.lakshya.lakshya.controller.basic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Chapter;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.EnquirySource;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.EnquirySourceRepository;
import com.lakshya.lakshya.repository.UserRepository;
import com.lakshya.lakshya.service.HibernateSearchService;

/*
 * Controller For Enquiry Source 
 * 
 * Created By Shahid Neermunda
 * Date: 24/09/2019
 */

@RestController
@RequestMapping("/api/enquirysource")
public class EnquirySourceController {

	@Autowired
	EnquirySourceRepository enquirySourceRepository;

	@Autowired
	private UserRepository userRepository;

	/*
	 * GET Method for listing Enquiry Sources
	 * 
	 */
	@GetMapping
	public HashMap<String, Object> getAllEnquirySources(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy) {

		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		Page<EnquirySource> enquirySources = enquirySourceRepository.findAll(paging);

		return ResponseFormat.createFormat(enquirySources, "Listed Successfully.");

	}
	
	/*
	 * GET Method for listing Chapter  By Id
	 *
	 */
	@GetMapping("/{id}")
	public HashMap<String, Object> getBranchById(@PathVariable(value = "id") Long enquirySourceId) {
		EnquirySource newEnquirySource = enquirySourceRepository.findById(enquirySourceId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Sources", "id", enquirySourceId));
		return ResponseFormat.createFormat(newEnquirySource, "Listed Successfully");
	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		User user = userRepository.findByUsername(username);

		return user;
	}

	/*
	 * POST Method for Creating Enquiry Sources
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> createEnquirySources(@Valid @RequestBody(required = false) EnquirySource enquirySource) {

		enquirySource.setUser(this.getUser());
		
				
		EnquirySource newEnquirySource = (EnquirySource) enquirySourceRepository.save(enquirySource);
		return ResponseFormat.createFormat(newEnquirySource, "Created Successfully.");

		// return branch;
	}

	/*
	 * PUT Method for Updating Enquiry Sources
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> updateEnquirySources(@PathVariable(value = "id") Long enquirySourceId,
			@Valid @RequestBody(required = false) EnquirySource enquirySource) {

		EnquirySource newEnquirySource = enquirySourceRepository.findById(enquirySourceId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Sources", "id", enquirySourceId));

		enquirySource.setUser(this.getUser());
		
		/*newEnquirySource.setName(enquirySource.getName());
		newEnquirySource.setColor(enquirySource.getColor());
		newEnquirySource.setRemark(enquirySource.getRemark());*/
		
		enquirySource.setId(enquirySourceId);
		enquirySource.setCreatedAt(newEnquirySource.getCreatedAt());
		
		EnquirySource updateEnquirySourced = enquirySourceRepository.save(enquirySource);

		return ResponseFormat.createFormat(updateEnquirySourced, "Updated Successfully.");
	}

	/*
	 * DELETE Method for Delete Enquiry Sources
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteEnquirySources(@PathVariable(value = "id") Long enquirySourceId) {
		EnquirySource enquirySource = enquirySourceRepository.findById(enquirySourceId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Source", "id", enquirySourceId));

		enquirySourceRepository.delete(enquirySource);
		return ResponseFormat.createFormat(enquirySource, "Deleted Successfully.");

	}

	/*
	 * PUT Method for Updating Enquiry Sources Status
	 * 
	 */
	@PutMapping("changestatus/{id}")
	public HashMap<String, Object> changeEnquirySourcesStatus(@PathVariable(value = "id") Long enquirySourceId) {

		EnquirySource enquirySource = enquirySourceRepository.findById(enquirySourceId)
				.orElseThrow(() -> new ItemNotFoundException("Enquiry Source", "id", enquirySourceId));

		if (enquirySource.getActive() == true)
			enquirySource.setActive(false);
		else
			enquirySource.setActive(true);

		EnquirySource updateEnquirySourced = enquirySourceRepository.save(enquirySource);

		return ResponseFormat.createFormat(updateEnquirySourced, "Status Updated Successfully.");
	}
	
	/*
	 * GET Method for Search in Enquiry sources table
	 *
	 */
	@Autowired
	private HibernateSearchService searchservice;

	@SuppressWarnings("unchecked")
	@GetMapping("/search/{text}")
	public HashMap<String, Object> getAllSearch(@PathVariable(value = "text") String text) {
		List<? extends EnquirySource> resultList = new ArrayList<EnquirySource>();
		try {
			//Class type (required) for query execution
			Class<?> classType = EnquirySource.class;
			//Fields need to be searched
			String[] fields = {"name","remark"};
			
			resultList = (List<? extends EnquirySource>) searchservice.search(text,classType,resultList,fields);			
		} catch (Exception ex) {
			System.out.println("Error " + ex);
		}
		return ResponseFormat.createFormat(resultList, "Listed Enquiry Sources Successfully. using " + text);
	}

}