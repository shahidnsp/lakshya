package com.lakshya.lakshya.controller.basic;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lakshya.lakshya.config.ResponseFormat;
import com.lakshya.lakshya.error.ItemNotFoundException;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.EnquiryStage;
import com.lakshya.lakshya.model.Lead;
import com.lakshya.lakshya.model.LeadStatus;
import com.lakshya.lakshya.model.School;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.UserLead;
import com.lakshya.lakshya.repository.BatchRepository;
import com.lakshya.lakshya.repository.BranchRepository;
import com.lakshya.lakshya.repository.CourseRepository;
import com.lakshya.lakshya.repository.EnquiryStageRepository;
import com.lakshya.lakshya.repository.LeadRepository;
import com.lakshya.lakshya.repository.LeadStatusRepository;
import com.lakshya.lakshya.repository.LossReasonRepository;
import com.lakshya.lakshya.repository.SchoolRepository;
import com.lakshya.lakshya.repository.StudentGuardianPhoneRepository;
import com.lakshya.lakshya.repository.StudentGuardianRepository;
import com.lakshya.lakshya.repository.StudentPhoneRepository;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserLeadRepository;
import com.lakshya.lakshya.repository.UserRepository;

@RestController
@RequestMapping("/api/student/leadslist")
public class UserLeadListController {

	@Autowired
	UserLeadRepository userLeadRepository;

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	EnquiryStageRepository enquiryStageRepository;

	@Autowired
	BranchRepository branchRepository;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	LeadStatusRepository leadStatusRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	LeadRepository leadRepository;
	/*
	 * GET Method for listing LeadList ---->/api/student/leadslist
	 * 
	 */
//	@GetMapping("/leadslist")

//	public HashMap<String, Object> getAllLeads(@RequestParam(defaultValue = "0") Integer pageNo,
//			@RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy,
//			@RequestParam(defaultValue = "0") Long branchId) {
//	Pageable paging = (Pageable) PageRequest.of(0, 3, Sort.by(sortBy));
//
//	Page<Student> students = studentRepository.findAllByBranchIdAndStatus(branchId, "Enquiry", paging);
//	return ResponseFormat.createFormat(students, "Listed Successfully.");

	@GetMapping
	public HashMap<String, Object> getAllLeads() {
		List<UserLead> userLead = (List<UserLead>) userLeadRepository.findAll();
		return ResponseFormat.createFormat(userLead, "Listed Successfully.");
	}

	/*
	 * POST Method for Creating LeadList ---->/api/student/leadslist
	 * 
	 */
	@PostMapping
	public HashMap<String, Object> createLeadslist(@Valid @RequestBody(required = false) UserLead userLead) {

		userLead.setPreUserId(Long.valueOf(0));

		Lead lead = leadRepository.findOneById(userLead.getLeadId());
		if (lead == null)
			throw new ItemNotFoundException("Lead", "id", userLead.getLeadId());
		else
			userLead.setLead(lead);

		EnquiryStage enquiryStage = enquiryStageRepository.findOneById(userLead.getEnquiryStageId());
		if (enquiryStage == null)
			throw new ItemNotFoundException("EnquiryStage", "id", userLead.getEnquiryStageId());
		else
			userLead.setEnquiryStage(enquiryStage);

		User user = userRepository.findById(userLead.getAssignUserId())
				.orElseThrow(() -> new ItemNotFoundException("AssignUser", "id", userLead.getAssignUserId()));
		userLead.setAssignUser(user);

		userLead.setUser(this.getUser());
		userLead.setUserId((this.getUser().getId()));

		UserLead newUserLead = (UserLead) userLeadRepository.save(userLead);
		return ResponseFormat.createFormat(newUserLead, "Created Successfully.");
	}

	/*
	 * PUT Method for edit LeadList ---->/api/student/leadslist/4
	 * 
	 */
	@PutMapping("/{id}")
	public HashMap<String, Object> editAllLeads(@PathVariable(value = "id") Long id,
			@Valid @RequestBody(required = false) UserLead userLead) {
		UserLead oldUserLead = null;
		Long assignedUserId;
		try {
			oldUserLead = userLeadRepository.findOneById(id);
			assignedUserId = oldUserLead.getAssignUserId();
		} catch (Exception e) {
			throw new ItemNotFoundException("UserLead", "id", id);
		}

		Lead lead = leadRepository.findOneById(userLead.getLeadId());
		if (lead == null)
			throw new ItemNotFoundException("Lead", "id", userLead.getLeadId());
		else
			userLead.setLead(lead);

		EnquiryStage enquiryStage = enquiryStageRepository.findOneById(userLead.getEnquiryStageId());
		if (enquiryStage == null)
			throw new ItemNotFoundException("EnquiryStage", "id", userLead.getEnquiryStageId());
		else
			userLead.setEnquiryStage(enquiryStage);

//		User user = userRepository.findById(oldUserLead.getAssignUserId())
//				.orElseThrow(() -> new ItemNotFoundException("User", "id", assignedUserId));
//		userLead.setUser(user);
		userLead.setUser(this.getUser());

		User preUser = userRepository.findById(assignedUserId)
				.orElseThrow(() -> new ItemNotFoundException("PreUser", "id", assignedUserId));
		userLead.setPreUser(preUser);

		User assignUser = userRepository.findById(userLead.getAssignUserId())
				.orElseThrow(() -> new ItemNotFoundException("AssignUser", "id", userLead.getAssignUserId()));
		userLead.setAssignUser(assignUser);

////		userLead.setPreUserId(oldUserLead.getAssignUserId());
////		userLead.setPreUser(user);
//		userLead.setUserId((this.getUser().getId()));

//		User user = userRepository.findById(userLead.getUserId())
//				.orElseThrow(() -> new ItemNotFoundException("User", "id", userLead.getUserId()));		
//		userLead.setUser(user);

		userLead.setId(id);
		userLead.setCreatedAt(oldUserLead.getCreatedAt());

		UserLead updatedUserLead = (UserLead) userLeadRepository.save(userLead);
		return ResponseFormat.createFormat(updatedUserLead, "Updated Successfully." + oldUserLead.getAssignUserId());
	}

	/*
	 * DELETE Method for Delete UserLead ---->/api/student/leadslist/4
	 * 
	 */
	@DeleteMapping("/{id}")
	public HashMap<String, Object> deleteStudent(@PathVariable(value = "id") Long userLeadId) {
		UserLead userLead;
		try {
			userLead = userLeadRepository.findOneById(userLeadId);
		} catch (Exception e) {
			throw new ItemNotFoundException("UserLead", "id", userLeadId);
		}
		userLeadRepository.delete(userLead);
		return ResponseFormat.createFormat(userLead, "Deleted Successfully.");

	}

	/*
	 * Function for get Currently Logged User
	 * 
	 */
	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username);
		return user;
	}

}
