package com.lakshya.lakshya.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
//import org.hibernate.search.annotations.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

//import org.hibernate.search.annotations.AnalyzerDef;
//import org.hibernate.search.annotations.TokenFilterDef;
//import org.hibernate.search.annotations.TokenizerDef;
//import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
//import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
//import org.hibernate.search.annotations.Parameter;
//import org.apache.lucene.analysis.ngram.NGramFilterFactory;
//import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
//@AnalyzerDef(name = "lowercase",
//			tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
//			filters = {
//					@TokenFilterDef(factory = LowerCaseFilterFactory.class),
//					@TokenFilterDef(factory = SnowballPorterFilterFactory.class),
//					@TokenFilterDef(factory = NGramFilterFactory.class
//							,params = { @Parameter(name = "minGramSize", value = "2"),@Parameter(name = "maxGramSize", value = "5")})
//			}
//		)

@Entity
@Indexed
@Table(name = "branch_categories")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class BranchCategory implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

//	@Field(name = "name")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "name")
	@NotBlank(message = "Name is mandatory")
	private String name;

//	@Field(name = "remark")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "remark")
	@Nullable
	private String remark;

	@Nullable
	private Boolean active = true;

//    @Field(name="createdAt")
	@CreationTimestamp
	private LocalDateTime createdAt;

//    @Field(name="updatedAt")
	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "branch_category_id")
	private Set<Branch> branches;

	public BranchCategory() {

	}

	public BranchCategory(String name, String remark) {
		this.name = name;
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

}
