package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.User;

@Entity
@Indexed
@Table(name = "offline_exam_models")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineExamModel implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "model_name")
	@NotEmpty(message = "model Name Required.")	
	private String modelName;

	@Column(name = "correct_answer")
	private Float correctAnswer;
	
	@Column(name = "wrong_answer")
	private Float wrongAnswer;
	
	@Column(name = "min_pass_mark")
	private Float minPassMark;
	
	@Column(name = "decimal_place_integer")
	private Long decimalPlaceInteger;
	
	@Nullable	
	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "user_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private User user;

	@Column(name = "user_id", insertable = false, updatable = false)	
	private Long userId;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OfflineExamModel(){}

	public OfflineExamModel(Long id, @NotEmpty(message = "model Name Required.") String modelName, Float correctAnswer,
			Float wrongAnswer, Float minPassMark, Long decimalPlaceInteger, Boolean active, Long userId) {
		super();
		this.id = id;
		this.modelName = modelName;
		this.correctAnswer = correctAnswer;
		this.wrongAnswer = wrongAnswer;
		this.minPassMark = minPassMark;
		this.decimalPlaceInteger = decimalPlaceInteger;
		this.active = active;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public String getModelName() {
		return modelName;
	}

	public Float getCorrectAnswer() {
		return correctAnswer;
	}

	public Float getWrongAnswer() {
		return wrongAnswer;
	}

	public Float getMinPassMark() {
		return minPassMark;
	}

	public Long getDecimalPlaceInteger() {
		return decimalPlaceInteger;
	}

	public Boolean getActive() {
		return active;
	}

	public User getUser() {
		return user;
	}

	public Long getUserId() {
		return userId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public void setCorrectAnswer(Float correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public void setWrongAnswer(Float wrongAnswer) {
		this.wrongAnswer = wrongAnswer;
	}

	public void setMinPassMark(Float minPassMark) {
		this.minPassMark = minPassMark;
	}

	public void setDecimalPlaceInteger(Long decimalPlaceInteger) {
		this.decimalPlaceInteger = decimalPlaceInteger;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
