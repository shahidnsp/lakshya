package com.lakshya.lakshya.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "course_fee_structures")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class CourseFeeStructure {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "name is mandatory")
	private String name;

	@DecimalMin(value = "0.1", inclusive = true)
	private BigDecimal amount;

	@Nullable
	private Long duration;

	@Nullable
	private Long noofemi;

	@Nullable
	private String repeatemitype;

	@Nullable
	private Long after_every;

	@Nullable
	private Long dayofmonth;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "course_id", nullable = false ) 
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Course course;

	@Column(name = "course_id", insertable = false, updatable = false)
	private Long courseId;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public CourseFeeStructure() {
		// TODO Auto-generated constructor stub
	}

	public CourseFeeStructure(String name, BigDecimal amount, Long duration, Long noofemi, String repeatemitype,
			Long after_every, Long dayofmonth, Course course) {
		this.name = name;
		this.amount = amount;
		this.duration = duration;
		this.noofemi = noofemi;
		this.repeatemitype = repeatemitype;
		this.after_every = after_every;
		this.dayofmonth = dayofmonth;
		this.course = course;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Long getNoofemi() {
		return noofemi;
	}

	public void setNoofemi(Long noofemi) {
		this.noofemi = noofemi;
	}

	public String getRepeatemitype() {
		return repeatemitype;
	}

	public void setRepeatemitype(String repeatemitype) {
		this.repeatemitype = repeatemitype;
	}

	public Long getAfterEvery() {
		return after_every;
	}

	public void setAfterEvery(Long after_every) {
		this.after_every = after_every;
	}

	public Long getDayofmonth() {
		return dayofmonth;
	}

	public void setDayOfMonth(Long dayofmonth) {
		this.dayofmonth = dayofmonth;
	}

	public void SetCourse(Course course) {
		this.course = course;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Course getCourse() {
		return course;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

}
