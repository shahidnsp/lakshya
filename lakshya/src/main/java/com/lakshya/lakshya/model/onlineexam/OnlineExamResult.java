package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Student;

@Entity
@Indexed
@Table(name = "online_exam_results")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OnlineExamResult implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	//@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_exam_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	private OnlineExam onlineexam;

	@Column(name = "online_exam_id", insertable = false, updatable = false)
	private Long onlineExamId;
	
	//@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "student_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	private Student student;

	@Column(name = "student_id", insertable = false, updatable = false)
	private Long student_id;
	
	
	private Integer noofanswered;
	
	private Integer noofunanswered;
	
	private Integer noofcorrect;
	
	private Integer noofincorrect;
	
	private Float totalcorrectmark;
	
	private Float totalnegativemark;
	
	private Float totalmark;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OnlineExamResult() {}
	
	public OnlineExamResult(OnlineExam onlineexam, Student student, Integer noofanswered,Integer noofunanswered,Integer noofcorrect,Integer noofincorrect
			,Float totalcorrectmark,Float totalnegativemark,Float totalmark) {
		super();		
		this.onlineexam = onlineexam;
		this.student = student;
		this.noofanswered = noofanswered;	
		this.noofunanswered = noofunanswered;	
		this.totalmark = totalmark;	
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	/*public OnlineExam getOnlineExam() {
		return onlineexam;
	}*/

	public void setOnlineExam(OnlineExam onlineexam) {
		this.onlineexam = onlineexam;
	}
	
	public Long getOnlineExamId() {
		return onlineExamId;
	}
	
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Long getStudentId() {
		return student_id;
	}
	
	public Integer getNoOfAnswered() {
		return noofanswered;
	}

	public void setNoOfAnswered(Integer noofanswered) {
		this.noofanswered = noofanswered;
	}
	
	public Integer getNoOfUnAnswered() {
		return noofunanswered;
	}

	public void setNoOfUnAnswered(Integer noofunanswered) {
		this.noofunanswered = noofunanswered;
	}
	
	public Integer getNoOfCorrect() {
		return noofcorrect;
	}

	public void setNoOfCorrect(Integer noofcorrect) {
		this.noofcorrect = noofcorrect;
	}
	
	public Integer getNoOfInCorrect() {
		return noofincorrect;
	}

	public void setNoOfInCorrect(Integer noofincorrect) {
		this.noofincorrect = noofincorrect;
	}
	
	public Float getTotalCorrectMark() {
		return totalcorrectmark;
	}

	public void setTotalCorrectMark(Float totalcorrectmark) {
		this.totalcorrectmark = totalcorrectmark;
	}
	
	public Float getTotalNegativeMark() {
		return totalnegativemark;
	}

	public void setTotalNegativeMark(Float totalnegativemark) {
		this.totalnegativemark = totalnegativemark;
	}
	
	public Float getTotalMark() {
		return totalmark;
	}

	public void setTotalMark(Float totalmark) {
		this.totalmark = totalmark;
	}
	
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
