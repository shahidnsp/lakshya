package com.lakshya.lakshya.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "student_guardians")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class StudentGuardian {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Please provide Guardian name")
	private String name;

	@Nullable
	private String relation;

	@Nullable
	private String adharno;

	@Nullable
	private String email;

	@Nullable
	private String occupation;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "student_id", nullable = false )//, insertable = false , updatable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Student student;

	@Column(name = "student_id", insertable = false, updatable = false)
	private Long studentId;

	public StudentGuardian() {}

	public StudentGuardian(String name, String relation, String adharno, String email, String occupation)/*Student student*/ {
		this.name = name;
		this.relation = relation;
		this.adharno = adharno;
		this.email = email;
		this.occupation = occupation;
//		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getAdharno() {
		return adharno;
	}

	public void setAdhar(String adharno) {
		this.adharno = adharno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	/*public Student getStudent() {
		return student;
	}*/

	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Long getStudentId() {
		return studentId;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "student_guardian_id")
	private Set<StudentGuardianPhone> studentGuardianPhones;
	
	public Set<StudentGuardianPhone> getStudentGuardianPhones() {
		return studentGuardianPhones;
	}

	public void setStudentGuadianPhone(Set<StudentGuardianPhone> studentGuardianPhones) {
		this.studentGuardianPhones = studentGuardianPhones;
	}
	
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

}
