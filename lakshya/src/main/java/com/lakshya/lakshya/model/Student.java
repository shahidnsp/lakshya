package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "students")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
//@JsonIgnoreProperties(value = { "createdAt", "updatedAt","enquiryStage" }, allowSetters = true)
public class Student implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Name Required.")
	private String name;

	@Nullable
	private String rollno;

	@Nullable
	private String photo;

	@Nullable
	private String gender;

	@Nullable
	private String religion;

	@Nullable
	private String caste;

	@Nullable
	private String category;

	@Nullable
	private String adharno;

	@Nullable
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateofbirth;

	@Nullable
	private String address;

	@Nullable
	private String area;

	@Nullable
	private String landmark;

	@Nullable
	private String city;

	@Nullable
	private String state;

	@Nullable
	private String pincode;

	@Nullable
	private String email;

	@Nullable
	private String status = "Enquiry";

	@Nullable
	private Boolean active = true;

	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "user_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private User user;

	@Column(name = "user_id", insertable = false, updatable = false)
	private Long userId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "batch_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Batch batch;

	@Column(name = "batch_id", insertable = false, updatable = false)
	private Long batchId;

	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "branch_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Branch branch;

	@Column(name = "branch_id", insertable = false, updatable = false)
	private Long branchId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "course_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Course course;

	@Column(name = "course_id", insertable = false, updatable = false)
	private Long courseId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "school_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private School school;

	@Column(name = "school_id", insertable = false, updatable = false)
	private Long schoolId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "loss_reason_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private LossReason lossReason;

	@Column(name = "loss_reason_id", insertable = false, updatable = false)
	private Long lossreasonId;

	/*
	 * @ManyToOne
	 * 
	 * @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) private User
	 * owner;
	 * 
	 * @Column(name = "owner_id", insertable = false, updatable = false) private
	 * Long owner_id;
	 */

	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private EnquiryStage enquiryStage;

	@Column(name = "enquiry_stage_id", insertable = false, updatable = false)
	private Long enquiryStageId;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "student_id")
	private Set<StudentPhone> studentPhones;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "student_id")
	private Set<StudentLandLine> studentLandLines;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "student_id")
//	@OnDelete(action = OnDeleteAction.CASCADE)
	private Set<StudentGuardian> studentGuardians;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	@Nullable
	@Temporal(TemporalType.TIMESTAMP)
	private Date closedOn;

	public Student() {
		// TODO Auto-generated constructor stub
	}

	public Student(String name, String rollno, String photo, String gender, String relegion, String caste,
			String adharno, Date dateofbirth, String address, String landmark, String city, String pincode,
			String email, User user) {
		this.name = name;
		this.rollno = rollno;
		this.photo = photo;
		this.gender = gender;
		this.religion = relegion;
		this.caste = caste;
		this.adharno = adharno;
		this.dateofbirth = dateofbirth;
		this.address = address;
		this.landmark = landmark;
		this.city = city;
		this.pincode = pincode;
		this.email = email;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRollno() {
		return rollno;
	}

	public void setRollno(String rollno) {
		this.rollno = rollno;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getAdharno() {
		return adharno;
	}

	public void setAdharno(String adharno) {
		this.adharno = adharno;
	}

	public Date getDateofbirth() {
		return dateofbirth;
	}

	public void setDateofbirth(Date dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getBatchId() {
		return batchId;
	}

	public Batch getBatch() {
		return batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public Branch getBranch() {
		return branch;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public School getSchool() {
		return school;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public LossReason getLossReason() {
		return lossReason;
	}

	public Long getLossReasonId() {
		return lossreasonId;
	}

	public void setLossReason(LossReason lossReason) {
		this.lossReason = lossReason;
	}

	/*
	 * public Long getOwnerId() { return owner_id; }
	 * 
	 * public User getOwner() { return owner; }
	 * 
	 * public void setOwner(User owner) { this.owner = owner; }
	 */

	public Long getEnquiryStageId() {
		return enquiryStageId;
	}

	public EnquiryStage getEnquiryStage() {
		return enquiryStage;
	}

	public void setEnquiryStage(EnquiryStage enquiryStage) {
		this.enquiryStage = enquiryStage;
	}

	public Set<StudentLandLine> getStudentLandlines() {
		return studentLandLines;
	}

	public void setStudentLandlines(Set<StudentLandLine> studentLandLines) {
		this.studentLandLines = studentLandLines;
	}

	public Date getClosedOn() {
		return closedOn;
	}

	public void setClosedOn(Date closedOn) {
		this.closedOn = closedOn;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setEnquiryStageId(Long enquiryStageId) {
		this.enquiryStageId = enquiryStageId;
	}

	public Set<StudentGuardian> getStudentGuardians() {
		return studentGuardians;
	}

	public void setStudentGuadians(Set<StudentGuardian> studentGuardians) {
		this.studentGuardians = studentGuardians;
	}

	public Set<StudentPhone> getStudentPhones() {
		return studentPhones;
	}

	public void setStudentPhones(Set<StudentPhone> studentPhones) {
		this.studentPhones = studentPhones;
	}

	public Course getCourse() {
		return course;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	
}
