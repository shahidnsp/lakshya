package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Subject;

@Entity
@Indexed
@Table(name = "online_exam_questions")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
//@JsonFilter("filter properties by name")
public class OnlineExamQuestion implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

//	@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_exam_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private OnlineExam onlineExam;

	@Column(name = "online_exam_id", insertable = false, updatable = false)
	private Long onlineExamId;
	
	
//	@IndexedEmbedded
	@ManyToOne//(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_question_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private OnlineQuestion  onlineQuestion;

	@Column(name = "online_question_id", insertable = false, updatable = false)
	private Long onlineQuestionId;

	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OnlineExamQuestion() {}
	
	public OnlineExamQuestion(Long onlineExamId, Long onlineQuestionId) {
		super();
		this.onlineExamId = onlineExamId;
		this.onlineQuestionId = onlineQuestionId;
	}

	public Long getId() {
		return id;
	}

	public OnlineExam getOnlineExam() {
		return onlineExam;
	}

	public Long getOnlineExamId() {
		return onlineExamId;
	}

	public OnlineQuestion getOnlineQuestion() {
		return onlineQuestion;
	}

	public Long getOnlineQuestionId() {
		return onlineQuestionId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOnlineExam(OnlineExam onlineExam) {
		this.onlineExam = onlineExam;
	}

	public void setOnlineExamId(Long onlineExamId) {
		this.onlineExamId = onlineExamId;
	}

	public void setOnlineQuestion(OnlineQuestion onlineQuestion) {
		this.onlineQuestion = onlineQuestion;
	}

	public void setOnlineQuestionId(Long onlineQuestionId) {
		this.onlineQuestionId = onlineQuestionId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
