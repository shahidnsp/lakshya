package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.Subject;

@Entity
@Indexed
@Table(name = "online_question_parts")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OnlineQuestionPart implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
		
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "questions_type")
//	@Nullable
	private String questionType;
	
	@Field(name = "question_text")
	@Nullable
	private String questionText;	
	
	
//	@IndexedEmbedded
//	@ManyToOne(fetch = FetchType.LAZY, optional = false , cascade = CascadeType.REMOVE)
//	@JoinColumn(name = "online_question_id", nullable = false ) 
//	@OnDelete(action = OnDeleteAction.CASCADE)
//	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
//	private OnlineQuestion onlineQuestion;

//	@Column(name = "online_question_part_id")
	
	@Column(name = "online_question_id",insertable = false, updatable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Long onlineQuestionId;
	
	@Nullable
	private float mark=2;
	
	@CreationTimestamp	
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

//	@IndexedEmbedded
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "online_question_part_id")
//	@OnDelete(action = OnDeleteAction.CASCADE)
	@OrderBy("id DESC")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Set<OnlineQuestionOption> onlineQuestionOptions;

	public Set<OnlineQuestionOption> getOnlineQuestionOptions() {
		return onlineQuestionOptions;
	}
	
	public void setOnlineQuestionOptions(Set<OnlineQuestionOption> onlineQuestionOptions) {
		this.onlineQuestionOptions = onlineQuestionOptions;
	}
	
	public OnlineQuestionPart(){}
	
	public OnlineQuestionPart(String questionType, String questionText) {
		super();
		this.questionType = questionType;
		this.questionText = questionText;
	}

	public Long getId() {
		return id;
	}

	public String getQuestionType() {
		return questionType;
	}

	public String getQuestionText() {
		return questionText;
	}
	

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	
	

//	public OnlineQuestion getOnlineQuestion() {
//		return onlineQuestion;
//	}
//
//	public void setOnlineQuestion(OnlineQuestion onlineQuestion) {
//		this.onlineQuestion = onlineQuestion;
//	}

	public Long getOnlineQuestionId() {
		return onlineQuestionId;
	}

	public void setOnlineQuestionId(Long onlineQuestionId) {
		this.onlineQuestionId = onlineQuestionId;
	}
	
	public float getMark() {
		return mark;
	}

	public void setMark(float mark) {
		this.mark = mark;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
