package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;

@Entity
@Indexed
@Table(name = "offline_answer_sheet_omr")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineAnswerSheetOMR implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "student_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Student student;
	
	@Column(name = "student_id", insertable = false, updatable = false)
	private Long studentId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "subject_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Subject subject;
	
	@Column(name = "subject_id", insertable = false, updatable = false)
	private Long subjectId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExam offlineExam;
	
	@Column(name = "offline_exam_id", insertable = false, updatable = false)
	private Long offlineExamId;
	
//	@Column(name = "vercode")	
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "vercode")
	private String verCode;
	
	@Column(name = "total_correct_mark")	
	private Float totalCorrectMark;
	
	@Column(name = "no_of_correct")	
	private Long noOfCorrect;
	
	@Column(name = "total_negative_mark")	
	private Float totalNegativeMark;
	
	@Column(name = "no_of_negative")	
	private Long noOfNegative;
	
	@Column(name = "no_of_blank")	
	private Long noOfBlank;
	
	@Column(name = "total_mark")	
	private Float totalMark;
	
	@Column(name = "total_dqfd")	
	private Long totalDQFD;
	
	@Column(name = "rank")	
	private Long rank;
	
	@Column(name = "status")	
	private String status;
	
//	@Column(name = "sub_total_string")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "sub_total_string")
	private String subTotalString;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "user_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private User user;
	
	@Column(name = "user_id", insertable = false, updatable = false)
	private Long userId;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OfflineAnswerSheetOMR(){}

	public OfflineAnswerSheetOMR(Long studentId,Long subjectId, Long offlineExamId, String verCode, Float totalCorrectMark,
			Long noOfCorrect, Float totalNegativeMark, Long noOfNegative, Long noOfBlank, Float totalMark,
			Long totalDQFD, Long rank, String status, String subTotalString, Long userId, LocalDateTime createdAt,
			LocalDateTime updatedAt) {
		super();
		this.subjectId = subjectId;
		this.offlineExamId = offlineExamId;
		this.verCode = verCode;
		this.totalCorrectMark = totalCorrectMark;
		this.noOfCorrect = noOfCorrect;
		this.totalNegativeMark = totalNegativeMark;
		this.noOfNegative = noOfNegative;
		this.noOfBlank = noOfBlank;
		this.totalMark = totalMark;
		this.totalDQFD = totalDQFD;
		this.rank = rank;
		this.status = status;
		this.subTotalString = subTotalString;
		this.userId = userId;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public Long getId() {
		return id;
	}

	public Subject getSubject() {
		return subject;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public OfflineExam getOfflineExam() {
		return offlineExam;
	}

	public Long getOfflineExamId() {
		return offlineExamId;
	}

	public String getVerCode() {
		return verCode;
	}

	public Float getTotalCorrectMark() {
		return totalCorrectMark;
	}

	public Long getNoOfCorrect() {
		return noOfCorrect;
	}

	public Float getTotalNegativeMark() {
		return totalNegativeMark;
	}

	public Long getNoOfNegative() {
		return noOfNegative;
	}

	public Long getNoOfBlank() {
		return noOfBlank;
	}

	public Float getTotalMark() {
		return totalMark;
	}

	public Long getTotalDQFD() {
		return totalDQFD;
	}

	public Long getRank() {
		return rank;
	}

	public String getStatus() {
		return status;
	}

	public String getSubTotalString() {
		return subTotalString;
	}

	public User getUser() {
		return user;
	}

	public Long getUserId() {
		return userId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Student getStudent() {
		return student;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public void setOfflineExam(OfflineExam offlineExam) {
		this.offlineExam = offlineExam;
	}

	public void setOfflineExamId(Long offlineExamId) {
		this.offlineExamId = offlineExamId;
	}

	public void setVerCode(String verCode) {
		this.verCode = verCode;
	}

	public void setTotalCorrectMark(Float totalCorrectMark) {
		this.totalCorrectMark = totalCorrectMark;
	}

	public void setNoOfCorrect(Long noOfCorrect) {
		this.noOfCorrect = noOfCorrect;
	}

	public void setTotalNegativeMark(Float totalNegativeMark) {
		this.totalNegativeMark = totalNegativeMark;
	}

	public void setNoOfNegative(Long noOfNegative) {
		this.noOfNegative = noOfNegative;
	}

	public void setNoOfBlank(Long noOfBlank) {
		this.noOfBlank = noOfBlank;
	}

	public void setTotalMark(Float totalMark) {
		this.totalMark = totalMark;
	}

	public void setTotalDQFD(Long totalDQFD) {
		this.totalDQFD = totalDQFD;
	}

	public void setRank(Long rank) {
		this.rank = rank;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setSubTotalString(String subTotalString) {
		this.subTotalString = subTotalString;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
