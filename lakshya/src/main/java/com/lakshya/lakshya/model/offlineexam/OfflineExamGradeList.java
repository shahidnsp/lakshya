package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.User;

@Entity
@Indexed
@Table(name = "offline_exam_grade_lists")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineExamGradeList implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

//	@Field(name="name")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "code")
	@NotEmpty(message = "code Required.")	
	private String code;

	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "to_ending")
	private Long toEnding;
	
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "from_starting")
	private Long fromStarting;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_grade_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExamGrade offlineExamGrade;

	@Column(name = "offline_exam_grade_id", insertable = false, updatable = false)	
	private Long offlineExamGradeId;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OfflineExamGradeList() {}

	public OfflineExamGradeList(Long id, @NotEmpty(message = "code Required.") String code, Long to, Long from,
			Long offlineExamGradeId) {
		super();
		this.id = id;
		this.code = code;
		this.toEnding = to;
		this.fromStarting = from;
		this.offlineExamGradeId = offlineExamGradeId;
	}

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}
	
	public OfflineExamGrade getOfflineExamGrade() {
		return offlineExamGrade;
	}

	public Long getOfflineExamGradeId() {
		return offlineExamGradeId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setOfflineExamGrade(OfflineExamGrade offlineExamGrade) {
		this.offlineExamGrade = offlineExamGrade;
	}

	public void setOfflineExamGradeId(Long offlineExamGradeId) {
		this.offlineExamGradeId = offlineExamGradeId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getToEnding() {
		return toEnding;
	}

	public Long getFromStarting() {
		return fromStarting;
	}

	public void setToEnding(Long toEnding) {
		this.toEnding = toEnding;
	}

	public void setFromStarting(Long fromStarting) {
		this.fromStarting = fromStarting;
	}
	
}
