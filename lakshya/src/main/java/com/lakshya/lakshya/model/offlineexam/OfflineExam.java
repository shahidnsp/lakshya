package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;

@Entity
@Indexed
@Table(name = "offline_exams")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineExam implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "name")
	@NotEmpty(message = "Offline Exam Grades name Required.")	
	private String name;
	
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "exam_type")		
	private String examType;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_grades_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExamGrade offlineExamGrade;

	@Column(name = "offline_exam_grades_id", insertable = false, updatable = false)	
	private Long offlineExamGradeId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_models_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExamModel offlineExamModel;

	@Column(name = "offline_exam_models_id", insertable = false, updatable = false)	
	private Long offlineExamModelId;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "course_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Course course;

	@Column(name = "course_id", insertable = false, updatable = false)	
	private Long courseId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "subject_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Subject subject;

	@Column(name = "subject_id", insertable = false, updatable = false)	
	private Long subjectId;
	
	@Column(name = "no_of_questions")	
	private Long noOfQuestions;
	
	@Column(name = "score")	
	private Float score;
	
	@Column(name = "total_score")	
	private Float totalScore;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "user_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private User user;

	@Column(name = "user_id", insertable = false, updatable = false)	
	private Long userId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "branch_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Branch branch;

	@Column(name = "branch_id", insertable = false, updatable = false)	
	private Long branchId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "academic_year_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private AcademicYear academicYear;
		
	@Column(name = "academic_year_id", insertable = false, updatable = false)	
	private Long academicYearId;
	
	@Column(name = "steps")	
	private Long steps;
	
	@Nullable		
	private Boolean active;
	
	@Nullable		
	private Boolean report;
	
	@Nullable		
	private Boolean web;
	
	@Nullable		
	private Boolean sms;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OfflineExam(){}

	public OfflineExam(Long id, @NotEmpty(message = "Offline Exam name Required.") String name, String examType,
			Long offlineExamGradeId, Long offlineExamModelId, Long courseId, Long subjectId, Long noOfQuestions,
			Float score, Float totalScore, Date examDate, LocalDateTime publishDate, LocalDateTime publishDateWeb,
			LocalDateTime publishDateSms, Long userId, Long branchId, Long academicYearId, Long steps, Boolean active,
			Boolean report, Boolean web, Boolean sms) {
		super();
		this.id = id;
		this.name = name;
		this.examType = examType;
		this.offlineExamGradeId = offlineExamGradeId;
		this.offlineExamModelId = offlineExamModelId;
		this.courseId = courseId;
		this.subjectId = subjectId;
		this.noOfQuestions = noOfQuestions;
		this.score = score;
		this.totalScore = totalScore;
		this.examDate = examDate;
		this.publishDate = publishDate;
		this.publishDateWeb = publishDateWeb;
		this.publishDateSms = publishDateSms;
		this.userId = userId;
		this.branchId = branchId;
		this.academicYearId = academicYearId;
		this.steps = steps;
		this.active = active;
		this.report = report;
		this.web = web;
		this.sms = sms;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getExamType() {
		return examType;
	}

	public OfflineExamGrade getOfflineExamGrade() {
		return offlineExamGrade;
	}

	public Long getOfflineExamGradeId() {
		return offlineExamGradeId;
	}

	public OfflineExamModel getOfflineExamModel() {
		return offlineExamModel;
	}

	public Long getOfflineExamModelId() {
		return offlineExamModelId;
	}

	public Course getCourse() {
		return course;
	}

	public Long getCourseId() {
		return courseId;
	}

	public Subject getSubject() {
		return subject;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public Long getNoOfQuestions() {
		return noOfQuestions;
	}

	public Float getScore() {
		return score;
	}

	public Float getTotalScore() {
		return totalScore;
	}

	public User getUser() {
		return user;
	}

	public Long getUserId() {
		return userId;
	}

	public Branch getBranch() {
		return branch;
	}

	public Long getBranchId() {
		return branchId;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public Long getSteps() {
		return steps;
	}

	public Boolean getActive() {
		return active;
	}

	public Boolean getReport() {
		return report;
	}

	public Boolean getWeb() {
		return web;
	}

	public Boolean getSms() {
		return sms;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setExamType(String examType) {
		this.examType = examType;
	}

	public void setOfflineExamGrade(OfflineExamGrade offlineExamGrade) {
		this.offlineExamGrade = offlineExamGrade;
	}

	public void setOfflineExamGradeId(Long offlineExamGradeId) {
		this.offlineExamGradeId = offlineExamGradeId;
	}

	public void setOfflineExamModel(OfflineExamModel offlineExamModel) {
		this.offlineExamModel = offlineExamModel;
	}

	public void setOfflineExamModelId(Long offlineExamModelId) {
		this.offlineExamModelId = offlineExamModelId;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public void setNoOfQuestions(Long noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public void setTotalScore(Float totalScore) {
		this.totalScore = totalScore;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public void setSteps(Long steps) {
		this.steps = steps;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setReport(Boolean report) {
		this.report = report;
	}

	public void setWeb(Boolean web) {
		this.web = web;
	}

	public void setSms(Boolean sms) {
		this.sms = sms;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "exam_date")//, insertable = false, updatable = false)	
	private Date examDate;
	
	@Column(name = "publish_date")//, insertable = false, updatable = false)	
	private LocalDateTime publishDate;
	
	@Column(name = "publish_date_web")//, insertable = false, updatable = false)	
	private LocalDateTime publishDateWeb;
	
	@Column(name = "publish_date_sms")//, insertable = false, updatable = false)	
	private LocalDateTime publishDateSms;
	
	public Date getExamDate() {
		return examDate;
	}

	public LocalDateTime getPublishDate() {
		return publishDate;
	}

	public LocalDateTime getPublishDateWeb() {
		return publishDateWeb;
	}

	public LocalDateTime getPublishDateSms() {
		return publishDateSms;
	}
	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public void setPublishDate(LocalDateTime publishDate) {
		this.publishDate = publishDate;
	}

	public void setPublishDateWeb(LocalDateTime publishDateWeb) {
		this.publishDateWeb = publishDateWeb;
	}

	public void setPublishDateSms(LocalDateTime publishDateSms) {
		this.publishDateSms = publishDateSms;
	}
	
}
