package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Indexed
@Table(name = "online_question_sections")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OnlineQuestionSection implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Nullable
	@Column(name = "section_text",length=10000)
	private String sectionText;

	@Nullable
	@Column(name = "name")
	private String name;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_exam_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private OnlineExam onlineExam;

	@Column(name = "online_exam_id", insertable = false, updatable = false)
	private Long onlineExamId;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "online_question_section_id")
//	@OnDelete(action = OnDeleteAction.CASCADE)
	@OrderBy("id ASC")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Set<OnlineQuestionSectionItem> onlineQuestionSectionItems;

	@Nullable
	@Column(name = "order_by")
	private Long orderBy;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public OnlineQuestionSection() {
	}

	public OnlineQuestionSection(Long id, String sectionText) {
		super();
		this.id = id;
		this.sectionText = sectionText;
	}

	public Long getId() {
		return id;
	}

	public String getSectionText() {
		return sectionText;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setSectionText(String sectionText) {
		this.sectionText = sectionText;
	}
	
	public void setOnlineExam(OnlineExam onlineExam) {
		this.onlineExam = onlineExam;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Long getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Long orderBy) {
		this.orderBy = orderBy;
	}
	
	public Set<OnlineQuestionSectionItem> getOnlineQuestionSectionItems() {
		return onlineQuestionSectionItems;
	}

	public void setOnlineQuestionSectionItems(Set<OnlineQuestionSectionItem> onlineQuestionSectionItems) {
		this.onlineQuestionSectionItems = onlineQuestionSectionItems;
	}

}
