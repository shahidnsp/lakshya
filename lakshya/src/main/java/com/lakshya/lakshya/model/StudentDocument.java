package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "student_documents")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class StudentDocument  implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )// , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "student_id", nullable = false )//, insertable = false , updatable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Student student;
	
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false )//, insertable = false , updatable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private User user;
	
//	@NotEmpty(message = "Student_Id Required.")
	@Column(name="student_id",insertable = false, updatable = false)
	private Long studentId;
	
//	@NotEmpty(message = "User_Id Required.")
	@Column(name="user_id",insertable = false, updatable = false)
	private Long userId;

	@Nullable
	private String file;
	
	@Nullable
	private String remarks;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public Long getId() {
		return id;
	}

	public String getFile() {
		return file;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public void setStudent_Id(Long student_Id) {
//		this.student_Id = student_Id;
//	}

	public void setFile(String file) {
		this.file = file;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Student getStudent() {
		return student;
	}

	public User getUser() {
		return user;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public StudentDocument() {}
	public StudentDocument(Student student, User user, String file, String remarks) {
		super();
		this.student = student;
		this.user = user;
		this.file = file;
		this.remarks = remarks;
	}

	public Long getStudentId() {
		return studentId;
	}

	public Long getUserId() {
		return userId;
	}
	
}
