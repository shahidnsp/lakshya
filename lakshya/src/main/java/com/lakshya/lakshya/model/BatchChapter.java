package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "batch_chapters")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},allowGetters = true)

public class BatchChapter implements Serializable{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
		 	
    @Nullable
    private String hours;
    
    @CreationTimestamp
    private LocalDateTime createdAt;
 
    @UpdateTimestamp
    private LocalDateTime updatedAt;
    
    @OneToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private	Batch batch;
    
    @Column(name = "batch_id", insertable = false, updatable = false)
    private Long batch_id;
    
    @OneToOne
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private	Chapter chapter;
    
    @Column(name = "chapter_id", insertable = false, updatable = false)
    private Long chapter_id;
    
    
    public BatchChapter() {
		// TODO Auto-generated constructor stub
	}
    
    public BatchChapter(Batch batch,Chapter chapter,String hours) {
    	this.batch=batch;
    	this.chapter=chapter;
    	this.hours=hours;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getHours() {
        return hours;
    }

    public void sethours(String hours) {
        this.hours = hours;
    }
    
    public Long getBatchId() {
        return batch_id;
    }
    
    public void setbatchId(Long batch_id) {
        this.batch_id=batch_id;
    }

    public Batch getBatch() {
        return batch;
    }

    public void setBatch(Batch batch) {
        this.batch = batch;
    }
    
    
    
}
