package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
//import org.hibernate.annotations.Cascade;
//import org.hibernate.annotations.CascadeType;
//import org.hibernate.annotations.CreationTimestamp;
//import org.hibernate.annotations.OnDelete;
//import org.hibernate.annotations.OnDeleteAction;
//import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.model.BranchLandLine;
import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.model.StudentPhone;
import com.lakshya.lakshya.model.Subject;

@Entity
@Indexed
@Table(name = "online_questions")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OnlineQuestion implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "course_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Course course;

	@Column(name = "course_id", insertable = false, updatable = false)
	private Long courseId;

	@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "subject_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Subject subject;

	@Column(name = "subject_id", insertable = false, updatable = false)
	private Long subjectId;

	@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "faculty_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Faculty faculty;

	@Column(name = "faculty_id", insertable = false, updatable = false)
	private Long facultyId;

	@IndexedEmbedded // Added for search removed in previous updation
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "question_year_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private QuestionYear questionYear;

	@Column(name = "question_year_id", insertable = false, updatable = false)
	private Long questionYearId;

	public QuestionYear getQuestionYear() {
		return questionYear;
	}

	public void setQuestionYear(QuestionYear questionYear) {
		this.questionYear = questionYear;
	}

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "difficultyLevel")
	private String difficultyLevel;

	/*
	 * @Nullable
	 * 
	 * @Field(analyzer = @Analyzer(definition = "onlinelowercase"), name =
	 * "solutions") private String solutions;
	 */
	@Nullable
	private Long mark = (long) 2;

	@Nullable
	private Boolean active = true;

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "format")
	private String format;

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "questionFor")
	private String questionFor;

//	@IndexedEmbedded
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "online_question_id")
//	@OnDelete(action = OnDeleteAction.CASCADE)
	@OrderBy("id ASC")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Set<OnlineQuestionPart> onlineQuestionPart;

	public Set<OnlineQuestionPart> getOnlineQuestionPart() {
		return onlineQuestionPart;
	}

	public void setOnlineQuestionPart(Set<OnlineQuestionPart> onlineQuestionPart) {
		this.onlineQuestionPart = onlineQuestionPart;
	}

	@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "online_question_topic_id", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private OnlineQuestionTopic onlineQuestionTopic;

	@Column(name = "online_question_topic_id", insertable = false, updatable = false)
	@Nullable
	private Long onlineQuestionTopicId;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public OnlineQuestion() {
	}

	public OnlineQuestion(Course course, Long courseId, Subject subject, Long subjectId, Long facultyId,
			QuestionYear questionYear, Long questionYearId, String difficultyLevel, Boolean active) {
		super();
		this.difficultyLevel = difficultyLevel;
//		this.solutions = solutions;
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Course getCourse() {
		return course;
	}

	public Long getCourseId() {
		return courseId;
	}

	public Subject getSubject() {
		return subject;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public Long getFacultyId() {
		return facultyId;
	}

	public String getQuestionFor() {
		return questionFor;
	}

	public void setQuestionFor(String questionFor) {
		this.questionFor = questionFor;
	}

	public Long getQuestionYearId() {
		return questionYearId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public String getDifficultyLevel() {
		return difficultyLevel;
	}

//	public String getSolutions() {
//		return solutions;
//	}

	public Boolean getActive() {
		return active;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public void setFacultyId(Long facultyId) {
		this.facultyId = facultyId;
	}

	public void setQuestionYearId(Long questionYearId) {
		this.questionYearId = questionYearId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setDifficultyLevel(String difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getFormat() {
		return format;
	}

//	public void setSolutions(String solutions) {
//		this.solutions = solutions;
//	}

	public void setFormat(String format) {
		this.format = format;
	}

	public OnlineQuestionTopic getOnlineQuestionTopic() {
		return onlineQuestionTopic;
	}

	public void setOnlineQuestionTopic(OnlineQuestionTopic onlineQuestionTopic) {
		this.onlineQuestionTopic = onlineQuestionTopic;
	}

	public Long getOnlineQuestionTopicId() {
		return onlineQuestionTopicId;
	}

	public void setOnlineQuestionTopicId(Long onlineQuestionTopicId) {
		this.onlineQuestionTopicId = onlineQuestionTopicId;
	}

	public Long getMark() {
		return mark;
	}

	public void setMark(Long mark) {
		this.mark = mark;
	}

}
