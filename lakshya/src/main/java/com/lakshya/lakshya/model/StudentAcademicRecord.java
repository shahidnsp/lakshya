package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "student_academic_records")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class StudentAcademicRecord  implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "student_id", nullable = false )//, insertable = false , updatable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Student student;
	
	@Column(name="student_id",insertable = false, updatable = false)
	private Long studentId;
		
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false )//, insertable = false , updatable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private User user;	
	
	@Nullable
	@Column(name="user_id",insertable = false, updatable = false)
	private Long userId;

	@Nullable
	private String stream;
	
	@Nullable
	private String month;
	
	@Nullable
	private String year;
	
	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "school_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private School school;

	@Column(name = "school_id", insertable = false, updatable = false)
	private Long schoolId;
	
	@Nullable
	private String mark_Or_Grade;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public StudentAcademicRecord(){}
	
	public StudentAcademicRecord(Student student, User user, String stream, String month, String year, School school,
			String mark_Or_Grade) {
		super();
		this.student = student;
		this.user = user;
		this.stream = stream;
		this.month = month;
		this.year = year;
		this.school = school;
		this.mark_Or_Grade = mark_Or_Grade;
	}

	public Long getId() {
		return id;
	}

	public Long getStudentId() {
		return studentId;
	}

	/*public Student getStudent() {
		return student;
	}*/

	public Long getUserId() {
		return userId;
	}

	public User getUser() {
		return user;
	}

	public String getStream() {
		return stream;
	}

	public String getMonth() {
		return month;
	}

	public String getYear() {
		return year;
	}

	public String getMark_Or_Grade() {
		return mark_Or_Grade;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	public void setUser(User user) {
		this.user = user;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public void setYear(String year) {
		this.year = year;
	}


	public void setMark_Or_Grade(String mark_Or_Grade) {
		this.mark_Or_Grade = mark_Or_Grade;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public School getSchool() {
		return school;
	}
	
	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchool(School school) {
		this.school = school;
	}
		
}
