package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Batch;

@Entity
@Indexed
@Table(name = "offline_exam_grade_lists")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineExamAssignBatch implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "batch_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Batch batch;

	@Column(name = "batch_id", insertable = false, updatable = false)	
	private Long batchId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExam offlineExam;

	@Column(name = "offline_exam_id", insertable = false, updatable = false)	
	private Long offlineExamId;	
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OfflineExamAssignBatch(){}

	public OfflineExamAssignBatch(Long id, Long batchId, Long offlineExamId) {
		super();
		this.id = id;
		this.batchId = batchId;
		this.offlineExamId = offlineExamId;
	}

	public Long getId() {
		return id;
	}

	public Batch getBatch() {
		return batch;
	}

	public Long getBatchId() {
		return batchId;
	}

	public OfflineExam getOfflineExam() {
		return offlineExam;
	}

	public Long getOfflineExamId() {
		return offlineExamId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public void setOfflineExam(OfflineExam offlineExam) {
		this.offlineExam = offlineExam;
	}

	public void setOfflineExamId(Long offlineExamId) {
		this.offlineExamId = offlineExamId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
