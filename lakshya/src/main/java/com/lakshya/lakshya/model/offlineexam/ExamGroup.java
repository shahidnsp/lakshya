package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.bridge.builtin.FloatBridge;
import org.hibernate.search.bridge.builtin.LongBridge;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Subject;

//@AnalyzerDef(name = "numbercase",
//tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
//filters = {
//		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
//		@TokenFilterDef(factory = SnowballPorterFilterFactory.class),
//		@TokenFilterDef(factory = NGramFilterFactory.class
//				,params = { @Parameter(name = "minGramSize", value = "2"),@Parameter(name = "maxGramSize", value = "5")})
//}
//)

@Entity
@Indexed
@Table(name = "exam_groups")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class ExamGroup implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exams_main_group_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExamMainGroup offlineExamMainGroup;
	
	@Column(name = "offline_exams_main_group_id", insertable = false, updatable = false)
	private Long offlineExamsMainGroupId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "subject_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Subject subject;

	@Column(name = "subject_id", insertable = false, updatable = false)	
	private Long subjectId;

	@Column(name = "no_of_questions")
	@FieldBridge(impl = LongBridge.class)	
	private Long noOfQuestion;
	
	@Column(name = "score")
	@FieldBridge(impl = FloatBridge.class)
	private Float score;
	
	@Column(name = "rank_order")
	private Long rankOrder;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExam offlineExam;

	@Column(name = "offline_exam_id", insertable = false, updatable = false)	
	private Long offlineExamId;	
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public ExamGroup() {}

	public ExamGroup(Long id, Long offlineExamsMainGroupId, Long subjectId, Long noOfQuestion, Float score,
			Long rankOrder, Long offlineExamId) {
		super();
		this.id = id;
		this.offlineExamsMainGroupId = offlineExamsMainGroupId;
		this.subjectId = subjectId;
		this.noOfQuestion = noOfQuestion;
		this.score = score;
		this.rankOrder = rankOrder;
		this.offlineExamId = offlineExamId;
	}

	public Long getId() {
		return id;
	}

	public OfflineExamMainGroup getOfflineExamMainGroup() {
		return offlineExamMainGroup;
	}

	public Long getOfflineExamsMainGroupId() {
		return offlineExamsMainGroupId;
	}

	public Subject getSubject() {
		return subject;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public Long getNoOfQuestion() {
		return noOfQuestion;
	}

	public Float getScore() {
		return score;
	}

	public Long getRankOrder() {
		return rankOrder;
	}

	public OfflineExam getOfflineExam() {
		return offlineExam;
	}

	public Long getOfflineExamId() {
		return offlineExamId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOfflineExamMainGroup(OfflineExamMainGroup offlineExamMainGroup) {
		this.offlineExamMainGroup = offlineExamMainGroup;
	}

	public void setOfflineExamsMainGroupId(Long offlineExamsMainGroupId) {
		this.offlineExamsMainGroupId = offlineExamsMainGroupId;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public void setNoOfQuestion(Long noOfQuestion) {
		this.noOfQuestion = noOfQuestion;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public void setRankOrder(Long rankOrder) {
		this.rankOrder = rankOrder;
	}

	public void setOfflineExam(OfflineExam offlineExam) {
		this.offlineExam = offlineExam;
	}

	public void setOfflineExamId(Long offlineExamId) {
		this.offlineExamId = offlineExamId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
	
}
