package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.snowball.SnowballPorterFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@AnalyzerDef(name = "onlinelowercase", tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class), filters = {
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = SnowballPorterFilterFactory.class),
		@TokenFilterDef(factory = NGramFilterFactory.class, params = { @Parameter(name = "minGramSize", value = "2"),
				@Parameter(name = "maxGramSize", value = "5") }) })

@Entity
@Indexed
@Table(name = "faculties")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class Faculty implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Name Required")
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "name")
	private String name;

	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "address")
	@Nullable
	private String address;

	@Nullable
	private String area;

	@Nullable
	private String landmark;

	@Nullable
	private String city;

	@Nullable
	private String state;

	@Nullable
	private String pincode;

	@Nullable
	private String email;

	@Nullable
	private String photo;

	@Nullable
	private String facultyType;

	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "gender")
	@Nullable
	private String gender;

	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "phone1")
	@Nullable
	private String phone1;

	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "phone2")
	@Nullable
	private String phone2;

	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "phone3")
	@Nullable
	private String remark;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public Faculty() {
	}

	public Faculty(Long id, String name, String address, String phone1, String phone2, String remark) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getArea() {
		return area;
	}

	public String getLandmark() {
		return landmark;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getEmail() {
		return email;
	}

	public String getPhoto() {
		return photo;
	}

	public String getFacultyType() {
		return facultyType;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public void setFacultyType(String facultyType) {
		this.facultyType = facultyType;
	}

}
