package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

@Entity
@Table(name = "student_admission_items")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class StudentAdmissionItem implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "studentadmission_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private StudentAdmission studentAdmission;

	@Column(name = "studentadmission_id", insertable = false, updatable = false)
	private Long studentAdmissionId;
	
	@NotNull
	private String feetype;
	
	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "course_detail_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private CourseDetail courseDetail;

	@Column(name = "course_detail_id", insertable = false, updatable = false)
	private Long courseDetailId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "additional_fee_id", nullable = false ) 
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private AdditionalFee additionalFee;

	@Column(name = "additional_fee_id", insertable = false, updatable = false)
	private Long additionalFeeId;
	
	@NotNull
	private BigDecimal amount;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public StudentAdmissionItem() {
		// TODO Auto-generated constructor stub
	}

	public StudentAdmissionItem(StudentAdmission studentAdmission,String feetype, CourseDetail courseDetail, AdditionalFee additionalFee,BigDecimal amount) {
		this.studentAdmission = studentAdmission;
		this.feetype = feetype;
		this.courseDetail = courseDetail;
		this.additionalFee=additionalFee;
		this.amount=amount;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public StudentAdmission getStudentAdmission() {
		return studentAdmission;
	}

	public void setStudentAdmission(StudentAdmission studentAdmission) {
		this.studentAdmission = studentAdmission;
	}
	
	public String getFeetype() {
		return feetype;
	}

	public void setFeetype(String feetype) {
		this.feetype = feetype;
	}
	
	public CourseDetail getCourseDetail() {
		return courseDetail;
	}
	
	public Long getCourseDetailId() {
		return courseDetailId;
	}

	public void setCourseDetail(CourseDetail courseDetail) {
		this.courseDetail = courseDetail;
	}
	
	public AdditionalFee getAdditionalFee() {
		return additionalFee;
	}
	
	public Long getAdditionalFeeId() {
		return additionalFeeId;
	}

	public void setAdditionalFee(AdditionalFee additionalFee) {
		this.additionalFee = additionalFee;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
}
