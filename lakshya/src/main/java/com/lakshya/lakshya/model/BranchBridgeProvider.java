//package com.lakshya.lakshya.model;
//
//import java.io.Serializable;
//
//import org.hibernate.search.bridge.FieldBridge;
//import org.hibernate.search.bridge.StringBridge;
//import org.hibernate.search.bridge.builtin.NumberBridge;
//import org.hibernate.search.bridge.spi.BridgeProvider;
//import org.hibernate.search.util.StringHelper;
//
//public class BranchBridgeProvider implements BridgeProvider {
//
//
//	public BranchBridgeProvider() {}
//	
//	@Override
//	public FieldBridge provideFieldBridge(BridgeProviderContext bridgeProviderContext) {
//		if ( bridgeProviderContext.getReturnType().equals( Branch.class ) ) {
//            return BranchFieldBridge.INSTANCE;
//        }
//        return null;
//	}
//}