package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.model.BranchPhone;

@Entity
@Indexed
@Table(name = "online_exams")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OnlineExam implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "name")
	@NotEmpty(message = "Name field Required.")
	private String name;

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "examType")
	private String examType;

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "examModel")
	private String examModel;

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "durationHours")
	private Long durationHours;

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "durationMinutes")
	private Long durationMinutes;

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "correctMark")
	private Float correctMark;

	@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "branch_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Branch branch;

	@Column(name = "branch_id", insertable = false, updatable = false)
	private Long branchId;

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "negativeMark")
	private Float negativeMark;

	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "examDate")
	private LocalDate examDate;

	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "noofquestions")
	@NotNull(message = "No of questions field Required.")
	private Integer noOfQuestions;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "online_exam_id")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Set<OnlineExamBatch> onlineExamBatchs;
	
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "online_exam_id")
//	@OnDelete(action = OnDeleteAction.CASCADE)
	@OrderBy("order_by ASC")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Set<OnlineQuestionSection> onlineQuestionSections;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public OnlineExam() {
	}

	public OnlineExam(String name, String examType, String examModel,
			Long durationHours, Long durationMinutes, Float correctMark, Long branchId, Float negativeMark,
			LocalDate examDate, Integer noOfQuestions) {
		super();
		this.name = name;
		this.examType = examType;
		this.examModel = examModel;
		this.durationHours = durationHours;
		this.durationMinutes = durationMinutes;
		this.correctMark = correctMark;
		this.branchId = branchId;
		this.negativeMark = negativeMark;
		this.examDate = examDate;
		this.noOfQuestions = noOfQuestions;

	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getExamType() {
		return examType;
	}

	public String getExamModel() {
		return examModel;
	}

	public Long getDurationHours() {
		return durationHours;
	}

	public Long getDurationMinutes() {
		return durationMinutes;
	}

	public Float getCorrectMark() {
		return correctMark;
	}

	public Float getNegativeMark() {
		return negativeMark;
	}

	public LocalDate getExamDate() {
		return examDate;
	}

	public Integer getNoOfQuestions() {
		return noOfQuestions;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setExamType(String examType) {
		this.examType = examType;
	}

	public void setExamModel(String examModel) {
		this.examModel = examModel;
	}

	public void setDurationHours(Long durationHours) {
		this.durationHours = durationHours;
	}

	public void setDurationMinutes(Long durationMinutes) {
		this.durationMinutes = durationMinutes;
	}

	public void setCorrectMark(Float correctMark) {
		this.correctMark = correctMark;
	}

	public void setNegativeMark(Float negativeMark) {
		this.negativeMark = negativeMark;
	}

	public void setExamDate(LocalDate examDate) {
		this.examDate = examDate;
	}

	public void setNoOfQuestions(Integer noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Branch getBranch() {
		return branch;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public Set<OnlineExamBatch> getOnlineExamBatchs() {
		return onlineExamBatchs;
	}

	public void setOnlineExamBatchs(Set<OnlineExamBatch> onlineExamBatchs) {
		this.onlineExamBatchs = onlineExamBatchs;
	}
	
	public Set<OnlineQuestionSection> getOnlineQuestionSections() {
		return onlineQuestionSections;
	}

	public void setOnlineQuestionSections(Set<OnlineQuestionSection> onlineQuestionSections) {
		this.onlineQuestionSections = onlineQuestionSections;
	}

}
