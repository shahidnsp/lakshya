package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author psybo-10
 *
 */
@Entity
@Indexed
@Table(name = "loss_reasons")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class LossReason implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Name is mandatory")
//    @Field(name="name")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "name")
	private String name;

	@Nullable
//    @Field(name="remark")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "remark")
	private String remark;

	@Nullable
	private String color = "#0000";

	@Nullable
	private Boolean active = true;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private User user;

//	@ManyToOne
//	@JoinColumn(name = "lead_status_id", nullable = false)
//	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
//	private LeadStatus leadStatus;
//
//	@Nullable
//	@Column(name = "lead_status_id", insertable = false, updatable = false)
//	private Long leadStatusId;

	@ManyToOne
	@JoinColumn(name = "loss_reason_id", nullable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private LossReason lossReason;

	@Nullable
	@Column(name = "loss_reason_id", insertable = false, updatable = false)
	private Long lossReasonId;

	@Column(name = "user_id", insertable = false, updatable = false)
	private Long userId;

	@CreationTimestamp
	private LocalDate createdAt;

	@UpdateTimestamp
	private LocalDate updatedAt;

	public LossReason() {

	}

	public LossReason(String name, String color, String remark) {
		this.name = name;
		this.color = color;
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getUserId() {
		return userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDate getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDate createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDate getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDate updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public LossReason getLossReason() {
		return lossReason;
	}

	public Long getLossReasonId() {
		return lossReasonId;
	}

	public void setLossReason(LossReason lossReason) {
		this.lossReason = lossReason;
	}

	public void setLossReasonId(Long lossReasonId) {
		this.lossReasonId = lossReasonId;
	}

}