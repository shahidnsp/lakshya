package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.github.javafaker.Bool;
import com.lakshya.lakshya.model.Subject;
import com.lakshya.lakshya.model.User;

@Entity
@Indexed
@Table(name = "offline_answer_sheet_omr_lists")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineAnswerSheetOMRList implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_answer_sheet_omr_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineAnswerSheetOMR offlineAnswerSheetOMR;
	
	@Column(name = "offline_answer_sheet_omr_id", insertable = false, updatable = false)
	private Long offlineAnswerSheetOMRId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "subject_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Subject subject;
	
	@Column(name = "subject_id", insertable = false, updatable = false)
	private Long subjectId;
	
	@Column(name = "qno")	
	private Long qno;
	
	@Column(name = "ans")	
	private String ans;
	
	@Column(name = "correct_ans")	
	private String correctAns;
	
	@Column(name = "mark")	
	private Float mark;
	
	@Column(name = "dqfd")	
	private Boolean dQFD;
	
	@Column(name = "removed")	
	private Boolean removed;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OfflineAnswerSheetOMRList(){}

	public OfflineAnswerSheetOMRList(Long offlineAnswerSheetOMRId, Long subjectId, Long qno, String ans,
			String correctAns, Float mark, Boolean dQFd, Boolean removed) {
		super();
		this.offlineAnswerSheetOMRId = offlineAnswerSheetOMRId;
		this.subjectId = subjectId;
		this.qno = qno;
		this.ans = ans;
		this.correctAns = correctAns;
		this.mark = mark;
		this.dQFD = dQFd;
		this.removed = removed;
	}

	public Long getId() {
		return id;
	}

	public OfflineAnswerSheetOMR getOfflineAnswerSheetOMR() {
		return offlineAnswerSheetOMR;
	}

	public Long getOfflineAnswerSheetOMRId() {
		return offlineAnswerSheetOMRId;
	}

	public Subject getSubject() {
		return subject;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public Long getQno() {
		return qno;
	}

	public String getAns() {
		return ans;
	}

	public String getCorrectAns() {
		return correctAns;
	}

	public Float getMark() {
		return mark;
	}

	public Boolean getdQFD() {
		return dQFD;
	}

	public Boolean getRemoved() {
		return removed;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOfflineAnswerSheetOMR(OfflineAnswerSheetOMR offlineAnswerSheetOMR) {
		this.offlineAnswerSheetOMR = offlineAnswerSheetOMR;
	}

	public void setOfflineAnswerSheetOMRId(Long offlineAnswerSheetOMRId) {
		this.offlineAnswerSheetOMRId = offlineAnswerSheetOMRId;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public void setQno(Long qno) {
		this.qno = qno;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}

	public void setCorrectAns(String correctAns) {
		this.correctAns = correctAns;
	}

	public void setMark(Float mark) {
		this.mark = mark;
	}

	public void setdQFD(Boolean dQFd) {
		this.dQFD = dQFd;
	}

	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
