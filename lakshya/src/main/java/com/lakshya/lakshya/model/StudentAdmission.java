package com.lakshya.lakshya.model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

@Entity
@Table(name = "student_admissions")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class StudentAdmission implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Nullable
	@Temporal(TemporalType.TIMESTAMP)
	private Date admission_date;
	
	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Student student;

	@Column(name = "student_id", insertable = false, updatable = false)
	private Long studentId;
	
	@NotNull
	private BigDecimal fee;
	
	@NotNull
	private BigDecimal discount;
	
	@NotNull
	private String discounttype;
	
	@NotNull
	private BigDecimal total_fees;
	
	@NotNull
	private BigDecimal paid;
	
	@NotNull
	private BigDecimal uncleared;
	
	@NotNull
	private BigDecimal refund;
	
	@NotNull
	private BigDecimal balance;
	
	@NotNull
	private String notes;
	
	@NotNull
	private Boolean isapproved=true;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public StudentAdmission() {
		// TODO Auto-generated constructor stub
	}

	public StudentAdmission(Date admission_date,Student student, BigDecimal fee, BigDecimal discount,
			String discounttype,BigDecimal total_fees,BigDecimal paid,BigDecimal uncleared,
			BigDecimal refund,BigDecimal balance,String notes) {
		this.admission_date = admission_date;
		this.student = student;
		this.fee = fee;
		this.paid=paid;
		this.uncleared=uncleared;
		this.refund=refund;
		this.balance=balance;
		this.discount=discount;
		this.discounttype=discounttype;
		this.total_fees=total_fees;
		this.notes=notes;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	/*public Student getStudent() {
		return student;
	}*/

	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Long getStudentId() {
		return studentId;
	}
	
	public Date getAdmissionDate() {
		return admission_date;
	}

	public void setAdmissionDate(Date admission_date) {
		this.admission_date = admission_date;
	}
	
	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	
	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	public String getDiscountType() {
		return discounttype;
	}

	public void setDiscountType(String discounttype) {
		this.discounttype = discounttype;
	}
	
	public BigDecimal getTotalFee() {
		return total_fees;
	}

	public void setTotalFee(BigDecimal total_fees) {
		this.total_fees = total_fees;
	}
	
	public BigDecimal getPaid() {
		return paid;
	}

	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}
	
	public BigDecimal getUncleared() {
		return uncleared;
	}

	public void setUncleared(BigDecimal uncleared) {
		this.uncleared = uncleared;
	}
	
	public BigDecimal getRefund() {
		return refund;
	}

	public void setRefund(BigDecimal refund) {
		this.refund = refund;
	}
	
	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "student_admission_id")
	private Set<StudentAdmissionItem> studentAdmissionItem;
	
	public Set<StudentAdmissionItem> getStudentAdmissionItem() {
		return studentAdmissionItem;
	}

	public void setStudentAdmissionItem(Set<StudentAdmissionItem> studentAdmissionItem) {
		this.studentAdmissionItem = studentAdmissionItem;
	}
}
