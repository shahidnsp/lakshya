package com.lakshya.lakshya.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

@Entity
@Indexed
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},allowGetters = true)

public class User implements Serializable {
	
	public enum Role {USER, ADMIN}
	 
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @Nullable
    private String email;
    
    @NotBlank
    @Column(unique=true)
    private String username;
    
    @NotBlank
    private String password;
    
    @NotBlank
    private String pass;
    
    @Nullable
    private Boolean isAdmin=true;
    
    @Nullable
    private String address;
    
    @Nullable
    private String mobile;
    
    @Nullable
    private String permission;
    
    @Nullable
    private Long role_id;
    
    @NotBlank
    private String photo="profile.png";
    
    @NotBlank
    private String typeofuser="Admin";
    
    @Nullable
    private Date lastlogin;
    
    @Nullable
    private String user_token;
    
    @Nullable
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "student_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Student student;

	@Column(name = "student_id", insertable = false, updatable = false)
	private Long studentId;
    
    @Nullable
    private Boolean active=true;;

    @CreationTimestamp
    private LocalDate createdAt;
 
    @UpdateTimestamp
    private LocalDate updatedAt;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;
    
    public User() {
    	
    }

    public User(String name, String email, String username, String password, Boolean isAdmin, String address, String mobile, String permission, Long role_id, String photo, String typeofuser) {
        this.name = name;
        this.email = email;
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;
        this.address = address;
        this.mobile = mobile;
        this.permission = permission;
        this.role_id = role_id;
        this.photo = photo;
        this.typeofuser = typeofuser; 
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public void setPass(String pass) {
        this.pass = pass;
    }
    
    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    
    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
    
    public Long getRoleId() {
        return role_id;
    }

    public void setRoleId(Long role_id) {
        this.role_id = role_id;
    }
    
    

    public String getPhoto() {
        return photo;
    }
    
    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
    public String getTypeOfUser() {
        return typeofuser;
    }
    
    public void setTypeOfUser(String typeofuser) {
        this.typeofuser = typeofuser;
    }
    
    public Date getLastLogin() {
        return lastlogin;
    }

    public void setLastLogin(Date lastlogin) {
        this.lastlogin = lastlogin;
    }
    
    public String getUserToken() {
        return user_token;
    }
    
    public void setUserToken(String user_token) {
        this.user_token = user_token;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }
    
    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }
    
    public Role getRole() {
        return role;
    }
}
