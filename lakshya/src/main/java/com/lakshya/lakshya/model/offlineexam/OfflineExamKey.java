package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Subject;

@Entity
@Indexed
@Table(name = "offline_exam_key")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineExamKey implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exams_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExam offlineExam;
	
	@Column(name = "offline_exam_id", insertable = false, updatable = false)
	private Long offlineExamId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_main_group_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExamMainGroup offlineExamMainGroup;
	
	@Column(name = "offline_exam_main_group_id", insertable = false, updatable = false)
	private Long offlineExamMainGroupId;
	
//	@Column(name = "type")	
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "type")
	private String type;
	
//	@Column(name = "filename")	
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "file_name")
	private String fileName;
	
//	@Column(name = "originalname")	
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "original_name")
	private String originalName;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
		
	public OfflineExamKey() {}

	public OfflineExamKey(Long offlineExamId, Long offlineExamsMainGroupId, String type, String fileName,
			String originalName) {
		super();
		this.offlineExamId = offlineExamId;
		this.offlineExamMainGroupId = offlineExamsMainGroupId;
		this.type = type;
		this.fileName = fileName;
		this.originalName = originalName;
	}

	public Long getId() {
		return id;
	}

	public OfflineExam getOfflineExam() {
		return offlineExam;
	}

	public Long getOfflineExamId() {
		return offlineExamId;
	}

	public OfflineExamMainGroup getOfflineExamMainGroup() {
		return offlineExamMainGroup;
	}

	public Long getOfflineExamMainGroupId() {
		return offlineExamMainGroupId;
	}

	public String getType() {
		return type;
	}

	public String getFileName() {
		return fileName;
	}

	public String getOriginalName() {
		return originalName;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOfflineExam(OfflineExam offlineExam) {
		this.offlineExam = offlineExam;
	}

	public void setOfflineExamId(Long offlineExamId) {
		this.offlineExamId = offlineExamId;
	}

	public void setOfflineExamMainGroup(OfflineExamMainGroup offlineExamMainGroup) {
		this.offlineExamMainGroup = offlineExamMainGroup;
	}

	public void setOfflineExamMainGroupId(Long offlineExamsMainGroupId) {
		this.offlineExamMainGroupId = offlineExamsMainGroupId;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
