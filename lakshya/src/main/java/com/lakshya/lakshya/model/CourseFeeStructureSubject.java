package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "course_fee_structure_subjects")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class CourseFeeStructureSubject implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "subject_id", nullable = false )
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Subject subject;

	@Column(name = "subject_id", insertable = false, updatable = false)
	private Long subjectId;

	@Nullable
	private String hours;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "course_fee_strcture_id", nullable = false ) 
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private CourseFeeStructure courseFeeStructure;

	@Column(name = "course_fee_strcture_id", insertable = false, updatable = false)
	private Long courseFeeStructureId;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public CourseFeeStructureSubject() {
		// TODO Auto-generated constructor stub
	}

	public CourseFeeStructureSubject(String hours, Subject subject, CourseFeeStructure courseFeeStructure) {
		this.hours = hours;
		this.subject = subject;
		this.courseFeeStructure = courseFeeStructure;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSubjectId() {

		return subjectId;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getHours() {
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	}

	public Long getCourseFeeStructureId() {

		return courseFeeStructureId;
	}

	public CourseFeeStructure getCourseFeeStructure() {
		return courseFeeStructure;
	}

	public void setCoursefeeStructure(CourseFeeStructure courseFeeStructure) {
		this.courseFeeStructure = courseFeeStructure;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

}
