package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;

@Entity
@Indexed
@Table(name = "offline_exam_mark")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineExamMark implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exams_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExam offlineExam;
	
	@Column(name = "offline_exam_id", insertable = false, updatable = false)
	private Long offlineExamId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "student_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Student student;
	
	@Column(name = "student_id", insertable = false, updatable = false)
	private Long studentId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "batch_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Batch batch;
	
	@Column(name = "batch_id", insertable = false, updatable = false)
	private Long batchId;
	
	@Column(name = "score")	
	private Float score;
	
	@Column(name = "status")	
	private String status;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "user_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private User user;
	
	@Column(name = "user_id", insertable = false, updatable = false)
	private Long userId;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OfflineExamMark(){}
	
	public OfflineExamMark(Long offlineExamId, Long studentId, Long batchId, Float score, String status, Long userId) {
		super();
		this.offlineExamId = offlineExamId;
		this.studentId = studentId;
		this.batchId = batchId;
		this.score = score;
		this.status = status;
		this.userId = userId;
	}



	public Long getId() {
		return id;
	}

	public OfflineExam getOfflineExam() {
		return offlineExam;
	}

	public Long getOfflineExamId() {
		return offlineExamId;
	}

	public Student getStudent() {
		return student;
	}

	public Long getStudentId() {
		return studentId;
	}

	public Batch getBatch() {
		return batch;
	}

	public Long getBatchId() {
		return batchId;
	}

	public Float getScore() {
		return score;
	}

	public String getStatus() {
		return status;
	}

	public User getUser() {
		return user;
	}

	public Long getUserId() {
		return userId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOfflineExam(OfflineExam offlineExam) {
		this.offlineExam = offlineExam;
	}

	public void setOfflineExamId(Long offlineExamId) {
		this.offlineExamId = offlineExamId;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
