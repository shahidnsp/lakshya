package com.lakshya.lakshya.model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

@Entity
@Table(name = "course_details")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class CourseDetail implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Nullable
	private String name;
	
	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Course course;

	@Column(name = "course_id", insertable = false, updatable = false)
	private Long courseId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "branch_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Branch branch;

	@Column(name = "branch_id", insertable = false, updatable = false)
	private Long branchId;
	
	@NotNull
	private Double course_fees;
	
	@Nullable
	private Integer course_duration_days;
	
	@Nullable
	private Integer noofinstallments;
	
	@Nullable
	private String repeat_instmnt_after_every;
	
	@Nullable
	private Integer day;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public CourseDetail() {
		// TODO Auto-generated constructor stub
	}

	public CourseDetail(String name,Course course, Double course_fees, Integer course_duration_days,
			Integer noofinstallments,String repeat_instmnt_after_every,Integer day,Branch branch) {
		this.name = name;
		this.course = course;
		this.course_fees = course_fees;
		this.course_duration_days=course_duration_days;
		this.branch=branch;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	public Long getCourseId() {
		return courseId;
	}
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public Long getBranchId() {
		return branchId;
	}
	
	public Double getCourseFees() {
		return course_fees;
	}

	public void setCourseFees(Double course_fees) {
		this.course_fees = course_fees;
	}
	
	public Integer getCourseDurationDays() {
		return course_duration_days;
	}

	public void setCourseDurationDays(Integer course_duration_days) {
		this.course_duration_days = course_duration_days;
	}
	
	public Integer getNoOfInstallments() {
		return noofinstallments;
	}

	public void setNoOfInstallments(Integer noofinstallments) {
		this.noofinstallments = noofinstallments;
	}
	
	public String getRepeatInstmntAfterEvery() {
		return repeat_instmnt_after_every;
	}

	public void setRepeatInstmntAfterEvery(String repeat_instmnt_after_every) {
		this.repeat_instmnt_after_every = repeat_instmnt_after_every;
	}
	
	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}
	
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
