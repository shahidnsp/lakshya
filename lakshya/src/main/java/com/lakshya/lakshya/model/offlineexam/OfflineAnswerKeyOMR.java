package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.User;

@Entity
@Indexed
@Table(name = "offline_answer_key_omr")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineAnswerKeyOMR implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "offline_exam_main_group_id", insertable = false, updatable = false)
	private Long offlineExamMainGroupId;
	
//	@Column(name = "offline_answer_key_omrscol")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "offline_answer_key_omrscol")
	private String offlineAnswerKeyOMRscol;
	
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "course_code")	
	private String courseCode;
		
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "quesion_no")
	private String qNo;
	
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "answer")
	private String ans;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exams_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExam offlineExam;
	
	@Column(name = "offline_exam_id")
	private Long offlineExamId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_key_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExamKey offlineExamKey;
	
	@Column(name = "offline_exam_key_id", insertable = false, updatable = false)
	private Long offlineExamKeyId;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_main_group_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExamMainGroup offlineExamMainGroup;
		
	@Column(name = "removed")	
	private Boolean removed;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "user_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private User user;
	
	@Column(name = "user_id", insertable = false, updatable = false)
	private Long userId;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OfflineAnswerKeyOMR(){}
	
	public OfflineAnswerKeyOMR(Long offlineExamId, Long offlineExamKeyId, Long offlineExamMainGroupId,
			String offlineAnswerKeyOMRscol, String courseCode, String qNo, String ans, Boolean removed, Long userId) {
		super();
		this.offlineExamId = offlineExamId;
		this.offlineExamKeyId = offlineExamKeyId;
		this.offlineExamMainGroupId = offlineExamMainGroupId;
		this.offlineAnswerKeyOMRscol = offlineAnswerKeyOMRscol;
		this.courseCode = courseCode;
		this.qNo = qNo;
		this.ans = ans;
		this.removed = removed;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public OfflineExam getOfflineExam() {
		return offlineExam;
	}

	public Long getOfflineExamId() {
		return offlineExamId;
	}

	public OfflineExamKey getOfflineExamKey() {
		return offlineExamKey;
	}

	public Long getOfflineExamKeyId() {
		return offlineExamKeyId;
	}

	public OfflineExamMainGroup getOfflineExamMainGroup() {
		return offlineExamMainGroup;
	}

	public Long getOfflineExamMainGroupId() {
		return offlineExamMainGroupId;
	}

	public String getOfflineAnswerKeyOMRscol() {
		return offlineAnswerKeyOMRscol;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public String getqNo() {
		return qNo;
	}

	public String getAns() {
		return ans;
	}

	public Boolean getRemoved() {
		return removed;
	}

	public User getUser() {
		return user;
	}

	public Long getUserId() {
		return userId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOfflineExam(OfflineExam offlineExam) {
		this.offlineExam = offlineExam;
	}

	public void setOfflineExamId(Long offlineExamId) {
		this.offlineExamId = offlineExamId;
	}

	public void setOfflineExamKey(OfflineExamKey offlineExamKey) {
		this.offlineExamKey = offlineExamKey;
	}

	public void setOfflineExamKeyId(Long offlineExamKeyId) {
		this.offlineExamKeyId = offlineExamKeyId;
	}

	public void setOfflineExamMainGroup(OfflineExamMainGroup offlineExamMainGroup) {
		this.offlineExamMainGroup = offlineExamMainGroup;
	}

	public void setOfflineExamMainGroupId(Long offlineExamMainGroupId) {
		this.offlineExamMainGroupId = offlineExamMainGroupId;
	}

	public void setOfflineAnswerKeyOMRscol(String offlineAnswerKeyOMRscol) {
		this.offlineAnswerKeyOMRscol = offlineAnswerKeyOMRscol;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public void setqNo(String qNo) {
		this.qNo = qNo;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}

	public void setRemoved(Boolean removed) {
		this.removed = removed;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
