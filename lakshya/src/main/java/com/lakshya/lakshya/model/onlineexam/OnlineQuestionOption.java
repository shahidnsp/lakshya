package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Indexed
@Table(name = "online_question_options")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OnlineQuestionOption implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Nullable
	@Field(analyzer = @Analyzer(definition = "onlinelowercase"), name = "answer_value")
	private String answerValue;
	
	@Nullable
	@Field(name = "is_correct")	
//	@Column(name="is_correct" ,columnDefinition = "boolean default 0")
//	@Value("${property_name:#{null}}")
	private Boolean isCorrect = false;
	
////	@IndexedEmbedded
//	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
//	@JoinColumn(name = "online_question_part_id", nullable = false)
////	@OnDelete(action = OnDeleteAction.CASCADE)
//	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
//	private OnlineQuestionPart onlineQuestionPart;

	@Column(name = "online_question_part_id", insertable = false, updatable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Long onlineQuestionPartId;
	
	@CreationTimestamp	
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OnlineQuestionOption(){}
	
	public OnlineQuestionOption(String answerValue, Boolean isCorrect, Long onlineQuestionPartId) {
		super();
		this.answerValue = answerValue;
		this.isCorrect = isCorrect;
		this.onlineQuestionPartId = onlineQuestionPartId;
	}
	
	public Long getId() {
		return id;
	}

	public String getAnswerValue() {
		return answerValue;
	}

	public Boolean getIsCorrect() {
		return isCorrect;
	}

	public Long getOnlineQuestionPartId() {
		return onlineQuestionPartId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAnswerValue(String answerValue) {
		this.answerValue = answerValue;
	}

	public void setIsCorrect(Boolean isCorrect) {
		this.isCorrect = isCorrect;
	}

	public void setOnlineQuestionPartId(Long onlineQuestionPartId) {
		this.onlineQuestionPartId = onlineQuestionPartId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

//	public OnlineQuestionPart getOnlineQuestionPart() {
//		return onlineQuestionPart;
//	}
//
//	public void setOnlineQuestionPart(OnlineQuestionPart onlineQuestionPart) {
//		this.onlineQuestionPart = onlineQuestionPart;
//	}
	
}
