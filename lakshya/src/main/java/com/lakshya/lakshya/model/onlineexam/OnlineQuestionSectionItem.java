package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.Nullable;

import net.bytebuddy.implementation.bind.annotation.IgnoreForBinding;

@Entity
@Indexed
@Table(name = "online_question_section_items")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OnlineQuestionSectionItem implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_question_section_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private OnlineQuestionSection onlineQuestionSection;

	@Column(name = "online_question_section_id", insertable = false, updatable = false)
	private Long onlineQuestionSectionId;

	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_question_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private OnlineQuestion onlineQuestion;

	@Column(name = "online_question_id", insertable = false, updatable = false)
	private Long onlineQuestionId;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public OnlineQuestionSectionItem() {
	}

	public Long getId() {
		return id;
	}

	public Long getOnlineQuestionSectionId() {
		return onlineQuestionSectionId;
	}

	public OnlineQuestion getOnlineQuestion() {
		return onlineQuestion;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOnlineQuestionSection(OnlineQuestionSection onlineQuestionSection) {
		this.onlineQuestionSection = onlineQuestionSection;
	}

	public void setOnlineQuestionSectionId(Long onlineQuestionSectionId) {
		this.onlineQuestionSectionId = onlineQuestionSectionId;
	}

	public void setOnlineQuestion(OnlineQuestion onlineQuestion) {
		this.onlineQuestion = onlineQuestion;
	}
	
	public void setOnlineQuestionId(Long onlineQuestionId) {
		this.onlineQuestionId = onlineQuestionId;
	}
	
	public Long getOnlineQuestionId() {
		return onlineQuestionId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

}
