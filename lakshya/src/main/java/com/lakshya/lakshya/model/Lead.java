package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "leads")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class Lead implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Name is mandatory")
	private String name;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "lead_id")
	private Set<LeadPhone> leadPhones;

	@Nullable
	private String standard;

	@Nullable
	private String stream;

	@Column(columnDefinition = "boolean default true")
	private Boolean active = true;

	@Nullable
	private String gender;

	@Nullable
	private String email;

	@Nullable
	private String address;

	@Nullable
	private String district;

	@Nullable
	private String state;

	@Nullable
	private String remark;

	@Column(columnDefinition = "VARCHAR(30) NOT NULL default 'lead'")
	private String type = "lead";

	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "school_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private School school;

	@Nullable
	@Column(name = "school_id", insertable = false, updatable = false)
	private Long schoolId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "enquiry_stage_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private EnquiryStage enquiryStage;

	@Column(name = "enquiry_stage_id", insertable = false, updatable = false)
	private Long enquiryStageId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "user_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private User user;

	@Column(name = "user_id", insertable = false, updatable = false)
	private Long userId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "loss_reason_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private LossReason lossReason;

	
	@Column(name = "loss_reason_id", insertable = false, updatable = false)
	private Long lossReasonId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "enquiry_source_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private EnquirySource enquirySource;

	
	@Column(name = "enquiry_source_id", insertable = false, updatable = false)
	private Long enquirySourceId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "branch_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Branch branch;

	@Column(name = "branch_id", insertable = false, updatable = false)
	private Long branchId;

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "course_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Course course;

	@Column(name = "course_id", insertable = false, updatable = false)
	private Long courseId;

	@CreationTimestamp
	private LocalDate createdAt;

	@UpdateTimestamp
	private LocalDate updatedAt;

//  @DateBridge(resolution = null)
	@Temporal(TemporalType.DATE)
	private Date closedAt;

	public Lead() {
		// TODO Auto-generated constructor stub
	}

	public Lead(@NotBlank(message = "Name is mandatory") String name, String standard, String stream, String remark,
			String type, School school) {
		this.name = name;
		this.standard = standard;
		this.stream = stream;
		this.remark = remark;
		this.school = school;
	}

	public EnquiryStage getEnquiryStage() {
		return enquiryStage;
	}

	public void setEnquiryStage(EnquiryStage enquiryStage) {
		this.enquiryStage = enquiryStage;
	}

	public Long getEnquiryStageId() {
		return enquiryStageId;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public LocalDate getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDate createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDate getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDate updatedAt) {
		this.updatedAt = updatedAt;
	}

	public EnquirySource getEnquirySource() {
		return enquirySource;
	}

	public Long getEnquirySourceId() {
		return enquirySourceId;
	}

	public void setEnquirySource(EnquirySource enquirySource) {
		this.enquirySource = enquirySource;
	}

	public void setEnquirySourceId(Long enquirySourceId) {
		this.enquirySourceId = enquirySourceId;
	}

	public Branch getBranch() {
		return branch;
	}

	public Long getBranchId() {
		return branchId;
	}

	public Course getCourse() {
		return course;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public LossReason getLossReason() {
		return lossReason;
	}

	public Long getLossReasonId() {
		return lossReasonId;
	}

	public Date getClosedAt() {
		return closedAt;
	}

	public void setLossReason(LossReason lossReason) {
		this.lossReason = lossReason;
	}

	public void setLossReasonId(Long lossReasonId) {
		this.lossReasonId = lossReasonId;
	}

	public void setClosedAt(Date closedAt) {
		this.closedAt = closedAt;
	}

	public Set<LeadPhone> getLeadPhones() {
		return leadPhones;
	}

	public void setLeadPhones(Set<LeadPhone> leadPhones) {
		this.leadPhones = leadPhones;
	}

	public String getGender() {
		return gender;
	}

	public String getEmail() {
		return email;
	}

	public String getAddress() {
		return address;
	}

	public String getDistrict() {
		return district;
	}

	public String getState() {
		return state;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public void setState(String state) {
		this.state = state;
	}

}
