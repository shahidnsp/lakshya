package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Indexed
@Table(name = "online_exam_result_lists")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OnlineExamResultList implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_exam_result_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private OnlineExamResult onlineexamresult;

	@Column(name = "online_exam_result_id", insertable = false, updatable = false)
	private Long onlineExamResultId;
	
	//@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_question_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private OnlineQuestion onlineQuestion;

	@Column(name = "online_question_id", insertable = false, updatable = false)
	private Long onlineQuestionId;
	
	@Nullable
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_question_part_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private OnlineQuestionPart onlineQuestionpart;

	@Column(name = "online_question_part_id", insertable = false, updatable = false)
	private Long onlineQuestionPartId;
	
	private Integer qno=1;
	
	private String markedanswer;
	
	private String correctanswer;
	
	private Float mark;
	
	private String status="Attent";
	
	private Boolean isCorrect=false;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OnlineExamResultList() {}
	
	public OnlineExamResultList(OnlineExamResult onlineexamresult, OnlineQuestion onlineQuestion,OnlineQuestionPart onlineQuestionpart,
			Integer qno,String markedanswer,String correctanswer,Float mark,String status) {
		super();		
		this.onlineexamresult = onlineexamresult;
		this.onlineQuestion = onlineQuestion;
		this.onlineQuestionpart=onlineQuestionpart;
		this.qno = qno;	
		this.markedanswer = markedanswer;	
		this.correctanswer = correctanswer;	
		this.mark=mark;
		this.status=status;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OnlineExamResult getOnlineExamResult() {
		return onlineexamresult;
	}
	
	public Long getOnlineExamResultId() {
		return onlineExamResultId;
	}

	public void setOnlineExamResult(OnlineExamResult onlineexamresult) {
		this.onlineexamresult = onlineexamresult;
	}
	
	public OnlineQuestion getOnlineQuestion() {
		return onlineQuestion;
	}
	
	public Long getOnlineQuestionId() {
		return onlineQuestionId;
	}

	public void setOnlineQuestion(OnlineQuestion onlineQuestion) {
		this.onlineQuestion = onlineQuestion;
	}
	
	/*public OnlineQuestionPart getOnlineQuestionPart() {
		return onlineQuestionpart;
	}*/

	public void setOnlineQuestionPart(OnlineQuestionPart onlineQuestionpart) {
		this.onlineQuestionpart = onlineQuestionpart;
	}
	
	public Long getOnlineQuestionPartId() {
		return onlineQuestionPartId;
	}
	
	public Integer getQno() {
		return qno;
	}

	public void setQno(Integer qno) {
		this.qno = qno;
	}
	
	public String getMarkedAnswer() {
		return markedanswer;
	}

	public void setMarkedAnswer(String markedanswer) {
		this.markedanswer = markedanswer;
	}
	
	public String getCorrectAnswer() {
		return correctanswer;
	}

	public void setCorrectAnswer(String correctanswer) {
		this.correctanswer = correctanswer;
	}
	
	public Float getMark() {
		return mark;
	}

	public void setMark(Float mark) {
		this.mark = mark;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Boolean getIsCorrect() {
		return isCorrect;
	}

	public void setIsCorrect(Boolean isCorrect) {
		this.isCorrect = isCorrect;
	}
	
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
