package com.lakshya.lakshya.model;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "lead_tasks")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class LeadTask {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "lead_id", nullable = false )
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Lead lead;
	
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false )
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private User user;
	
	//@NotEmpty(message = "StudentId Required.")
	@Column(name="lead_id",insertable = false, updatable = false)
	private Long leadId;
	
	//@NotEmpty(message = "UserId Required.")
	@Column(name="user_id",insertable = false, updatable = false)
	private Long userId;

	@Nullable
	private String message;
	
	@Nullable
	@Temporal(TemporalType.TIMESTAMP)
	private Date notifyDate;
	
	@Nullable
	private String notifyTime;
	
	
	@Nullable
	private Boolean isnoted=false;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public Long getId() {
		return id;
	}

	public Long getLeadId() {
		return leadId;
	}

	public Long getUserId() {
		return userId;
	}

	public String getMessage() {
		return message;
	}

	public Date getNotifyDate() {
		return notifyDate;
	}

	public String getNotifyTime() {
		return notifyTime;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setNotifyDate(Date notifyDate) {
		this.notifyDate = notifyDate;
	}

	public void setNotifyTime(String notifyTime) {
		this.notifyTime = notifyTime;
	}
	
	public void setIsnoted(Boolean isnoted) {
		this.isnoted = isnoted;
	}
	
	public Boolean getIsnoted() {
		return isnoted;
	}
	

	/*public void setNote(String note) {
		this.note = note;
	}*/

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}


	public User getUser() {
		return user;
	}

	public void setLead(Lead lead) {
		this.lead = lead;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public LeadTask() {
		
	}

	public LeadTask(Lead lead, User user, String message, Date notifyDate, String notifyTime) {
		super();
		this.lead = lead;
		this.user = user;
		this.message = message;
		this.notifyDate = notifyDate;
		this.notifyTime = notifyTime;
	}
}
