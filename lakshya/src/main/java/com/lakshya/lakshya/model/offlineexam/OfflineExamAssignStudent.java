package com.lakshya.lakshya.model.offlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Indexed;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Chapter;
import com.lakshya.lakshya.model.Student;

@Entity
@Indexed
@Table(name = "offline_exam_assign_students")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OfflineExamAssignStudent implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
		
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "student_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Student student;

	@Column(name = "student_id", insertable = false, updatable = false)	
	private Long studentId;	
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false )
	@JoinColumn(name = "offline_exam_id", nullable = false )
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private OfflineExam offlineExam;

	@Column(name = "offline_exam_id", insertable = false, updatable = false)	
	private Long offlineExamId;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public OfflineExamAssignStudent(){}

	public OfflineExamAssignStudent(Long studentId, OfflineExam offlineExam) {
		super();
		this.studentId = studentId;
		this.offlineExam = offlineExam;
	}

	public Long getId() {
		return id;
	}

	public Student getStudent() {
		return student;
	}

	public Long getStudentId() {
		return studentId;
	}

	public OfflineExam getOfflineExam() {
		return offlineExam;
	}

	public Long getOfflineExamId() {
		return offlineExamId;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setOfflineExam(OfflineExam offlineExam) {
		this.offlineExam = offlineExam;
	}

	public void setOfflineExamId(Long offlineExamId) {
		this.offlineExamId = offlineExamId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
		
}
