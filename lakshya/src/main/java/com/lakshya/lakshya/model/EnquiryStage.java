package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "enquiry_stages")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},allowGetters = true)
public class EnquiryStage implements Serializable {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Name is mandatory")
    private String name;
    
    @Nullable
    private String remark;

    @Nullable
    private String color="#0000";
            
    @Nullable
    private Boolean active=true;
    
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false )
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private	User user;
    
    @Nullable
    @Column(name = "user_id", insertable = false, updatable = false)
    private Long userId;
    
    @Nullable
    private Boolean isRemovable=true;
    
    @CreationTimestamp
    private LocalDate createdAt;
 
    @UpdateTimestamp
    private LocalDate updatedAt;
    
    public EnquiryStage() {
    	
    }

    public EnquiryStage(String name,String color,String remark,User user) {
        this.name = name;
        this.color= color; 
        this.remark= remark;  
        this.user=user;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public Boolean getIsRemovable() {
        return isRemovable;
    }
    
    public void setIsRemovable(Boolean isRemovable) {
        this.isRemovable=isRemovable;
    }
    
    public Long getUserId() {
        return userId;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }
    
    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }
	
}