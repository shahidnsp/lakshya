package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "enquiry_sms")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class EnquirySMS  implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "student_id", nullable = false )
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Student student;
	
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false )
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private User user;
	
	@NotEmpty(message = "Student_Id Required.")
	@Column(name = "student_id" ,insertable = false, updatable = false)
	private Long student_Id;

	@NotEmpty(message = "User_Id Required.")
	@Column(name = "user_id",insertable = false, updatable = false)
	private Long user_Id;
		
	@Nullable
	private String send_To;
	
	@Nullable
	private String message;
	
	@Nullable
	private String phone1;
	
	@Nullable
	private String phone2;
	
	@Nullable
	private String status;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStudent_Id() {
		return student_Id;
	}

	/*public void setStudent_Id(Long student_Id) {
		this.student_Id = student_Id;
	}*/

	public String getSend_To() {
		return send_To;
	}

	public void setSend_To(String send_To) {
		this.send_To = send_To;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getUser_Id() {
		return user_Id;
	}

	/*public void setUser_Id(Long user_Id) {
		this.user_Id = user_Id;
	}*/

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Student getStudent() {
		return student;
	}

	public User getUser() {
		return user;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EnquirySMS(Student student, User user, String send_To, String message, String phone1, String phone2,
			String status) {
		super();
		this.student = student;
		this.user = user;
		this.send_To = send_To;
		this.message = message;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.status = status;
	}
	
	
}
