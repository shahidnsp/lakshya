package com.lakshya.lakshya.model;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Indexed
@Table(name = "branches")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class Branch implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
//	@Field(name="name")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "name")
	@NotEmpty(message = "Branch Name Required...")
	private String name;

//	@Field(name = "code")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "code")
	@NotEmpty(message = "Branch Code Required...")
	private String code;

//	@Field(name = "address")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "address")
	@Nullable	
	private String address;

//	@Nullable
//	@Field(name = "area")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "area")
	private String area;

//	@Nullable
//	@Field(name="city")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "city")
	private String city;

	@Nullable	
//	@Field(name="state")	
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "state")
	private String state;
	
	@Nullable
//	@Field(name="pincode")
	@Field(analyzer = @Analyzer(definition = "lowercase"), name = "pincode")
	private String pincode;
	
	@Nullable
//	@Field(name="active")	
	private Boolean active = true;

//	 @Field(index = Index.YES, analyze = Analyze.NO, store = Store.YES)
//	  @DateBridge(resolution = Resolution.DAY)	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "branch_category_id", nullable = false ) 
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private BranchCategory branchCategory;

	public BranchCategory getBranchCategory() {
		return branchCategory;
	}

	public void setBranchCategory(BranchCategory branchCategory) {
		this.branchCategory = branchCategory;
	}

	@Column(name = "branch_category_id", insertable = false, updatable = false)	
	private Long branchCategoryId;

	public Branch() {

	}

	public Branch(String name, String code, String address, String area, String city, String state, String pincode,
			BranchCategory branchCategory) {
		this.name = name;
		this.code = code;
		this.address = address;
		this.area = area;
		this.city = city;
		this.state = state;
		this.pincode = pincode;
		this.branchCategory = branchCategory;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Long getBranchCategoryId() {
		return branchCategoryId;
	}

	public void setBranchCategoryId(Long branchCategoryId) {
		this.branchCategoryId = branchCategoryId;
	}
	
//	@ManyToOne(fetch = FetchType.LAZY, optional = false , cascade = CascadeType.REMOVE)
//	@JoinColumn(name = "user_id", nullable = false ) 
//	@OnDelete(action = OnDeleteAction.CASCADE)
//	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	
	@ManyToOne//(targetEntity = BranchCategory.class)
	@JoinColumn(name = "user_id", nullable = false )
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private User user;

	@Column(name = "user_id", insertable = false, updatable = false)	
	private Long userId;
	
	public Long getUserId() {
		return userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@IndexedEmbedded
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "branch_id")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })	
	private Set<BranchLandLine> branchLandLines;
	
	public Set<BranchLandLine> getBranchLandLines() {
		return branchLandLines;
	}

	public void setLandLines(Set<BranchLandLine> branchLandLines) {
		this.branchLandLines = branchLandLines;
	}
	
	@IndexedEmbedded
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "branch_id")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })		
	private Set<BranchPhone> branchPhones;
	
	public Set<BranchPhone> getBranchPhones() {
		return branchPhones;
	}

	public void setBranchPhones(Set<BranchPhone> branchPhones) {
		this.branchPhones = branchPhones;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

}
