package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

@Entity
@Table(name = "enquiry_lists")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class EnquiryList implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Student student;

	@Column(name = "student_id", insertable = false, updatable = false)
	private Long studentId;
	
	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private AcademicYear academicYear;

	@Column(name = "academic_year_id", insertable = false, updatable = false)
	private Long academicYearId;
	
	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private CourseDetail courseDetail;

	@Column(name = "course_detail_id", insertable = false, updatable = false)
	private Long courseDetailId;
	
	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private EnquirySource enquirySource;

	@Column(name = "enquiry_source_id", insertable = false, updatable = false)
	private Long enquirySourceId;
	
	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private User user;

	@Column(name = "user_id", insertable = false, updatable = false)
	private Long userId;
	
	@Nullable
	@Temporal(TemporalType.TIMESTAMP)
	private Date enquiry_date;
	
	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public EnquiryList() {
		// TODO Auto-generated constructor stub
	}

	public EnquiryList(Student student,AcademicYear academicYear, CourseDetail courseDetail, EnquirySource enquirySource,User user,Date enquiry_date) {
		this.student = student;
		this.academicYear = academicYear;
		this.courseDetail = courseDetail;
		this.enquirySource=enquirySource;
		this.user=user;
		this.enquiry_date=enquiry_date;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	/*public Student getStudent() {
		return student;
	}*/

	public void setStudent(Student student) {
		this.student = student;
	}
	
	public Long getStudentId() {
		return studentId;
	}
	
	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}
	
	public Long getAcademicYearId() {
		return academicYearId;
	}
	
	public CourseDetail getCourseDetail() {
		return courseDetail;
	}

	public void setCourseDetail(CourseDetail courseDetail) {
		this.courseDetail = courseDetail;
	}
	
	public Long getCourseDetailId() {
		return courseDetailId;
	}
	
	public EnquirySource getEnquirySource() {
		return enquirySource;
	}

	public void setEnquirySource(EnquirySource enquirySource) {
		this.enquirySource = enquirySource;
	}
	
	public Long getEnquirySourceId() {
		return enquirySourceId;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Long getUserId() {
		return userId;
	}
	
	public Date getEnquiryDate() {
		return enquiry_date;
	}

	public void setEnquiryDate(Date enquiry_date) {
		this.enquiry_date = enquiry_date;
	}
	
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
}
