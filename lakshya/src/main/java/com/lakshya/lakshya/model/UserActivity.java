package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "user_activities")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class UserActivity  implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false) // , cascade = CascadeType.REMOVE)
	@JoinColumn(name = "student_id", nullable = false , insertable = false , updatable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Student student;
	
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false , insertable = false , updatable = false)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private User user;
	
	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private EnquiryStage enquiryStage;
	
	@ManyToOne
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Course course;
	
	@NotEmpty(message = "Student_Id Required.")
	@Column(name="student_id",insertable = false, updatable = false)
	private Long student_Id;

	@NotEmpty(message = "User_Id Required.")
	@Column(name="user_id" ,insertable = false, updatable = false)
	private Long user_Id;
	
	@Nullable
	private String message;
	
	@Nullable
	private String type;
	
	@Nullable
	@Column(name="enquiry_stage_id",insertable = false, updatable = false)
	private Long enquiry_Stage_Id;
	
	@Nullable
	@Column(name="course_id",insertable = false, updatable = false)
	private Long course_Id;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStudent_Id() {
		return student_Id;
	}

//	public void setStudent_Id(Long student_Id) {
//		this.student_Id = student_Id;
//	}

	public Long getUser_Id() {
		return user_Id;
	}

//	public void setUser_Id(Long user_Id) {
//		this.user_Id = user_Id;
//	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getEnquiry_Stage_Id() {
		return enquiry_Stage_Id;
	}

//	public void setEnquiry_Stage_Id(Long enquiry_Stage_Id) {
//		this.enquiry_Stage_Id = enquiry_Stage_Id;
//	}

	public Long getCourse_Id() {
		return course_Id;
	}

	/*public void setCourse_Id(Long course_Id) {
		this.course_Id = course_Id;
	}*/

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Student getStudent() {
		return student;
	}

	public User getUser() {
		return user;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EnquiryStage getEnquiryStage() {
		return enquiryStage;
	}

	public void setEnquiryStage(EnquiryStage enquiryStage) {
		this.enquiryStage = enquiryStage;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public UserActivity(Student student, User user, EnquiryStage enquiryStage, Course course, String message,
			String type) {
		super();
		this.student = student;
		this.user = user;
		this.enquiryStage = enquiryStage;
		this.course = course;
		this.message = message;
		this.type = type;
	}
	
	
	
}
