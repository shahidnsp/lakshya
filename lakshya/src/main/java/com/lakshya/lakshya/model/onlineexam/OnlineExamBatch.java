package com.lakshya.lakshya.model.onlineexam;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lakshya.lakshya.model.Batch;

@Entity
@Indexed
@Table(name = "online_exam_batches")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class OnlineExamBatch implements Serializable  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	
	
	//@IndexedEmbedded

	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "online_exam_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private  OnlineExam onlineExam ;
	
	@Column(name = "online_exam_id",insertable = false, updatable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Long onlineExamId;
	
	//@IndexedEmbedded
	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "batch_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Batch batch;
	

	@Column(name = "batch_id",insertable = false, updatable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Long batchId;
	
	
	@CreationTimestamp	
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public OnlineExamBatch(){}
	
	public OnlineExamBatch(Long batchId,Long onlineExamId ) {
		super();
//		this.onlineExamId = onlineExamId;
		this.batchId = batchId;
		
	}
	
	public Long getId() {
		return id;
	}

	public Long getBatchId() {
		return batchId;
	}
	

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOnlineExamId(Long onlineExamId) {
		this.onlineExamId = onlineExamId;
	}
	
	
	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

//	public OnlineExam getOnlineExam() {
//		return onlineExam;
//	}

	public Long getOnlineExamId() {
		return onlineExamId;
	}
	

	public Batch getBatch() {
		return batch;
	}

	public void setOnlineExam(OnlineExam onlineExam) {
		this.onlineExam = onlineExam;
	}


	
}
