package com.lakshya.lakshya.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "course_fee_structure_installments")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class CourseFeeStructureInstallment implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@DecimalMin(value = "0.1", inclusive = true)
	private BigDecimal amount;

	@ManyToOne(fetch = FetchType.LAZY, optional = false )//, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "course_fee_strcture_id", nullable = false ) 
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private CourseFeeStructure courseFeeStructure;

	@Column(name = "course_fee_strcture_id", insertable = false, updatable = false)
	private Long courseFeeStructureId;

	@CreationTimestamp
	private LocalDateTime createdAt;

	@UpdateTimestamp
	private LocalDateTime updatedAt;

	public CourseFeeStructureInstallment() {
		// TODO Auto-generated constructor stub
	}

	public CourseFeeStructureInstallment(BigDecimal amount, CourseFeeStructure courseFeeStructure) {
		this.amount = amount;
		this.courseFeeStructure = courseFeeStructure;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getCourseFeeStructureId() {

		return courseFeeStructureId;
	}

	public CourseFeeStructure getCourseFeeStructure() {
		return courseFeeStructure;
	}

	public void setCoursefeeStructure(CourseFeeStructure courseFeeStructure) {
		this.courseFeeStructure = courseFeeStructure;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

}
