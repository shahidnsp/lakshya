package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.service.impl.ComboInterface;

public interface AcademicYearRepository extends CrudRepository<AcademicYear, Long>{
	public static final String ACADEMIC_YEAR_FOR_COMBO = "SELECT e.id as id,e.name as name FROM academic_years e";

	@Query(value = ACADEMIC_YEAR_FOR_COMBO, nativeQuery = true)
	public List<ComboInterface> getAcademicYearForCombo();
}
