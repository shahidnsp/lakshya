package com.lakshya.lakshya.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.Lead;

public interface LeadRepository extends PagingAndSortingRepository<Lead, Long> {
	Lead findOneById(Long leadId);
}
