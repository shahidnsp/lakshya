package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.BatchChapter;

@Repository
public interface BatchChapterRepository extends CrudRepository<BatchChapter, Long>{

}
