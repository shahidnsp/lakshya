package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.AcademicYear;
import com.lakshya.lakshya.model.EnquiryCollaborator;
import com.lakshya.lakshya.model.Student;

public interface EnquiryCollaboratorRepository extends CrudRepository<EnquiryCollaborator, Long>{
	List<EnquiryCollaborator> findAllByStudentId(Long studentId);
}
