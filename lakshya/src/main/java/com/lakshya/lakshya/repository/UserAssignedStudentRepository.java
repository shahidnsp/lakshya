package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.UserAssignedStudent;



public interface UserAssignedStudentRepository extends CrudRepository<UserAssignedStudent, Long>{
	
	List<UserAssignedStudent> findByUserId(Long user_id);
}
