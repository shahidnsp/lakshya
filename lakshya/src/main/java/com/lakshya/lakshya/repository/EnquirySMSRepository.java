package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.EnquirySMS;

public interface EnquirySMSRepository extends CrudRepository<EnquirySMS, Long > {

}
