package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.LeadNote;

public interface LeadNoteRepository extends CrudRepository<LeadNote, Long>{
	List<LeadNote> findAllByLeadId(Long leadId);
}
