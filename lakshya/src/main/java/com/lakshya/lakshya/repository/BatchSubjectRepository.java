package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.BatchSubject;

@Repository
public interface BatchSubjectRepository extends CrudRepository<BatchSubject, Long> {

}
