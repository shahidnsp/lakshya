package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.EnquirySource;

public interface EnquirySourceRepository extends PagingAndSortingRepository<EnquirySource, Long> {

}
