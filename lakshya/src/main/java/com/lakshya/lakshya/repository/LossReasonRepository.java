package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.LossReason;
import com.lakshya.lakshya.service.impl.ComboInterface;


public interface LossReasonRepository extends PagingAndSortingRepository<LossReason, Long> {
	public static final String ENQUIRY_STAGE_FOR_COMBO = "SELECT e.id as id,e.name as name FROM loss_reasons e";

	@Query(value = ENQUIRY_STAGE_FOR_COMBO, nativeQuery = true)
	public List<ComboInterface> getLossReasonForCombo();
}
