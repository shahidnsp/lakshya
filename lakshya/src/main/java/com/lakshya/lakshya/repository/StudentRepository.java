package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.Student;



public interface StudentRepository extends PagingAndSortingRepository<Student, Long> {

	Student getOne(Long studentId);

	Page<Student> findAllByBranchIdAndStatus(Long branchId,String status,Pageable paging);
	
	Page<Student> findAllByBranchId(Long branchId,Pageable paging);

	 Long countByRollno(String rollno);
	 
	 Student getStudentByRollno(String rollNo);

}
