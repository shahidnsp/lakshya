package com.lakshya.lakshya.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.BranchCategory;
import com.lakshya.lakshya.service.impl.ComboInterface;

@Repository
public interface BranchCategoryRepository extends JpaRepository<BranchCategory, Long> {

	BranchCategory getOne(Long categoryId);
//	List<BranchCategory> findByName(String name); //Long id
	
	public static final String BRANCH_CATEGORY_FOR_COMBO = "SELECT e.id as id,e.name as name FROM branch_categories e";

	@Query(value = BRANCH_CATEGORY_FOR_COMBO, nativeQuery = true)
	public List<ComboInterface> getBranchCategoryForCombo();
}
