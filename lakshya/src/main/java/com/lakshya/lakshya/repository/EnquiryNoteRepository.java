package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.EnquiryNote;
import com.lakshya.lakshya.model.EnquiryTask;

public interface EnquiryNoteRepository extends CrudRepository<EnquiryNote, Long>{
	List<EnquiryNote> findAllByStudentId(Long studentId);
}
