package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.Course;
import com.lakshya.lakshya.service.impl.ComboInterface;

public interface CourseRepository extends PagingAndSortingRepository<Course, Long> {
	public static final String COURSE_FOR_COMBO = "SELECT e.id as id,e.name as name FROM courses e";

	@Query(value = COURSE_FOR_COMBO, nativeQuery = true)
	public List<ComboInterface> getCourseForCombo();
}
