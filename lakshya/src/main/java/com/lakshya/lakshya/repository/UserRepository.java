package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.service.impl.ComboInterface;
import com.lakshya.lakshya.service.impl.UserInterface;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	User findByUsername(String username);
	
	User getOne(Long userId);
	
	List<User> findAllByTypeofuser(String userType);
	
	List<User> findAllByStudentId(Long studentId);
	
	public static final String USER_LIST = "SELECT e.id as id,e.username as username,e.pass as pass,e.typeofuser as typeofuser FROM users e WHERE e.student_id=?1";

	@Query(value = USER_LIST, nativeQuery = true)
	public List<UserInterface> getStudentUserList(Long studentId);
}

