package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.EnquiryList;

public interface EnquiryListRepository extends PagingAndSortingRepository<EnquiryList, Long> {
    public List<EnquiryList> findAllByStudentId(Long id);
}
