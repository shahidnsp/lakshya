package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.StudentLandLine;

public interface StudentLandlineRepository extends CrudRepository<StudentLandLine, Long> {

}
