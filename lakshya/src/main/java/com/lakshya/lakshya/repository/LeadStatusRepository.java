package com.lakshya.lakshya.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.LeadStatus;

@Repository
public interface LeadStatusRepository extends PagingAndSortingRepository<LeadStatus, Long>{
	LeadStatus findOneById(Long leadStatusId);
}
