package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.BranchPhone;

@Repository
public interface BranchPhoneRepository extends CrudRepository<BranchPhone, Long>{
	
}
