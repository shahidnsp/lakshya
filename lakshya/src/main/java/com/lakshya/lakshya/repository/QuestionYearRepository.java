package com.lakshya.lakshya.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.onlineexam.QuestionYear;

public interface QuestionYearRepository extends PagingAndSortingRepository<QuestionYear, Long>{

}
