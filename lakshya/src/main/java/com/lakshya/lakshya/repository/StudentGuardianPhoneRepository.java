package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.StudentGuardianPhone;


public interface StudentGuardianPhoneRepository extends CrudRepository<StudentGuardianPhone, Long> {

}
