package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.lakshya.lakshya.model.CourseDetail;
import com.lakshya.lakshya.service.impl.ComboInterface;

public interface CourseDetailRepository extends PagingAndSortingRepository<CourseDetail, Long>{
	public static final String COURSE_FOR_COMBO = "SELECT e.id as id,e.name as name FROM course_details e WHERE e.branch_id = ?1";

	@Query(value = COURSE_FOR_COMBO, nativeQuery = true)
	public List<ComboInterface> getCourseDetailsForCombo(Long branchId);
}
