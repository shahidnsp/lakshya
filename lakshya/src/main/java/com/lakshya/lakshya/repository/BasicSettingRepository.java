package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.BasicSetting;

@Repository
public interface BasicSettingRepository extends CrudRepository<BasicSetting, Long> {

	BasicSetting findTopByOrderByIdDesc();

}
