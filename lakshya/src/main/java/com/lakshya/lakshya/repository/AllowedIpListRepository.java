package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.AllowedIpList;

@Repository
public interface AllowedIpListRepository extends CrudRepository<AllowedIpList, Long> {

}
