package com.lakshya.lakshya.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.CourseFeeStructure;

public interface CourseFeeStructureRepository extends PagingAndSortingRepository<CourseFeeStructure, Long> {

}
