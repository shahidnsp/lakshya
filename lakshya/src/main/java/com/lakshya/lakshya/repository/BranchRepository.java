package com.lakshya.lakshya.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.Branch;
import com.lakshya.lakshya.service.impl.ComboInterface;

//interface QueryByExampleExecutor<T> extends PagingAndSortingRepository<Branch, Long> {
//	 Optional<Branch>  findOne(Example<Branch> example);
//	 Iterable<Branch> findAll(Example<Branch> example);
//}

@Repository
public interface BranchRepository extends QueryByExampleExecutor<Branch>,PagingAndSortingRepository<Branch, Long> {
	List<Branch> findByBranchCategoryId(Long branchCategoryId);
	//	List<Branch> findAll(ExampleMatcher matcher);
	
	public static final String BRANCH_FOR_COMBO = "SELECT e.id as id,e.name as name FROM branches e";

	@Query(value = BRANCH_FOR_COMBO, nativeQuery = true)
	public List<ComboInterface> getBranchForCombo();
}

