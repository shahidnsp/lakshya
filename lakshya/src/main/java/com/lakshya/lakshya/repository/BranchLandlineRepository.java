package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.BranchLandLine;
import com.lakshya.lakshya.model.BranchPhone;

@Repository
public interface BranchLandlineRepository extends CrudRepository<BranchLandLine, Long>{

}
