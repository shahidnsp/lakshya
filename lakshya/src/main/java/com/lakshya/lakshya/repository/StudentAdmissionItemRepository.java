package com.lakshya.lakshya.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.StudentAdmissionItem;

public interface StudentAdmissionItemRepository extends PagingAndSortingRepository<StudentAdmissionItem, Long>{

}
