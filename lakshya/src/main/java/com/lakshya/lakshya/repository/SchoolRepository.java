package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.School;

public interface SchoolRepository extends CrudRepository<School, Long>{
	School getOne(Long schoolId);
}