package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.StudentPhone;

public interface StudentPhoneRepository extends CrudRepository<StudentPhone, Long> {

}
