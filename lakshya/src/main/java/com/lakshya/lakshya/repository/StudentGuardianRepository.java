package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.StudentGuardian;

public interface StudentGuardianRepository extends CrudRepository<StudentGuardian, Long> {
	List<StudentGuardian> findAllByStudentId(Long studentId);
}
