package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.service.impl.ComboInterface;

@Repository
public interface BatchRepository extends PagingAndSortingRepository<Batch, Long> {
	public static final String BATCH_FOR_COMBO = "SELECT e.id as id,e.name as name FROM batches e WHERE course_id=?1 AND branch_id=?2";

	@Query(value = BATCH_FOR_COMBO, nativeQuery = true)
	public List<ComboInterface> getBatchForCombo(Long courseId,Long branchId);
}
