package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.StudentAdmission;

public interface StudentAdmissionRepository extends PagingAndSortingRepository<StudentAdmission, Long>{
    public List<StudentAdmission> findAllByStudentId(Long studentId);
}
