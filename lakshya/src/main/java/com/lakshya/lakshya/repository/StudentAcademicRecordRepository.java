package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.lakshya.lakshya.model.StudentAcademicRecord;

public interface StudentAcademicRecordRepository extends CrudRepository<StudentAcademicRecord, Long>{
	public List<StudentAcademicRecord> findAllByStudentId(Long id);
}
