package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.StudentDocument;

public interface StudentDocumentRepository extends CrudRepository<StudentDocument, Long> {

}
