package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.EnquiryTask;

public interface EnquiryTaskRepository extends CrudRepository< EnquiryTask, Long>{
	
	List<EnquiryTask> findAllByStudentIdAndUserIdAndIsnoted(Long studentId,Long userId,Boolean isnoted);
	
	List<EnquiryTask> findAllByUserIdAndIsnoted(Long userId,Boolean isnoted);
}
