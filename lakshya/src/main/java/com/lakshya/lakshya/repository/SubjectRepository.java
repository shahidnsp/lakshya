package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.Subject;

public interface SubjectRepository extends PagingAndSortingRepository<Subject, Long> {

}
