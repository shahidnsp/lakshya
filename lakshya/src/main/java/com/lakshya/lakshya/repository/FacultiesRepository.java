package com.lakshya.lakshya.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.onlineexam.Faculty;

public interface FacultiesRepository extends PagingAndSortingRepository<Faculty, Long>{

}
