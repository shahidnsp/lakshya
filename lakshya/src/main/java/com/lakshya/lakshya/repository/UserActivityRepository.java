package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.UserActivity;

public interface UserActivityRepository extends CrudRepository<UserActivity, Long>{ 

}
