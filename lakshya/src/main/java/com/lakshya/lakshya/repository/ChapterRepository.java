package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.lakshya.lakshya.model.Chapter;

@Repository
public interface ChapterRepository extends PagingAndSortingRepository<Chapter, Long> {
	
}
