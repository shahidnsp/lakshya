package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.LeadTask;

public interface LeadTaskRepository extends CrudRepository<LeadTask, Long>{
	List<LeadTask> findAllByLeadIdAndUserIdAndIsnoted(Long leadId,Long userId,Boolean isnoted);
	List<LeadTask> findAllByLeadIdAndIsnoted(Long leadId,Boolean isnoted);
	
	List<LeadTask> findAllByUserIdAndIsnoted(Long userId,Boolean isnoted);
}
