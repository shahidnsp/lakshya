package com.lakshya.lakshya.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.AdditionalFee;

public interface AdditionalFeeRepository extends PagingAndSortingRepository<AdditionalFee, Long> {

}
