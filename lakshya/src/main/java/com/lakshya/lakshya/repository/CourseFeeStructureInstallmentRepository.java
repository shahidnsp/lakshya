package com.lakshya.lakshya.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.CourseFeeStructureInstallment;

public interface CourseFeeStructureInstallmentRepository
		extends PagingAndSortingRepository<CourseFeeStructureInstallment, Long> {

}
