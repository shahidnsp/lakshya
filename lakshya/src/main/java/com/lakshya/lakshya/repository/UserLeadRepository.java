package com.lakshya.lakshya.repository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.UserLead;

public interface UserLeadRepository  extends CrudRepository<UserLead, Long>{

	UserLead findOneById(Long id);

}
