package com.lakshya.lakshya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.lakshya.lakshya.model.EnquiryStage;
import com.lakshya.lakshya.model.LeadStatus;
import com.lakshya.lakshya.service.impl.ComboInterface;


public interface EnquiryStageRepository extends PagingAndSortingRepository<EnquiryStage, Long> {
	public static final String ENQUIRY_STAGE_FOR_COMBO = "SELECT e.id as id,e.name as name FROM enquiry_stages e";

	@Query(value = ENQUIRY_STAGE_FOR_COMBO, nativeQuery = true)
	public List<ComboInterface> getEnquiryStageForCombo();
	
	EnquiryStage findOneById(Long enquiryStatusId);
}
