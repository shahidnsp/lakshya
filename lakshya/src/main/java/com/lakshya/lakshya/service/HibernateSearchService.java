package com.lakshya.lakshya.service;

import org.apache.lucene.search.Query;
import org.springframework.stereotype.Service;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
//import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

@Service
public class HibernateSearchService {

//	@Autowired
//	private EntityManagerFactory entityManagerFactory;
//
////	@Autowired
////	SessionFactory sessionFactory;
//
//	@Transactional
//	public List<?> search(String text, Class<?> classType, Collection<?> resultList, String[] fields) {
////
////		EntityManager entityManager = entityManagerFactory.createEntityManager();
////		entityManager.getTransaction().begin();
////
////		try {
////
////			if (text.equals("*"))
//		return (List<?>) resultList;
////
////			FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
////			fullTextEntityManager.createIndexer().startAndWait();
////			QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory().buildQueryBuilder()
////					.forEntity(classType).get();
////
////			if (text.length() == 1) {
////				// working code
////				Query query = queryBuilder.bool().must(queryBuilder.keyword().wildcard().onFields(fields)
////						.matching("*" + text.toLowerCase() + "*").createQuery()).createQuery();
////				FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, classType);
////				resultList = fullTextQuery.getResultList();
////			} else {
////
////				// working code
//////				org.apache.lucene.search.Query
////				Query query = queryBuilder.keyword().onFields(fields).matching(text).createQuery();
////				javax.persistence.Query fullTextQuery = (javax.persistence.Query) fullTextEntityManager
////						.createFullTextQuery(query, classType);
////				resultList = fullTextQuery.getResultList();
////
////			}
//////			fullTextQuery.setFirstResult(15); //start from the 15th element
//////			fullTextQuery.setMaxResults(10); //return 10 elements
////		} catch (Exception e) {
////			e.printStackTrace();
////		}
////		return (List<?>) resultList;
//	}
//
//	@Bean
////	public Properties hibernateProperties(){
//	public Map<String, String> hibernateProperties() {
////	    final Properties properties = new Properties();
//		Map<String, String> properties = new HashMap<String, String>();
//
//		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
//		properties.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
//		properties.put("hibernate.hbm2ddl.auto", "update");
//		properties.put("hibernate.hbm2ddl.url", "jdbc:mysql://localhost:3306/lakshya");
//		properties.put("hibernate.hbm2ddl.username", "root");
//		properties.put("hibernate.hbm2ddl.password", "admin");
//		return properties;
//	}
//
////	@Bean
////	public EntityManagerFactory entityManagerFactory( Properties hibernateProperties ){
////	    final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//////	    em.setDataSource( dataSource );
//////	    em.setPackagesToScan( "net.initech.domain" );
////	    
////		em.setJpaVendorAdapter( new HibernateJpaVendorAdapter() );
////	    em.setJpaProperties( hibernateProperties );
//////	    em.setPersistenceUnitName( "mytestdomain" );
////	    em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
////	    em.afterPropertiesSet();
////
////	    return em.getObject();
////	}	

	@PersistenceContext
	private EntityManager entityManager;

	public Collection<?> search(String text, Class<?> classType, Collection<?> resultList, String[] fields) {
		try {
			if (text.equals("*"))
				return (List<?>) resultList;
			Query keywordQuery;
			if (text.length() == 1)
				keywordQuery = getQueryBuilder(classType).keyword().wildcard().onFields(fields).matching("*" + text.toLowerCase() + "*").createQuery();
			else
				keywordQuery = getQueryBuilder(classType).keyword().onFields(fields).matching("*" + text.toLowerCase() + "*").createQuery();
			
			resultList = getJpaQuery(keywordQuery,classType).getResultList();
		} catch (Exception e) {
			System.out.println("Error occured while searching , Cause : " + e);
		}
		return resultList;
	}

	private FullTextQuery getJpaQuery(org.apache.lucene.search.Query luceneQuery, Class<?> classType) {
		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
		return fullTextEntityManager.createFullTextQuery(luceneQuery, classType);
	}

	private QueryBuilder getQueryBuilder( Class<?> classType) {
		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
		return fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(classType).get();
	}

}
