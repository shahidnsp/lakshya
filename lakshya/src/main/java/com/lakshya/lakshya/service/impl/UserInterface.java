package com.lakshya.lakshya.service.impl;

public interface UserInterface {
	String getId();
    String getUserName();
    String getPass();
    String getTypeOfUser();
}
