package com.lakshya.lakshya.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.repository.UserRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService,UserService {
	
	@Autowired
	private UserRepository userDao;

	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		User user = userDao.findByUsername(userId);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority());
	}
	
	
	/*public HashMap<String, Object> loadUserByUsername(String userId) throws UsernameNotFoundException {
		User user = userDao.findByUsername(userId);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		
		UserDetails userDetails=new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority());
		
		HashMap<String, Object> currentUser = new HashMap<>();
		currentUser.put("user", user);
		currentUser.put("tokens", userDetails);
		
		
		return currentUser;
	}*/


	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List<User> findAll() {
		List<User> list = new ArrayList<>();
		userDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public User save(User user) {
		return userDao.save(user);
	}

	@Override
	public void delete(long id) {
		userDao.deleteById(id);
	}





}