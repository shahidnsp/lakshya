package com.lakshya.lakshya.service.impl;

import java.util.List;

import com.lakshya.lakshya.model.User;

public interface UserService {

    User save(User user);
    List<User> findAll();
    void delete(long id);
}
