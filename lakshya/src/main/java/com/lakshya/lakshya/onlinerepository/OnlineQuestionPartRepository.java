package com.lakshya.lakshya.onlinerepository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.onlineexam.OnlineQuestionPart;
import com.lakshya.lakshya.service.impl.ComboInterface;

public interface OnlineQuestionPartRepository extends CrudRepository< OnlineQuestionPart,Long> {
		
}
