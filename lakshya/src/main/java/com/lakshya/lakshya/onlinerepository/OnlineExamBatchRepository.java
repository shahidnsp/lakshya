package com.lakshya.lakshya.onlinerepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.Batch;
import com.lakshya.lakshya.model.onlineexam.OnlineExamBatch;

public interface OnlineExamBatchRepository extends CrudRepository<OnlineExamBatch,Long>  {

	List<OnlineExamBatch> findOnlineExamBatchByBatchId(Long batchId);

	 Long deleteByOnlineExamId(Long onlineExamId);
}
