package com.lakshya.lakshya.onlinerepository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.onlineexam.OnlineQuestionOption;
import com.lakshya.lakshya.model.onlineexam.OnlineQuestionPart;

public interface OnlineQuestionOptionRepository extends CrudRepository< OnlineQuestionOption ,Long>{
	List<OnlineQuestionOption> findAllByonlineQuestionPartIdAndIsCorrectAndAnswerValue(Long onlineQuestionPartId,Boolean isCorrect,String answerValue);
	List<OnlineQuestionOption> findAllByonlineQuestionPartIdAndIsCorrect(Long onlineQuestionPartId,Boolean isCorrect);
}
