package com.lakshya.lakshya.onlinerepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.onlineexam.OnlineExam;
import com.lakshya.lakshya.model.onlineexam.OnlineExamResult;

public interface OnlineExamResultRepository extends CrudRepository<OnlineExamResult,Long> {
	List<OnlineExamResult> findAllByStudent_Id(Long studentId);
	List<OnlineExamResult> findAllByOnlineExamId(Long onlineexamId);
	OnlineExamResult findOnlineExamResultByOnlineExamId(Long onlineExamId);
}
