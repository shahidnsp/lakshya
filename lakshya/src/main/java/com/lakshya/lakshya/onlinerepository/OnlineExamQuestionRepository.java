package com.lakshya.lakshya.onlinerepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.onlineexam.OnlineExam;
import com.lakshya.lakshya.model.onlineexam.OnlineExamQuestion;

public interface OnlineExamQuestionRepository extends CrudRepository< OnlineExamQuestion, Long>{

	List<OnlineExamQuestion> findOnlineExamQuestionByOnlineExamId(Long onlineExamId);
	
	
}
