package com.lakshya.lakshya.onlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.onlineexam.OnlineQuestionSectionItem;

public interface OnlineQuestionSectionItemRepository extends CrudRepository<OnlineQuestionSectionItem, Long>  {

	OnlineQuestionSectionItem findOneById(Long onlineQuestionSectionItemId);
	
}
