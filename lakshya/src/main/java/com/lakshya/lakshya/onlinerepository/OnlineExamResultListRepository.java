package com.lakshya.lakshya.onlinerepository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.onlineexam.OnlineExamResultList;


public interface OnlineExamResultListRepository extends CrudRepository<OnlineExamResultList,Long>{
	List<OnlineExamResultList> findAllByOnlineExamResultId(Long onlineResultId);
}
