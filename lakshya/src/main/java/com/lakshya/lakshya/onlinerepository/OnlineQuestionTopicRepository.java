package com.lakshya.lakshya.onlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.onlineexam.OnlineQuestionTopic;

public interface OnlineQuestionTopicRepository extends CrudRepository<OnlineQuestionTopic, Long>{

}
