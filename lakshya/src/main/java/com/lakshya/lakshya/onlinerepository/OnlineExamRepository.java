package com.lakshya.lakshya.onlinerepository;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.onlineexam.OnlineExam;

public interface OnlineExamRepository extends CrudRepository<OnlineExam,Long> {
	
}
