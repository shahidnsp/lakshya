package com.lakshya.lakshya.onlinerepository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.lakshya.lakshya.model.onlineexam.OnlineQuestionSection;

public interface OnlineQuestionSectionRepository extends CrudRepository<OnlineQuestionSection , Long>{

	OnlineQuestionSection findOneById(Long onlineQuestionSectionId);

	Long deleteByOnlineExamId(Long onlineExamId);
	
	List<OnlineQuestionSection> findAllByOnlineExamIdOrderByOrderBy(Long onlineExamId);
}
