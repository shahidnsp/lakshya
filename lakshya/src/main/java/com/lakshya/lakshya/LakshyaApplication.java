package com.lakshya.lakshya;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import com.lakshya.lakshya.property.FileStorageProperties;

import javassist.bytecode.stackmap.BasicBlock.Catch;

@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
public class LakshyaApplication extends SpringBootServletInitializer {

	private static String[] args;
	private static ConfigurableApplicationContext context;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(LakshyaApplication.class);
	}

	public static void main(String[] args) {
		LakshyaApplication.args = args;
		context = SpringApplication.run(LakshyaApplication.class, args);
	}

	public static void restart() {
		try {
			ApplicationArguments args = context.getBean(ApplicationArguments.class);
			Thread thread = new Thread(() -> {
				if (context.getId().equals(null))
					context = SpringApplication.run(LakshyaApplication.class, args.getSourceArgs());
				else {
					context.close();
					context = SpringApplication.run(LakshyaApplication.class, args.getSourceArgs());
				}
			});
			thread.setDaemon(false);
			thread.start();
		} catch (Exception e) {
			try {
				Thread thread = new Thread(() -> {

					context.close();
					context = SpringApplication.run(LakshyaApplication.class, args);

				});
				thread.setDaemon(false);
				thread.start();
			} catch (NullPointerException ne) {
				System.out.println("Last" + ne);
				context = SpringApplication.run(LakshyaApplication.class, args);
			}
		}

	}

}
