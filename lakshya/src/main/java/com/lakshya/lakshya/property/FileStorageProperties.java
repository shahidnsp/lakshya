package com.lakshya.lakshya.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "file")
@Component
public class FileStorageProperties {
    private String uploadDir;
    private String formatDir;

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }

	public String getFormatDir() {
		return formatDir;
	}

	public void setFormatDir(String formatDir) {
		this.formatDir = formatDir;
	}
    
    
}