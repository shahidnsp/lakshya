package com.lakshya.lakshya.config;

import java.util.ArrayList;
import java.util.HashMap;

public class MenuHandler {
	public static ArrayList<HashMap<String, Object>> getMenu() {
		ArrayList<HashMap<String, Object>> pages = new ArrayList<HashMap<String, Object>>();
		
		 /**
		  * FOR MAIN DASHBOARD.......................................
		  */
		 HashMap<String, Object> page1 = new HashMap<>();
		 page1.put("title", "Dashboard");
		 page1.put("name", "Dashboard");
		 page1.put("alias", "dashboard");
		 page1.put("route", "/");
		 page1.put("type", "Single");
		 page1.put("group", "");
		 page1.put("subgroup", "");
		 page1.put("for", "Main");
		 
		 pages.add(page1);
		 
		 HashMap<String, Object> page2 = new HashMap<>();
		 page2.put("title", "Branches");
		 page2.put("name", "Branches");
		 page2.put("alias", "branches");
		 page2.put("route", "/branches");
		 page2.put("type", "Single");
		 page2.put("group", "");
		 page2.put("subgroup", "");
		 page2.put("for", "Main");
		 
		 pages.add(page2);
		 
		 HashMap<String, Object> page3 = new HashMap<>();
		 page3.put("title", "Phone Books");
		 page3.put("name", "PhoneBooks");
		 page3.put("alias", "phonebooks");
		 page3.put("route", "/phonebooks");
		 page3.put("type", "Single");
		 page3.put("group", "");
		 page3.put("subgroup", "");
		 page3.put("for", "Main");
		 
		 pages.add(page3);
		 
		 HashMap<String, Object> page4 = new HashMap<>();
		 page4.put("title", "Admission");
		 page4.put("name", "Admission");
		 page4.put("alias", "admissionreport");
		 page4.put("route", "/admissionreport");
		 page4.put("type", "Group");
		 page4.put("group", "Report");
		 page4.put("subgroup", "");
		 page4.put("for", "Main");
		 
		 pages.add(page4);
		 
		 HashMap<String, Object> page5 = new HashMap<>();
		 page5.put("title", "Outstanding Fees");
		 page5.put("name", "OutstandingFees");
		 page5.put("alias", "outstandingfees");
		 page5.put("route", "/outstandingfees");
		 page5.put("type", "GroupSub");
		 page5.put("group", "Report");
		 page5.put("subgroup", "Fee Collection");
		 page5.put("for", "Main");
		 
		 pages.add(page5);
		 
		 HashMap<String, Object> page6 = new HashMap<>();
		 page6.put("title", "Overdue Fees");
		 page6.put("name", "OverdueFees");
		 page6.put("alias", "overduefees");
		 page6.put("route", "/overduefees");
		 page6.put("type", "GroupSub");
		 page6.put("group", "Report");
		 page6.put("subgroup", "Fee Collection");
		 page6.put("for", "Main");
		 
		 pages.add(page6);
		 
		 
		 HashMap<String, Object> page8 = new HashMap<>();
		 page8.put("title", "Fees Received");
		 page8.put("name", "FeesReceived");
		 page8.put("alias", "feesreceived");
		 page8.put("route", "/feesreceived");
		 page8.put("type", "GroupSub");
		 page8.put("group", "Report");
		 page8.put("subgroup", "Fee Collection");
		 page8.put("for", "Main");
		 
		 pages.add(page8);
		 
		 HashMap<String, Object> page9 = new HashMap<>();
		 page9.put("title", "Fees Cleared");
		 page9.put("name", "FeesCleared");
		 page9.put("alias", "feescleared");
		 page9.put("route", "/feescleared");
		 page9.put("type", "GroupSub");
		 page9.put("group", "Report");
		 page9.put("subgroup", "Fee Collection");
		 page9.put("for", "Main");
		 
		 pages.add(page9);
		 
		 HashMap<String, Object> page10 = new HashMap<>();
		 page10.put("title", "Enquiries Report");
		 page10.put("name", "EnquiriesReport");
		 page10.put("alias", "enquiriesreport");
		 page10.put("route", "/enquiriesreport");
		 page10.put("type", "Group");
		 page10.put("group", "Enquiry");
		 page10.put("subgroup", "");
		 page10.put("for", "Main");
		 
		 pages.add(page10);
		 
		 HashMap<String, Object> page11 = new HashMap<>();
		 page11.put("title", "Enquiries Closed Report");
		 page11.put("name", "EnquiriesClosedReport");
		 page11.put("alias", "enquiriesclosedreport");
		 page11.put("route", "/enquiriesclosedreport");
		 page11.put("type", "Group");
		 page11.put("group", "Enquiry");
		 page11.put("subgroup", "");
		 page11.put("for", "Main");
		 
		 pages.add(page11);
		 
		 HashMap<String, Object> page12 = new HashMap<>();
		 page12.put("title", "Web Enquiries Report");
		 page12.put("name", "WebEnquiriesClosedReport");
		 page12.put("alias", "webenquiriesclosedreport");
		 page12.put("route", "/webenquiriesclosedreport");
		 page12.put("type", "Group");
		 page12.put("group", "Enquiry");
		 page12.put("subgroup", "");
		 page12.put("for", "Main");
		 
		 pages.add(page12);
		 
		 HashMap<String, Object> page13 = new HashMap<>();
		 page13.put("title", "Users");
		 page13.put("name", "Users");
		 page13.put("alias", "users");
		 page13.put("route", "/users");
		 page13.put("type", "Group");
		 page13.put("group", "User Management");
		 page13.put("subgroup", "");
		 page13.put("for", "Main");
		 
		 pages.add(page13);
		 
		 HashMap<String, Object> page14 = new HashMap<>();
		 page14.put("title", "Roles");
		 page14.put("name", "Roles");
		 page14.put("alias", "roles");
		 page14.put("route", "/roles");
		 page14.put("type", "Group");
		 page14.put("group", "User Management");
		 page14.put("subgroup", "");
		 page14.put("for", "Main");
		 
		 pages.add(page14);
		 
		 HashMap<String, Object> page15 = new HashMap<>();
		 page15.put("title", "Login History");
		 page15.put("name", "LoginHistory");
		 page15.put("alias", "loginhistory");
		 page15.put("route", "/loginhistory");
		 page15.put("type", "Group");
		 page15.put("group", "User Management");
		 page15.put("subgroup", "");
		 page15.put("for", "Main");
		 
		 pages.add(page15);
		 
		 HashMap<String, Object> page16 = new HashMap<>();
		 page16.put("title", "User Activities");
		 page16.put("name", "UserActivities");
		 page16.put("alias", "useractivities");
		 page16.put("route", "/useractivities");
		 page16.put("type", "Group");
		 page16.put("group", "User Management");
		 page16.put("subgroup", "");
		 page16.put("for", "Main");
		 
		 pages.add(page16);
		 
		 
		 /**
		  * FOR MAIN BASIC SETTINGS.......................................
		  */
		 HashMap<String, Object> page17 = new HashMap<>();
		 page17.put("title", "Branch Category");
		 page17.put("name", "BranchCategory");
		 page17.put("alias", "branchcategory");
		 page17.put("route", "/branchcategory");
		 page17.put("type", "Single");
		 page17.put("group", "");
		 page17.put("subgroup", "");
		 page17.put("for", "Settings");
		 
		 pages.add(page17);
		 
		 HashMap<String, Object> page18 = new HashMap<>();
		 page18.put("title", "Academic Year");
		 page18.put("name", "AcademicYear");
		 page18.put("alias", "academicyear");
		 page18.put("route", "/academicyear");
		 page18.put("type", "Single");
		 page18.put("group", "");
		 page18.put("subgroup", "");
		 page18.put("for", "Settings");
		 
		 pages.add(page18);
		 
		 HashMap<String, Object> page19 = new HashMap<>();
		 page19.put("title", "Enquiry Source");
		 page19.put("name", "EnquirySource");
		 page19.put("alias", "enquirysource");
		 page19.put("route", "/enquirysource");
		 page19.put("type", "Single");
		 page19.put("group", "");
		 page19.put("subgroup", "");
		 page19.put("for", "Settings");
	
		 pages.add(page19);
		 
		 HashMap<String, Object> page20 = new HashMap<>();
		 page20.put("title", "Enquiry Stages");
		 page20.put("name", "EnquiryStages");
		 page20.put("alias", "enquirystages");
		 page20.put("route", "/enquirystages");
		 page20.put("type", "Single");
		 page20.put("group", "");
		 page20.put("subgroup", "");
		 page20.put("for", "Settings");
		 
		 pages.add(page20);
		 
		 HashMap<String, Object> page21 = new HashMap<>();
		 page21.put("title", "Loss Reason");
		 page21.put("name", "LossReason");
		 page21.put("alias", "lossreason");
		 page21.put("route", "/lossreason");
		 page21.put("type", "Single");
		 page21.put("group", "");
		 page21.put("subgroup", "");
		 page21.put("for", "Settings");
		 
		 pages.add(page21);
		 
		 HashMap<String, Object> page22 = new HashMap<>();
		 page22.put("title", "IP List");
		 page22.put("name", "IPList");
		 page22.put("alias", "iplist");
		 page22.put("route", "/iplist");
		 page22.put("type", "Single");
		 page22.put("group", "");
		 page22.put("subgroup", "");
		 page22.put("for", "Settings");
		 
		 pages.add(page22);
		 
		 
		 /**
		  * FOR BRANCH DASHBOARD.......................................
		  */
		 
		 HashMap<String, Object> page23 = new HashMap<>();
		 page23.put("title", "Batches");
		 page23.put("name", "Batches");
		 page23.put("alias", "batches");
		 page23.put("route", "/batches");
		 page23.put("type", "Single");
		 page23.put("group", "");
		 page23.put("subgroup", "");
		 page23.put("for", "Branch");
		 
		 pages.add(page23);
		 
		 HashMap<String, Object> page24 = new HashMap<>();
		 page24.put("title", "Courses");
		 page24.put("name", "Courses");
		 page24.put("alias", "courses");
		 page24.put("route", "/courses");
		 page24.put("type", "Single");
		 page24.put("group", "");
		 page24.put("subgroup", "");
		 page24.put("for", "Branch");
		 
		 pages.add(page24);
		 
		 HashMap<String, Object> page25 = new HashMap<>();
		 page25.put("title", "Chapters");
		 page25.put("name", "Chapters");
		 page25.put("alias", "chapters");
		 page25.put("route", "/chapters");
		 page25.put("type", "Single");
		 page25.put("group", "");
		 page25.put("subgroup", "");
		 page25.put("for", "Branch");
		 
		 pages.add(page25);
		 
		 
		 return pages;
	}
}
