package com.lakshya.lakshya.config;

import java.util.ArrayList;
import java.util.HashMap;

public class UserHelper {
	
	public static ArrayList<HashMap<String, Object>>  notPages(String userPermission)
    {
		ArrayList<HashMap<String, Object>> pages = new ArrayList<HashMap<String, Object>>();
		ArrayList<HashMap<String, Object>> allPages = MenuHandler.getMenu();
		


        //Check if no permissions are set
        if(userPermission.length() == 0)
            return allPages;

        String[] permissions = userPermission.split(",");
        int key=0;
        for (String permission : permissions) {
        	char[] pagePermission= permission.toCharArray();
        	if(pagePermission[0] == '1'){
        		
        		if(allPages.size()>key){
        			HashMap<String, Object> page = allPages.get(key);
        			pages.add(page);
                }
        	}
        	key++;
        }
        return pages;
    }
	
	
	public static ArrayList<HashMap<String, Object>> pages(String userPermission,Boolean all)
    {
		ArrayList<HashMap<String, Object>> pages = new ArrayList<HashMap<String, Object>>();
		ArrayList<HashMap<String, Object>> allPages = MenuHandler.getMenu();
		
		String[] permissions = userPermission.split(",");
		int key=0;
        for (String permission : permissions) {
        	char[] pagePermission= permission.toCharArray();
        	if(pagePermission[0] == '1'){
        		if(allPages.size()>key){
        			HashMap<String, Object> page = allPages.get(key);
        			if(all) {
        				HashMap<String, Object> pagepermission = new HashMap<String, Object>();
        				pagepermission.put("write", pagePermission[1] == '1' ? true :false);
        				pagepermission.put("edit", pagePermission[2] == '1' ? true :false);
        				pagepermission.put("delete", pagePermission[3] == '1' ? true :false);
        				page.put(page.get("alias").toString(), pagepermission);
        			}
        			
        			pages.add(page);
        			
                }
        	}
        	key++;
        }
		
        return pages;
    }
}
