package com.lakshya.lakshya.config;

import java.util.Date;
import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseFormat {
	public static HashMap<String, Object> createFormat(Object object,String message) {
		HashMap<String, Object> successResponse= new HashMap<>();
		 
		 successResponse.put("timestamp", new Date());
		 successResponse.put("data",object);
		 successResponse.put("status",true);
		 successResponse.put("statuscode",200);
		 successResponse.put("message",message);
		 
    	return successResponse;
	}
	
	public static HashMap<String, Object> failedResponse(String message) {
		 HashMap<String, Object> successResponse= new HashMap<>();
		
		 successResponse.put("timestamp", new Date());
		 successResponse.put("status",true);
		 successResponse.put("statuscode",200);
		 successResponse.put("message",message);
		 
   	return successResponse;
	}
	

}
