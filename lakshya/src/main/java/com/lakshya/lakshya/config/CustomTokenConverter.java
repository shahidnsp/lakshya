package com.lakshya.lakshya.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.stereotype.Component;

import com.lakshya.lakshya.model.Student;
import com.lakshya.lakshya.model.User;
import com.lakshya.lakshya.model.UserAssignedStudent;
import com.lakshya.lakshya.repository.StudentRepository;
import com.lakshya.lakshya.repository.UserAssignedStudentRepository;
import com.lakshya.lakshya.repository.UserRepository;



@Component
public class CustomTokenConverter extends JwtAccessTokenConverter {
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserAssignedStudentRepository userAssignedStudentRepository;
	@Autowired
	private StudentRepository studentRepository;
	
	 @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        final Map<String, Object> additionalInfo = new HashMap<>();
        User user = userRepository.findByUsername(authentication.getName()); 
        
        additionalInfo.put("usertype", user.getTypeOfUser());	 
        if(user.getTypeOfUser().equals("Student")) {
        	List<UserAssignedStudent> userAssignedStudent=userAssignedStudentRepository.findByUserId(user.getId());
        	if(!userAssignedStudent.isEmpty()) {
        		Student student=studentRepository.getOne(userAssignedStudent.get(0).getStudentId());
        		
        		final Map<String, Object> studentInfo = new HashMap<>();
        		studentInfo.put("id", student.getId());
        		studentInfo.put("name", student.getName());
        		studentInfo.put("rollno", student.getRollno());
        		studentInfo.put("photo", student.getPhoto());
        		studentInfo.put("gender", student.getGender());
        		studentInfo.put("caste", student.getCaste());        		
        		studentInfo.put("adharno", student.getAdharno());
        		studentInfo.put("dateofbirth", student.getDateofbirth());
        		studentInfo.put("address", student.getAddress());
        		studentInfo.put("landmark", student.getLandmark());
        		
        		additionalInfo.put("student", studentInfo);	
        		additionalInfo.put("student_id", student.getId());
        		additionalInfo.put("batchId", student.getBatchId());
        	}
        }
        
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);

        return super.enhance(accessToken, authentication);
    }
	 
}
