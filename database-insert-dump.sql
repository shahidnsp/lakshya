


INSERT INTO `online_exam_results` (`id`, `created_at`, `noofanswered`, `noofunanswered`, `online_exam_id`, `student_id`, `totalmark`, `updated_at`, `noofcorrect`, `noofincorrect`, `totalcorrectmark`, `totalnegativemark`) VALUES
(10, '2020-01-29 18:50:56', 5, 0, 16, 1, '4.00', '2020-01-29 18:50:56', 3, 2, 6, 2),
(11, '2020-01-30 11:32:20', 5, 0, 16, 1, '1.00', '2020-01-30 11:32:20', 2, 3, 4, 3),
(12, '2020-01-30 12:24:22', 0, 0, 16, 1, '0.00', '2020-01-30 12:24:22', 0, 0, 0, 0);



INSERT INTO `online_questions` (`id`, `active`, `course_id`, `created_at`, `difficulty_level`, `faculty_id`, `format`, `question_year_id`, `solutions`, `subject_id`, `updated_at`, `online_question_topic_id`, `question_for`) VALUES
(83, b'1', 13, '2020-01-29 10:30:16', 'Moderate', 16, 'Multiple', 27, NULL, 7, '2020-01-29 10:30:16', NULL, NULL),
(88, b'1', 13, '2020-01-29 11:09:21', 'Moderate', 16, 'Multiple', 28, NULL, 7, '2020-01-29 11:09:21', NULL, NULL),
(89, b'1', 1, '2020-01-29 11:11:28', 'Moderate', 16, 'Multiple', 27, NULL, 7, '2020-01-29 11:11:28', NULL, NULL),
(91, b'1', 13, '2020-01-29 11:25:29', 'Easy', 16, 'Multiple', 27, NULL, 7, '2020-01-29 11:25:29', NULL, NULL),
(92, b'1', 13, '2020-01-29 11:27:15', 'Easy', 16, 'Multiple', 27, NULL, 7, '2020-01-29 11:27:15', NULL, NULL),
(93, b'1', 13, '2020-01-29 12:09:41', 'Easy', 16, 'Single', 28, NULL, 7, '2020-01-29 12:09:41', NULL, NULL),
(94, b'1', 13, '2020-01-29 13:15:19', 'Easy', 16, 'Single', 27, NULL, 7, '2020-01-29 13:15:19', NULL, NULL),
(96, b'1', 13, '2020-01-29 13:44:19', 'Moderate', 16, 'Multiple', 27, NULL, 7, '2020-01-29 13:44:19', NULL, NULL),
(97, b'1', 13, '2020-01-29 13:46:13', 'Easy', 16, 'Multiple', 27, NULL, 6, '2020-01-29 13:46:13', NULL, NULL),
(98, b'1', 1, '2020-01-29 13:47:07', 'Easy', 16, 'Single', 27, NULL, 6, '2020-01-29 13:47:07', NULL, NULL),
(99, b'1', 13, '2020-01-29 13:48:56', 'Easy', 16, 'Single', 27, NULL, 7, '2020-01-29 13:48:56', NULL, NULL),
(100, b'1', 13, '2020-01-29 13:51:15', NULL, 16, 'Paragraph', 27, NULL, 6, '2020-01-29 13:51:15', 8, NULL),
(102, b'1', 14, '2020-01-29 15:36:14', 'Easy', 17, 'Single', 27, NULL, 7, '2020-01-29 15:36:14', NULL, 'ACCA'),
(103, b'1', 14, '2020-01-29 15:37:56', 'Easy', 17, 'Single', 27, NULL, 7, '2020-01-29 15:37:56', NULL, 'ACCA'),
(104, b'1', 14, '2020-01-29 15:40:32', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 15:40:32', 10, NULL),
(105, b'1', 14, '2020-01-29 16:04:55', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 16:04:55', 11, NULL),
(106, b'1', 14, '2020-01-29 16:05:01', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 16:05:01', 12, NULL),
(107, b'1', 14, '2020-01-29 16:07:53', 'Easy', 17, 'Single', 27, NULL, 7, '2020-01-29 16:07:53', NULL, 'ACCA'),
(115, b'1', 14, '2020-01-29 16:27:05', 'Easy', 17, 'Single', 27, NULL, 7, '2020-01-29 16:27:05', NULL, 'ACCA'),
(116, b'1', 14, '2020-01-29 16:29:14', 'Easy', 17, 'Single', 27, NULL, 7, '2020-01-29 16:29:14', NULL, 'ACCA'),
(117, b'1', 14, '2020-01-29 16:31:03', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 16:31:03', 14, NULL),
(118, b'1', 14, '2020-01-29 17:25:11', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 17:25:11', 15, NULL),
(119, b'1', 14, '2020-01-29 17:29:45', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 17:29:45', 16, NULL),
(120, b'1', 14, '2020-01-29 17:32:06', 'Easy', 17, 'Single', 27, NULL, 7, '2020-01-29 17:32:06', NULL, 'ACCA'),
(121, b'1', 14, '2020-01-29 17:41:11', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 17:41:11', 17, NULL),
(122, b'1', 14, '2020-01-29 17:42:35', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 17:42:35', 18, NULL),
(123, b'1', 14, '2020-01-29 17:52:55', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 17:52:55', 19, NULL),
(124, b'1', 14, '2020-01-29 17:52:56', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 17:52:56', 19, NULL),
(125, b'1', 14, '2020-01-29 17:52:56', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 17:52:56', 19, NULL),
(126, b'1', 14, '2020-01-29 17:52:56', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 17:52:56', 19, NULL),
(127, b'1', 14, '2020-01-29 17:52:56', NULL, 17, 'Paragraph', 27, NULL, 7, '2020-01-29 17:52:56', 19, NULL),
(129, b'1', 1, '2020-01-30 13:32:01', NULL, 16, 'Paragraph', 27, NULL, 6, '2020-01-30 13:32:01', 20, NULL),
(130, b'1', 1, '2020-01-30 13:32:02', NULL, 16, 'Paragraph', 27, NULL, 6, '2020-01-30 13:32:02', 20, NULL),
(131, b'1', 1, '2020-01-30 13:36:42', 'Easy', 17, 'Single', 27, NULL, 6, '2020-01-30 13:36:42', NULL, 'ACCA');



INSERT INTO `online_question_options` (`id`, `answer_value`, `created_at`, `is_correct`, `online_question_part_id`, `updated_at`) VALUES
(234, 'mamba', '2020-01-29 10:30:16', b'0', 104, '2020-01-29 10:30:16'),
(235, 'buffalo', '2020-01-29 10:30:16', b'0', 104, '2020-01-29 10:30:16'),
(236, 'cat', '2020-01-29 10:30:16', b'0', 104, '2020-01-29 10:30:16'),
(237, 'dog ', '2020-01-29 10:30:16', b'0', 104, '2020-01-29 10:30:16'),
(254, 'fgdf', '2020-01-29 11:09:21', b'0', 109, '2020-01-29 11:09:21'),
(255, 'fdg', '2020-01-29 11:09:21', b'0', 109, '2020-01-29 11:09:21'),
(256, 'fg', '2020-01-29 11:09:21', b'0', 109, '2020-01-29 11:09:21'),
(257, 'dfg', '2020-01-29 11:09:21', b'0', 109, '2020-01-29 11:09:21'),
(258, 'fgdf', '2020-01-29 11:09:21', b'0', 110, '2020-01-29 11:09:21'),
(259, 'fgdf', '2020-01-29 11:09:21', b'0', 110, '2020-01-29 11:09:21'),
(260, 'dfgfd', '2020-01-29 11:09:21', b'0', 110, '2020-01-29 11:09:21'),
(261, 'dfg', '2020-01-29 11:09:21', b'0', 110, '2020-01-29 11:09:21'),
(262, 'lorry', '2020-01-29 11:11:29', b'0', 111, '2020-01-29 11:11:29'),
(263, 'Cow', '2020-01-29 11:11:29', b'0', 111, '2020-01-29 11:11:29'),
(264, 'car', '2020-01-29 11:11:29', b'1', 111, '2020-01-29 11:11:29'),
(265, 'bus', '2020-01-29 11:11:29', b'0', 111, '2020-01-29 11:11:29'),
(271, 'sad', '2020-01-29 11:25:29', b'0', 114, '2020-01-29 11:25:29'),
(272, 'asd', '2020-01-29 11:25:29', b'0', 114, '2020-01-29 11:25:29'),
(273, 'sad', '2020-01-29 11:25:29', b'1', 114, '2020-01-29 11:25:29'),
(274, 'asd', '2020-01-29 11:25:29', b'0', 114, '2020-01-29 11:25:29'),
(275, 'asd', '2020-01-29 11:27:15', b'0', 115, '2020-01-29 11:27:15'),
(276, 'asd', '2020-01-29 11:27:15', b'1', 115, '2020-01-29 11:27:15'),
(277, 'asd', '2020-01-29 11:27:15', b'0', 115, '2020-01-29 11:27:15'),
(278, 'asd', '2020-01-29 11:27:15', b'0', 115, '2020-01-29 11:27:15'),
(279, 'language', '2020-01-29 12:09:41', b'0', 116, '2020-01-29 12:09:41'),
(280, 'dfgf', '2020-01-29 13:15:19', b'0', 117, '2020-01-29 13:15:19'),
(281, 'dfgf', '2020-01-29 13:15:19', b'0', 117, '2020-01-29 13:15:19'),
(282, 'dfgf', '2020-01-29 13:15:19', b'0', 117, '2020-01-29 13:15:19'),
(283, 'fdg', '2020-01-29 13:15:19', b'1', 117, '2020-01-29 13:15:19'),
(285, 'c', '2020-01-29 13:44:19', b'0', 120, '2020-01-29 13:44:19'),
(286, 'b', '2020-01-29 13:44:19', b'0', 120, '2020-01-29 13:44:19'),
(287, 'a', '2020-01-29 13:44:19', b'1', 120, '2020-01-29 13:44:19'),
(288, 'd', '2020-01-29 13:44:19', b'0', 120, '2020-01-29 13:44:19'),
(289, 'x', '2020-01-29 13:44:19', b'0', 121, '2020-01-29 13:44:19'),
(290, 'z', '2020-01-29 13:44:19', b'0', 121, '2020-01-29 13:44:19'),
(291, 'y', '2020-01-29 13:44:19', b'1', 121, '2020-01-29 13:44:19'),
(292, 'w', '2020-01-29 13:44:19', b'0', 121, '2020-01-29 13:44:19'),
(293, 'programming', '2020-01-29 13:46:13', b'0', 122, '2020-01-29 13:46:13'),
(294, 'aaaaaa', '2020-01-29 13:47:07', b'0', 124, '2020-01-29 13:47:07'),
(295, 'programmong', '2020-01-29 13:47:07', b'1', 124, '2020-01-29 13:47:07'),
(296, 'ccccccccc', '2020-01-29 13:47:07', b'0', 124, '2020-01-29 13:47:07'),
(297, 'bbbbbbbbbbbbb', '2020-01-29 13:47:07', b'0', 124, '2020-01-29 13:47:07'),
(298, 'c', '2020-01-29 13:48:56', b'0', 125, '2020-01-29 13:48:56'),
(299, 'b', '2020-01-29 13:48:56', b'0', 125, '2020-01-29 13:48:56'),
(300, 'a', '2020-01-29 13:48:56', b'0', 125, '2020-01-29 13:48:56'),
(301, 'd', '2020-01-29 13:48:56', b'0', 125, '2020-01-29 13:48:56'),
(302, 'c', '2020-01-29 13:51:15', b'0', 126, '2020-01-29 13:51:15'),
(303, 'd', '2020-01-29 13:51:15', b'0', 126, '2020-01-29 13:51:15'),
(304, 'a', '2020-01-29 13:51:15', b'0', 126, '2020-01-29 13:51:15'),
(305, 'b', '2020-01-29 13:51:15', b'0', 126, '2020-01-29 13:51:15'),
(307, 'Commercial paper', '2020-01-29 15:36:14', b'0', 128, '2020-01-29 15:36:14'),
(308, 'Convertible loan notes', '2020-01-29 15:36:14', b'1', 128, '2020-01-29 15:36:14'),
(309, 'Treasury bills', '2020-01-29 15:36:14', b'0', 128, '2020-01-29 15:36:14'),
(310, 'Certificates of deposit', '2020-01-29 15:36:14', b'0', 128, '2020-01-29 15:36:14'),
(311, 'Dividend policy', '2020-01-29 15:37:56', b'1', 129, '2020-01-29 15:37:56'),
(312, 'Interest rate management', '2020-01-29 15:37:56', b'0', 129, '2020-01-29 15:37:56'),
(313, 'Liquidity management', '2020-01-29 15:37:56', b'0', 129, '2020-01-29 15:37:56'),
(314, 'Management of relationship with the bank', '2020-01-29 15:37:56', b'0', 129, '2020-01-29 15:37:56'),
(315, '1 and 3', '2020-01-29 15:40:32', b'0', 130, '2020-01-29 15:40:32'),
(316, '2 and 4 only', '2020-01-29 15:40:32', b'1', 130, '2020-01-29 15:40:32'),
(317, '2, 3 and 4', '2020-01-29 15:40:32', b'0', 130, '2020-01-29 15:40:32'),
(318, '1 only', '2020-01-29 15:40:32', b'0', 130, '2020-01-29 15:40:32'),
(319, '$30m', '2020-01-29 16:04:55', b'0', 131, '2020-01-29 16:04:55'),
(320, '$40m', '2020-01-29 16:04:55', b'0', 131, '2020-01-29 16:04:55'),
(321, '$17m', '2020-01-29 16:04:55', b'0', 131, '2020-01-29 16:04:55'),
(322, '$10m', '2020-01-29 16:04:55', b'1', 131, '2020-01-29 16:04:55'),
(323, '$17m', '2020-01-29 16:05:01', b'0', 132, '2020-01-29 16:05:01'),
(324, '$30m', '2020-01-29 16:05:01', b'0', 132, '2020-01-29 16:05:01'),
(325, '$40m', '2020-01-29 16:05:01', b'0', 132, '2020-01-29 16:05:01'),
(326, '$10m', '2020-01-29 16:05:01', b'1', 132, '2020-01-29 16:05:01'),
(327, 'As risk rises, the market value of the security will fall to ensure that investors receive an increased yield', '2020-01-29 16:07:53', b'1', 133, '2020-01-29 16:07:53'),
(328, 'As risk rises, the market value of the security will fall to ensure that investors receive a reduced yield', '2020-01-29 16:07:53', b'0', 133, '2020-01-29 16:07:53'),
(329, 'As risk rises, the market value of the security will rise to ensure that investors receive an increased yield', '2020-01-29 16:07:53', b'0', 133, '2020-01-29 16:07:53'),
(330, 'As risk rises, the market value of the security will rise to ensure that investors receive a reduced yield', '2020-01-29 16:07:53', b'0', 133, '2020-01-29 16:07:53'),
(335, 'Producing monthly management accounts', '2020-01-29 16:27:05', b'0', 135, '2020-01-29 16:27:05'),
(336, 'Deciding pay rates for staff', '2020-01-29 16:27:05', b'0', 135, '2020-01-29 16:27:05'),
(337, 'Advising on investment in non-current assets', '2020-01-29 16:27:05', b'1', 135, '2020-01-29 16:27:05'),
(338, 'Producing annual accounts', '2020-01-29 16:27:05', b'0', 135, '2020-01-29 16:27:05'),
(339, ' €1·588 per $1', '2020-01-29 16:29:14', b'0', 136, '2020-01-29 16:29:14'),
(340, ' €1·520 per $1', '2020-01-29 16:29:14', b'0', 136, '2020-01-29 16:29:14'),
(341, ' €1·566 per $1', '2020-01-29 16:29:14', b'1', 136, '2020-01-29 16:29:14'),
(342, '€1·499 per $1', '2020-01-29 16:29:14', b'0', 136, '2020-01-29 16:29:14'),
(343, '$52·71', '2020-01-29 16:31:03', b'0', 137, '2020-01-29 16:31:03'),
(344, '$62·71', '2020-01-29 16:31:03', b'0', 137, '2020-01-29 16:31:03'),
(345, '$82·71', '2020-01-29 16:31:03', b'0', 137, '2020-01-29 16:31:03'),
(346, '$62·71', '2020-01-29 16:31:03', b'0', 137, '2020-01-29 16:31:03'),
(347, '1 only', '2020-01-29 17:25:11', b'0', 138, '2020-01-29 17:25:11'),
(348, '1, 2 and 3', '2020-01-29 17:25:11', b'0', 138, '2020-01-29 17:25:11'),
(349, '2 and 3 only', '2020-01-29 17:25:11', b'0', 138, '2020-01-29 17:25:11'),
(350, '1 and 2 only', '2020-01-29 17:25:11', b'1', 138, '2020-01-29 17:25:11'),
(351, '1 and 3', '2020-01-29 17:29:46', b'0', 139, '2020-01-29 17:29:46'),
(352, '1 only', '2020-01-29 17:29:46', b'0', 139, '2020-01-29 17:29:46'),
(353, '2 and 4 only', '2020-01-29 17:29:46', b'1', 139, '2020-01-29 17:29:46'),
(354, '2, 3 and 4', '2020-01-29 17:29:46', b'0', 139, '2020-01-29 17:29:46'),
(355, 'Increasing taxation and keeping government expenditure the same', '2020-01-29 17:32:06', b'0', 140, '2020-01-29 17:32:06'),
(356, 'Decreasing money supply', '2020-01-29 17:32:06', b'1', 140, '2020-01-29 17:32:06'),
(357, 'Decreasing interest rates', '2020-01-29 17:32:06', b'0', 140, '2020-01-29 17:32:06'),
(358, 'Decreasing taxation and increasing government expenditure', '2020-01-29 17:32:06', b'1', 140, '2020-01-29 17:32:06'),
(359, '14%', '2020-01-29 17:41:11', b'0', 141, '2020-01-29 17:41:11'),
(360, '18%', '2020-01-29 17:41:11', b'0', 141, '2020-01-29 17:41:11'),
(361, '25%', '2020-01-29 17:41:11', b'0', 141, '2020-01-29 17:41:11'),
(362, '20%', '2020-01-29 17:41:11', b'1', 141, '2020-01-29 17:41:11'),
(363, '$1·60', '2020-01-29 17:42:35', b'0', 142, '2020-01-29 17:42:35'),
(364, '$0·40', '2020-01-29 17:42:35', b'1', 142, '2020-01-29 17:42:35'),
(365, '$0·50', '2020-01-29 17:42:35', b'0', 142, '2020-01-29 17:42:35'),
(366, '$1·50', '2020-01-29 17:42:35', b'0', 142, '2020-01-29 17:42:35'),
(367, '€1·499 per $1', '2020-01-29 17:52:55', b'0', 143, '2020-01-29 17:52:55'),
(368, '€1·520 per $1', '2020-01-29 17:52:55', b'0', 143, '2020-01-29 17:52:55'),
(369, '€1·566 per $1', '2020-01-29 17:52:55', b'1', 143, '2020-01-29 17:52:55'),
(370, '€1·588 per $1', '2020-01-29 17:52:55', b'0', 143, '2020-01-29 17:52:55'),
(371, 'Transaction risk', '2020-01-29 17:52:56', b'1', 144, '2020-01-29 17:52:56'),
(372, 'Translation risk', '2020-01-29 17:52:56', b'0', 144, '2020-01-29 17:52:56'),
(373, 'Business risk', '2020-01-29 17:52:56', b'0', 144, '2020-01-29 17:52:56'),
(374, 'Economic risk', '2020-01-29 17:52:56', b'0', 144, '2020-01-29 17:52:56'),
(375, 'Currency futures', '2020-01-29 17:52:56', b'0', 145, '2020-01-29 17:52:56'),
(376, 'Money market hedge', '2020-01-29 17:52:56', b'0', 145, '2020-01-29 17:52:56'),
(377, 'Currency swap', '2020-01-29 17:52:56', b'1', 145, '2020-01-29 17:52:56'),
(378, 'Forward exchange contract', '2020-01-29 17:52:56', b'0', 145, '2020-01-29 17:52:56'),
(379, 'The dollar nominal interest rate is less than the euro nominal interest rate', '2020-01-29 17:52:56', b'1', 146, '2020-01-29 17:52:56'),
(380, 'The dollar inflation rate is greater than the euro inflation rate', '2020-01-29 17:52:56', b'0', 146, '2020-01-29 17:52:56'),
(381, 'In exchange for a premium, Herd Co could hedge its interest rate risk by buying interest rate options', '2020-01-29 17:52:56', b'1', 147, '2020-01-29 17:52:56'),
(382, 'Herd Co can hedge its interest rate risk by buying interest rate futures now in order to sell them at a future date', '2020-01-29 17:52:56', b'0', 147, '2020-01-29 17:52:56'),
(383, 'Taking out a variable rate overdraft will allow Herd Co to hedge the interest rate risk through matching', '2020-01-29 17:52:56', b'0', 147, '2020-01-29 17:52:56'),
(384, 'Buying a floor will give Herd Co a hedge against interest rate increases', '2020-01-29 17:52:56', b'0', 147, '2020-01-29 17:52:56'),
(386, 'zdda', '2020-01-30 13:32:01', b'0', 149, '2020-01-30 13:32:01'),
(387, 'dad', '2020-01-30 13:32:02', b'0', 150, '2020-01-30 13:32:02'),
(388, 'dad', '2020-01-30 13:32:02', b'0', 150, '2020-01-30 13:32:02'),
(389, 'Value 1', '2020-01-30 13:36:42', b'0', 151, '2020-01-30 13:36:42');



INSERT INTO `online_question_parts` (`id`, `created_at`, `online_question_id`, `question_text`, `question_type`, `updated_at`, `online_question_part_id`) VALUES
(104, '2020-01-29 10:30:16', 83, 'who is balck in color', 'Radio Button', '2020-01-29 10:30:16', NULL),
(109, '2020-01-29 11:09:21', 88, 'dfgfd', 'Radio Button', '2020-01-29 11:09:21', NULL),
(110, '2020-01-29 11:09:21', 88, 'cvd', 'Radio Button', '2020-01-29 11:09:21', NULL),
(111, '2020-01-29 11:11:28', 89, 'what is this', 'Radio Button', '2020-01-29 11:11:28', NULL),
(114, '2020-01-29 11:25:29', 91, 'sadsad', 'Radio Button', '2020-01-29 11:25:29', NULL),
(115, '2020-01-29 11:27:15', 92, 'testttttttttttttttttttttttttttttttt', 'Radio Button', '2020-01-29 11:27:15', NULL),
(116, '2020-01-29 12:09:41', 93, 'java is programming ', 'Text Feild', '2020-01-29 12:09:41', NULL),
(117, '2020-01-29 13:15:19', 94, 'test from select', 'Select Box', '2020-01-29 13:15:19', NULL),
(120, '2020-01-29 13:44:19', 96, 'aaaaaaaaaaaa', 'Radio Button', '2020-01-29 13:44:19', NULL),
(121, '2020-01-29 13:44:19', 96, 'zzzzzzzzzzzzz', 'Radio Button', '2020-01-29 13:44:19', NULL),
(122, '2020-01-29 13:46:13', 97, 'java is a', 'Text Feild', '2020-01-29 13:46:13', NULL),
(123, '2020-01-29 13:46:13', 97, 'language', 'None', '2020-01-29 13:46:13', NULL),
(124, '2020-01-29 13:47:07', 98, 'java is a', 'Radio Button', '2020-01-29 13:47:07', NULL),
(125, '2020-01-29 13:48:56', 99, 'alpha', 'Radio Button', '2020-01-29 13:48:56', NULL),
(126, '2020-01-29 13:51:15', 100, 'dfg', 'Radio Button', '2020-01-29 13:51:15', NULL),
(128, '2020-01-29 15:36:14', 102, 'Which of the following financial instruments will NOT be traded on a money market?', 'Radio Button', '2020-01-29 15:36:14', NULL),
(129, '2020-01-29 15:37:56', 103, 'Andrew Co is a large listed company financed by both equity and debt. In which of the following areas of financial management will the impact of working capital management be smallest?', 'Radio Button', '2020-01-29 15:37:56', NULL),
(130, '2020-01-29 15:40:32', 104, '  ', 'Check Box', '2020-01-29 15:40:32', NULL),
(131, '2020-01-29 16:04:55', 105, ' ', 'Select Box', '2020-01-29 16:04:55', NULL),
(132, '2020-01-29 16:05:01', 106, ' ', 'Select Box', '2020-01-29 16:05:01', NULL),
(133, '2020-01-29 16:07:53', 107, 'In relation to an irredeemable security paying a fixed rate of interest, which of the following statements is correct?', 'Radio Button', '2020-01-29 16:07:53', NULL),
(135, '2020-01-29 16:27:05', 115, 'Which of the following would you expect to be the responsibility of financial management?', 'Radio Button', '2020-01-29 16:27:05', NULL),
(136, '2020-01-29 16:29:14', 116, 'What is the six-month forward exchange rate predicted by interest rate parity?', 'Radio Button', '2020-01-29 16:29:14', NULL),
(137, '2020-01-29 16:31:03', 117, '  ', 'Radio Button', '2020-01-29 16:31:03', NULL),
(138, '2020-01-29 17:25:11', 118, '  ', 'Radio Button', '2020-01-29 17:25:11', NULL),
(139, '2020-01-29 17:29:46', 119, ' ', 'Radio Button', '2020-01-29 17:29:46', NULL),
(140, '2020-01-29 17:32:06', 120, 'Which of the following government actions would lead to an increase in aggregate demand?', 'Check Box', '2020-01-29 17:32:06', NULL),
(141, '2020-01-29 17:41:11', 121, 'What is Peach Co’s return on capital employed (ROCE)?', 'Radio Button', '2020-01-29 17:41:11', NULL),
(142, '2020-01-29 17:42:35', 122, 'What is the theoretical value of a right per existing share?', 'Radio Button', '2020-01-29 17:42:35', NULL),
(143, '2020-01-29 17:52:55', 123, 'What is the six-month forward exchange rate predicted by interest rate parity?', 'Radio Button', '2020-01-29 17:52:55', NULL),
(144, '2020-01-29 17:52:56', 124, 'As regards the euro receipt, what is the primary nature of the risk faced by Herd Co?', 'Radio Button', '2020-01-29 17:52:56', NULL),
(145, '2020-01-29 17:52:56', 125, 'Which of the following hedging methods will NOT be suitable for hedging the euro receipt?', 'Radio Button', '2020-01-29 17:52:56', NULL),
(146, '2020-01-29 17:52:56', 126, 'Which of the following statements support the finance director’s belief that the euro will depreciate against the dollar?', 'Check Box', '2020-01-29 17:52:56', NULL),
(147, '2020-01-29 17:52:56', 127, 'As regards the interest rate risk faced by Herd Co, which of the following statements is correct?', 'Radio Button', '2020-01-29 17:52:56', NULL),
(149, '2020-01-30 13:32:01', 129, 'zzzfz', 'Text Feild', '2020-01-30 13:32:01', NULL),
(150, '2020-01-30 13:32:02', 130, 'ddad', 'Select Box', '2020-01-30 13:32:02', NULL),
(151, '2020-01-30 13:36:42', 131, 'TEST', 'Text Feild', '2020-01-30 13:36:42', NULL);

-- --------------------------------------------------------



INSERT INTO `online_question_topics` (`id`, `content`, `created_at`, `updated_at`) VALUES
(8, '<p>cx gfgfdgdfg</p>\n', '2020-01-29 13:51:15', '2020-01-29 13:51:15'),
(10, '<p>Which of the following are descriptions of basis risk?<br />\n(1)&nbsp;It is&nbsp;the difference between the spot exchange rate and currency futures exchange rate<br />\n(2)&nbsp;It is&nbsp;the possibility that the movements in the currency futures price and spot price will be different<br />\n(3)&nbsp;It is&nbsp;the difference between fixed and floating interest rates<br />\n(4) It is&nbsp;one of the reasons for an imperfect currency futures hedge</p>\n', '2020-01-29 15:40:32', '2020-01-29 15:40:32'),
(11, '<p>Crag Co has sales of $200m per year and the gross profit margin is 40%. Finished goods inventory days vary throughout the year within the following range:</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Maximum&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Minimum<br />\nInventory (days)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;120&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;90</p>\n\n<p>All purchases and sales are made on a cash basis and no inventory of raw materials or work in progress is carried.<br />\nCrag Co intends to finance permanent current assets with equity and fluctuating current assets with its overdraft.</p>\n\n<p><strong>In relation to finished goods inventory and assuming a 360-day year, how much finance will be needed from the<br />\noverdraft?</strong></p>\n', '2020-01-29 16:04:55', '2020-01-29 16:04:55'),
(12, '<p>Crag Co has sales of $200m per year and the gross profit margin is 40%. Finished goods inventory days vary throughout the year within the following range:</p>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Maximum&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Minimum<br />\nInventory (days)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;120&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;90</p>\n\n<p>All purchases and sales are made on a cash basis and no inventory of raw materials or work in progress is carried.<br />\nCrag Co intends to finance permanent current assets with equity and fluctuating current assets with its overdraft.</p>\n\n<p><strong>In relation to finished goods inventory and assuming a 360-day year, how much finance will be needed from the<br />\noverdraft?</strong></p>\n', '2020-01-29 16:05:01', '2020-01-29 16:05:01'),
(14, '<p>Lane Co has in issue 3% convertible loan notes which are redeemable in five years&rsquo; time at their nominal value of<br />\n$100 per loan note. Alternatively, each loan note can be converted in five years&rsquo; time into 25 Lane Co ordinary shares.</p>\n\n<p><br />\nThe current share price of Lane Co is $3&middot;60 per share and future share price growth is expected to be 5% per year.</p>\n\n<p><br />\nThe before-tax cost of debt of these loan notes is 10% and corporation tax is 30%.</p>\n\n<p><br />\n<strong>What is the current market value of a Lane Co convertible loan note?</strong></p>\n', '2020-01-29 16:31:03', '2020-01-29 16:31:03'),
(15, '<p>Country X uses the dollar as its currency and country Y uses the dinar.<br />\nCountry X&rsquo;s expected inflation rate is 5% per year, compared to 2% per year in country Y. Country Y&rsquo;s nominal interest<br />\nrate is 4% per year and the current spot exchange rate between the two countries is 1&middot;5000 dinar per $1.<br />\n<strong>According to the four-way equivalence model, which of the following statements is/are true?</strong><br />\n(1) Country X&rsquo;s nominal interest rate should be 7&middot;06% per year<br />\n(2) The future (expected) spot rate after one year should be 1&middot;4571 dinar per $1<br />\n(3) Country X&rsquo;s real interest rate should be higher than that of country Y</p>\n', '2020-01-29 17:25:11', '2020-01-29 17:25:11'),
(16, '<p>Which of the following government actions would lead to an increase in aggregate demand?</p>\n\n<p><br />\n(1)&nbsp; Increasing taxation and keeping government expenditure the same<br />\n(2)&nbsp;Decreasing taxation and increasing government expenditure<br />\n(3)&nbsp;Decreasing money supply<br />\n(4)&nbsp;Decreasing interest rates</p>\n', '2020-01-29 17:29:45', '2020-01-29 17:29:45'),
(17, '<p>Peach Co&rsquo;s latest results are as follows:</p>\n\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n	<tbody>\n		<tr>\n			<td colspan=\"2\"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;$000</strong></td>\n		</tr>\n		<tr>\n			<td>Profit before interest and taxation</td>\n			<td>2,500</td>\n		</tr>\n		<tr>\n			<td>Profit before taxation</td>\n			<td>2,250</td>\n		</tr>\n		<tr>\n			<td>Profit after tax</td>\n			<td>1,400</td>\n		</tr>\n		<tr>\n			<td colspan=\"2\">In addition, extracts from its latest statement of financial position are as follows:</td>\n		</tr>\n		<tr>\n			<td colspan=\"2\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>$000</strong></td>\n		</tr>\n		<tr>\n			<td>Equity</td>\n			<td>10,000</td>\n		</tr>\n		<tr>\n			<td>Non-current liabilities</td>\n			<td>2,500</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n', '2020-01-29 17:41:11', '2020-01-29 17:41:11'),
(18, '<p>Drumlin Co has $5m of $0&middot;50 nominal value ordinary shares in issue. It recently announced a 1 for 4 rights issue<br />\nat $6 per share. Its share price on the announcement of the rights issue was $8 per share.</p>\n', '2020-01-29 17:42:35', '2020-01-29 17:42:35'),
(19, '<p>Herd Co is based in a country whose currency is the dollar ($). The company expects to receive &euro;1,500,000 in six<br />\nmonths&rsquo; time from Find Co, a foreign customer. The finance director of Herd Co is concerned that the euro (&euro;) may<br />\ndepreciate against the dollar before the foreign customer makes payment and she is looking at hedging the receipt.<br />\nHerd Co has in issue loan notes with a total nominal value of $4 million which can be redeemed in 10 years&rsquo; time. The<br />\ninterest paid on the loan notes is at a variable rate linked to LIBOR. The finance director of Herd Co believes that interest<br />\nrates may increase in the near future.<br />\nThe spot exchange rate is &euro;1&middot;543 per $1. The domestic short-term interest rate is 2% per year, while the foreign<br />\nshort-term interest rate is 5% per year.</p>\n', '2020-01-29 17:52:55', '2020-01-29 17:52:55'),
(20, '<p>zdzafafaf</p>\n', '2020-01-30 13:32:01', '2020-01-30 13:32:01');



INSERT INTO `question_years` (`id`, `active`, `created_at`, `name`, `remark`, `updated_at`) VALUES
(27, '1', '2020-01-28 13:58:11', '2020', 'ACCA test', '2020-01-28 13:58:11'),
(28, '1', '2020-01-28 16:12:09', '2008', 'CSS', '2020-01-28 16:12:09'),
(30, '1', '2020-01-30 11:55:13', '2010', 'acca', '2020-01-30 11:55:13');


INSERT INTO `students` (`id`, `active`, `address`, `adharno`, `caste`, `city`, `closed_on`, `created_at`, `dateofbirth`, `email`, `enquiry_stage_id`, `gender`, `landmark`, `name`, `photo`, `pincode`, `religion`, `rollno`, `status`, `updated_at`, `user_id`, `batch_id`, `branch_id`, `area`, `category`, `school_id`, `state`, `loss_reason_id`, `course_id`) VALUES
(1, b'1', 'Manjeri, Malappuram', '784545454545', 'Ezhava', 'Manjeri', NULL, '2019-12-24 16:12:19', '25/10/1993', 'abc@gmail.com', NULL, 'Male', 'Manjeri', 'Chandrasekhar Azad', 'profile.png', '676121', 'Hindu', 'R110', 'Student', '2019-12-24 16:12:19', 2, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL);



INSERT INTO `subjects` (`id`, `active`, `branch_id`, `code`, `course_id`, `created_at`, `hours`, `name`, `remark`, `subject_from`, `updated_at`, `user_id`) VALUES
(6, b'1', 1, '1001', 1, '2020-01-28 14:05:04', NULL, 'AAA', 'AAA TEST', 'Branch', '2020-01-28 14:05:04', 1),
(7, b'1', 1, 'FM05', 1, '2020-01-29 09:14:44', NULL, 'Fundamentals Level', 'Test From Cloudbery', 'Branch', '2020-01-29 15:28:44', 1),
(8, b'1', 1, 'FM01', 1, '2020-01-29 15:27:49', NULL, 'Financial Management', 'Cloudbery testing purpose', 'Branch', '2020-01-29 15:28:09', 1);



INSERT INTO `users` (`id`, `active`, `address`, `created_at`, `email`, `is_admin`, `lastlogin`, `mobile`, `name`, `password`, `permission`, `photo`, `role`, `role_id`, `typeofuser`, `updated_at`, `user_token`, `username`, `pass`, `student_id`) VALUES
(1, b'1', '', '2019-12-12', 'admin@admin.com', b'1', NULL, '', 'Administrator', '$2a$10$cSKTztXuwR7YaeIvMKmaM.Hl7gMKDtKHhU8o5X5Rv2u2AZNG60XXq', '', 'profile.png', NULL, 1, 'Admin', '2019-12-12', NULL, 'admin', 'admin', NULL),
(2, b'1', '', '2019-12-24', 'student@student.com', b'0', NULL, '', 'Student', '$2a$10$vfZQNMWnWJAusBwjCXNrfePfPVng8ou9hON8jXYzMhf9FnsZaVxNi', '', 'profile.png', NULL, 1, 'Student', '2019-12-24', NULL, 'student', 'student', 1);



INSERT INTO `user_assigned_students` (`id`, `created_at`, `student_id`, `updated_at`, `user_id`) VALUES
(1, '2019-12-24 16:12:19', 1, '2019-12-24 16:12:19', 2);